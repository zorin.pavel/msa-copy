import { defineConfig, splitVendorChunkPlugin } from 'vite'
import react from '@vitejs/plugin-react'
import basicSsl from '@vitejs/plugin-basic-ssl';
import * as path from 'path';
// import federation from "@originjs/vite-plugin-federation";
import htmlPlugin from 'vite-plugin-html-config'


export default defineConfig({
    envPrefix: 'MSA',
    base: '/',
    build: {
        target: 'esnext' //browsers can handle the latest ES features
    },
    server: {
        port: Number(process.env.PORT),
        https: true,
    },
    plugins: [
        react(),
        basicSsl(),
        splitVendorChunkPlugin(),
        // federation({
        //     name: 'pipelines',
        //     filename: 'pipelines.js',
        //     exposes: {
        //         './Pipelines': './src/components/Pipelines',
        //     },
        //     shared: ['react', 'classnames']
        // })
        htmlPlugin({
            favicon: '/favicon.svg',
            headScripts: [
                {
                    src: '/env.js',
                },
            ],
        })
    ],
    css: {
        modules: {
            localsConvention: 'camelCase'
        },
        preprocessorOptions: {
            scss: {
                additionalData: '@import "@msa/kit/scss/config.scss";'
            }
        }
    },
    resolve: {
        alias: {
            env: path.resolve('src/env'),
            settings: path.resolve('src/settings'),
            types: path.resolve('src/types'),
            helpers: path.resolve('src/helpers'),
            actions: path.resolve('src/actions'),
            Routes: path.resolve('src/Routes'),
            navigation: path.resolve('src/navigation'),
            components: path.resolve('src/components'),
            locale: path.resolve('src/locale'),
        },
    }
})
