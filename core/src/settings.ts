import { settings } from '@msa/kit'


export default {
    ...settings,
    DELAY: {
        LOG: 10000,
        CLUSTER_INFO: 10000,
        HELM_APP_MARKET: 10000,
    },
    SCOPES_OPTIONS: [
        { label: 'list', value: 'list' },
        { label: 'edit', value: 'edit' },
        { label: 'create', value: 'create' },
        { label: 'get', value: 'get' },
        { label: 'delete', value: 'delete' },
    ]
};
