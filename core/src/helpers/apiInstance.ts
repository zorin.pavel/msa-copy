import { ApiInstance } from '@msa/host/src/helpers/apiInstance';

import { env } from 'env';


const API = ApiInstance(env.MSA_PACKAGE_API_URL);

export { ApiInstance, API };
