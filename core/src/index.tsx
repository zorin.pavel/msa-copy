import i18n from '@msa/host/src/i18n';

import translations from 'locale';

import './index.scss';
import '@msa/host/src/index';

i18n.addResourceBundle('en', import.meta.env.MSA_PACKAGE_NAME, translations.en, true, true);
i18n.addResourceBundle('ru', import.meta.env.MSA_PACKAGE_NAME, translations.ru, true, true);
i18n.setDefaultNamespace(import.meta.env.MSA_PACKAGE_NAME);
