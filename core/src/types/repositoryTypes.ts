import { IBaseORM } from '@msa/host/types';


export interface IRepository extends IBaseORM {
    URL: string,
    type: 'helm' | 'docker' | 'raw',
    credentials?: boolean,
    username?: string,
    password?: string,
    proxy?: boolean,
    proxy_server?: string,
    http_proxy?: string,
    https_proxy?: string,
    is_online: boolean,
}
