import { IBaseORM } from '@msa/host/types';


export interface IProvider extends IBaseORM {
    type: EProviderType,
    settings: TProviderSettingsEmail & TProviderSettingsTelegram,
    notifications?: IAlert[],
}

export interface IAlert extends IBaseORM {
    destination: TAlertDestEmail & TAlertDestTelegram,
    enabled: boolean,
    level: EAlertLevel[],
    notification_provider: IProvider,
}

export type TNotificationForm = Omit<IAlert, 'notification_provider' | 'destination'> & { provider_name: string, destination?: string }
export type TNotificationPostData = Omit<TNotificationForm, 'destination'> & { destination: IAlert['destination'] }

export type TProviderSettingsEmail = {
    smtp_server?: string,
    smtp_port?: number,
    smtp_tls?: boolean,
    smtp_username?: string,
    smtp_password?: string,
}

export type TProviderSettingsTelegram = {
    telegram_bot_token?: string,
}

export enum EProviderType {
    EMAIL = 'Email',
    TELEGRAM = 'Telegram',
    TRUCONF = 'True Conf',
    TEAMS = 'MS Teams',
}

export enum EAlertLevel {
    ERROR = "ERROR",
    WARNING = "WARNING",
    INFO = "INFO",
    // ONLINE = "ONLINE"
}

export type TAlertDestEmail = {
    mail_addresses?: string[]
}

export type TAlertDestTelegram = {
    telegram_chat_ids?: string[]
}
