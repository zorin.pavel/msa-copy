export interface ILdap {
    name: string;
    config: {
        enabled: boolean;
        connectionUrl: string;
    };
    data?: ILdapPost;
}


export interface ILdapPost {
    ldapName: string;
    bindCredential: string;
    bindDn: string;
    connectionUrl: string;
    usernameLDAPAttribute: string;
    usersDn: string;
    groupDn: string;
    firstNameLdapAttribute: string;
}
