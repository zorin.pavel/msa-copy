export type TState = 'Success' | 'In Progress' | 'Failed'
export type TAction = 'Import' | 'Update' | 'Create' | 'Delete'

export interface ISSHKey {
    name: string,
    username: string,
    tags?: string,
}

export enum ECloudProvider {
    amazon = 'Amazon',
    azure = 'Azure',
    digitalocean = 'DigitalOcean',
    gcp = 'Google',
}

export interface ILogMessage {
    action: string, // TAction
    details: string,
    access: string,
    object_name: string,
    object_type: string,
    platform: string,
    timestamp: string,
    user_agent?: string,
    username: string,
    advanced: boolean,
    host?: string,
}

export interface IPagesResponse<T> {
    items: T,
    count: number
}
