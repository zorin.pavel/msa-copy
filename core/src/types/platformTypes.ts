import { IBaseORM } from '@msa/host/types';

import { ICredential } from 'types';


export enum EPlatformKind {
    baremetal = 'Bare metal',
    yc = 'YC',
    gcp = 'GCP',
    mcs = 'MCS',
    aws = 'AWS',
    azure = 'Azure',
}


export interface IPlatform extends IBaseORM {
    description?: string,
    kind?: keyof typeof EPlatformKind,
    clusters?: IBaseORM[],
    infrastructures?: IBaseORM[]
    cloud_credentials?: IBaseORM[],
    nodes?: IBaseORM[],
    nodes_templates?: IBaseORM[],
    ssh_keys?: IBaseORM[],
    groups?: string[] | string
}


export interface IPlatformNode extends IBaseORM {
    ip: string,
    description?: string,
    username?: string,
    password?: string,
    upload_key?: boolean,
    credential: ICredential | null
    cluster?: IBaseORM,
    infrastructure?: IBaseORM,
    is_online: boolean
    os_family?: string
    os_version?: string,
    kernel?: string,
    cpu?: number,
    disk?: number,
    mem?: number
}


export enum ENodeTemplateProvider {
    amazon = 'Amazon',
    azure = 'Azure',
    digitalocean = 'DigitalOcean',
}


export interface INodeTemplate {
    provider: keyof typeof ENodeTemplateProvider,
    name: string,
    owner: string,
    location: string,
    size: string,
    status: string,
}

