import { IBaseORM } from '@msa/host/types';

import { TAction, TState, ISSHKey, INodeLabel, INodeTaint } from 'types';


export interface IInfrastructure extends IBaseORM {
    platform: IBaseORM,
    description?: string,
    type: IInfraService['type'],
    values: Record<string, any>,
    state: TState,
    action: TAction,
    nodes: IInfrastructureNode[],
}

export interface IInfrastructureNode extends IBaseORM {
    ip: string,
    ssh_key: ISSHKey,
    labels: INodeLabel[],
    taints: INodeTaint[],
}

export interface IInfraService {
    title: string,
    description: string,
    type: string,
    values: Record<string, any>,
    badge?: string,
}


