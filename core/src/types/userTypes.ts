import { IRbacGroupObject, IRbacResource } from '@msa/kit/types';


export interface IUserInfo {
    id: string;
    createdTimestamp: number;
    email: string;
    enabled: boolean;
    firstName: string;
    lastName: string;
    group: TGroup | TGroup[];
    role: TRole;
    username: string;
    password?: string;
    repeat_pass?: string;
    credentials?: Record<string, string>[];
    totp?: boolean,
    provider: string | null,
}

export type TRole = string;
export type TGroup = string;

export interface IRbacGroup {
    id: string,
    name: string,
    provider: string | null,
    realmRoles: {
        default: IRbacResource[],
        object: IRbacGroupObject[]
    },
    subGroups?: IRbacGroup[],
}

export interface ISessionInfo {
    id: string;
    ipAddress: string;
    lastAccess: number;
    start: number;
    username: string;
}

export interface IRealmRole {
    id: string;
    name: string;
    object_name: string;
    resource: string;
    scope: string;
    title: string;
    object?:any
}
