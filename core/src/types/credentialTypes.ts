import { IBaseORM } from '@msa/host/types';

import { ECloudProvider } from 'types';


export interface ICredential extends IBaseORM {
    description: string,
    type: string,
    login_pass?: {
        username: string,
    }
}

export interface ICredentialPost extends ICredential {
    data?: any
}

export interface ICloudCredential {
    provider: keyof typeof ECloudProvider,
    id: string,
    name: string,
    description: string,
    apiKey: string,
    age: string,
    key: string
}
