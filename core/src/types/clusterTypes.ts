import { IBaseORM } from '@msa/host/types';

import { TAction, TState, IRepository } from 'types';


export interface ICluster extends IBaseORM {
    platform: IBaseORM,
    description: string,
    kube_version: string,
    nodes: IClusterNode[]
    cluster_domain: string,
    cni: string,
    cri: string,
    pod_subnet: string,
    service_subnet: string,
    offline_install: boolean,
    kubeconfig?: string,
    state: TState,
    action: TAction,
    is_online: boolean,
    cluster_tools: boolean,
    kubernetes_audit: boolean
    vendor_helm_repo_name?: string,
    vendor_raw_repo_name?: string,
    vendor_docker_repo_name?: string,
    vendor_git_repo_name?: string,
    vendor_helm_offline_repo_name?: string,
    proxy?: boolean,
    proxy_server?: string,
    http_proxy?: string,
    https_proxy?: string,
    proxy_registry?: boolean,
    docker_proxy?: string,
    dashboardURL?: string,
    groups: string[] | string
}


export interface IClusterValues {
    kube_version: string[]
    CNI: string[],
    CRI: string[],
    pod_subnet: string,
    cluster_domain: string,
    service_subnet: string
}


export interface IClusterNode extends IBaseORM {
    UUID: string,
    ansible_group: string| number | null,
    cloud_group: string| number | null,
    cluster: Pick<ICluster, 'name' | 'UUID'>,
    credential: any,
    description: string,
    etcd: boolean,
    infrastructure: any
    ip: string,
    is_online: boolean
    kernel: string
    key_is_upload: boolean,
    master: boolean,
    name: string,
    os_family: string,
    os_version: string,
    labels: INodeLabel[] | null,
    taints: INodeTaint[] | null,
    worker: boolean,
}


export interface INodeLabel {
    key: string,
    value: string,
}


export interface INodeTaint {
    effect: ETaintEffect,
    key: string,
    value: string,
}


export enum ETaintEffect {
    no_execute = 'NoExecute',
    no_schedule = 'NoSchedule',
    prefer_no_schedule = 'PreferNoSchedule'
}


export interface IClusterNodeLog {
    Component?: string,
    Error?: string,
    Stage: string,
    level: string,
    msg: string,
    time: string,
}

export interface IClusterInfo {
    capacity: {
        memory: number,
        cpu: number,
        pods: number,
        hugepages: number,
    },
    allocated: {
        memory: number,
        cpu: number,
        pods: number,
        hugepages: number,
    }
}


export interface IClusterNodeInfo {
    address: string,
    age: string,
    capacity: IClusterNodeMetrics
    container_runtime: string,
    created: string,
    kernel: string,
    name: string,
    os_image: string,
    version: string,
}


export interface IClusterNodeMetrics {
    name: string,
    cpu: number,
    memory: number,
}


export interface IClusterNodeMetricsCapacity {
    pods: number,
    cpu: number,
    memory: number,
}


export interface IClusterTool extends IBaseORM {
    chart_name: string,
    version: string,
    release_name: string,
    title: string,
    state: TState,
    action: TAction,
    type: string,
    vendor_helm_repo: IRepository,
    id: number,
    values: Record<string, any>,
    namespace: string,
    wait_flag: boolean,
    description: string,
    cluster_id: number,
    icon?: string,
    curve?: boolean
}


export interface IRepoList {
    URL: string
    UUID: string,
    name: string
}


export interface IDevTool extends IBaseORM {
    chart_name: string,
    version: string,
    release_name: string,
    title: string,
    state: TState,
    action: TAction,
    type: string,
    user_helm_repo: IRepository,
    id: number,
    values: Record<string, any>,
    namespace: string,
    wait_flag: boolean,
    description: string,
    cluster_id: number,
    icon?: string,
    curve?: boolean
}


export interface IProject extends IBaseORM {
    name: string,
    status: string
}


export interface INamespace extends IBaseORM {
    cluster_name?: string,
    limit_range: {
        defaultLimitsCPU?: string,
        defaultLimitsMemory?: string,
        defaultRequestsCPU?: string,
        defaultRequestsMemory?: string,
        max_cpu?: string, // CPU limit
        max_memory?: string,
    } | null,
    resource_quota: {
        limits_cpu?: string,
        limits_memory?: string,
        requests_cpu?: string, // CPU reservation
        requests_memory?: string,
    } | null,
}
