export interface IRbacResource {
    [key: string]: string[]
}


export interface IRbacObjects {
    platforms: {
        [key: string]: {
            clusters: string[]
        }
    },
    scopes: string[]
}


export interface IRbacGroupObject extends Record<string, string | string[]> {
    group: string,
    platform: string,
    cluster: string,
    scopes: string[]
}
