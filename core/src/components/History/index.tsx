import React, { useCallback, useEffect, useMemo, useState } from 'react';
import { useQuery, useQueryClient } from '@tanstack/react-query';
import { useDebounce } from 'use-debounce';
import { useTranslation } from 'react-i18next';

import { IError, IOption, OrderDirection } from '@msa/kit/types';
import { IconCloseCircle, Input, Pagination, Select, sort, Table, usePagination, useTable } from '@msa/kit';

import { EMessageSeverity, IMessageData } from '@msa/host/types';
import { getLoggerMessages } from '@msa/host';

import { LogRow } from './LogRow';

import css from './log.module.scss';


export const History = () => {
    const queryClient = useQueryClient();
    const { t } = useTranslation();

    const messageTypeOptions: IOption[] = useMemo(() => {
        const messageTypeOptions = Object.keys(EMessageSeverity).map(status => ({ label: t(`status.${status.toLowerCase()}`), value: status }));
        messageTypeOptions.unshift({ label: t('logs.allSeverity'), value: '' });

        return messageTypeOptions;
    }, [t]);

    const periodOptions: IOption[] = useMemo(() => ([
            { label: t('logs.5_min'), value: 300 },
            { label: t('logs.10_min'), value: 600 },
            { label: t('logs.30_min'), value: 1800 },
            { label: t('logs.1_hour'), value: 3600 },
            { label: t('logs.6_hours'), value: 21600 },
            { label: t('logs.1_day'), value: 86400 },
            { label: t('logs.1_week'), value: 604800 },
            { label: t('logs.1_month'), value: 2592000 },
        ]), [t]);

    const actionOptions: IOption[] = [
        { label: t('logs.all_Actions'), value: '' },
        { label: t('logs.add_Worker'), value: 'Add Worker(s)' },
        { label: t('logs.create'), value: 'Create' },
        { label: t('logs.update'), value: 'Update' },
        { label: t('logs.upgrade'), value: 'Upgrade' },
        { label: t('logs.recreate'), value: 'Recreate' },
        { label: t('logs.delete'), value: 'Delete' },
        { label: t('logs.force_Delete'), value: 'Force Delete' },
        { label: t('logs.remove_Worker'), value: 'Remove Worker(s)' },
        { label: t('logs.import'), value: 'Import' },
        { label: t('logs.upgrade_Version'), value: 'Upgrade Version' },
        { label: t('logs.edit_Permission'), value: 'Edit Permission' },
        { label: t('logs.export'), value: 'Export' },
        // { label: 'Get', value: 'Get' },
    ];

    const objectOptions: IOption[] = [
        { label: t('logs.all_Objects'), value: '' },
        { label: t('logs.platforms'), value: 'platform' },
        { label: t('logs.clusters'), value: 'cluster' },
        { label: t('logs.user'), value: 'user' },
        { label: t('logs.group'), value: 'group' },
        { label: t('logs.LDAP'), value: 'LDAP' },
        { label: t('logs.credential'), value: 'credential' },
        { label: t('logs.nodes'), value: 'node' },
        { label: t('logs.message'), value: 'message' },
        { label: t('logs.userLog'), value: 'user log' },
        { label: t('logs.infrastructures'), value: 'infrastructure' },
        { label: t('logs.userHelmCharts'), value: 'user helm chart' },
        { label: t('logs.vendorHelmCharts'), value: 'vendor helm chart' },
        { label: t('logs.userHelmRepo'), value: 'user helm repo' },
        { label: t('logs.vendorHelmRepo'), value: 'vendor helm repo' },
        { label: t('logs.vendorDockerRepo'), value: 'vendor docker repo' },
        { label: t('logs.vendorRawRepo'), value: 'vendor raw repo' },
    ];

    const stateOptions: IOption[] = [
        { label: t('logs.allStates'), value: '' },
        { label: t('logs.waiting'), value: 'Waiting' },
        { label: t('logs.success'), value: 'Success' },
        { label: t('logs.inProgress'), value: 'In Progress' },
        { label: t('logs.failed'), value: 'Failed' },
    ];


    const { currentPage, limit, getPaginatedItems, onPageChange, setCurrentPage } = usePagination<IMessageData[]>(20);
    const { orderedBy, setOrderedBy } = useTable({ 'timestamp': OrderDirection.desc });

    const [searchParams, setSearchParams] = useState<Record<string, string | number | undefined>>({
        selectedPeriod: 86400,
        name: ''
    });

    const [selectedUserName, setSelectedUserName] = useState<string>('');
    const [name] = useDebounce<string | number | undefined>(selectedUserName ?? undefined, 500);

    const { data, isFetching } = useQuery<IMessageData[], IError>(
        ['user_logs', currentPage, limit, ...Object.values(searchParams)],
        () => getLoggerMessages({ ...searchParams }),
        {
            // refetchInterval: settings.DELAY.LOG
        }
    );

    const [logs, setLog] = useState<IMessageData[]>(data ?? []);


    useEffect(() => {
        setSearchParams(prevParams => ({ ...prevParams, name: name }));
    }, [name]);


    useEffect(() => {
        queryClient.invalidateQueries(['user_logs']);
        setCurrentPage(0);
    }, [searchParams]);


    useEffect(() => {
        if(data)
            setLog(sort<IMessageData[]>(data, 'timestamp', OrderDirection.desc));
    }, [data]);


    const orderBy = useCallback((fieldName: string, orderDirection: OrderDirection) => {
        if(setOrderedBy)
            setOrderedBy({ [fieldName]: orderDirection });

        setLog(sort<IMessageData[]>(data ?? [], fieldName, orderDirection));
    }, [data, setOrderedBy]);


    const handleFilterChange = (event: React.ChangeEvent<HTMLInputElement>, filter: string) => {
        setSearchParams(prevParams => ({ ...prevParams, [filter]: event.target.value }));
    }

    return (
        <>
            <div className="layout__header">
                <h1 className="layout__title">{t('common.messageLog')}</h1>
            </div>

            <div className={css.filters}>
                <Input
                    placeholder={t('common.nameFilter')}
                    className={css.input}
                    value={selectedUserName}
                    onChange={(event) => setSelectedUserName(event.target.value)}
                    autoComplete="off"
                    iconRight={<IconCloseCircle onClick={() => setSelectedUserName('')} />}
                    disabled={isFetching} />
                <Select
                    value={searchParams['object_type'] ?? ''}
                    autoComplete="off"
                    className={css.select}
                    options={objectOptions}
                    onChange={(event) => handleFilterChange(event, 'object_type')} />
                <Select
                    value={searchParams['state'] ?? ''}
                    autoComplete="off"
                    className={css.select}
                    options={stateOptions}
                    // loading={isFetching}
                    onChange={(event) => handleFilterChange(event, 'state')} />
                <Select
                    value={searchParams['action'] ?? ''}
                    autoComplete="off"
                    className={css.select}
                    options={actionOptions}
                    // loading={isFetching}
                    onChange={(event) => handleFilterChange(event, 'action')} />
                <Select
                    value={searchParams['severity'] ?? ''}
                    labelPosition="left"
                    autoComplete="off"
                    className={css.select}
                    options={messageTypeOptions}
                    // loading={isFetching}
                    onChange={(event) => handleFilterChange(event, 'severity')} />
                <Select
                    value={searchParams['selectedPeriod'] ?? 86400}
                    labelPosition="left"
                    autoComplete="off"
                    className={css.select}
                    options={periodOptions}
                    // loading={isFetching}
                    onChange={(event) => handleFilterChange(event, 'selectedPeriod')} />
            </div>
            <Table cells={10}>
                <Table.Header>
                    <Table.Cell colspan={2}>{t('common.date')}<Table.ActionSort fieldName="timestamp" orderedBy={orderedBy} orderBy={orderBy} /></Table.Cell>
                    <Table.Cell colspan={1}>{t('platforms.platform', {context: 'one'})}<Table.ActionSort fieldName="platform" orderedBy={orderedBy} orderBy={orderBy} /></Table.Cell>
                    <Table.Cell colspan={1}>{t('common.object')}<Table.ActionSort fieldName="object_type" orderedBy={orderedBy} orderBy={orderBy} /></Table.Cell>
                    <Table.Cell colspan={1}>{t('common.name')}<Table.ActionSort fieldName="name" orderedBy={orderedBy} orderBy={orderBy} /></Table.Cell>
                    <Table.Cell colspan={1}>{t('common.action')}<Table.ActionSort fieldName="action" orderedBy={orderedBy} orderBy={orderBy} /></Table.Cell>
                    <Table.Cell colspan={1}>{t('common.severity')}</Table.Cell>
                    <Table.Cell colspan={3}>{t('common.details')}</Table.Cell>
                </Table.Header>
                {
                    getPaginatedItems(logs).map((log: IMessageData, key: number) => <LogRow key={key} log={log} />)
                }
                {
                    logs &&
                    logs.length > limit &&
                    <Pagination itemsCount={logs.length} limit={limit} onChangePage={onPageChange} />
                }
            </Table>
        </>
    );
};
