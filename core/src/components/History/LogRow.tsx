import React from 'react';
import classNames from 'classnames';
import { format } from 'date-fns';

import { Table } from '@msa/kit';

import { IMessageData } from '@msa/host/types';

import css from './log.module.scss';


interface IProps {
    log: IMessageData,
}

export const LogRow = (props: IProps) => {
    const { log } = props;


    return (
        <>
            <Table.Row valign="center" align="center" className="entity__row">
                <Table.Cell colspan={2} className={css.dateRow}>{format(new Date(log.timestamp), 'yyyy-MM-dd HH:mm:ss')}</Table.Cell>
                <Table.Cell colspan={1}>{log.platform}</Table.Cell>
                <Table.Cell colspan={1}>{log.object_type}</Table.Cell>
                <Table.Cell colspan={1}>{log.name}</Table.Cell>
                <Table.Cell colspan={1}>{log.action}</Table.Cell>
                <Table.Cell colspan={1} className={classNames(css.state, log.severity && css[log.severity.toLowerCase()])}>{log.severity}</Table.Cell>
                <Table.Cell colspan={3}>{log.detail}</Table.Cell>
            </Table.Row>
        </>
    );
};
