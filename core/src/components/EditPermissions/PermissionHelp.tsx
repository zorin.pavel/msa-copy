import { useTranslation } from 'react-i18next';
import i18next from 'i18next';

import css from './edit.module.scss';

export const PermissionHelp: React.FC<{ permissions: any }> = ({ permissions }) => {
    const { t } = useTranslation()
    return (
        <dl>
          {Object.keys(permissions).map((key) => (
            <div key={key}>
                
              {key !== 'note' && <dt className={css.helpTitle}>{i18next.format(t(`${key}.${key}`), 'capitalize')}</dt>}
              {/* {key !== 'note' && <dt className={css.helpTitle}>{t(`${key}.${key}`, {context: 'upper'})}</dt>} */}
                {Object.keys(permissions[key]).map((subKey) => (
                     <dd key={subKey}>{permissions[key][subKey]}</dd>
                ))}
                <br />
            </div>
          ))}
        </dl>
    );
  };