import React, { useEffect, useState } from 'react';
import { Controller, UseFieldArrayRemove, useFormContext } from 'react-hook-form';
import { useQuery } from '@tanstack/react-query';
import { useKeycloak } from '@react-keycloak/web';

import { IError, IOption, IRbacObjects } from '@msa/kit/types';
import { IconDelete, Select, Table } from '@msa/kit';

import { env } from 'env';
import settings from 'settings';
import { getObjects } from 'actions';
import { BadgeButtons } from 'components/EditPermissions/BadgeButtons';


interface IProps {
    index: number;
    groups: string[],
    remove: UseFieldArrayRemove;
}


export const PermissionRow = (props: IProps) => {
    const { groups, index, remove } = props;
    const { keycloak } = useKeycloak();

    const {
        control,
        getValues,
        formState: { errors }
    } = useFormContext();

    const { isLoading, data: objects } = useQuery<IRbacObjects, IError>(
        ['objects'],
        getObjects
    );

    const [scopeOptions, setScopeOptions] = useState<IOption[]>(settings.SCOPES_OPTIONS);
    const groupsOptions: IOption[] = groups.map((group: string) => ({ label: group, value: group }));
    const isAdmin = keycloak.tokenParsed?.groups.includes(env.MSA_DEFAULT_USER_GROUP);
    const isDisabled = (!keycloak.tokenParsed?.groups.includes(getValues(`items.${index}.group`)) && !!getValues(`items.${index}.group`).length && !isAdmin);

    useEffect(() => {
        if(objects) {
            setScopeOptions(objects.scopes.map((scope: string) => ({ label: scope, value: scope })));
        }
    }, [objects]);


    return (
        <Table.Row valign="center">
            <Table.Cell> 
                <Controller
                    control={control}
                    name={`items.${index}.group`}
                    rules={{
                        required: 'Platform is required'
                    }} render={({ field }) => (
                        <Select
                            {...field}
                            required
                            readonly={isDisabled}
                            error={!!errors.items}
                            options={groupsOptions}
                            loading={isLoading} />
                    )} />
            </Table.Cell>
            <Table.Cell colspan={2}>
                <BadgeButtons scopeOptions={scopeOptions} name={`items.${index}.scopes`} disabled={isDisabled} />
            </Table.Cell>
            <Table.Cell actions={1}>
                <IconDelete disabled={isDisabled} onClick={() => remove(index)} />
            </Table.Cell>
        </Table.Row>
    );
};
