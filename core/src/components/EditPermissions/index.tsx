import React, { ReactElement, useEffect, useState } from 'react';
import { FormProvider, useFieldArray, useForm, useWatch } from 'react-hook-form';
import { useQuery, useQueryClient } from '@tanstack/react-query';
import { useKeycloak } from '@react-keycloak/web';
import { useTranslation } from 'react-i18next';
import i18next from 'i18next';

import { IError, IRbacGroupObject } from '@msa/kit/types';
import { Button, Dialog, Drawer, IconHelpCircle, IconPlus, IconUsers, Table, Toast } from '@msa/kit';

import { env } from 'env';
import { getGroupsList, putClusterGroups, putPlatformGroups } from 'actions';
import { PermissionRow } from 'components/EditPermissions/PermissionRow';
import { PermissionHelp } from 'components/EditPermissions/PermissionHelp';

import css from './edit.module.scss';


// interface IRbacGroupObjectValues extends Omit<IRbacGroupObject, 'scopes'> {
//     scopes: string[]
// }

interface IProps {
    permissions?: IRbacGroupObject[],
    platformName?: string,
    clusterName?: string
    button?: ReactElement;
    disabled?: boolean;
}

export const EditPermissions = (props: IProps) => {
    const { platformName, clusterName, permissions, button, disabled } = props;
    const queryClient = useQueryClient();
    const { keycloak } = useKeycloak();
    const { t } = useTranslation()

    const [submitted, setSubmitted] = useState<boolean>(false);
    const [drawer, setDrawer] = useState<boolean>(false);
    const [groups, setGroups] = useState<string[]>(keycloak.tokenParsed?.groups);
    const [objectDialog, setObjectDialog] = useState<boolean>(false);

    const isAdmin = keycloak.tokenParsed?.groups.includes(env.MSA_DEFAULT_USER_GROUP);

    const { data: availableGroups } = useQuery<string[], IError>(
        ['group', 'list'],
        getGroupsList
    );

    const defaultValues = permissions ?
    {
        items: [...permissions]
    } :
    {
        items: [
            {
                group: '',
                platform: '',
                cluster: '',
                scopes: []
            }
        ]
    };

    const formMethods = useForm({
        mode: 'onChange',
        reValidateMode: 'onChange',
        defaultValues,
    });

    const { fields, append, remove } = useFieldArray({
        control: formMethods.control,
        name: 'items'
    });

    const values = useWatch({
        control: formMethods.control,
        name: 'items'
    });

    // TODO как-то стремно
    const currentLanguage = i18next.language.split('-')[0];
    const permissionsHelp = i18next.getResource(currentLanguage, env.MSA_PACKAGE_NAME, 'helpers.permissions');

    useEffect(() => {
        if(availableGroups)
            setGroups((isAdmin ? availableGroups : (keycloak.tokenParsed?.groups ?? [])).filter((group: string) => !values.some(value => value.group === group)));
    }, [values, availableGroups]);


    const onReset = () => {
        formMethods.reset(defaultValues);
    };

    const addObject = () => {
        append({
            group: '',
            platform: '',
            cluster: '',
            scopes: []
        });
    };

    useEffect(() => {
        if(permissions)
            formMethods.reset({items: permissions});
    }, [permissions]);


    const onSubmit = (formData: any) => {
        setSubmitted(true);

        for(const item in formData.items) {
            formData.items[item].platform = platformName;
            formData.items[item].cluster = clusterName;

            if(typeof formData.items[item].scopes === 'string') {
                formData.items[item].scopes = formData.items[item].scopes ? formData.items[item].scopes.split(',') : [];
            }
        }

        if(!clusterName) {
            return putPlatformGroups(platformName, formData.items)
                .then(() => {
                    setDrawer(false);
                    onReset();
                    Toast.success({ message: t('toasts.edit', { entity: t('permissions.groups', { context: 'edit' }),  entityName: platformName }) });

                    queryClient.invalidateQueries(['platform', platformName, 'permissions']);
                })
                .catch((error: IError) => {
                    Toast.error({ message: error.message });

                    return false;
                })
                .finally(() => setSubmitted(false));
        } else {
            return putClusterGroups(platformName, clusterName, formData.items)
                .then(() => {
                    setDrawer(false);
                    onReset();
                    Toast.success({ message: t('toasts.edit', { entity: t('credentials.groups', { context: 'edit' }),  entityName: platformName }) });

                    queryClient.invalidateQueries(['platform', platformName, 'cluster', clusterName, 'permissions']);
                })
                .catch((error: IError) => {
                    Toast.error({ message: error.message });

                    return false;
                })
                .finally(() => setSubmitted(false));
        }
    };

    return (
        <>
            {
                button ?
                    React.cloneElement(button, { onClick: () => setDrawer(true) }) :
                    <IconUsers onClick={(e) => { e.stopPropagation(); setDrawer(true); }} disabled={disabled} />
            }

            {
                drawer &&
                <Drawer wide header={t('permissions.edit')} open={drawer} onClose={() => {
                    setDrawer(false);
                    onReset();
                }} backdrop>
                <FormProvider {...formMethods}>
                    <form onSubmit={formMethods.handleSubmit(onSubmit)} noValidate onReset={onReset}>
                        <Drawer.Body>
                            <h4 className="section__header">{t('permissions.permissions')} 
                                {permissionsHelp && <IconHelpCircle onClick={() => setObjectDialog(true)} size="small" />}
                            </h4>
                            <Table cells={3}>
                                <Table.Header>
                                    <Table.Cell>{t('common.group')}</Table.Cell>
                                    <Table.Cell colspan={2}>{t('common.scopes')}</Table.Cell>
                                    <Table.Cell actions={1} />
                                </Table.Header>
                                {
                                    fields.map((field, index) => (
                                        <PermissionRow key={field.id} index={index} remove={remove} groups={groups} />
                                    ))
                                }
                            </Table>
                            <div className={css.buttons}>
                                <Button view="outlined" icon={<IconPlus />} size="small" className={css.buttonLong} onClick={addObject} disabled={!groups.length} />
                            </div>

                        </Drawer.Body>
                        <Drawer.Footer>
                            <Button type="reset" view="outlined" color="red" onClick={onReset}>{t('buttons.clear')}</Button>
                            <Button disabled={!formMethods.formState.isValid || !values.length} loading={submitted} type="submit" onClick={formMethods.handleSubmit(onSubmit)}>{t('buttons.done')}</Button>
                        </Drawer.Footer>
                    </form>
                    </FormProvider>
                </Drawer>
            }

            {
                objectDialog &&
                <Dialog open={objectDialog} className={css.helperDialog} buttons={[
                    <Button key="cancel" onClick={() => setObjectDialog(false)} view="outlined">Ok</Button>,
                ]} onClose={() => setObjectDialog(false)}>
                    <PermissionHelp permissions={permissionsHelp} />
                </Dialog>
            }
        </>
    );
};
