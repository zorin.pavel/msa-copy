import React from 'react'
import { useFormContext } from 'react-hook-form';
import classNames from 'classnames';
import i18next from 'i18next';
import { useTranslation } from 'react-i18next';


import { IOption } from '@msa/kit/types'
import { Badge } from '@msa/kit'

import { badgeColor, badgeColorRu } from 'components/Iam';

import css from './edit.module.scss';


interface IProps {
    scopeOptions: IOption[],
    name: string,
    disabled?: boolean
}

export const BadgeButtons = (props: IProps) => {
    const { scopeOptions, name, disabled } = props;
    const { t } = useTranslation();

    const currentLanguage = i18next.language.split('-')[0];

    const {
        setValue,
        getValues
    } = useFormContext();

    const scopes = getValues(name);

    const selectScope = (scope: string) => {
        if(disabled)
            return;

        setValue(name,
            scopes.includes(scope) ? scopes.filter((el: string) => el !== scope) : [...scopes, scope],
            { shouldValidate: true, shouldTouch: true }
        );
    };


    return (
        scopeOptions.map((scope: IOption) => (
            <Badge
                className={classNames(css.scope, disabled && css.disabled)}
                onClick={() => selectScope(scope.label)}
                key={scope.label}
                label={t(`scopes.${scope.label}`)}
                size="small"
                color={scopes.includes(scope.label) ? badgeColor[scope.label] : 'default'}
            />)
        )
    )
}
