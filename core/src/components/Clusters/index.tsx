import React, { useCallback, useContext, useEffect, useState } from 'react';
import { Outlet, useMatch } from 'react-router-dom';
import { useQuery, useQueryClient } from '@tanstack/react-query';
import { useTranslation } from 'react-i18next';
import i18next from 'i18next';

import { IError, OrderDirection } from '@msa/kit/types';
import { Button, Error, IconPlus, Loader, Table, useTable, sort, Placeholder } from '@msa/kit';
import { useSocket } from '@msa/host';

import { env } from 'env';
import { ICluster } from 'types';
import { getClusters } from 'actions';
import { EditWizard } from './EditWizard';
import { ClusterRow } from './ClusterRow';
import { PlatformContext } from 'components/Platforms/Platform';


export const Clusters = () => {
    const match = useMatch('/platforms/:platformSlug/clusters/:children/*');
    const { platform, can } = useContext(PlatformContext);
    const queryClient = useQueryClient();
    const { t } = useTranslation()

    useSocket(
        { tenant: env.MSA_DEFAULT_TENANT, platform: platform?.name, object_type: 'cluster', type: 'status' },
        {
            callback: () => queryClient.invalidateQueries(['platform', platform?.name, 'cluster']),
            toast: !match
        }
    );

    const { isLoading, data, error } = useQuery<ICluster[], IError>(
        ['platform', platform?.name, 'cluster'],
        () => getClusters(platform?.name)
    );

    const { orderedBy, setOrderedBy } = useTable({ 'name': OrderDirection.asc });
    const [clusters, setClusters] = useState<ICluster[]>(data ?? []);


    useEffect(() => {
        if(data) {
            setClusters(sort<ICluster[]>(data, 'name', OrderDirection.asc));
        }
    }, [data, setClusters]);


    const orderBy = useCallback((fieldName: string, orderDirection: OrderDirection) => {
        if(setOrderedBy)
            setOrderedBy({ [fieldName]: orderDirection });

        setClusters(sort(data ?? [], fieldName, orderDirection));
    }, [data, setOrderedBy]);


    if(match)
        return <Outlet />;

    if(isLoading)
        return <Loader layout />;

    if(error)
        return <Error {...error} layout />;


    return (
        <>
            <div className="layout__header">
                <h1 className="layout__title">{platform?.name} <span>| {i18next.format(t('clusters.clusters'), 'capitalize')}</span></h1>
                {
                    clusters && !!clusters.length &&
                    <EditWizard button={<Button color="red" iconLeft={<IconPlus />} disabled={!can?.create}>{t('clusters.add', { context: 'single' })}</Button>} />
                }
            </div>

            {
                (clusters && !!clusters.length) ?
                    <Table cells={18}>
                        <Table.Header>
                            <Table.Cell colspan={2}>{t('common.name')} <Table.ActionSort fieldName="name" orderedBy={orderedBy} orderBy={orderBy} /></Table.Cell>
                            <Table.Cell colspan={3}>{t('common.description')} </Table.Cell>
                            <Table.Cell colspan={4}>{t('clusters.clusterNodes')} </Table.Cell>
                            <Table.Cell colspan={3}>{t('platforms.platform', {context: 'one'})} <Table.ActionSort fieldName="platform.name" orderedBy={orderedBy} orderBy={orderBy} /></Table.Cell>
                            <Table.Cell colspan={3}>{t('common.deployStatus')} </Table.Cell>
                            <Table.Cell colspan={2}>{t('common.status')} </Table.Cell>
                        </Table.Header>
                        {
                            clusters &&
                            clusters.map((cluster: ICluster) => <ClusterRow cluster={cluster} key={cluster.UUID} />)
                        }
                    </Table> :
                    <Placeholder
                        message={t('clusters.noClusters')}
                        button={<EditWizard button={<Button iconLeft={<IconPlus />} disabled={!can?.create}>{t('clusters.add')}</Button>} />} />
            }
        </>
    );
};
