import React, { useCallback, useContext, useState } from 'react';
import { useQueryClient } from '@tanstack/react-query';

import {
    Button,
    Dialog, IconDelete,
    Table, Toast
} from '@msa/kit';

import { INamespace } from 'types';
import { deleteClusterNamespace } from 'actions';
import { PlatformContext } from 'components/Platforms/Platform';
import { ClusterContext } from 'components/Clusters/Cluster';
import { Edit } from './Edit';


interface IProps {
    namespace: INamespace;
}

export const NamespaceRow = React.memo((props: IProps) => {
    const { namespace } = props;
    const queryClient = useQueryClient();
    const { platform } = useContext(PlatformContext);
    const { cluster, disabled } = useContext(ClusterContext);

    const [deleteDialog, setDeleteDialog] = useState<boolean>(false);
    const [submitted, setSubmitted] = useState<boolean>(false);


    const onDelete = useCallback(() => {
        setSubmitted(true);

        return deleteClusterNamespace(platform?.name, cluster?.name, namespace.name)
            .then(() => {
                Toast.success({ message: `Node <b>${namespace.name}</b> remove in progress` });
                queryClient.invalidateQueries(['platform', platform?.name, 'cluster', cluster?.name, 'namespace']);
            })
            .catch((error) => {
                Toast.error({ message: error.message });

                return false;
            })
            .finally(() => setSubmitted(false));
    }, [namespace]);


    return (
        <>
            <Table.Row className="entity__row" valign="center">
                <Table.Cell>{namespace.name}</Table.Cell>
                <Table.Cell align="center">
                    {
                        namespace.resource_quota?.requests_cpu ?
                            `${namespace.resource_quota?.requests_cpu} mCPUs` : ''
                    }
                </Table.Cell>
                <Table.Cell align="center">
                    {
                        namespace.limit_range?.max_cpu ?
                            `${namespace.limit_range?.max_cpu} mCPUs` : ''
                    }
                </Table.Cell>
                <Table.Cell align="center">
                    {
                        namespace.resource_quota?.requests_memory ?
                            `${namespace.resource_quota?.requests_memory} Mb` : ''
                    }
                </Table.Cell>
                <Table.Cell align="center">
                    {
                        namespace.limit_range?.max_memory ?
                            `${namespace.limit_range?.max_memory} Mb` : ''
                    }
                </Table.Cell>
                <Table.Cell actions={2}>
                    <Edit namespace={namespace} />
                    <IconDelete onClick={() => setDeleteDialog(true)} disabled={disabled} />
                </Table.Cell>
            </Table.Row>

            {
                deleteDialog &&
                <Dialog open={deleteDialog} header="Delete namespace" buttons={[
                    <Button key="cancel" onClick={() => setDeleteDialog(false)} view="outlined">Cancel</Button>,
                    <Button key="delete" onClick={() => onDelete()} loading={submitted}>Delete</Button>
                ]} onClose={() => setDeleteDialog(false)}>
                    Do you really want delete namespace <b>{namespace.name}</b>?
                </Dialog>
            }
        </>
    );
});
