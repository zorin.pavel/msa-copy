import React, { useCallback, useContext, useEffect, useState } from 'react';
import { useQuery } from '@tanstack/react-query';
import { Outlet, useMatch } from 'react-router-dom';
import { useTranslation } from 'react-i18next';

import { IError, OrderDirection } from '@msa/kit/types';
import { Button, Error, IconPlus, Loader, sort, Table, useTable } from '@msa/kit';

import type { INamespace } from 'types';
import { getClusterNamespaces } from 'actions';
import { PlatformContext } from 'components/Platforms/Platform';
import { ClusterContext } from 'components/Clusters/Cluster';
import { NamespaceRow } from 'components/Clusters/Cluster/Namespaces/NamespaceRow';
import { Edit } from './Edit';



export const Namespaces = () => {
    const match = useMatch('/platforms/:platformSlug/clusters/:clusterSlug/namespaces/:children/*');
    const { cluster, disabled, info } = useContext(ClusterContext);
    const { platform } = useContext(PlatformContext);
    const { orderedBy, setOrderedBy } = useTable({ 'name': OrderDirection.asc });
    const { t } = useTranslation()

    const { isLoading, data, error } = useQuery<INamespace[], IError>(
        ['platform', platform?.name, 'cluster', cluster?.name, 'namespace'],
        () => getClusterNamespaces(platform?.name, cluster?.name)
    );

    const [namespace, setNamespaces] = useState<INamespace[]>(data ?? []);

    useEffect(() => {
        if(data)
            setNamespaces(sort<INamespace[]>(data, 'name', OrderDirection.asc));
    }, [data]);


    const orderBy = useCallback((fieldName: string, orderDirection: OrderDirection) => {
        if(setOrderedBy)
            setOrderedBy({ [fieldName]: orderDirection });

        setNamespaces(sort(data ?? [], fieldName, orderDirection));
    }, [data]);


    if(match)
        return <Outlet />

    if(isLoading)
        return <Loader layout />;

    if(error)
        return <Error {...error} layout />;


    return (
        <>
            <div className="layout__header">
                <h1 className="layout__title">{cluster?.name} <span>| {t('navigation.namespaces')}</span></h1>
                <Edit button={<Button iconLeft={<IconPlus />} disabled={disabled}>{t('common.add', { entity: t('common.namespace') })}</Button>} />
            </div>

            <Table cells={6}>
                <Table.Header >
                    <Table.Cell>{t('common.name')} <Table.ActionSort fieldName="name" orderedBy={orderedBy} orderBy={orderBy} /></Table.Cell>
                    <Table.Cell align="center">Requests CPU ({`${(info?.capacity.cpu ?? 0) * 1000} mCPUs`})</Table.Cell>
                    <Table.Cell align="center">Limits CPU ({`${(info?.capacity.cpu ?? 0) * 1000} mCPUs`})</Table.Cell>
                    <Table.Cell align="center">Requests memory ({`${info?.capacity.memory} Mb`})</Table.Cell>
                    <Table.Cell align="center">Limits memory ({`${info?.capacity.memory} Mb`})</Table.Cell>
                    <Table.Cell actions={2} />
                </Table.Header>
                {
                    namespace &&
                    namespace.map((project: INamespace) => <NamespaceRow namespace={project} key={project.name} />)
                }
            </Table>
        </>
    );
};
