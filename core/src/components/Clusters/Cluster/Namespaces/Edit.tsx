import React, { useContext, useEffect, useState } from 'react';
import { Controller, useForm, useWatch } from 'react-hook-form';
import { useQueryClient } from '@tanstack/react-query';
import { useDebounce } from 'use-debounce';
import { useTranslation } from 'react-i18next';

import { IError } from '@msa/kit/types';
import { Button, Drawer, IconEdit, Input, Range, Toast } from '@msa/kit';

import settings from 'settings';
import type { INamespace } from 'types';
import { postClusterNamespace, putClusterNamespace } from 'actions';
import { ClusterContext } from 'components/Clusters/Cluster';
import { PlatformContext } from 'components/Platforms/Platform';


interface IProps {
    namespace?: INamespace,
    button?: React.ReactElement,
}

export const Edit = (props: IProps) => {
    const { namespace, button } = props;
    const queryClient = useQueryClient();
    const { cluster, info, disabled } = useContext(ClusterContext);
    const { platform } = useContext(PlatformContext);
    const { t } = useTranslation()

    const [drawer, setDrawer] = useState<boolean>(false);
    const [submitted, setSubmitted] = useState<boolean>(false);

    const defaultValues = namespace ??
        {
            name: '',
            limit_range: {
                max_cpu: '',
                max_memory: ''
            },
            resource_quota: {
                requests_cpu: '',
                requests_memory: ''
            }
        };

    const {
        getValues,
        setValue,
        handleSubmit,
        reset,
        control,
        formState: { errors, isValid },
    } = useForm({
        mode: 'onChange',
        reValidateMode: 'onChange',
        defaultValues
    });


    const requestCpu = useWatch({
        name: 'resource_quota.requests_cpu',
        control
    });
    const maxCpu = useWatch({
        name: 'limit_range.max_cpu',
        control
    });
    const [debouncedRequestCpu] = useDebounce<string>(requestCpu ?? '', 300);
    const [debouncedMaxCpu] = useDebounce<string>(maxCpu ?? '', 300);


    useEffect(() => {
        const max_cpu = getValues('limit_range.max_cpu');
        const request_cpu = getValues('resource_quota.requests_cpu');

        if(Number(request_cpu) > Number(max_cpu))
            setValue('limit_range.max_cpu', request_cpu, { shouldValidate: true, shouldTouch: true });
    }, [debouncedRequestCpu]);


    useEffect(() => {
        const max_cpu = getValues('limit_range.max_cpu');
        const request_cpu = getValues('resource_quota.requests_cpu');

        if(Number(max_cpu) < Number(request_cpu))
            setValue('resource_quota.requests_cpu', max_cpu ?? '', { shouldValidate: true, shouldTouch: true });
    }, [debouncedMaxCpu]);


    const requestMemory = useWatch({
        name: 'resource_quota.requests_memory',
        control
    });
    const maxMemory = useWatch({
        name: 'limit_range.max_memory',
        control
    });
    const [debouncedRequestMemory] = useDebounce<string>(requestMemory ?? '', 300);
    const [debouncedMaxMemory] = useDebounce<string>(maxMemory ?? '', 300);


    useEffect(() => {
        const max_memory = getValues('limit_range.max_memory');
        const request_memory = getValues('resource_quota.requests_memory');

        if(Number(request_memory) > Number(max_memory))
            setValue('limit_range.max_memory', request_memory, { shouldValidate: true, shouldTouch: true });
    }, [debouncedRequestMemory]);


    useEffect(() => {
        const max_memory = getValues('limit_range.max_memory');
        const request_memory = getValues('resource_quota.requests_memory');

        if(Number(max_memory) < Number(request_memory))
            setValue('resource_quota.requests_memory', max_memory ?? '', { shouldValidate: true, shouldTouch: true });
    }, [debouncedMaxMemory]);


    const onSubmit = (formData: typeof defaultValues) => {
        setSubmitted(true);

        // if(!Number(formData.resource_quota?.requests_cpu)) {
        //     delete formData.resource_quota?.requests_cpu;
        // } else
        //     formData.resource_quota?.requests_cpu + 'm';
        //
        // if(!Number(formData.resource_quota?.requests_memory)) {
        //     delete formData.resource_quota?.requests_memory;
        // } else
        //     formData.resource_quota?.requests_memory + 'Mi';
        //
        // if(!Number(formData.limit_range?.max_cpu)) {
        //     delete formData.limit_range?.max_cpu;
        //     delete formData.resource_quota?.requests_cpu;
        // } else
        //     // formData.limit_range.max_cpu = formData.limit_range?.max_cpu + 'm';
        //
        // if(!Number(formData.limit_range?.max_memory)) {
        //     delete formData.limit_range?.max_memory;
        //     delete formData.resource_quota?.requests_memory;
        // } else
        //     // formData.limit_range?.max_memory = formData.limit_range?.max_memory + 'Mi';
        //
        // if(!formData.limit_range?.max_cpu && !formData.limit_range?.max_memory) {
        //     formData.limit_range = null;
        //     formData.resource_quota = null;
        // }
        //
        // if(!formData.resource_quota?.requests_cpu && !formData.resource_quota?.requests_memory) {
        //     formData.resource_quota = null;
        // }

        formData.limit_range = null;
        formData.resource_quota = null;

        // console.log(formData);
        // return;

        return (
            namespace ?
                putClusterNamespace(platform?.name, cluster?.name, namespace?.name, formData) :
                postClusterNamespace(platform?.name, cluster?.name, formData)
        )
            .then((response: INamespace) => {
                setDrawer(false);
                onReset();
                namespace ?
                Toast.success({ message: t('toasts.edit', { entity: t('common.namespaces'), entityName: response.name }) }) :
                Toast.success({ message: t('toasts.add', { entity: t('common.namespaces'), entityName: response.name }) });
                queryClient.invalidateQueries(['platform', platform?.name, 'cluster', cluster?.name, 'namespace']);
            })
            .catch((error: IError) => {
                Toast.error({ message: error.message });

                return false;
            })
            .finally(() => setSubmitted(false));
    };


    const onReset = () => {
        reset(defaultValues);
    };


    const onClose = () => {
        setDrawer(false);
        onReset();
    };


    return (
        <>
            {
                button ?
                    React.cloneElement(button, { onClick: () => setDrawer(true), disabled }) :
                    <IconEdit onClick={() => setDrawer(true)} disabled={disabled} />
            }

            <Drawer header={namespace ? t('common.edit', { entity: t('common.namespace') }) : t('common.add', { entity: t('common.namespace') })} open={drawer} onClose={onClose} backdrop>
                <form onSubmit={handleSubmit(onSubmit)} noValidate onReset={onReset}>
                    <Drawer.Body>
                        <Controller
                            control={control}
                            name="name"
                            rules={
                                {
                                    required: t('validation.isRequired', { entityName: t('common.name' )}),
                                    pattern: {
                                        value: settings.NAME_REGEX,
                                        message: t('validation.alphanumericCharacters', { entityName: t('common.name' )})
                                    }
                                }
                            }
                            render={({ field }) => (
                                <Input
                                    {...field}
                                    label={t('common.name' )}
                                    labelPosition="left"
                                    required={!namespace}
                                    disabled={!!namespace}
                                    error={!!errors.name && errors.name.message as string}
                                    displayError
                                    autoComplete="off" />
                            )} />

                        <h4 className="section__header">CPU</h4>
                        <Controller
                            control={control}
                            name="resource_quota.requests_cpu"
                            render={({ field }) => (
                                <Range
                                    {...field}
                                    label="Default CPU requests"
                                    labelPosition="left"
                                    max={(info?.capacity.cpu ?? 0) * 1000}
                                    labelMax={`${(info?.capacity.cpu ?? 0) * 1000} mCPUs`}
                                    step={100}
                                    displayValue
                                    error={!!errors.resource_quota && !!errors.resource_quota.requests_cpu && errors.resource_quota.requests_cpu.message as string}
                                    displayError
                                    autoComplete="off" />
                            )} />
                        <Controller
                            control={control}
                            name="limit_range.max_cpu"
                            render={({ field }) => (
                                <Range
                                    {...field}
                                    label="Default CPU limit"
                                    labelPosition="left"
                                    max={(info?.capacity.cpu ?? 0) * 1000}
                                    labelMax={`${(info?.capacity.cpu ?? 0) * 1000} mCPUs`}
                                    step={100}
                                    displayValue
                                    error={!!errors.limit_range && !!errors.limit_range.max_cpu && errors.limit_range.max_cpu.message as string}
                                    displayError
                                    autoComplete="off" />
                            )} />

                        <h4 className="section__header">RAM</h4>
                        <Controller
                            control={control}
                            name="resource_quota.requests_memory"
                            render={({ field }) => (
                                <Range
                                    {...field}
                                    label="Default RAM requests"
                                    labelPosition="left"
                                    max={info?.capacity.memory}
                                    labelMax={`${info?.capacity.memory} Mb`}
                                    step={256}
                                    displayValue
                                    error={!!errors.resource_quota && !!errors.resource_quota.requests_memory && errors.resource_quota.requests_memory.message as string}
                                    displayError
                                    autoComplete="off" />
                            )} />
                        <Controller
                            control={control}
                            name="limit_range.max_memory"
                            render={({ field }) => (
                                <Range
                                    {...field}
                                    label="Default RAM limit"
                                    labelPosition="left"
                                    max={info?.capacity.memory}
                                    labelMax={`${info?.capacity.memory} Mb`}
                                    step={256}
                                    displayValue
                                    error={!!errors.limit_range && !!errors.limit_range.max_memory && errors.limit_range.max_memory.message as string}
                                    displayError
                                    autoComplete="off" />
                            )} />
                    </Drawer.Body>
                    <Drawer.Footer>
                        <Button type="reset" view="outlined">{t('buttons.clear')}</Button>
                        <Button disabled={!isValid} loading={submitted} type="submit">{t('buttons.done')}</Button>
                    </Drawer.Footer>
                </form>
            </Drawer>
        </>
    );
};
