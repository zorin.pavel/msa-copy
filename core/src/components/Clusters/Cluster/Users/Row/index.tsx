import React, { useState } from 'react';
import classNames from 'classnames';

import {
    Button,
    Dialog, IconDelete,
    StatusIcon,
    Table
} from '@msa/kit';

import type { IProject } from 'types';
import { EditWizard } from '../EditWizard';

import css from './user.module.scss';


interface IProps {
    user: IProject;
}

export const Row = React.memo((props: IProps) => {
    const { user } = props;

    const [deleteDialog, setDeleteDialog] = useState<boolean>(false);
    

    // const onDelete = useCallback(() => {
    //     deleteProject(user.id)
    //         .then(() => {
    //             Toast.success({ message: `Project <b>${user.name}</b> delete in progress...` });
    //             dispatch(updateProject({ ...user, action: 'Delete', state: 'In Progress' }));
    //         })
    //         .catch((error: { message: React.ReactNode }) => {
    //             Toast.error({ message: error.message });
    //         });
    // }, [user]);


    return (
        <>
            <Table.Row key={user.name} className={classNames('entity__row', css.user_row)} valign="center">
                <Table.Cell colspan={2}>{user.name}</Table.Cell>
                {/*<Table.Cell colspan={3}>{user.disp_name ? user.disp_name : '-'}</Table.Cell>*/}
                {/*<Table.Cell colspan={2}>{user.requester ? user.requester : '-'}</Table.Cell>*/}
                {/*<Table.Cell colspan={2}>{user.created ? user.created : '-' }</Table.Cell>*/}
                <Table.Cell colspan={2}><StatusIcon status={user.status} /></Table.Cell>
                <Table.Cell actions={2}>
                    <EditWizard user={user} />
                    <IconDelete onClick={() => setDeleteDialog(true)} />
                </Table.Cell>
            </Table.Row>

            {
                deleteDialog &&
                <Dialog open={deleteDialog} header="Delete Project" buttons={[
                    <Button key="cancel" onClick={() => setDeleteDialog(false)} view="outlined">Cancel</Button>,
                    <Button key="delete" onClick={() => console.log()}>Delete</Button>
                ]} onClose={() => setDeleteDialog(false)}>
                    Do you really want delete user <b>{user.name}</b>?
                </Dialog>
            }
        </>
    );
});
