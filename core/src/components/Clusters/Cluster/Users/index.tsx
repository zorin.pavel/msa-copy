import React, { useCallback, useContext, useEffect, useState } from 'react';
import { useQuery } from '@tanstack/react-query';

import type { IError, OrderDirection } from '@msa/kit/types';
import {
    Error,
    Loader, sort, Space,
    Table,
    useTable
} from '@msa/kit';

import type { IProject } from 'types';
// import { getClusterProjects } from 'actions';
import { ClusterContext } from 'components/Clusters/Cluster';
import { EditWizard } from './EditWizard';
import { Row } from './Row';

// import css from './projects.module.scss';


interface IProps {
}

export const Users = (props: IProps) => {
    const {} = props;
    const { cluster } = useContext(ClusterContext);

    const { orderedBy, setOrderedBy } = useTable();

    // TODO что это за говно?
    const { isLoading, data, error } = useQuery<IProject[], IError>(
        ['cluster_users'],
        () => ([{ name: '', status: ''}])
    );
    const [users, setUsers] = useState<IProject[]>(data ?? []);

    useEffect(() => {
        if(data) {
            setUsers(data);
        }
    }, [data]);


    const orderBy = useCallback((fieldName: string, orderDirection: OrderDirection) => {
        if(setOrderedBy)
            setOrderedBy({ [fieldName]: orderDirection });

        setUsers(sort(data ?? [], fieldName, orderDirection));
    }, [data, setOrderedBy]);


    // if(match)
    //     return <Outlet />

    if(isLoading)
        return <Loader />;

    if(error)
        return <Error {...error} />;


    return (
        <>
            <div className="layout__header">
                <h1 className="layout__title">{cluster?.name} <span>| Users</span></h1>
                <EditWizard />
            </div>

            <Table cells={16}>
                <Table.Header >
                    <Table.Cell colspan={2}>Name <Table.ActionSort fieldName="name" orderedBy={orderedBy} orderBy={orderBy} /></Table.Cell>
                    <Table.Cell colspan={3}>Display Name</Table.Cell>
                    <Table.Cell colspan={2}>Requester</Table.Cell>
                    <Table.Cell colspan={2}>Created</Table.Cell>
                    <Table.Cell colspan={2}>Status</Table.Cell>
                    <Table.Cell actions={2}><Space buttons={2} />Actions</Table.Cell>
                </Table.Header>
                {
                    users &&
                    users.map((user: IProject) => <Row user={user} key={user.name} />)
                }
            </Table>
        </>
    );
};
