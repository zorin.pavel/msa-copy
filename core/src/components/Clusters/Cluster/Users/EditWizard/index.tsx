import React, { useState } from 'react';
import { Controller, useForm } from 'react-hook-form';

import { Button, Drawer, IconEdit, IconPlus, Input, timer, Toast } from '@msa/kit';

import type { IProject } from 'types';
import settings from 'settings';


interface IProps {
    user?: IProject
}

export const EditWizard = (props: IProps) => {
    const { user } = props;

    const [drawer, setDrawer] = useState<boolean>(false);
    const [submitted, setSubmitted] = useState<boolean>(false);

    const defaultValues = user ?? { name: '' };

    const {
        handleSubmit,
        reset,
        control,
        formState: { errors, isValid },
    } = useForm({
        mode: 'onChange',
        reValidateMode: 'onChange',
        defaultValues
    });


    const onSubmit = (formData: any) => {
        console.log('formData', formData);
        setSubmitted(true);

        timer<boolean>(true, 1000)
            .then(() => {
                Toast.error({ message: '<b>An error has occurred. Please try later</b>' });
                setSubmitted(false);
                onClose();
            });
    };


    const onReset = () => {
        reset(defaultValues);
    };


    const onClose = () => {
        if(drawer) {
            setDrawer(false);
        }
    };


    return (
        <>
            {
                user ?
                <IconEdit onClick={() => setDrawer(true)} /> :
                <Button color="red" iconLeft={<IconPlus />} onClick={() => setDrawer(true)}>Add User</Button>
            }

            <Drawer header={user ? 'Edit User' : 'New User'} open={drawer} onClose={onClose} backdrop>
                <Drawer.Body>
                        <form onSubmit={handleSubmit(onSubmit)} noValidate onReset={onReset}>
                            <Controller control={control} name="name" rules={
                                {
                                    required: 'Name is required',
                                    pattern: {
                                        value: settings.NAME_REGEX,
                                        message: 'Name must consist of lowercase alphanumeric characters'
                                    }
                                }
                            } render={({ field }) => (
                                <Input
                                    {...field}
                                    label="Name"
                                    labelPosition="left"
                                    required
                                    error={!!errors.name && errors.name.message as string}
                                    displayError
                                    autoComplete="off" />
                            )} />
                            <Controller control={control} name="name" rules={
                                {
                                    required: 'Name is required',
                                    pattern: {
                                        value: settings.NAME_REGEX,
                                        message: 'Name must consist of lowercase alphanumeric characters'
                                    }
                                }
                            } render={({ field }) => (
                                <Input
                                    {...field}
                                    label="Name"
                                    labelPosition="left"
                                    required
                                    error={!!errors.name && errors.name.message as string}
                                    displayError
                                    autoComplete="off" />
                            )} />
                        </form>
                </Drawer.Body>
                <Drawer.Footer>
                    <Button type="reset" view="outlined">Clear all</Button>
                    <Button disabled={!isValid} loading={submitted} type="submit">Done</Button>
                </Drawer.Footer>
            </Drawer>
        </>
    );
};
