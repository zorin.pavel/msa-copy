import React, { useState, useEffect, useContext, useCallback } from 'react';
import { Outlet, useMatch } from 'react-router-dom';
import { useTranslation } from 'react-i18next';
import i18next from 'i18next';

import { OrderDirection } from '@msa/kit/types';
import { sort, Table, useTable } from '@msa/kit';

import type { IClusterNode } from 'types';
import { ClusterContext } from 'components/Clusters/Cluster';
import { NodeRow } from './NodeRow';
import { AddNode } from './AddNode';

import css from './nodes.module.scss';



export const ClusterNodes = () => {
    const { cluster, disabled } = useContext(ClusterContext);
    const { orderedBy, setOrderedBy } = useTable({ 'name': OrderDirection.asc });
    const match = useMatch('/platforms/:platformSlug/clusters/:clusterSlug/nodes/:children/*');
    const { t } = useTranslation()

    const [nodes, setNodes] = useState<IClusterNode[]>(cluster?.nodes ?? []);


    useEffect(() => {
        if(cluster)
            setNodes(sort<IClusterNode[]>(cluster?.nodes, 'name', OrderDirection.asc));
    }, [disabled, cluster?.nodes]);


    const orderBy = useCallback((fieldName: string, orderDirection: OrderDirection) => {
        if(setOrderedBy)
            setOrderedBy({ [fieldName]: orderDirection });

        setNodes(sort<IClusterNode[]>(nodes ?? [], fieldName, orderDirection));
    }, [disabled, nodes]);


    if(match)
        return <Outlet />;


    return (
        <>
            <div className="layout__header">
                <h1 className={css.title}>
                    {cluster?.name} <span>| {i18next.format(t('nodes.nodes'), 'capitalize')} </span>
                </h1>
                <AddNode />
            </div>

            <Table cells={22} className={css.nodes}>
                <Table.Header className={css.nodesHeader}>
                    <Table.Cell actions={1} />
                    <Table.Cell colspan={2}>{t('common.version')}<Table.ActionSort fieldName="os_version" orderedBy={orderedBy} orderBy={orderBy} /></Table.Cell>
                    <Table.Cell colspan={3}>{t('common.kernel')}<Table.ActionSort fieldName="kernel" orderedBy={orderedBy} orderBy={orderBy} /></Table.Cell>
                    <Table.Cell colspan={3}>IP <Table.ActionSort fieldName="ip" orderedBy={orderedBy} orderBy={orderBy} /></Table.Cell>
                    <Table.Cell colspan={3}>{t('common.params')}</Table.Cell>
                    <Table.Cell colspan={2}>{t('common.name')} <Table.ActionSort fieldName="name" orderedBy={orderedBy} orderBy={orderBy} /></Table.Cell>
                    <Table.Cell colspan={3}>{t('common.description')}</Table.Cell>
                    <Table.Cell colspan={2}>{t('credentials.credentials')}<Table.ActionSort fieldName="credential.name" orderedBy={orderedBy} orderBy={orderBy} /></Table.Cell>
                    <Table.Cell>{t('common.online')}</Table.Cell>
                    <Table.Cell>{t('common.labels')}</Table.Cell>
                    <Table.Cell>{t('common.taints')}</Table.Cell>
                    <Table.Cell actions={1} />
                </Table.Header>
                {
                    nodes.map((node: IClusterNode) => <NodeRow key={node.UUID} node={node} />)
                }
            </Table>
        </>
    );
};
