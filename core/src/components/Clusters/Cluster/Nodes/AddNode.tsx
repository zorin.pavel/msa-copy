import React, { useContext, useState } from 'react';
import { FormProvider, useFieldArray, useForm } from 'react-hook-form';
import { useQueryClient } from '@tanstack/react-query';
import { useTranslation } from 'react-i18next';

import { IError } from '@msa/kit/types';
import { Button, Drawer, IconPlus, Table, Toast } from '@msa/kit';

import { addClusterWorker } from 'actions';
import { PlatformContext } from 'components/Platforms/Platform';
import { ClusterContext } from 'components/Clusters/Cluster';
import { NodeRow } from 'components/Clusters/EditWizard/NodeRow';

import css from 'components/Clusters/EditWizard/nodes.module.scss';



export const AddNode = () => {
    const queryClient = useQueryClient();
    const { cluster, disabled, can } = useContext(ClusterContext);
    const { platform } = useContext(PlatformContext);
    const { t } = useTranslation()

    const defaultValues = {
        nodes: [
            {
                name: '',
                ip: '',
                master: false,
                etcd: false,
                worker: true,
                labels: [],
                taints: [],
            }
        ],
    };

    const formMethods = useForm({
        mode: 'onChange',
        reValidateMode: 'onChange',
        defaultValues
    });

    const { fields, append, remove } = useFieldArray({
        control: formMethods.control,
        name: 'nodes'
    });

    const [drawer, setDrawer] = useState<boolean>(false);
    const [submitted, setSubmitted] = useState<boolean>(false);


    const onReset = () => {
        formMethods.reset(defaultValues);
    };


    const addNode = () => {
        append(defaultValues.nodes[0]);
    };


    const onSubmit = (formData: typeof defaultValues) => {
        setSubmitted(true);

        return addClusterWorker(platform?.name, cluster?.name, formData.nodes)
            .then(() => {
                setDrawer(false);
                onReset();
                Toast.success({ message: t('toasts.add', { entity: t('nodes.count', { context: 'few' })}) });
                queryClient.invalidateQueries(['platform', platform?.name, 'cluster']);
            })
            .catch((error: IError) => {
                Toast.error({ message: error.message });

                return false;
            })
            .finally(() => setSubmitted(false));
    };    

    return (
        <>
            <Button
                iconLeft={<IconPlus />}
                disabled={disabled || !can?.edit}
                onClick={() => setDrawer(true)}>{t('nodes.add', {context: "few"})}</Button>

            {
                drawer &&
                <Drawer header={t('nodes.add', {context: "few"})} open={drawer} onClose={() => {
                    setDrawer(false);
                    onReset();
                }} backdrop>
                    <FormProvider {...formMethods}>
                        <form onSubmit={formMethods.handleSubmit(onSubmit)} noValidate onReset={onReset}>
                            <Drawer.Body>
                                <Table cells={9} className={css.nodes}>
                                    <Table.Header>
                                        <Table.Cell actions={1} />
                                        <Table.Cell colspan={3}>{t('nodes.count', {context: "one"})}</Table.Cell>
                                        <Table.Cell colspan={1}>Master</Table.Cell>
                                        <Table.Cell colspan={1}>Etcd</Table.Cell>
                                        <Table.Cell colspan={1}>Worker</Table.Cell>
                                        <Table.Cell colspan={1}>Labels</Table.Cell>
                                        <Table.Cell colspan={1}>Taints</Table.Cell>
                                        <Table.Cell actions={1} />
                                    </Table.Header>
                                    {
                                        fields.map((field, index) => {
                                            return (
                                                <NodeRow key={field.id} worker {...{ index, remove }} />
                                            );
                                        })
                                    }
                                </Table>
                                <div className={css.nodesButtons}>
                                    <Button view="outlined" icon={<IconPlus />} size="small" className={css.buttonLong} onClick={addNode}/>
                                </div>
                            </Drawer.Body>
                            <Drawer.Footer>
                                <Button type="reset" view="outlined">{t('buttons.clear')}</Button>
                                <Button disabled={!formMethods.formState.isValid} loading={submitted} type="submit">{t('buttons.done')}</Button>
                            </Drawer.Footer>
                        </form>
                    </FormProvider>
                </Drawer>
            }
        </>
    );
};
