import React, { useContext, useEffect, useRef, useState } from 'react';
import { useParams } from 'react-router-dom';
import { useQuery, useQueryClient } from '@tanstack/react-query';
import { format } from 'date-fns';
import classNames from 'classnames';
import parse from 'html-react-parser';

import { IError } from '@msa/kit/types';
import { Button, Error, IconLabels, IconPlus } from '@msa/kit';

import settings from 'settings';
import { IClusterNode, IClusterNodeLog, INodeLabel, INodeTaint } from 'types';
import { loadClusterNodeLogs } from 'actions';
import { PlatformContext } from 'components/Platforms/Platform';
import { ClusterContext } from 'components/Clusters/Cluster';
import { LabelForm } from 'components/Clusters/EditWizard/Labels';
import { TaintForm } from 'components/Clusters/EditWizard/Taints';

import css from './logs.module.scss';


const startTime = new Date().getTime() / 1000 - 24 * 60 * 60;

export const ClusterNode = () => {
    const { nodeSlug } = useParams();
    const queryClient = useQueryClient();
    const { cluster } = useContext(ClusterContext);
    const { platform } = useContext(PlatformContext);
    const logBody = useRef<HTMLDivElement>(null);

    const node = cluster?.nodes.find((node: IClusterNode) => node.name === nodeSlug);
    const stopTime = new Date().getTime() / 1000;

    const [errorLogs, setErrorLogs] = useState<boolean>(false);
    const [params] = useState<{ start: number, stop: number }>({ start: startTime, stop: stopTime });

    const { data: logs, error } = useQuery<IClusterNodeLog[], IError>(
        ['platform', platform?.name, 'cluster', cluster?.name, 'nodes', nodeSlug, 'logs', ...Object.values(params)],
        () => loadClusterNodeLogs(platform?.name, cluster?.name, nodeSlug, params),
        {
            refetchInterval: settings.DELAY.LOG,
        }
    );


    useEffect(() => {
        if(logBody.current) {
            logBody.current.scrollTop = logBody.current.scrollHeight;
        }
    }, [logs, logBody]);


    useEffect(() => {
        setErrorLogs(!!error);
        queryClient.invalidateQueries(['platform', platform?.name, 'cluster', cluster?.name, 'nodes', nodeSlug, 'logs']);
    }, [error]);


    if(!node)
        return <Error message="Node is not in the cluster" />


    return (
        <>
            <div className="layout__header">
                <h1 className="layout__title">{cluster?.name} <span>| {nodeSlug}</span></h1>
            </div>

            <div className="row gap15">
                <div className={classNames('c2of3', css.logs)}>
                    <div className={css.body} ref={logBody}>
                            {
                                errorLogs &&
                                <>
                                    <div className={css.row}>
                                        <span className={css.time}>{format(new Date(), 'HH:mm:ss')}</span>
                                        <span className={classNames(css['error'], css.message)}>{error?.status}</span>
                                    </div>
                                    <div className={css.row}>
                                        <span className={css.time}></span>
                                        <span className={classNames(css['error'], css.message)}>{error?.statusText}</span>
                                    </div>
                                    <div className={css.row}>
                                        <span className={css.time}></span>
                                        <span className={classNames(css['error'], css.message)}>{error?.message}</span>
                                    </div>
                                </>
                            }
                            {
                                logs &&
                                logs.map((log, index) => {
                                    if(log.msg === '')
                                        return null;

                                    return (
                                        <div className={css.row} key={index}>
                                            {
                                                (log.Component || log.Stage) ?
                                                <>

                                                        <span className={css.time}>{log.time && format(Date.parse(log.time), 'HH:mm:ss')}</span>
                                                        {
                                                            log.Component &&
                                                            <span className={classNames(css['component'], css.message)}>{parse(log.Component.replace(/\n/g, '<br />'))}</span>
                                                        }
                                                        {
                                                            log.Stage &&
                                                            <span className={classNames(css['stage'], css.message)}>{parse(log.Stage.replace(/\n/g, '<br />'))}</span>
                                                        }
                                                        <span className={classNames(css[log.level ?? ''], css.message)}>{parse(log.msg.replace(/\n/g, '<br />'))}</span>
                                                </> :
                                                <>
                                                    <span className={css.time}>{log.time && format(Date.parse(log.time), 'HH:mm:ss')}</span>
                                                    <span className={classNames(css[log.level ?? ''], css.message)}>{parse(log.msg.replace(/\n/g, '<br />'))}</span>
                                                </>
                                            }
                                            {
                                                log.Error &&
                                                    <>
                                                        <span className={css.time} />
                                                        <span className={classNames(css['error'], css.message)}>{parse(log.Error.replace(/\n/g, '<br />'))}</span>
                                                    </>
                                            }
                                        </div>
                                    );
                                })
                            }
                            <div className={css.row}>
                                <span className={css.time} />
                                <span className={classNames(css.message, css.cursor)}>_</span>
                            </div>
                        </div>
                </div>
                <div className="c1of3 stack-papers">
                    <div className="paper">
                        <h4 className="paper__header start"><IconLabels disabled /> Labels</h4>
                        {
                            node.labels &&
                            node.labels.map((label: INodeLabel) => {
                                return (
                                    <div key={label.key} className={classNames('row', 'gap1', css.info)}>
                                        <div className={classNames('c2of6', css.infoKey)}>{label.key}</div>
                                        <div className="c4of6">{label.value}</div>
                                    </div>
                                );
                            })
                        }
                        <LabelForm node={node} button={<Button iconLeft={<IconPlus />} label="Labels" className={css.addLabels} view={!node.labels || !node.labels.length ? 'outlined' : undefined} />} />
                    </div>
                    <div className="paper">
                        <h4 className="paper__header start"><IconLabels disabled /> Taints</h4>
                        {
                            node.taints &&
                            node.taints.map((taint: INodeTaint) => {
                                return (
                                    <div key={taint.key} className={classNames('row', 'gap1', css.info)}>
                                        <div className={classNames('c2of6', css.infoKey)}>{taint.key}</div>
                                        <div className="c2of6">{taint.value}</div>
                                        <div className={classNames('c2of6', css.infoEffect)}>{taint.effect}</div>
                                    </div>
                                );
                            })
                        }
                        <TaintForm node={node} button={<Button iconLeft={<IconPlus />} label="Taints" className={css.addLabels} view={!node.taints || !node.taints.length ? 'outlined' : undefined}/>} />
                    </div>
                </div>
            </div>
        </>
    );
};
