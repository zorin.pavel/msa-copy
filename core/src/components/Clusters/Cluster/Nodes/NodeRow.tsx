import React, { useCallback, useContext, useEffect, useState } from 'react';
import classNames from 'classnames';
import { useQueryClient } from '@tanstack/react-query';
import { useNavigate } from 'react-router-dom';
import { useTranslation } from 'react-i18next';

import { Badge, Button, Dialog, IconDelete, Table, Toast, Image, StatusIcon } from '@msa/kit';

import type { IClusterNode } from 'types';
import { removeClusterWorker } from 'actions';
import { PlatformContext } from 'components/Platforms/Platform';
import { ClusterContext } from 'components/Clusters/Cluster';
import { LabelForm } from 'components/Clusters/EditWizard/Labels';
import { TaintForm } from 'components/Clusters/EditWizard/Taints';

import css from './nodes.module.scss';


interface IProps {
    node: IClusterNode,
}

export const NodeRow = (props: IProps) => {
    const { node } = props;
    const queryClient = useQueryClient();
    const navigate = useNavigate();
    const { cluster, disabled, can } = useContext(ClusterContext);
    const { platform } = useContext(PlatformContext);
    const { t } = useTranslation()

    const [deleteDialog, setDeleteDialog] = useState<boolean>(false);
    const [submitted, setSubmitted] = useState<boolean>(false);
    const [forceDelete, setForceDelete] = useState<boolean>(false);
    const [forceDeleteDialog, setForceDeleteDialog] = useState<boolean>(false);


    useEffect(() => {
        document.addEventListener('keydown', handleKeyDown);
        document.addEventListener('keyup', handleKeyUp);

        return () => {
            document.removeEventListener('keydown', handleKeyDown);
            document.removeEventListener('keyup', handleKeyDown);
        };
    }, []);


    const handleKeyDown = useCallback((event: KeyboardEvent) => {
        if(event.key === 'Control' || event.metaKey) {
            setForceDelete(true);
        }
    }, []);


    const handleKeyUp = useCallback(() => {
        setForceDelete(false);
    }, []);


    const onDelete = (force = false) => {
        setSubmitted(true);

        return removeClusterWorker(platform?.name, cluster?.name, node.name, force)
            .then(() => {
                Toast.success({ message: t('toasts.delete', { entity: t('nodes.node', { context: 'single' }), entityName: node.ip }) });
                queryClient.invalidateQueries(['platform', platform?.name, 'cluster', cluster?.name]);
            })
            .catch((error) => {
                Toast.error({ message: error.message });

                return false;
            })
            .finally(() => setSubmitted(false));
    };


    const handleClick = () => {
        navigate(`${node.name}`);
    };


    return (
        <Table.Row valign="center" className={classNames('entity__row entity__row--hover', css.nodeRow, !node.is_online && css.fail)}>
            <Table.Cell actions={1}>
                <Image src={`/icons/${node.os_family?.toLowerCase()}.svg`} fallBack={<></>} alt="os" className={css.image} />
            </Table.Cell>
            <Table.Cell colspan={2} onClick={handleClick}>{node.os_version}</Table.Cell>
            <Table.Cell colspan={3} onClick={handleClick}>{node.kernel}</Table.Cell>
            <Table.Cell colspan={3} onClick={handleClick}>{node.ip}</Table.Cell>
            <Table.Cell colspan={3} onClick={handleClick}>
                {
                    node.master &&
                    <Badge label="master" size="xsmall" color="blue" />
                }
                {
                    node.etcd &&
                    <Badge label="etcd" size="xsmall" color="dark" />
                }
                {
                    node.worker &&
                    <Badge label="worker" size="xsmall" color="green" />
                }
            </Table.Cell>
            <Table.Cell colspan={2} onClick={handleClick}>{node.name}</Table.Cell>
            <Table.Cell colspan={3} onClick={handleClick}>{node.description}</Table.Cell>
            <Table.Cell colspan={2} onClick={handleClick}>
                {
                    node.credential ? node.credential.name : ''
                }
            </Table.Cell>
            <Table.Cell onClick={handleClick}>
                <StatusIcon status={node.is_online ? 'Success' : 'Fail'} caption={false} />
            </Table.Cell>
            <Table.Cell>
                <LabelForm node={node} />
            </Table.Cell>
            <Table.Cell>
                <TaintForm node={node} />
            </Table.Cell>
            <Table.Cell actions={1}>
                {
                    forceDelete && !node.master && can?.delete ?
                        <IconDelete disabled={node.master} onClick={() => setForceDeleteDialog(true)} className={css.force}/> :
                        <IconDelete disabled={node.master || disabled || !can?.delete} onClick={() => setDeleteDialog(true)} />
                }
            </Table.Cell>

            {
                deleteDialog &&
                <Dialog open={deleteDialog} header={t('nodes.delete', { context: 'single' })} buttons={[
                    <Button key="cancel" onClick={() => setDeleteDialog(false)} view="outlined">{t('buttons.cancel')}</Button>,
                    <Button key="delete" onClick={() => onDelete()} loading={submitted}>{t('buttons.delete')}</Button>
                ]} onClose={() => setDeleteDialog(false)}>
                    {t('dialogs.delete', {entityName: t('nodes.count', { context: 'single' })})} <b>{node.ip}</b>?
                </Dialog>
            }

            {
                forceDeleteDialog &&
                <Dialog open={forceDeleteDialog} header={t('nodes.delete', { context: 'single' })} buttons={[
                    <Button key="cancel" onClick={() => setDeleteDialog(false)} view="outlined">{t('buttons.cancel')}</Button>,
                    <Button key="delete" onClick={() => onDelete(true)} loading={submitted}>{t('buttons.forceDelete')}</Button>
                ]} onClose={() => setForceDeleteDialog(false)}>
                    {t('dialogs.delete', {entityName: t('nodes.count', { context: 'single' })})} <b>{node.ip}</b>?
                </Dialog>
            }
        </Table.Row>
    );
};
