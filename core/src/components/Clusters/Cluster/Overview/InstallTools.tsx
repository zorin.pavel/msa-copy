import React, { useContext, useMemo, useState } from 'react';
import { Controller, useForm } from 'react-hook-form';
import { useQuery, useQueryClient } from '@tanstack/react-query';
import { useTranslation } from 'react-i18next';

import { DrawerFooterJustify, IError, Variants } from '@msa/kit/types';
import { Button, Drawer, IconTools, Select, Toast } from '@msa/kit';

import type { IRepository } from 'types';
import { getVendorRepos, installTools } from 'actions';
import { Edit as EditHelm } from 'components/Repositories/Edit';
import { ClusterContext } from 'components/Clusters/Cluster';
import { PlatformContext } from 'components/Platforms/Platform';


interface IProps {
    view?: Variants
}

export const InstallTools = (props: IProps) => {
    const { view = Variants.outlined } = props;
    const queryClient = useQueryClient();
    const { platform } = useContext(PlatformContext);
    const { cluster, disabled, can } = useContext(ClusterContext);
    const { t } = useTranslation();

    const defaultValues = { vendor_repo_name: '', cluster_tools: true };
    const {
        handleSubmit,
        reset,
        control,
        formState: { errors, isValid },
    } = useForm({
        mode: 'onChange',
        reValidateMode: 'onChange',
        defaultValues
    });

    const [submitted, setSubmitted] = useState<boolean>(false);
    const [drawer, setDrawer] = useState<boolean>(false);

    const { data: helmRepos } = useQuery<IRepository[], IError>(
        ['repos', 'helm'],
        () => getVendorRepos('helm')
    );
    const helmReposOptions = useMemo(
        () => helmRepos ? helmRepos.map((repo: IRepository) => ({ label: repo.name, value: repo.name })) : [],
        [helmRepos]
    );    

    const onSubmit = (formData: typeof defaultValues) => {
        setSubmitted(true);

        return installTools(platform?.name, cluster?.name, formData)
            .then(() => {
                setDrawer(false);
                onReset();
                Toast.success({ message: t('toasts.add', { entity: 'Cluster tools', entityName: cluster?.name }) });

                queryClient.invalidateQueries(['platform', platform?.name, 'cluster']);
            })
            .catch((error: IError) => {
                Toast.error({ message: error.message });

                return false;
            })
            .finally(() => setSubmitted(false));
    };


    const onReset = () => {
        reset(defaultValues);
    };


    if(!helmRepos || !helmRepos.length)
        return null;        

    return (
        <>
            <Button
                view={view}
                disabled={cluster?.cluster_tools || disabled || !can?.edit}
                loading={submitted}
                iconRight={<IconTools />}
                onClick={() => setDrawer(true)}>{t('buttons.tools' )}</Button>

            {
                drawer &&
                <Drawer header={t('buttons.tools' ) + ' ' + cluster?.name} open={drawer} onClose={() => {
                    setDrawer(false);
                    onReset();
                }} backdrop>
                    <form onSubmit={handleSubmit(onSubmit)} noValidate onReset={onReset}>
                        <Drawer.Body>
                            {
                                (!helmRepos || !helmRepos.length) ?
                                    <>
                                        {t('common.noteClusterTools' )}
                                        <EditHelm repoType={'helm'} />
                                    </> :
                                    <Controller
                                        control={control}
                                        name="vendor_repo_name"
                                        rules={
                                            {
                                                required: t('validation.isRequired', { entityName: t('common.helmReg' )}),
                                            }
                                        }
                                        render={({ field }) => (
                                            <Select
                                                {...field}
                                                label={t('common.helmReg' )}
                                                required
                                                labelPosition="left"
                                                error={!!errors.vendor_repo_name}
                                                autoComplete="off"
                                                options={helmReposOptions}
                                            />
                                        )} />
                            }
                        </Drawer.Body>
                        <Drawer.Footer justify={DrawerFooterJustify.end}>
                            <Button disabled={!isValid} loading={submitted} type="submit" color="red">{t('buttons.install' )}</Button>
                        </Drawer.Footer>
                    </form>
                </Drawer>
            }
        </>
    );
};
