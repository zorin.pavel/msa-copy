import React, { useCallback, useContext, useEffect, useRef, useState } from 'react';
import classNames from 'classnames';

import { Backdrop, Button, IconDoubleLines, IconExternalLink } from '@msa/kit';

import { env } from 'env';
import { API } from 'helpers/apiInstance';
import { ClusterContext } from 'components/Clusters/Cluster';
import { PlatformContext } from 'components/Platforms/Platform';

import css from './kubectl.module.scss';


const maxHeight = 120;

export const KubeCtl = () => {
    const { platform } = useContext(PlatformContext);
    const { cluster, can } = useContext(ClusterContext);
    const containerRef = useRef<HTMLDivElement>(null);
    const iframeRef = useRef<HTMLIFrameElement>(null);

    const [ctlDialog, setCtlDialog] = useState<boolean>(false);
    const [terminalUrl, setTerminalUrl] = useState<string>('');

    const [isResizing, setResizing] = useState<boolean>(false);
    const [startY, setStartY] = useState<number>(0);
    const [startHeight, setStartHeight] = useState<number>(0);


    const getClusterConfig = () => {
        return API.get<any, Blob>(
            `/tenants/${env.MSA_DEFAULT_TENANT}/platforms/${platform?.name}/clusters/${cluster?.name}/kubeconfig_file`,
            { responseType: 'blob' }
        )
            .then((response: Blob) => {
                const reader = new FileReader();

                reader.readAsDataURL(response);
                reader.onload = () => {
                    const fileContent = reader.result as string;
                    const kubConfig = fileContent.replace(/^data:text\/yaml;base64,/, '');

                    getCtlToken(kubConfig);
                };
            });
    };


    const getCtlToken = (kubeConfig: string) => {
        return API.post<{ name: string, kubeConfig: string }, { success: boolean, token: string, message: string }>(`https://${env.MSA_BASE_DOMAIN}/platforms/${platform?.name}/clusters/${cluster?.name}/cli/api/kube-config`,
            { name: cluster?.name, kubeConfig }
        )
            .then((response) => {
                setTerminalUrl(`https://${env.MSA_BASE_DOMAIN}/platforms/${platform?.name}/clusters/${cluster?.name}/cli/terminal/?token=${response.token}`);
                setCtlDialog(true);
            })
    };


    const handleMouseDown = useCallback((event: React.MouseEvent<HTMLDivElement>) => {
        if(containerRef.current && event.button === 0) { // Проверяем, что нажата левая кнопка мыши
            setResizing(true);
            setStartY(event.clientY);
            setStartHeight(containerRef.current?.offsetHeight);
        }
    }, [containerRef, setResizing, setStartY, setStartHeight]);


    const handleMouseMove = useCallback((event: MouseEvent) => {
        if(isResizing && containerRef.current) {
            const deltaY = startY - event.clientY;
            const height = Math.max(maxHeight, startHeight + deltaY);

            if(height > window.document.body.clientHeight - 50)
                return;

            containerRef.current.style.height = `${height}px`;

            if(height <= maxHeight + 20)
                setCtlDialog(false);
        }
    }, [containerRef, isResizing]);


    const handleMouseUp = useCallback(() => {
        setResizing(false);

        if(iframeRef.current && iframeRef.current.contentWindow) {
            iframeRef.current.contentWindow.document.body.focus();
        }
    }, [setResizing, iframeRef]);


    useEffect(() => {
        document.addEventListener('mousemove', handleMouseMove);
        document.addEventListener('mouseup', handleMouseUp);

        return () => {
            document.removeEventListener('mousemove', handleMouseMove);
            document.removeEventListener('mouseup', handleMouseUp);
        };
    }, [isResizing]);


    return (
        <>
            <Button size="medium" view="outlined" onClick={getClusterConfig} disabled={!cluster?.is_online || !can?.get} iconRight={<IconExternalLink />}>KubeCTL</Button>

            {
                ctlDialog &&
                <>
                    <Backdrop open onClick={() => setCtlDialog(false)} />
                    <div ref={containerRef} className={classNames(css.container, isResizing && css.resizing)}>
                        <div className={css.borderTop} onMouseDown={handleMouseDown}>
                            <IconDoubleLines className={css.icon} />
                        </div>
                        <iframe ref={iframeRef} src={terminalUrl} className={classNames(isResizing && css.resizing)} />
                    </div>
                </>
            }
        </>
    );
};
