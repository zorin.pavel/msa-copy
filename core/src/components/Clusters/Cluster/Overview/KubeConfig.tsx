import React, { useCallback, useContext } from 'react';

import { Button, IconDownload, Toast } from '@msa/kit';

import { getClusterConfig } from 'actions';
import { PlatformContext } from 'components/Platforms/Platform';
import { ClusterContext } from 'components/Clusters/Cluster';


export const KubeConfig = () => {
    const { cluster, disabled, can } = useContext(ClusterContext);
    const { platform } = useContext(PlatformContext);

    const onClick = useCallback(() => {
        return getClusterConfig(platform?.name, cluster?.name)
            .catch(error => {
                Toast.error({ message: error.message });

                return false;
            });
    }, [cluster, platform]);


    return (
        <Button
            view="outlined"
            onClick={onClick}
            disabled={disabled || !can?.get}
            iconRight={<IconDownload />}>Kubeconfig</Button>
    );
};
