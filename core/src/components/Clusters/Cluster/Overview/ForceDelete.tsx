import React, { useCallback, useContext, useEffect, useState } from 'react';
import { useNavigate } from 'react-router-dom';
import { useQueryClient } from '@tanstack/react-query';
import { useTranslation } from 'react-i18next';

import { Button, Dialog, IconDelete, Toast } from '@msa/kit';

import { deleteClusterForce } from 'actions';
import { ClusterContext } from 'components/Clusters/Cluster';
import { PlatformContext } from 'components/Platforms/Platform';


export const ForceDelete = () => {
    const navigate = useNavigate();
    const queryClient = useQueryClient();
    const { platform } = useContext(PlatformContext);
    const { cluster, can } = useContext(ClusterContext);
    const { t } = useTranslation();

    const [deleteDialog, setDeleteDialog] = useState<boolean>(false);
    const [forceDelete, setForceDelete] = useState<boolean>(false);

    useEffect(() => {
        document.addEventListener('keydown', handleKeyDown);
        document.addEventListener('keyup', handleKeyUp);

        return () => {
            document.removeEventListener('keydown', handleKeyDown);
            document.removeEventListener('keyup', handleKeyDown);
        };
    }, []);


    const handleKeyDown = useCallback((event: KeyboardEvent) => {
        if(event.key === 'Control' || event.metaKey) {
            setForceDelete(true);
        }
    }, []);


    const handleKeyUp = useCallback(() => {
        setForceDelete(false);
    }, []);


    const onDelete = () => {
        return deleteClusterForce(platform?.name, cluster?.name)
            .then(() => {
                Toast.success({ message: t('toasts.delete', { entity: t('clusters.cluster'), entityName: cluster?.name }) });
                queryClient.invalidateQueries(['platform', platform?.name, 'cluster']);
                navigate(`/platforms/${platform?.name}/clusters`);
            })
            .catch((error) => {
                Toast.error({ message: error.message });

                return false;
            });
    };


    if(!platform || !cluster)
        return null;


    return (
        <>
            {
                (forceDelete && can?.delete) &&
                <Button
                    onClick={() => setDeleteDialog(true)}
                    iconRight={<IconDelete />}>{t('buttons.forceDelete')}</Button>
            }
            {
                deleteDialog &&
                <Dialog open={deleteDialog} header={t('clusters.forceDelete')} buttons={[
                    <Button key="cancel" onClick={() => setDeleteDialog(false)} view="outlined">{t('buttons.cancel')}</Button>,
                    <Button key="delete" onClick={onDelete}>{t('buttons.forceDelete')}</Button>
                ]} onClose={() => setDeleteDialog(false)}>
                    {t('dialogs.delete', {entityName: t('clusters.cluster')})} <b>{cluster?.name}</b>?
                </Dialog>
            }
        </>
    );
};
