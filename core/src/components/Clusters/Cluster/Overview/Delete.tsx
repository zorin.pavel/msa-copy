import React, { useCallback, useContext, useState } from 'react';
import { useNavigate } from 'react-router-dom';
import { useQueryClient } from '@tanstack/react-query';
import { useTranslation } from 'react-i18next';

import { Button, Dialog, Helper, IconDelete, Toast } from '@msa/kit';

import settings from 'settings';
import type { ICluster } from 'types';
import { deleteCluster } from 'actions';
import { PlatformContext } from 'components/Platforms/Platform';
import { ClusterContext } from 'components/Clusters/Cluster';


interface IProps {
    button?: React.ReactElement,
    cluster?: ICluster
}

export const Delete = (props: IProps) => {
    const { button } = props;
    const navigate = useNavigate();
    const queryClient = useQueryClient();
    const { platform } = useContext(PlatformContext);
    const { cluster, can } = useContext(ClusterContext);
    const { t } = useTranslation();

    const disabled = !cluster?.is_online &&
        !(
            settings.STATE.FAIL.includes(cluster?.state as string) &&
            ['Create', 'Recreate'].includes(cluster?.action as string)
        );

    const [deleteDialog, setDeleteDialog] = useState<boolean>(false);
    const [loading, setLoading] = useState<boolean>(false);


    const onDelete = useCallback(() => {
        setLoading(true);

        return deleteCluster(platform?.name, cluster?.name)
            .then(() => {
                Toast.success({ message: t('toasts.delete', { entity: t('clusters.cluster'), entityName: cluster?.name }) });
                queryClient.invalidateQueries(['platform', platform?.name, 'cluster']);
                navigate(`/platforms/${platform?.name}/clusters`);
            })
            .catch((error) => {
                Toast.error({ message: error.message });

                return false;
            })
            .finally(() => setLoading(false));
    }, [cluster, platform]);


    return (
        <>
            {
                button ?
                    React.cloneElement(button, { onClick: () => setDeleteDialog(true), disabled: (disabled || !can?.delete) }) :
                    <Helper description={t('common.deleteHelper')}>
                        <Button
                            view="outlined"
                            disabled={disabled || !can?.delete}
                            loading={loading}
                            onClick={() => setDeleteDialog(true)}
                            iconRight={<IconDelete />}>{t('buttons.delete')}</Button>
                    </Helper>
            }
            {
                deleteDialog &&
                <Dialog open={deleteDialog} header={t('clusters.delete')} buttons={[
                    <Button key="cancel" onClick={() => setDeleteDialog(false)} view="outlined" disabled={loading}>{t('buttons.cancel')}</Button>,
                    <Button key="delete" onClick={onDelete} loading={loading}>{t('buttons.delete')}</Button>
                ]} onClose={() => setDeleteDialog(false)}>
                    {t('dialogs.delete', {entityName: t('clusters.cluster')})} <b>{cluster?.name}</b>?
                </Dialog>
            }
        </>
    );
};
