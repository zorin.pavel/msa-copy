import React, { useCallback, useContext, useEffect, useState } from 'react';
import { useQueryClient } from '@tanstack/react-query';
import { useTranslation } from 'react-i18next';

import { Button, Dialog, IconSpinner, Toast } from '@msa/kit';

import settings from 'settings';
import { redeployCluster } from 'actions';
import { ClusterContext } from 'components/Clusters/Cluster';
import { PlatformContext } from 'components/Platforms/Platform';

import css from './overview.module.scss';


export const Redeploy = () => {
    const { cluster, can } = useContext(ClusterContext);
    const { platform } = useContext(PlatformContext);
    const queryClient = useQueryClient();
    const { t } = useTranslation();

    const [redeployLoading, setRedeployLoading] = useState<boolean>(false);
    const [redeployDialog, setRedeployDialog] = useState<boolean>(false);
    const [forceCreate, setForceCreate] = useState<boolean>(false);

    useEffect(() => {
        document.addEventListener('keydown', handleKeyDown);
        document.addEventListener('keyup', handleKeyUp);

        return () => {
            document.removeEventListener('keydown', handleKeyDown);
            document.removeEventListener('keyup', handleKeyDown);
        };
    }, []);


    const handleKeyDown = useCallback((event: KeyboardEvent) => {
        if(event.key === 'Control' || event.metaKey) {
            setForceCreate(true);
        }
    }, []);


    const handleKeyUp = useCallback(() => {
        setForceCreate(false);
    }, []);


    const onDeploy = useCallback(() => {
        setRedeployLoading(true);

        return redeployCluster(platform?.name, cluster)
            .then((_response) => {
                Toast.success({ message: t('toasts.redeploy', { entity: t('clusters.count', {context: 'few'}), entityName: cluster?.name }) });
                queryClient.invalidateQueries(['platform', platform?.name, 'cluster', cluster?.name ]);
            })
            .catch(error => {
                Toast.error({ message: error.message });

                return false;
            })
            .finally(() => setRedeployLoading(false));
    }, [cluster, platform]);


    return (
        <>
            <Button
                view="outlined"
                size="medium"
                disabled={(!forceCreate && settings.STATE.PROGRESS.includes(cluster?.state as string)) || !can?.edit}
                loading={redeployLoading}
                iconLeft={forceCreate && settings.STATE.PROGRESS.includes(cluster?.state as string) ? <IconSpinner className="spin" /> : undefined}
                onClick={() => setRedeployDialog(true)}>{t('buttons.recreate' )}</Button>

            {
                redeployDialog &&
                <Dialog
                    open={redeployDialog}
                    header={t('buttons.recreate' ) + ' ' + t('clusters.count', {context: "one"} )}
                    buttons={[
                        <Button key="cancel" onClick={() => setRedeployDialog(false)} view="outlined">{t('buttons.cancel' )}</Button>,
                        <Button key="delete" onClick={onDeploy} loading={redeployLoading}>{t('buttons.recreate' )}</Button>
                    ]}
                    onClose={() => setRedeployDialog(false)}>
                    {t('clusters.recreate', {entityName: cluster?.name})}
                    <p className={css.recreate}> {t('clusters.dataRemoved')}</p>
                </Dialog>
            }
        </>
    );
};
