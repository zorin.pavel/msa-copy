import React, { useContext } from 'react';
import classNames from 'classnames';
import { useQuery } from '@tanstack/react-query';
import { useTranslation } from 'react-i18next';

import { IError } from '@msa/kit/types';
import { brakeWord, StatusIcon } from '@msa/kit';

import { getClusterPods } from 'actions';
import { ClusterContext } from 'components/Clusters/Cluster';
import { PlatformContext } from 'components/Platforms/Platform';
import { ClusterInfo } from './ClusterInfo';
import { Upgrade } from './Upgrade';
import { Redeploy } from './Redeploy';
import { KubeConfig } from './KubeConfig';
import { Delete } from './Delete';
import { ForceDelete } from './ForceDelete';
import { KubeCtl } from './KubeCtl';
import { InstallTools } from './InstallTools';
import { Dashboard } from './Dashboard';
import { Edit } from './Edit';

import css from './overview.module.scss';


export const ClusterOverview = () => {
    const { platform } = useContext(PlatformContext);
    const { cluster, disabled } = useContext(ClusterContext);
    const { t } = useTranslation();

    const { data: pods } = useQuery<any, IError>(
        ['platform', platform?.name, 'cluster', cluster?.name, 'pods'],
        () => getClusterPods(platform?.name, cluster?.name),
        {
            enabled: !disabled,
            placeholderData: {
                'kube-apiserver': { 'count': '0/0', 'status': 'N/A' },
                'kube-controller': { 'count': '0/0', 'status': 'N/A' },
                'kube-scheduler': { 'count': '0/0', 'status': 'N/A' },
                'etcd': { 'count': '0/0', 'status': 'N/A' }
            }
        }
    );


    if(!cluster || !platform)
        return null;


    return (
        <>
            <div className="layout__header">
                <h1 className="layout__title">{cluster?.name} <span>| {t('common.summary' )}</span></h1>

                <div className={css.buttons}>
                    <Upgrade />
                    <Redeploy />
                </div>
            </div>

            <div className="row gap15">
                <div className="c2of3">
                    <ClusterInfo />
                </div>
                <div className="c1of3 stack-papers">
                    <div className="paper">
                        <h4 className="paper__header">{t('clusters.clusterInfo' )} <Edit /></h4>

                        <div className={classNames('row', 'gap1', 'align-center', css.info)}>
                            <div className={classNames('c3of5', css.infoLabel)}>{t('common.deployStatus' )}</div>
                            <div className="c2of5">
                                <StatusIcon status={cluster?.state as string} action={cluster?.action} />
                            </div>
                        </div>
                        <div className={classNames('row', 'gap1', 'align-center', css.info)}>
                            <div className={classNames('c3of5', css.infoLabel)}>{t('common.status' )}</div>
                            <div className="c2of5">
                                <StatusIcon status={cluster?.is_online ? 'Online' : 'Offline'} />
                            </div>
                        </div>
                        <div className={classNames('row', 'gap1', 'align-center', css.info)}>
                            <div className={classNames('c3of5', css.infoLabel)}>{t('platforms.platform', {context: 'one'} )}</div>
                            <div className="c2of5"><b>{cluster?.platform.name}</b></div>
                        </div>
                        <div className={classNames('row', 'gap1', 'align-center', css.info)}>
                            <div className={classNames('c3of5', css.infoLabel)}>{t('common.kubVer' )}</div>
                            <div className="c2of5">
                                <b>{cluster?.kube_version}</b>
                            </div>
                        </div>
                        <div className={classNames('row', 'gap1', 'align-center', css.info)}>
                            <div className={classNames('c3of5', css.infoLabel)}>{t('common.description' )}</div>
                            <div className="c2of5"><p>{brakeWord(cluster?.description, 135)}</p></div>
                        </div>
                    </div>

                    <div className="paper">
                        <h4 className="paper__header">{t('common.compStatus' )}</h4>

                        <div className={classNames('row', 'gap1', 'align-center', css.info)}>
                            <div className={classNames('c5of7', css.infoLabel)}>API server</div>
                            {
                                pods && pods['kube-apiserver'] &&
                                <>
                                    <div className={classNames('c1of7', css.infoLabel)}>{pods['kube-apiserver'].count}</div>
                                    <div className={'c1of7'}><StatusIcon status={pods['kube-apiserver'].status} caption={false} /></div>
                                </>
                            }
                        </div>
                        <div className={classNames('row', 'gap1', 'align-center', css.info)}>
                            <div className={classNames('c5of7', css.infoLabel)}>Controller Manager </div>
                            {
                                pods && pods['kube-controller'] &&
                                <>
                                    <div className={classNames('c1of7', css.infoLabel)}>{pods['kube-controller'].count}</div>
                                    <div className={'c1of7'}><StatusIcon status={pods['kube-controller'].status} caption={false} /></div>
                                </>
                            }
                        </div>
                        <div className={classNames('row', 'gap1', 'align-center', css.info)}>
                            <div className={classNames('c5of7', css.infoLabel)}>Scheduler</div>
                            {
                                pods && pods['kube-scheduler'] &&
                                <>
                                    <div className={classNames('c1of7', css.infoLabel)}>{pods['kube-scheduler'].count}</div>
                                    <div className={'c1of7'}><StatusIcon status={pods['kube-scheduler'].status} caption={false} /></div>
                                </>
                            }
                        </div>
                        <div className={classNames('row', 'gap1', 'align-center', css.info)}>
                            <div className={classNames('c5of7', css.infoLabel)}>Etcd</div>
                            {
                                pods && pods['etcd'] &&
                                <>
                                    <div className={classNames('c1of7', css.infoLabel)}>{pods['etcd'].count}</div>
                                    <div className={'c1of7'}><StatusIcon status={pods['etcd'].status} caption={false} /></div>
                                </>
                            }
                        </div>

                    </div>
                    <div className="paper">
                        <h4 className="paper__header">{t('common.tools' )}</h4>
                        <div className={css.toolsButtons}>
                            <KubeCtl />
                            <KubeConfig />
                            <InstallTools />
                            <Dashboard />
                            <Delete />
                            <ForceDelete />
                        </div>
                    </div>
                </div>
            </div>
        </>
    );
};
