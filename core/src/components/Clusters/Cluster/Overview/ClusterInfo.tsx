import React, { useContext, useEffect, useState } from 'react';
import { useQuery } from '@tanstack/react-query';
import { useTranslation } from 'react-i18next';

import type { IError } from '@msa/kit/types';
import { Circle, Loader } from '@msa/kit';

import settings from 'settings';
import type { IClusterInfo, IClusterNodeMetrics } from 'types';
import { getClusterMetrics } from 'actions';
import { ClusterContext } from 'components/Clusters/Cluster';
import { PlatformContext } from 'components/Platforms/Platform';


export const ClusterInfo = () => {
    const { platform } = useContext(PlatformContext);
    const { cluster, disabled, info, loadingData } = useContext(ClusterContext);
    const { t } = useTranslation();

    const [nodeMetrics, setNodeMetrics] = useState<IClusterInfo['allocated']>({
        memory: 0,
        cpu: 0,
        pods: 0,
        hugepages: 0,
    });

    const { isLoading: metricsLoading, data: metrics } = useQuery<IClusterNodeMetrics[], IError>(
        ['platform', platform?.name, 'cluster', cluster?.name, 'metrics'],
        () => getClusterMetrics(platform?.name, cluster?.name),
        {
            refetchInterval: settings.DELAY.CLUSTER_INFO,
            enabled: !disabled
        }
    );

    useEffect(() => {
        if(metrics) {
            const cMetrics = {
                memory: 0,
                cpu: 0,
                pods: 0,
                hugepages: 0,
            };

            metrics.map((nodeInfo: IClusterNodeMetrics) => {
                cMetrics.pods++;
                cMetrics.memory += nodeInfo.memory;
                cMetrics.cpu += nodeInfo.cpu;
            });

            setNodeMetrics(cMetrics);
        }
    }, [setNodeMetrics, metrics]);


    return (
        <div className="stack-papers">
            <div className="paper">
                <h4 className="paper__header">{t('clusters.info' )}</h4>
                <div className="row " style={{ justifyContent: 'space-evenly' }}>
                    {
                        loadingData ?
                            <Loader size="small" /> :
                            info &&
                            <>
                                <Circle caption="PODS" displayPercentage={true} progress={info.allocated.pods} capacity={info.capacity.pods} size="xsmall" colors={false} />
                                <Circle caption="CPU" displayPercentage={true} progress={info.allocated.cpu} capacity={info.capacity.cpu} size="xsmall" colors={false} />
                                <Circle caption="RAM" displayPercentage={true} progress={info.allocated.memory} capacity={info.capacity.memory} size="xsmall" capacityDesc="Mb" colors={false} />
                            </>
                    }
                </div>
            </div>
            <div className="paper">
                <h4 className="paper__header">{t('clusters.statistics' )}</h4>
                <div className="row" style={{ justifyContent: 'space-evenly' }}>
                    {
                        metricsLoading || loadingData ?
                            <Loader size="small" /> :
                            metrics && info &&
                            <>
                                <Circle caption="CPU" progress={Number(nodeMetrics.cpu.toFixed(2))} capacity={info.capacity.cpu} />
                                <Circle caption="RAM" progress={nodeMetrics.memory} capacity={info.capacity.memory} />
                            </>
                    }
                </div>
            </div>
        </div>
    );
};
