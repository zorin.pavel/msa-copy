import React, { useCallback, useContext, useState } from 'react';
import { Controller, useForm } from 'react-hook-form';
import classNames from 'classnames';
import { useQueryClient } from '@tanstack/react-query';
import { useTranslation } from 'react-i18next';

import { DrawerFooterJustify, IOption } from '@msa/kit/types';
import { Button, Dialog, Drawer, Select, Toast } from '@msa/kit';

import { getUpgradeVersions, upgradeCluster } from 'actions';
import { ClusterContext } from 'components/Clusters/Cluster';
import { PlatformContext } from 'components/Platforms/Platform';

import css from './overview.module.scss';


export const Upgrade = () => {
    const { cluster, disabled, can } = useContext(ClusterContext);
    const { platform } = useContext(PlatformContext);
    const queryClient = useQueryClient();
    const { t } = useTranslation();

    const {
        handleSubmit,
        reset,
        control,
        getValues,
        formState: { errors, isValid },
    } = useForm({
        mode: 'onChange',
        reValidateMode: 'onChange',
        defaultValues: { kube_version: '' }
    });

    const [submitted, setSubmitted] = useState<boolean>(false);
    const [drawer, setDrawer] = useState<boolean>(false);
    const [alert, setAlert] = useState<boolean>(false);
    const [kubernetesOptions, setOptions] = useState<IOption[]>([]);
    const [upgradeDialog, setUpgradeDialog] = useState<boolean>(false);


    const canUpgrade = useCallback(() => {
        return getUpgradeVersions(platform?.name, cluster?.name)
            .then(kubernetesVersions => {
                let enabled = true;

                setOptions(Object.keys(kubernetesVersions)
                    .map((ver: string) => {
                        const option = { label: `${ver}`, value: ver, disabled: !kubernetesVersions[ver] || !enabled };

                        if(kubernetesVersions[ver] && enabled)
                            enabled = false;

                        return option;
                    })
                    .reverse());

                for(const version of Object.keys(kubernetesVersions)) {
                    if(kubernetesVersions[version])
                        return true;
                }

                return false;
            })
            .catch(() => false);
    }, [cluster]);


    const onSubmit = (formData: { kube_version: string }) => {
        setSubmitted(true);

        return upgradeCluster(platform?.name, cluster?.name, { ...formData })
            .then(() => {
                Toast.success({ message: t('toasts.update', { entity: t('clusters.cluster'), entityName: cluster?.name }) });
                queryClient.invalidateQueries(['platform', platform?.name, 'cluster']);
            })
            .catch(error => {
                Toast.error({ message: error.message });

                return false;
            })
            .finally(() => {
                setSubmitted(false);
                setUpgradeDialog(false);
                setDrawer(false);
            });
    };


    const onReset = () => {
        reset({ kube_version: '' });
    };


    const onClick = useCallback(() => {
        canUpgrade()
            .then((can) => {
                can ?
                    setDrawer(true) :
                    setAlert(true);
            });
    }, [canUpgrade]);


    return (
        <>
            <Button
                view="outlined"
                disabled={disabled || !can?.edit}
                onClick={onClick}>{t('buttons.upgrade' )}</Button>

            {
                drawer&&
                <Drawer header={t('buttons.upgrade' ) + " " + cluster?.name} open={drawer} onClose={() => {
                    setDrawer(false);
                    onReset();
                }} backdrop>
                    <form noValidate onReset={onReset}>
                        <Drawer.Body>
                            <div className={classNames('row', 'gap1', css.info)}>
                                <div className={classNames('c2of5', css.infoLabel)}>{t('common.kubVer' )}</div>
                                <div className="c3of5">
                                    <b>{cluster?.kube_version}</b>
                                </div>
                            </div>

                            <div className="section__divider"/>

                            <Controller
                                control={control}
                                name="kube_version"
                                rules={
                                    {
                                        required: t('validation.isRequired', { entityName: t('common.kubVer' )}),
                                    }
                                }
                                render={({ field }) => (
                                    <Select
                                        {...field}
                                        label={t('common.updateAvailable' )}
                                        placeholder={cluster?.kube_version}
                                        labelPosition="left"
                                        required
                                        error={!!errors.kube_version}
                                        options={kubernetesOptions} />
                                )} />
                            <p><i>{t('common.updateNote' )}</i></p>
                        </Drawer.Body>
                        <Drawer.Footer justify={DrawerFooterJustify.end}>
                            <Button disabled={!isValid} loading={submitted} onClick={() => setUpgradeDialog(true)} color="red">{t('buttons.upgrade' )}</Button>
                        </Drawer.Footer>
                    </form>
                </Drawer>
            }

            {
                upgradeDialog &&
                <Dialog
                    open={upgradeDialog}
                    header={t('buttons.upgrade' ) + ' ' + t('clusters.count', {context: "one"} )}
                    buttons={[
                        <Button key="cancel" onClick={() => {
                            setUpgradeDialog(false);
                            setDrawer(false);
                        }} view="outlined">{t('buttons.cancel')}</Button>,
                        <Button key="upgrade" onClick={handleSubmit(onSubmit)}>{t('buttons.upgrade' )}</Button>
                    ]}
                    onClose={() => setUpgradeDialog(false)}>
                    {t('clusters.upgrade', {entityName: cluster?.name, oldVer: cluster?.kube_version, newVer: getValues('kube_version')})}?
                </Dialog>
            }

            {
                alert &&
                <Dialog
                    open={alert}
                    header={t('buttons.upgrade' ) + ' ' + t('clusters.count', {context: "one"} )}
                    buttons={[
                        <Button key="ok" onClick={() => {
                            setAlert(false);
                            setDrawer(false);
                        }} view="outlined">Ok</Button>,
                    ]}
                    onClose={() => setAlert(false)}>
                    {t('clusters.lastVer', {newVer: cluster?.kube_version})}
                </Dialog>
            }
        </>
    );
};
