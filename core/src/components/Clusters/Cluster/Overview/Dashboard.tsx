import React, { useContext, useState } from 'react';
import { useQueryClient } from '@tanstack/react-query';
import { Controller, useForm } from 'react-hook-form';

import { IError, DrawerFooterJustify } from '@msa/kit/types';
import { Button, Drawer, IconExternalLink, Input, Toast } from '@msa/kit';

import { env } from 'env';
import settings from 'settings';
import { installClusterDashboard } from 'actions';
import { PlatformContext } from 'components/Platforms/Platform';
import { ClusterContext } from 'components/Clusters/Cluster';


export const Dashboard = () => {
    const queryClient = useQueryClient();
    const { platform } = useContext(PlatformContext);
    const { cluster, can } = useContext(ClusterContext);

    const defaultValues = { url: '', oauth2domain: '' };
    const {
        handleSubmit,
        reset,
        control,
        formState: { errors, isValid },
    } = useForm({
        mode: 'onChange',
        reValidateMode: 'onChange',
        defaultValues
    });

    const [submitted, setSubmitted] = useState<boolean>(false);
    const [drawer, setDrawer] = useState<boolean>(false);


    const onSubmit = (formData: typeof defaultValues) => {
        setSubmitted(true);

        formData.oauth2domain = env.MSA_BASE_DOMAIN;
        formData.url = formData.url.replace(/^https?:\/\//, '');

        return installClusterDashboard(platform?.name, cluster?.name, formData)
            .then(() => {
                setDrawer(false);
                onReset();
                Toast.success({ message: `Dushboard for <b>${cluster?.name}</b> cluster was successfully added` });
                queryClient.invalidateQueries(['platform', platform?.name, 'cluster', cluster?.name]);
            })
            .catch((error: IError) => {
                Toast.error({ message: error.message });

                return false;
            })
            .finally(() => setSubmitted(false));
    };


    const onReset = () => {
        reset(defaultValues);
    };


    if(!platform || !cluster)
        return null;


    return (
        <>
            {
                cluster.dashboardURL &&
                    <Button
                        view="outlined"
                        size="medium"
                        loading={submitted}
                        iconRight={<IconExternalLink />}
                        disabled={!can?.get}
                        onClick={() => window.open(`https://${env.MSA_BASE_DOMAIN}/platforms/${platform.name}/clusters/${cluster.name}/dashboard/`)}
                    >Dashboard</Button>
            }

            {
                drawer &&
                <Drawer header={'Add dashboard for ' + cluster.name} open={drawer} onClose={() => {
                    setDrawer(false);
                    onReset();
                }} backdrop>
                    <form onSubmit={handleSubmit(onSubmit)} noValidate onReset={onReset}>
                        <Drawer.Body>
                            <Controller
                                control={control}
                                name="url"
                                rules={
                                    {
                                        required: 'URL is required',
                                        pattern: {
                                            value: settings.DOMAIN_REGEX,
                                            message: 'URL is not valid'
                                        }
                                    }
                                }
                                render={({ field }) => (
                                    <Input
                                        {...field}
                                        label="Dashboard url"
                                        required
                                        labelPosition="left"
                                        error={errors.url && errors.url.message as string}
                                        displayError
                                        autoComplete="off"
                                        addonLeft={{ label: 'https://' }} />
                                )} />
                            <p><i>This URL should be directed via dns to all workers node of this cluster.</i></p>
                        </Drawer.Body>
                        <Drawer.Footer justify={DrawerFooterJustify.end}>
                            <Button disabled={!isValid} loading={submitted} type="submit" color="red">Add</Button>
                        </Drawer.Footer>
                    </form>
                </Drawer>
            }
        </>
    );
};
