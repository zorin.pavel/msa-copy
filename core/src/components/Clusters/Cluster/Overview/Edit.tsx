import React, { ChangeEvent, useCallback, useContext, useState } from 'react';
import { Controller, useForm } from 'react-hook-form';
import { useQueryClient } from '@tanstack/react-query';
import { useNavigate } from 'react-router';
import { useTranslation } from 'react-i18next';

import { Button, Drawer, IconEdit, Input, Textarea, Toast } from '@msa/kit';
import { IError } from '@msa/kit/types';

import settings from 'settings';
import type { ICluster } from 'types';
import { putCluster } from 'actions';
import { ClusterContext } from 'components/Clusters/Cluster';
import { PlatformContext } from 'components/Platforms/Platform';


export const Edit = () => {
    const navigate = useNavigate();
    const queryClient = useQueryClient();
    const { platform } = useContext(PlatformContext);
    const { cluster, disabled, can } = useContext(ClusterContext);
    const { t } = useTranslation();

    const defaultValues = {
        name: cluster?.name,
        description: cluster?.description,
    };
    const {
        handleSubmit,
        reset,
        control,
        formState: { errors, isValid },
    } = useForm({
        mode: 'onChange',
        reValidateMode: 'onChange',
        defaultValues
    });

    const [submitted, setSubmitted] = useState<boolean>(false);
    const [drawer, setDrawer] = useState<boolean>(false);
    

    const onSubmit = useCallback((formData: typeof defaultValues) => {
        setSubmitted(true);

        return putCluster(platform?.name, cluster?.name, formData)
            .then((response: ICluster) => {
                setDrawer(false);
                Toast.success({ message: t('toasts.edit', { entity: t('clusters.count', {context: 'few'}), entityName: response.name }) });
                queryClient.setQueryData(['platform', platform?.name, 'cluster', cluster?.name], response);
                queryClient.invalidateQueries(['platform', platform?.name, 'cluster', cluster?.name]);
                navigate(`../${formData.name}`);
            })
            .catch((error: IError) => {
                Toast.error({ message: error.message });

                return false;
            })
            .finally(() => setSubmitted(false));
    }, [cluster, putCluster]);


    const onReset = () => {
        reset(defaultValues);
    };


    return (
        <>
            <IconEdit
                disabled={disabled || !can?.edit}
                onClick={() => setDrawer(true)} />
            {
                drawer &&
                <Drawer header={t('clusters.edit' )} open={drawer} onClose={() => {
                    setDrawer(false);
                    onReset();
                }} backdrop>
                    <Drawer.Body>
                        <form onSubmit={handleSubmit(onSubmit)} noValidate onReset={onReset}>
                            <Controller
                                control={control}
                                name="name"
                                rules={
                                    {
                                        required: t('validation.isRequired', { entityName: t('clusters.name' )}),
                                        pattern: {
                                            value: settings.NAME_REGEX,
                                            message: t('validation.alphanumericCharacters', { entityName: t('clusters.name' )})
                                        }
                                    }
                                }
                                render={({ field: { name, value, onChange } }) => (
                                    <Input
                                        name={name}
                                        value={value}
                                        label={t('clusters.name' )}
                                        required
                                        labelPosition="left"
                                        error={errors.name && errors.name.message as string}
                                        displayError
                                        onChange={(e: ChangeEvent<HTMLInputElement>) => onChange(e.target.value)}
                                        autoComplete="off" />
                                )} />
                            <Controller
                                control={control}
                                name="description"
                                render={({ field }) => (
                                    <Textarea
                                        {...field}
                                        label={t('common.description' )}
                                        placeholder={t('common.description' )}
                                        labelPosition="left"
                                        rows={5}
                                        error={!!errors.description} />
                                )} />
                        </form>
                    </Drawer.Body>
                    <Drawer.Footer>
                        <Button type="reset" view="outlined" color="red" onClick={onReset}>{t('buttons.clear')}</Button>
                        <Button disabled={!isValid} loading={submitted} type="submit" onClick={handleSubmit(onSubmit)}>{t('buttons.done')}</Button>
                    </Drawer.Footer>
                </Drawer>
            }
        </>
    );
};
