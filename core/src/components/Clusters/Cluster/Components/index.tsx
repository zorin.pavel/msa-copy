import React, { useContext } from 'react';

import { Table } from '@msa/kit';
import { ClusterContext } from 'components/Clusters/Cluster';

// import css from './components.module.scss';


interface IProps {
}

export const Components = (props: IProps) => {
    const {} = props;
    const { cluster } = useContext(ClusterContext);

    if(!cluster)
        return null;

    return (
        <>
            <h1 className="layout__title">{cluster?.name} <span>| Components</span></h1>

            <Table cells={12}>
                <Table.Header>
                    <Table.Cell colspan={3}>Component</Table.Cell>
                    <Table.Cell colspan={4}>Description</Table.Cell>
                    <Table.Cell colspan={2}>Status</Table.Cell>
                    <Table.Cell colspan={2} />
                    <Table.Cell actions={1} />
                </Table.Header>
                {/*{accordionDataComponents.map(({ title, items }, i) => (*/}
                {/*    <Accordion key={i} title={title} items={items}>*/}
                {/*        {items.map((item, i) =>*/}
                {/*            <Row key={i} item={item} />*/}
                {/*        )}*/}
                {/*    </Accordion>*/}
                {/*))}*/}
            </Table>

        
        </>
    );
};
