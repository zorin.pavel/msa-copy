import React, { useState } from 'react';

import {
    Button,
    Dialog,
    IconDelete,
    Table,
    Toast,
    AccordionSectionItems,
    StatusIcon
} from '@msa/kit';
// import { Edit } from '../Edit';

import css from '../components.module.scss';

interface IProps {
    key: string | number;
    item: AccordionSectionItems;
}

export const Row = (props: IProps) => {
    const { name, description, status } = props.item;
    // const queryClient = useQueryClient();

    // let { data: dataRoles } = useQuery<IRole[], IError>('roles', getRoles);

    const [deleteDialog, setDeleteDialog] = useState<boolean>(false);
    const [inProgress, setInProgress] = useState<boolean>(false);
    // const [rolesOptions, setRolesOptions] = useState<IOption[]>([]);


    // useEffect(() => {
    //     if(dataRoles) {
    //         setRolesOptions(dataRoles.map((role: IRole) => ({ label: role.name, value: role.id })));
    //     }
    // }, [dataRoles]);


    // const handleChange = useCallback((event: ChangeEvent<HTMLInputElement>) => {
    //     putUser({ ...user, role: event.target.value })
    //         .then((response: IUserInfo) => {
    //             Toast.success({ message: `<b>${response.username}</b> was successfully updated` });
    //             queryClient.invalidateQueries('users');
    //         })
    //         .catch((error: IError) => {
    //             Toast.error({ message: error.message });
    //         });
    // }, [user]);


    const onDelete = () => {
        Toast.error({ message: 'Error occurred while deleting' });
    };


    return (
        <>
            <Table.Row valign="center" align="center" className="entity__row">
                <Table.Cell colspan={3}>{name}</Table.Cell>
                <Table.Cell colspan={4}>{description}</Table.Cell>
                <Table.Cell colspan={2}>
                    {
                        inProgress ?
                            <StatusIcon status="In progress" /> :
                            <div className={css.status}>
                                {
                                    status === 'installed' ?
                                        <StatusIcon status="Installed" caption={false} /> :
                                        <span className={css.status}>Ready to install</span>
                                }
                            </div>
                    }
                </Table.Cell>
                <Table.Cell colspan={2}>
                    {
                        status === 'installed' ?
                            <Button color="dark" view="outlined" disabled={inProgress} className={css.btn} onClick={() => setInProgress(true)}>Update</Button> :
                            <Button color="dark" disabled={inProgress} onClick={() => setInProgress(true)}>Install</Button>
                    }
                </Table.Cell>
                <Table.Cell actions={1}>
                    {
                        status === 'installed' &&
                        <IconDelete onClick={() => setDeleteDialog(true)} />
                    }
                </Table.Cell>
            </Table.Row>

            {
                deleteDialog &&
                <Dialog open={deleteDialog} header="Delete user" buttons={[
                    <Button key="cancel" onClick={() => setDeleteDialog(false)} view="outlined">Cancel</Button>,
                    <Button key="delete" onClick={onDelete}>Delete</Button>
                ]} onClose={() => setDeleteDialog(false)}>
                    Do you really want delete component <b>{name}</b>?
                </Dialog>
            }
        </>
    );
};
