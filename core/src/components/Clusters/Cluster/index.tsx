import React, { createContext, useContext, useEffect, useMemo, useState } from 'react';
import { Outlet, useParams } from 'react-router-dom';
import { useQuery, useQueryClient } from '@tanstack/react-query';
import { useKeycloak } from '@react-keycloak/web';

import { IError, IRbacGroupObject } from '@msa/kit/types';
import { Error, Loader } from '@msa/kit';
import { useSocket } from '@msa/host';

import { env } from 'env';
import settings from 'settings';
import type { ICluster, IClusterInfo } from 'types';
import { getClusterInfo, loadCluster, loadClusterPermissions } from 'actions';
import { PlatformContext } from 'components/Platforms/Platform';


interface ContextProps {
    cluster?: ICluster,
    disabled?: boolean,
    info?: IClusterInfo,
    loadingData?: boolean,
    can?: Record<string, boolean | undefined>
}

export const ClusterContext = createContext<ContextProps>({});

export const Cluster = () => {
    const { clusterSlug } = useParams();
    const { keycloak } = useKeycloak();
    const queryClient = useQueryClient();
    const { platform } = useContext(PlatformContext);

    useSocket(
        { tenant: env.MSA_DEFAULT_TENANT, platform: platform?.name, object_type: 'cluster', name: clusterSlug, type: 'status' },
        {
            callback: () => queryClient.invalidateQueries(['platform', platform?.name, 'cluster', clusterSlug]),
            toast: true
        }
    );

    const [disabled, setDisabled] = useState<boolean>(true);
    const [loadingData, setLoadingData] = useState<boolean>(true);

    const { isLoading, data: cluster, error } = useQuery<ICluster, IError>(
        ['platform', platform?.name, 'cluster', clusterSlug],
        () => loadCluster(platform?.name, clusterSlug)
    );

    const { isLoading: infoLoading, data: info } = useQuery<IClusterInfo, IError>(
        ['platform', platform?.name, 'cluster', clusterSlug, 'info'],
        () => getClusterInfo(platform?.name, clusterSlug),
        {
            refetchInterval: settings.DELAY.CLUSTER_INFO,
            enabled: !!platform && !!cluster && !disabled
        }
    );
    const { data: clusterPermissions } = useQuery<IRbacGroupObject[], IError>(
        ['platform', platform?.name, 'cluster', cluster?.name, 'permissions'],
        () => loadClusterPermissions(platform?.name, cluster?.name),
        {
            enabled: !!platform && !!cluster
        }
    );

    const groupPermissions = useMemo(() => {
        if(!clusterPermissions)
            return [];

        return clusterPermissions
            .filter(permission => !!permission['cluster'])
            .filter(permission => keycloak.tokenParsed?.groups.includes(permission.group))
            .map(gp => gp.scopes)
            .flat();
    }, [clusterPermissions, keycloak]);

    const can = useMemo(() => {
        if(!groupPermissions.length)
            return {};

        return {
            list: groupPermissions?.includes('list'),
            get: groupPermissions?.includes('get'),
            create: groupPermissions?.includes('create'),
            edit: groupPermissions?.includes('edit'),
            delete: groupPermissions?.includes('delete'),
        }
    }, [groupPermissions]);


    useEffect(() => {
        if(cluster)
            setDisabled((!cluster.is_online || !settings.STATE.SUCCESS.includes(cluster.state)));
    }, [cluster]);


    useEffect(() => {
        setLoadingData(infoLoading);
    }, [infoLoading]);


    if(isLoading)
        return <Loader layout />;

    if(error)
        return <Error {...error} layout />;


    return (
        <ClusterContext.Provider value={{
            cluster,
            disabled,
            info,
            loadingData,
            can
        }}>
            <Outlet />
        </ClusterContext.Provider>
    );
};
