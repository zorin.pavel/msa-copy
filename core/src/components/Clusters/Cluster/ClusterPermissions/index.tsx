import React, { useContext } from 'react';
import { useQuery } from '@tanstack/react-query';
import { useTranslation } from 'react-i18next';


import { Badge, Button, IconEdit, Table } from '@msa/kit';
import { Colors, IError, IRbacGroupObject } from '@msa/kit/types';

import { loadClusterPermissions } from 'actions';
import { badgeColor } from 'components/Iam';
import { PlatformContext } from 'components/Platforms/Platform';
import { ClusterContext } from 'components/Clusters/Cluster';
import { EditPermissions } from 'components/EditPermissions';


export const ClusterPermissions = () => {
    const { platform } = useContext(PlatformContext);
    const { cluster, can } = useContext(ClusterContext);
    const { t } = useTranslation()

    const { data: clusterPermissions } = useQuery<IRbacGroupObject[], IError>(
        ['platform', platform?.name, 'cluster', cluster?.name, 'permissions'],
        () => loadClusterPermissions(platform?.name, cluster?.name),
        {
            enabled: !!platform && !!cluster
        }
    );


    return (
        <>
            <div className="layout__header">
                <h1 className="layout__title">{platform?.name} <span>| {t('navigation.permissions')}</span></h1>
                <EditPermissions
                    button={<Button disabled={!can?.edit} iconLeft={<IconEdit />} color="dark">{t('common.edit', { entity: t('permissions.permission') })}</Button>}
                    platformName={platform?.name}
                    clusterName={cluster?.name}
                    permissions={clusterPermissions}/>
            </div>

            <Table cells={3}>
                <Table.Header>
                    <Table.Cell>{t('common.group')}</Table.Cell>
                    <Table.Cell>{t('common.objectName')}</Table.Cell>
                    <Table.Cell>{t('common.scope')}</Table.Cell>
                </Table.Header>

                {
                    clusterPermissions &&
                    clusterPermissions.map((object: IRbacGroupObject) => (
                        <Table.Row valign="center" key={`${object.platform}${object.cluster}${object.group}`} className="entity__row">
                            <Table.Cell>{object.group}</Table.Cell>
                            <Table.Cell>{object.platform}{object.cluster ? ` / ${object.cluster}` : ''}</Table.Cell>
                            <Table.Cell>
                                {
                                    object.scopes.map((scope: string) => <Badge key={scope} label={t(`scopes.${scope}`)} size="small" color={badgeColor[scope] as keyof typeof Colors}/>)
                                }
                            </Table.Cell>
                        </Table.Row>
                    ))
                }
            </Table>
        </>
    );
};
