import React, { useContext } from 'react';
import { Outlet, useParams } from 'react-router-dom';
import { useQuery } from '@tanstack/react-query';

import { IError, ITab } from '@msa/kit/types';
import { Error, Loader, Tab, TabBody, TabList, Tabs, useTabs } from '@msa/kit';

import { IProject } from 'types';
import { loadClusterProject } from 'actions';
import { PlatformContext } from 'components/Platforms/Platform';
import { ClusterContext } from 'components/Clusters/Cluster';


const tabs: ITab[] = [
    {
        index: 'summary',
        header: 'Summary',
        router: '.'
    },
    {
        index: 'cicd',
        header: 'CI/CD',
        router: 'cicd'
    }
];

export const Project = () => {
    const { projectSlug } = useParams();
    const { platform } = useContext(PlatformContext);
    const { cluster } = useContext(ClusterContext);
    const { selectedTab, onTabChange } = useTabs(tabs, 'Users', { useRouter: true });

    const { isLoading, data: project, error } = useQuery<IProject, IError>(
        ['platform', platform?.name, 'cluster', cluster?.name, 'project', projectSlug],
        () => loadClusterProject(platform?.name, cluster?.name, projectSlug)
    );


    if(isLoading)
        return <Loader />;

    if(error)
        return <Error {...error} />;


    return (
        <>
            <div className="layout__header">
                <h1 className="layout__title">{project?.name} <span>| Summary</span></h1>
            </div>

            <Tabs>
                <TabList>
                    {
                        tabs.map((tab: ITab) => (
                            <Tab key={tab.index} value={selectedTab} index={tab.index} onClick={() => onTabChange(tab)}>
                                {tab.header}
                            </Tab>
                        ))
                    }
                </TabList>
                {
                    tabs.map((tab: ITab) => (
                        <TabBody key={tab.index} value={selectedTab} index={tab.index}>
                            {
                                // tab.component &&
                                // React.createElement(tab.component)
                            }
                            <Outlet />
                        </TabBody>
                    ))
                }
            </Tabs>
        </>
    );
};
