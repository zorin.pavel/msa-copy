import React, { useCallback, useContext, useState } from 'react';
import { useQueryClient } from '@tanstack/react-query';
import { useNavigate } from 'react-router-dom';

import {
    Button,
    Dialog, IconDelete,
    StatusIcon,
    Table, Toast
} from '@msa/kit';

import { IProject } from 'types';
import { deleteClusterProject } from 'actions';
import { PlatformContext } from 'components/Platforms/Platform';
import { ClusterContext } from 'components/Clusters/Cluster';
import { EditWizard } from './EditWizard';


interface IProps {
    project: IProject;
}

export const ProjectRow = React.memo((props: IProps) => {
    const { project } = props;
    const queryClient = useQueryClient();
    const navigate = useNavigate();
    const { platform } = useContext(PlatformContext);
    const { cluster } = useContext(ClusterContext);

    const [deleteDialog, setDeleteDialog] = useState<boolean>(false);
    const [submitted, setSubmitted] = useState<boolean>(false);


    const onDelete = useCallback(() => {
        setSubmitted(true);

        return deleteClusterProject(platform?.name, cluster?.name, project.name)
            .then(() => {
                Toast.success({ message: `Node <b>${project.name}</b> remove in progress` });
                queryClient.invalidateQueries(['platform', platform?.name, 'cluster', cluster?.name, 'project']);
            })
            .catch((error) => {
                Toast.error({ message: error.message });

                return false;
            })
            .finally(() => setSubmitted(false));
    }, [project]);


    return (
        <>
            <Table.Row  onClick={() => navigate(`${project.name}`)} className="entity__row entity__row--hover" valign="center">
                <Table.Cell colspan={2}>{project.name}</Table.Cell>
                <Table.Cell><StatusIcon status={project.status} /></Table.Cell>
                <Table.Cell actions={2}>
                    <EditWizard project={project} />
                    <IconDelete onClick={() => setDeleteDialog(true)} />
                </Table.Cell>
            </Table.Row>

            {
                deleteDialog &&
                <Dialog open={deleteDialog} header="Delete project" buttons={[
                    <Button key="cancel" onClick={() => setDeleteDialog(false)} view="outlined">Cancel</Button>,
                    <Button key="delete" onClick={() => onDelete()} loading={submitted}>Delete</Button>
                ]} onClose={() => setDeleteDialog(false)}>
                    Do you really want delete project <b>{project.name}</b>?
                </Dialog>
            }
        </>
    );
});
