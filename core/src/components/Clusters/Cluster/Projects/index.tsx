import React, { useCallback, useContext, useEffect, useState } from 'react';
import { useQuery } from '@tanstack/react-query';
import { Outlet, useMatch } from 'react-router-dom';

import { IError, OrderDirection } from '@msa/kit/types';
import {
    Error,
    Loader, sort,
    Table,
    useTable
} from '@msa/kit';

import type { IProject } from 'types';
import { getClusterProjects } from 'actions';
import { PlatformContext } from 'components/Platforms/Platform';
import { ClusterContext } from 'components/Clusters/Cluster';
import { ProjectRow } from 'components/Clusters/Cluster/Projects/ProjectRow';
import { EditWizard } from './EditWizard';



export const Projects = () => {
    const match = useMatch('/platforms/:platformSlug/clusters/:clusterSlug/projects/:children/*');
    const { cluster } = useContext(ClusterContext);
    const { platform } = useContext(PlatformContext);
    const { orderedBy, setOrderedBy } = useTable({ 'name': OrderDirection.asc });

    const { isLoading, data, error } = useQuery<IProject[], IError>(
        ['platform', platform?.name, 'cluster', cluster?.name, 'project'],
        () => getClusterProjects(platform?.name, cluster?.name)
    );
    const [projects, setProjects] = useState<IProject[]>(data ?? []);

    useEffect(() => {
        if(data)
            setProjects(sort<IProject[]>(data, 'name', OrderDirection.asc));
    }, [data]);


    const orderBy = useCallback((fieldName: string, orderDirection: OrderDirection) => {
        if(setOrderedBy)
            setOrderedBy({ [fieldName]: orderDirection });

        setProjects(sort(data ?? [], fieldName, orderDirection));
    }, [data]);


    if(match)
        return <Outlet />

    if(isLoading)
        return <Loader layout />;

    if(error)
        return <Error {...error} layout />;


    return (
        <>
            <div className="layout__header">
                <h1 className="layout__title">{cluster?.name} <span>| Projects</span></h1>
                <EditWizard />
            </div>

            <Table cells={4}>
                <Table.Header >
                    <Table.Cell colspan={2}>Name <Table.ActionSort fieldName="name" orderedBy={orderedBy} orderBy={orderBy} /></Table.Cell>
                    <Table.Cell>Status</Table.Cell>
                    <Table.Cell actions={2} />
                </Table.Header>
                {
                    projects &&
                    projects.map((project: IProject) => <ProjectRow project={project} key={project.name} />)
                }
            </Table>
        </>
    );
};
