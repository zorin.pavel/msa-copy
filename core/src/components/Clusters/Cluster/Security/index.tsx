import React, { useContext, useState } from 'react';
import classNames from 'classnames';
import { format } from 'date-fns';

import {
    Button,
    IconDownload,
    IconExternalLink,
    IconLogs,
    IconUpload,
    Table,
    Accordion,
    ProgressBar, Divider, StatusIcon
} from '@msa/kit';

import { ClusterContext } from 'components/Clusters/Cluster';
import { EditSecurity } from './EditSecurity';

import css from './security.module.scss';


interface IProps {
}

export const Security = (props: IProps) => {
    const {} = props;
    const { cluster } = useContext(ClusterContext);

    const [date, setDate] = useState<Date | undefined>();


    const onAnalyze = () => {
        setDate(new Date());
    };

    if(!cluster)
        return null;

    return (
        <>
            <div className="layout__header">
                <h1 className="layout__title">{cluster?.name} <span>| Security</span></h1>
                <Button view="outlined" iconLeft={<IconDownload />} label="Docs" onClick={() => console.log('Docs')} />
            </div>

            <div className="row gap15">
                <div className={classNames('c1of2', 'paper', css.benchmark)}>
                    <div className={css.header}>
                        <h3 className="paper__header">Audit & Kernel Security</h3>
                        <div className={css.analyse}>
                            <ProgressBar onSuccess={onAnalyze} />
                        </div>
                    </div>
                    <div className={css.report}>
                        <span className={css.reportTitle}>Latest report</span>
                        <span className={css.reportDate}>
                            {
                                date ? format(date ?? new Date(), 'dd.MM.yyyy HH:mm') : ''
                            }
                        </span>
                        <Divider />
                        <IconLogs onClick={() => console.log('Report')} disabled={!date} />
                        <IconDownload onClick={() => console.log('Download')} disabled={!date} />
                    </div>
                </div>
                <div className="c1of2 paper">
                    <h3 className="paper__header">Audit & Kernel Security</h3>

                    <Button view="outlined" size="large" color="dark" iconRight={<IconExternalLink />} label="Audit" onClick={() => console.log('Audit')} />
                    <Button view="outlined" size="large" color="dark" iconRight={<IconExternalLink />} label="Seccomp" onClick={() => console.log('Seccomp')} />
                    <Button view="outlined" size="large" color="dark" iconRight={<IconExternalLink />} label="Kernel Security" onClick={() => console.log('Kernel Security')} />
                </div>
            </div>

            <div className={classNames('paper', css.policy)}>
                <div className={css.header}>
                    <h3 className="paper__header">Engine Policy</h3>
                    <div className={css.actions}>
                        <Button color="dark" view="outlined" onClick={() => console.log('Dashboard')} iconRight={<IconExternalLink />} label="Dashboard" />
                        <Divider />
                        <EditSecurity button={<Button color="dark" view="outlined" label="Create custom" />} />
                        <Button color="dark" onClick={() => console.log('Upload')} iconRight={<IconUpload />} label="Upload" />
                    </div>
                </div>

                <Table cells={9}>
                    <Table.Header>
                        <Table.Cell colspan={3}>Policy name</Table.Cell>
                        <Table.Cell colspan={3}>Description</Table.Cell>
                        <Table.Cell>Status</Table.Cell>
                        <Table.Cell>Actions</Table.Cell>
                        <Table.Cell actions={2} />
                    </Table.Header>

                    <Accordion title="Cluster-wide">
                        <Table.Row valign="center" className="entity__row">
                            <Table.Cell colspan={3}>Policy</Table.Cell>
                            <Table.Cell colspan={3}>Some description of the component</Table.Cell>
                            <Table.Cell>
                                <span className={classNames(css.status, css.ready)}>Read to enable</span>
                            </Table.Cell>
                            <Table.Cell>
                                <Button color="dark" label="Enable" />
                            </Table.Cell>
                            <Table.Cell actions={2}>
                                <Divider />
                                <EditSecurity policy={{ name: 'Policy', description: 'Some description of the component' }} />
                            </Table.Cell>
                        </Table.Row>

                        <Table.Row valign="center" className="entity__row">
                            <Table.Cell colspan={3}>Name</Table.Cell>
                            <Table.Cell colspan={3}>Description of the component</Table.Cell>
                            <Table.Cell>
                                <StatusIcon status="Success" />
                            </Table.Cell>
                            <Table.Cell>
                                <Button color="dark" view="outlined" label="Disable" />
                            </Table.Cell>
                            <Table.Cell actions={2}>
                                <Divider />
                                <EditSecurity policy={{ name: 'Name', description: 'Description of the component' }} />
                            </Table.Cell>
                        </Table.Row>
                    </Accordion>
                </Table>
            </div>
        </>
    );
};
