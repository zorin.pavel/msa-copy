import React, { useEffect, useState } from 'react';
import { useFormContext } from 'react-hook-form';

import type { IWizardStepComponentProps } from '@msa/kit/types';
import { IconError, IconFullScreen, IconSuccess, YamlEditor } from '@msa/kit';

import css from './values.module.scss';


interface IProps extends IWizardStepComponentProps {
}

export const Values = (props: IProps) => {
    const { setStepValid, wideDrawer, setWideDrawer } = props;

    const {
        setValue,
        formState: { isValid }
    } = useFormContext();

    const [error, setError] = useState<boolean>(false);

    useEffect(() => {
        setStepValid && setStepValid('values', isValid && !error);
    }, [isValid, error]);


    const onChange = (values: Record<string, any>) => {
        setValue('values', values, { shouldValidate: true, shouldTouch: true });
    };


    return (
        <>
            <div className={css.buttons}>
                {
                    error ?
                        <IconError className={css.iconError} /> :
                        <IconSuccess className={css.iconSuccess} />
                }
                <IconFullScreen className={css.buttonIcon} onClick={() => setWideDrawer && setWideDrawer(!wideDrawer)} />
            </div>

            <YamlEditor
                value={{}}
                onChange={onChange}
                onError={setError} />
        </>
    );
};
