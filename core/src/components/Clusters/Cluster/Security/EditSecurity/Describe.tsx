import React, { useEffect } from 'react';
import { Controller, useFormContext } from 'react-hook-form';

import type { IWizardStepComponentProps } from '@msa/kit/types';
import { Input, Textarea } from '@msa/kit';


interface IProps extends IWizardStepComponentProps {
}

export const Describe = (props: IProps) => {
    const { setStepValid, index } = props;

    const {
        control,
        formState: { errors, isValid }
    } = useFormContext();


    useEffect(() => {
        setStepValid && setStepValid(index, isValid);
    }, [isValid]);


    return (
        <>
            <Controller control={control} name="name" rules={
                {
                    required: 'Name is required',
                }
            } render={({ field }) => (
                <Input
                    {...field}
                    label="Policy name"
                    labelPosition="left"
                    required
                    error={!!errors.name}
                    autoComplete="off" />
            )} />
            <Controller
                control={control}
                name="description"
                render={({ field }) => (
                    <Textarea
                        {...field}
                        label="Description"
                        labelPosition="left"
                        rows={5}
                        error={!!errors.description} />
                )} />
        </>
    );
};
