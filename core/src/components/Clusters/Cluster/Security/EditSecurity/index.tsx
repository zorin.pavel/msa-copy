import React, { useState } from 'react';
import { FormProvider, useForm } from 'react-hook-form';

import { IWizardStep, TStepKey } from '@msa/kit/types';
import {
    Button,
    Drawer,
    Tabs,
    Tab,
    TabList,
    TabBody,
    useWizard,
    IconArrowRight,
    IconArrowLeft,
    Toast,
    IconEdit,
    timer
} from '@msa/kit';

import { Describe } from './Describe';
import { Values } from './Values';


interface IProps {
    policy?: Record<string, any>,
    button?: React.ReactElement,
}


const wizardSteps: IWizardStep[] = [
    {
        index: 'describe',
        header: 'Describe policy',
        component: Describe
    },
    {
        index: 'values',
        header: 'Set values',
        component: Values
    }
];


export const EditSecurity = (props: IProps) => {
    const { policy, button } = props;

    const [drawer, setDrawer] = useState<boolean>(false);
    const [wideDrawer, setWideDrawer] = useState<boolean>(false);
    const [submitted, setSubmitted] = useState<boolean>(false);


    const defaultValues = policy ?? {
        name: '',
        description: ''
    };

    const formMethods = useForm({
        mode: 'onChange',
        reValidateMode: 'onChange',
        defaultValues
    });

    const {
        currentStepKey,
        setCurrentStepKey,
        isStepValid,
        isAllValid,
        setStepValid,
        prevStep,
        nextStep,
        resetStep,
        isLast,
        isFirst,
    } = useWizard(wizardSteps[0].index, wizardSteps);


    const onSubmit = (formData: any) => {
        console.log('formData', formData);
        setSubmitted(true);

        timer<boolean>(true, 1000)
            .then(() => {
                Toast.error({ message: '<b>An error has occurred. Please try later</b>' });
                setSubmitted(false);
            });
    };


    const onReset = () => {
        formMethods.reset(defaultValues);
        resetStep();
    };


    const handleNext = async (index?: TStepKey) => {
        const isStepValid = await formMethods.trigger();

        setStepValid(index ?? currentStepKey, isStepValid);

        return isStepValid;
    };


    const handlePrev = async (index?: TStepKey) => {
        const isStepValid = await formMethods.trigger();

        setStepValid(index ?? currentStepKey, isStepValid);
        index && setCurrentStepKey(index);

        return true;
    };


    return (
        <>
            {
                button ?
                    React.cloneElement(button, { onClick: () => setDrawer(true) }) :
                    <IconEdit onClick={() => setDrawer(true)} />
            }

            <Drawer header="Add SSH key" open={drawer} onClose={() => setDrawer(false)} backdrop>
                <FormProvider {...formMethods}>
                    <form onSubmit={formMethods.handleSubmit(onSubmit)} noValidate onReset={onReset}>
                        <Drawer.Body>
                            <Tabs>
                                <TabList>
                                    {
                                        wizardSteps.map((step: IWizardStep) => (
                                            <Tab
                                                key={step.index}
                                                value={currentStepKey}
                                                index={step.index}
                                                onClick={() => handlePrev(step.index)}>{step.header}</Tab>
                                        ))
                                    }
                                </TabList>
                                {
                                    wizardSteps.map((step: IWizardStep) => (
                                        <TabBody key={step.index} value={currentStepKey} index={step.index}>
                                            {
                                                step.component &&
                                                React.createElement(step.component, { setStepValid, isStepValid, wideDrawer, setWideDrawer, index: step.index })
                                            }
                                        </TabBody>
                                    ))
                                }
                            </Tabs>
                        </Drawer.Body>
                    </form>
                </FormProvider>
                <Drawer.Footer>
                    <div className="inline-group">
                        {
                            !isFirst &&
                            <Button view="outlined" onClick={() => prevStep(handlePrev)} icon={<IconArrowLeft />} />
                        }
                        <Button onClick={onReset} view="outlined">Clear all</Button>
                    </div>
                    {
                        isLast ?
                            <Button disabled={!isAllValid()} loading={submitted} onClick={formMethods.handleSubmit(onSubmit)}>Done</Button> :
                            <Button loading={submitted} onClick={() => nextStep(handleNext)} icon={<IconArrowRight />} />
                    }
                </Drawer.Footer>
            </Drawer>
        </>
    );
};
