import React, { useContext } from 'react';
import { useNavigate } from 'react-router-dom';
import { useQuery } from '@tanstack/react-query';
import classNames from 'classnames';

import { IError, IRbacGroupObject } from '@msa/kit/types';
import { Badge, brakeWord, useObjectPermissions, StatusIcon, Table } from '@msa/kit';

import type { ICluster, IClusterNode  } from 'types';
import { loadClusterPermissions } from 'actions';
import { PlatformContext } from 'components/Platforms/Platform';

import css from './cluster.module.scss';

interface IProps {
    cluster: ICluster;
}

export const ClusterRow = React.memo((props: IProps) => {
    const { cluster } = props;
    const { platform } = useContext(PlatformContext);
    const navigate = useNavigate();

    const { data: clusterPermissions } = useQuery<IRbacGroupObject[], IError>(
        ['platform', platform?.name, 'cluster', cluster.name, 'permissions'],
        () => loadClusterPermissions(platform?.name, cluster.name),
        {
            enabled: !!platform && !!cluster
        }
    );

    const canGetCluster = useObjectPermissions(clusterPermissions, 'cluster', ['get']);

    return (
        <>
            <Table.Row key={cluster.name} onClick={() => {canGetCluster && navigate(`${cluster.name}`)}} className={classNames('entity__row', canGetCluster && 'entity__row--hover')} valign="center">
                <Table.Cell colspan={2}>{cluster.name}</Table.Cell>
                <Table.Cell colspan={3}>{brakeWord(cluster.description, 50)}</Table.Cell>
                <Table.Cell colspan={4} className={css.nodes}>
                    {
                        cluster.nodes.map((node: IClusterNode) => {
                            return (
                                <div key={node.ip} className={css.node}>
                                    <span>{node.ip}</span>
                                    <div>
                                        {
                                            node.master &&
                                            <Badge label="master" size="xsmall" color="blue" />
                                        }
                                        {
                                            node.etcd &&
                                            <Badge label="etcd" size="xsmall" color="dark" />
                                        }
                                        {
                                            node.worker &&
                                            <Badge label="worker" size="xsmall" color="green" />
                                        }
                                    </div>
                                </div>
                            );
                        })
                    }
                </Table.Cell>
                <Table.Cell colspan={3}>{cluster.platform.name}</Table.Cell>
                <Table.Cell colspan={3}>
                    <StatusIcon status={cluster.state} action={cluster.action} />
                </Table.Cell>
                <Table.Cell colspan={2}>
                    <StatusIcon status={cluster.is_online ? 'Online' : 'Offline'} />
                </Table.Cell>
            </Table.Row>
        </>
    );
});
