import React from 'react';
import { format } from 'date-fns';

import { IconError, IconSuccess, Table } from '@msa/kit';

import { ILogMessage } from 'types';

import css from './log.module.scss';


interface IProps {
    log: ILogMessage,
    advanced: boolean
}

export const LogRow = (props: IProps) => {
    const { log, advanced } = props;

    const getFormattedText = () => {
        if(log.details) {
            return log.details.includes('\n') ?
                log.details.split('\n').map((line, index) => (
                    <React.Fragment key={index}>
                        {line}
                        <br />
                    </React.Fragment>
                ))
                : log.details
        } else {
            return ''
        }
    }

    return (
        <>
            <Table.Row valign="center" className="entity__row">
                <Table.Cell colspan={2} className={css.dateRow}>{format(new Date(log.timestamp), 'yyyy-MM-dd HH:mm:ss')}</Table.Cell>
                <Table.Cell>{log.username}</Table.Cell>
                <Table.Cell align="center">
                    {
                        log.access === 'Access' ?
                            <IconSuccess className={css.success} /> :
                            <IconError className={css.error} />
                    }
                </Table.Cell>
                {advanced && <Table.Cell colspan={2}>{log.host}</Table.Cell>}
                {advanced && <Table.Cell colspan={2}>{log.user_agent}</Table.Cell>}
                <Table.Cell>{log.platform}</Table.Cell>
                <Table.Cell className={log.action && css[log.action.toLowerCase()]}>{log.action}</Table.Cell>
                <Table.Cell>{log.object_type}</Table.Cell>
                <Table.Cell>{log.object_name}</Table.Cell>
                <Table.Cell className={css.cell} colspan={advanced ? 3 : 5}>
                    {getFormattedText()}
                </Table.Cell>

            </Table.Row>
        </>
    );
};
