import React, { useCallback, useEffect, useState } from 'react';
import { useQuery, useQueryClient } from '@tanstack/react-query';
import { useDebounce } from 'use-debounce';
import { useTranslation } from 'react-i18next';

import { OrderDirection, IError, IOption } from '@msa/kit/types';
import {
    Loader,
    Select,
    Table,
    useTable,
    Pagination,
    sort,
    usePagination,
    Input, IconCloseCircle, Switcher
} from '@msa/kit';

import { ILogMessage } from 'types';
import { getLogs } from 'actions';
import { LogRow } from './LogRow';

import css from './log.module.scss';


interface IProps {
}

export const Logs = (props: IProps) => {
    const {} = props;
    const queryClient = useQueryClient();
    const { limit, getPaginatedItems, onPageChange, setCurrentPage } = usePagination<ILogMessage[]>(20);
    const { orderedBy, setOrderedBy } = useTable({ 'timestamp': OrderDirection.desc });
    const { t } = useTranslation();

    const searchOptions: IOption[] = [
        { label: t('logs.5_min'), value: 300 },
        { label: t('logs.10_min'), value: 600 },
        { label: t('logs.30_min'), value: 1800 },
        { label: t('logs.1_hour'), value: 3600 },
        { label: t('logs.6_hours'), value: 21600 },
        { label: t('logs.1_day'), value: 86400 },
        { label: t('logs.1_week'), value: 604800 },
        { label: t('logs.1_month'), value: 2592000 },
    ];

    const actionOptions: IOption[] = [
        { label: t('logs.all_Actions'), value: '' },
        { label: t('logs.add_Worker'), value: 'Add Worker(s)' },
        { label: t('logs.create'), value: 'Create' },
        { label: t('logs.update'), value: 'Update' },
        { label: t('logs.upgrade'), value: 'Upgrade' },
        { label: t('logs.recreate'), value: 'Recreate' },
        { label: t('logs.delete'), value: 'Delete' },
        { label: t('logs.force_Delete'), value: 'Force Delete' },
        { label: t('logs.remove_Worker'), value: 'Remove Worker(s)' },
        { label: t('logs.import'), value: 'Import' },
        { label: t('logs.upgrade_Version'), value: 'Upgrade Version' },
        { label: t('logs.edit_Permission'), value: 'Edit Permission'},
        { label: t('logs.export'), value: 'Export'},
        // { label: 'Get', value: 'Get' },
    ];

    const objectOptions: IOption[] = [
        { label: t('logs.all_Objects'), value: '' },
        { label: t('logs.platforms'), value: 'platform' },
        { label: t('logs.clusters'), value: 'cluster' },
        { label: t('logs.user'), value: 'user' },
        { label: t('logs.group'), value: 'group' },
        { label: t('logs.LDAP'), value: 'LDAP' },
        { label: t('logs.credential'), value: 'credential' },
        { label: t('logs.nodes'), value: 'node' },
        { label: t('logs.message'), value: 'message' },
        { label: t('logs.userLog'), value: 'user log' },
        { label: t('logs.infrastructures'), value: 'infrastructure' },
        { label: t('logs.userHelmCharts'), value: 'user helm chart' },
        { label: t('logs.vendorHelmCharts'), value: 'vendor helm chart' },
        { label: t('logs.userHelmRepo'), value: 'user helm repo' },
        { label: t('logs.vendorHelmRepo'), value: 'vendor helm repo' },
        { label: t('logs.vendorDockerRepo'), value: 'vendor docker repo' },
        { label: t('logs.vendorRawRepo'), value: 'vendor raw repo' },
    ];

    const [actualActionOptions, setActualActionOptions] = useState<IOption[]>(actionOptions);
    const [advanced, setAdvanced] = useState<boolean>(false);
    const [selectedPeriod, setSelectedPeriod] = useState<number>(86400);
    const [action, setAction] = useState<string>();
    const [selectedUserName, setSelectedUserName] = useState<string | undefined>('');
    const [object_type, setObjectType] = useState<string>();
    const [selectedObjectName, setSelectedObjectName] = useState<string | undefined>('');

    const [username] = useDebounce<string | undefined>(selectedUserName ?? undefined, 500);
    const [object_name] = useDebounce<string | undefined>(selectedObjectName ?? undefined, 500);

    const { isLoading, data, isFetching } = useQuery<ILogMessage[], IError>(
        ['logs', selectedPeriod, action, username, object_type, object_name, advanced],
        () => getLogs({
            selectedPeriod,
            action,
            object_type,
            username: username !== '' ? username : undefined,
            object_name: object_name !== '' ? object_name : undefined,
            advanced: advanced
        }),
        {
            // refetchInterval: settings.DELAY.LOG,
        }
    );

    const [logs, setLog] = useState<any>(data ?? []);


    useEffect(() => {
        queryClient.invalidateQueries(['logs', selectedPeriod, action]);
        setCurrentPage(0);
    }, [queryClient, setCurrentPage, selectedPeriod, action, username, object_type, object_name]);


    useEffect(() => {
        if(data){
            const optionGet = actualActionOptions.find(el => el.label === 'Get')

            if(advanced && !optionGet) {
                setActualActionOptions([...actionOptions, { label: 'Get', value: 'Get' }])
            }
            else if(!advanced && optionGet) {
                setActualActionOptions(actionOptions.filter(el => el.label !== 'Get'))
            }
            setLog(sort<ILogMessage[]>(data, 'timestamp', OrderDirection.desc));
        }

    }, [data, advanced]);


    const orderBy = useCallback((fieldName: string, orderDirection: OrderDirection) => {
        if(setOrderedBy)
            setOrderedBy({ [fieldName]: orderDirection });

        setLog(sort<ILogMessage[]>(data ?? [], fieldName, orderDirection));
    }, [data, setOrderedBy]);

    const handleFilterChange = (value: string , filter: string) => {
        switch(filter) {
            case 'name':
                setSelectedUserName(value);
                break;
            case 'obj_name':
                setSelectedObjectName(value);
                break;
            case 'type':
                setObjectType(value !== '' ? value : undefined);
                break;
            case 'action':
                setAction(value !== '' ? value : undefined);
                break;
            case 'period':
                setSelectedPeriod(Number(value));
                break;
            default:
                return;
        }
    }


    if(isLoading)
        return <Loader layout />;


    return (
        <>
            <div className="layout__header">
                <h1 className="layout__title">{t('navigation.userlog')}</h1>
                <Switcher 
                    label={t('common.advanced')}
                    labelPosition='left'
                    value='Advanced'
                    checked={advanced}
                    className={css.switcher}
                    onChange={() => setAdvanced(!advanced)}
                />
            </div>
            <div className={css.filters}>
                    <Input
                        placeholder={t('common.usernameFilter')}
                        className={css.input}
                        value={selectedUserName}
                        onChange={(event) => handleFilterChange(event.target.value, 'name')}
                        autoComplete="off"
                        iconRight={<IconCloseCircle onClick={() => handleFilterChange('', 'name')} />}
                        disabled={isFetching} />
                    <Input
                        placeholder={t('common.objectnameFilter')}
                        className={css.input}
                        value={selectedObjectName}
                        onChange={(event) => handleFilterChange(event.target.value, 'obj_name')}
                        iconRight={<IconCloseCircle onClick={() => handleFilterChange('', 'obj_name')} />}
                        autoComplete="off" />
                    <Select
                        value={object_type}
                        autoComplete="off"
                        className={css.select}
                        options={objectOptions}
                        onChange={(event) => handleFilterChange(event.target.value, 'type')} />
                    <Select
                        value={action}
                        autoComplete="off"
                        className={css.select}
                        options={actualActionOptions}
                        onChange={(event) => handleFilterChange(event.target.value, 'action')}
                        loading={isFetching} />
                    <Select
                        value={selectedPeriod}
                        labelPosition="left"
                        autoComplete="off"
                        className={css.select}
                        options={searchOptions}
                        onChange={(event) => handleFilterChange(event.target.value, 'period')}
                        loading={isFetching} />
                </div>
            <Table cells={advanced ? 15 : 13}>
                <Table.Header>
                    <Table.Cell className={css.sort} colspan={2}>{t('common.date')}<Table.ActionSort fieldName="timestamp" orderedBy={orderedBy} orderBy={orderBy} /></Table.Cell>
                    <Table.Cell className={css.sort}>{t('common.user')} <Table.ActionSort fieldName="username" orderedBy={orderedBy} orderBy={orderBy} /></Table.Cell>
                    <Table.Cell className={css.sort}>{t('common.access')}<Table.ActionSort fieldName="access" orderedBy={orderedBy} orderBy={orderBy} /></Table.Cell>
                    {advanced && <Table.Cell colspan={2} className={css.sort}>{t('common.host')} <Table.ActionSort fieldName="host" orderedBy={orderedBy} orderBy={orderBy} /></Table.Cell>}
                    {advanced && <Table.Cell colspan={2} className={css.sort}>{t('common.agent')} <Table.ActionSort fieldName="user-agent" orderedBy={orderedBy} orderBy={orderBy} /></Table.Cell>}
                    <Table.Cell className={css.sort}>{t('platforms.platform', {context: 'one'})} <Table.ActionSort fieldName="platform" orderedBy={orderedBy} orderBy={orderBy} /></Table.Cell>
                    <Table.Cell className={css.sort}>{t('common.action')} <Table.ActionSort fieldName="action" orderedBy={orderedBy} orderBy={orderBy} /></Table.Cell>
                    <Table.Cell className={css.sort}>{t('common.object')} <Table.ActionSort fieldName="object_type" orderedBy={orderedBy} orderBy={orderBy} /></Table.Cell>
                    <Table.Cell className={css.sort}>{t('common.name')} <Table.ActionSort fieldName="object_name" orderedBy={orderedBy} orderBy={orderBy} /></Table.Cell>
                    <Table.Cell className={css.sort} colspan={advanced ? 3 : 5}>{t('common.details')}</Table.Cell>
                </Table.Header>
                {
                    getPaginatedItems(logs).map((log: ILogMessage) => <LogRow key={log.timestamp + Math.random()} log={log} advanced={advanced} />)
                }
                {
                    logs &&
                    logs.length > limit &&
                    <Pagination
                        itemsCount={logs.length}
                        limit={limit}
                        onChangePage={onPageChange} />
                }
            </Table>
        </>
    );
};
