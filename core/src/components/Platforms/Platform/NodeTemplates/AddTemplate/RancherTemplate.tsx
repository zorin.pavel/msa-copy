import React, { ChangeEvent, useEffect } from 'react';
import { Controller, useFormContext } from 'react-hook-form';

import { IStepComponentProps } from '@msa/kit/types';
import { Input, Textarea } from '@msa/kit';

import settings from 'settings';


interface IProps extends IStepComponentProps {}

export const RancherTemplate = (props: IProps) => {
    const { setStepValid, index } = props;

    const {
        control,
        formState: { errors, isValid }
    } = useFormContext();


    useEffect(() => {
        setStepValid && setStepValid(index, isValid);
    }, [index, isValid]);


    return (
        <>
            <Controller control={control} name="rancher_name" rules={
                {
                    required: 'Name is required',
                    pattern: {
                        value: settings.NAME_REGEX,
                        message: 'Name must consist of lowercase alphanumeric characters'
                    }
                }
            } render={({ field: { name, value, onChange } }) => (
                <Input
                    name={name}
                    value={value}
                    label="Rancher template name"
                    labelPosition="left"
                    required
                    error={errors.rancher_name && errors.rancher_name.message as string}
                    displayError
                    onChange={(e: ChangeEvent<HTMLInputElement>) => onChange(e.target.value)}
                    autoComplete="off" />
            )} />
            <Controller
                control={control}
                name="description"
                render={({ field }) => (
                    <Textarea
                        {...field}
                        label="Description"
                        placeholder="Description"
                        labelPosition="left"
                        rows={5}
                        error={!!errors.description} />
                )} />
        </>
    );
};
