import React, { ForwardedRef, forwardRef } from 'react';
import classNames from 'classnames';

import { getEnumValueByKey } from '@msa/kit/types';
import { Image } from '@msa/kit';

import { ECloudProvider } from 'types';

import css from './CloudProvider.module.scss';


interface IProps {
    provider: keyof typeof ECloudProvider,
    name: string,
    value: string,
    onChange: (_value: string) => void,
}

export const CloudProvider = forwardRef((props: IProps, ref: ForwardedRef<HTMLDivElement>) => {
    const { provider, value, onChange } = props;

    const kindValue = getEnumValueByKey(ECloudProvider, provider);

    return (
        <div
            ref={ref}
            className={classNames(
                css.provider,
                (value === provider && css.selected)
            )}
            onClick={() => onChange(provider)}>
            <div className={css.head}>
                <Image src={`/icons/${provider}.svg`} alt={kindValue} className={css.logo}/>
                <h4 className={css.name}>{kindValue}</h4>
            </div>
        </div>
    );
});
