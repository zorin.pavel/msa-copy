import React, { ChangeEvent, useCallback, useEffect, useMemo, useState } from 'react';
import { Controller, useFormContext } from 'react-hook-form';

import { IOption } from '@msa/kit/types';
import { Select, timer } from '@msa/kit';

import { AmazonCredentials } from './AmazonCredentials';

import css from './amazon.module.scss';


interface IProps {
}

export const AmazonFields = (props: IProps) => {
    const {} = props;
    const {
        control,
        register,
        unregister,
        formState: { errors }
    } = useFormContext();

    const registeredFields = useMemo(() => (['accessKey', 'secretKey', 'defaultRegion' ]), []);

    const [ccOptions, setCCOptions] = useState<IOption[]>([{ label: 'Credentials 1', value: 'cred-1' }]);
    const [ccLoading, setCCLoading] = useState<boolean>(false);


    useEffect(() => {
        registeredFields.map(fieldName => register(fieldName));

        return () => {
            unregister(registeredFields, { keepDefaultValue: true });
        };
    }, [register, unregister, registeredFields]);


    const updateCloudCredentials = useCallback(() => {
        setCCLoading(true);

        timer<boolean>(true, 1000)
            .then(() => {
                setCCOptions([{ label: 'Credentials 1', value: 'cred-1' }, { label: 'Credentials 2', value: 'cred-2' }]);
                setCCLoading(false);
            });
    }, [setCCLoading, setCCOptions]);


    useEffect(() => {
        updateCloudCredentials();
    }, [updateCloudCredentials]);


    return (
        <>
            <h4 className="section__header">Account access</h4>

            <Controller
                control={control}
                name="defaultRegion"
                rules={
                    {
                        required: 'Default region is required',
                    }
                }
                render={({ field: { name, value, onChange } }) => (
                    <Select
                        name={name}
                        value={value}
                        label="Default region"
                        labelPosition="left"
                        required
                        error={errors.defaultRegion && errors.defaultRegion.message as string}
                        onChange={(e: ChangeEvent<HTMLInputElement>) => onChange(e.target.value)}
                        autoComplete="off"
                        options={[{ label: 'us-west-2', value: 'us-west-2' }]} />
                )} />

            <Controller
                control={control}
                name="cloudCredentials"
                rules={
                    {
                        required: 'Cloud credentials is required',
                    }
                }
                render={({ field: { name, value, onChange } }) => (
                    <Select
                        className={css.input}
                        name={name}
                        value={value}
                        label="Cloud credentials"
                        labelPosition="left"
                        required
                        error={errors.cloudCredentials && errors.cloudCredentials.message as string}
                        onChange={(e: ChangeEvent<HTMLInputElement>) => onChange(e.target.value)}
                        autoComplete="off"
                        options={ccOptions}
                        addonRight={<AmazonCredentials onSave={updateCloudCredentials} />}
                        loading={ccLoading} />
                )} />
        </>
    );
};
