import React, { ChangeEvent, useState } from 'react';
import { Controller, useForm } from 'react-hook-form';

import { Button, Drawer, Input, Select, Textarea } from '@msa/kit';

import settings from 'settings';


interface IProps {
    onSave: () => void
}

export const AmazonCredentials = (props: IProps) => {
    const {} = props;

    const [drawer, setDrawer] = useState<boolean>(false);
    const [submitted, setSubmitted] = useState<boolean>(false);

    const defaultValues = {
        name: '',
        description: '',
        accessKey: '',
        secretKey: '',
        defaultRegion: ''
    };
    const {
        control,
        handleSubmit,
        reset,
        formState: { errors, isValid }
    } = useForm({
        mode: 'onChange',
        reValidateMode: 'onChange',
        defaultValues
    });


    const onSubmit = (formData: any) => {
        console.log('formData', formData);
        setSubmitted(true);
    };


    const onReset = () => {
        reset(defaultValues);
    };


    const onClose = () => {
        setDrawer(false);
        onReset();
    };


    return (
        <>
            <Button label="Add new" view="outlined" size="small" onClick={() => setDrawer(true)}/>

            <Drawer header="Create Amazon credentials" open={drawer} onClose={onClose} narrow id="amazon-credentials" backdrop>
                <form onSubmit={handleSubmit(onSubmit)} noValidate onReset={onReset}>
                    <Drawer.Body>
                        <Controller
                            control={control}
                            name="name"
                            render={({ field: { name, value, onChange } }) => (
                                <Input
                                    name={name}
                                    value={value}
                                    label="Access key"
                                    labelPosition="left"
                                    required
                                    error={errors.name && errors.name.message as string}
                                    displayError
                                    onChange={(e: ChangeEvent<HTMLInputElement>) => onChange(e.target.value)}
                                    autoComplete="off" />
                            )} />
                        <Controller
                            control={control}
                            name="description"
                            render={({ field: { name, value, onChange } }) => (
                                <Textarea
                                    name={name}
                                    value={value}
                                    label="Description"
                                    labelPosition="left"
                                    rows={3}
                                    onChange={(e: ChangeEvent<HTMLTextAreaElement>) => onChange(e.target.value)} />
                            )} />
                        <Controller
                            control={control}
                            name="accessKey"
                            rules={
                                {
                                    required: 'Access key is required',
                                    pattern: {
                                        value: settings.NAME_REGEX,
                                        message: 'Access key must consist of lowercase alphanumeric characters'
                                    }
                                }
                            }
                            render={({ field: { name, value, onChange } }) => (
                                <Input
                                    name={name}
                                    value={value}
                                    label="Access key"
                                    labelPosition="left"
                                    required
                                    error={errors.accessKey && errors.accessKey.message as string}
                                    displayError
                                    onChange={(e: ChangeEvent<HTMLInputElement>) => onChange(e.target.value)}
                                    autoComplete="off" />
                            )} />
                        <Controller
                            control={control}
                            name="secretKey"
                            rules={
                                {
                                    required: 'Secret key is required',
                                    pattern: {
                                        value: settings.NAME_REGEX,
                                        message: 'Secret key must consist of lowercase alphanumeric characters'
                                    }
                                }
                            }
                            render={({ field: { name, value, onChange } }) => (
                                <Input
                                    name={name}
                                    value={value}
                                    label="Secret key"
                                    labelPosition="left"
                                    required
                                    error={errors.secretKey && errors.secretKey.message as string}
                                    displayError
                                    onChange={(e: ChangeEvent<HTMLInputElement>) => onChange(e.target.value)}
                                    autoComplete="off" />
                            )} />
                        <Controller
                            control={control}
                            name="defaultRegion"
                            rules={
                                {
                                    required: 'Default region is required',
                                }
                            }
                            render={({ field: { name, value, onChange } }) => (
                                <Select
                                    name={name}
                                    value={value}
                                    label="Default region"
                                    labelPosition="left"
                                    required
                                    error={errors.defaultRegion && errors.defaultRegion.message as string}
                                    onChange={(e: ChangeEvent<HTMLInputElement>) => onChange(e.target.value)}
                                    autoComplete="off"
                                    options={[{ label: 'us-west-2', value: 'us-west-2' }]} />
                            )} />
                    </Drawer.Body>
                    <Drawer.Footer>
                        <Button view="outlined" color="red" onClick={onReset}>Clear all</Button>
                        <Button disabled={!isValid} loading={submitted} type="submit" color="red" onClick={handleSubmit(onSubmit)}>Done</Button>
                    </Drawer.Footer>
                </form>
            </Drawer>
        </>
    );
};
