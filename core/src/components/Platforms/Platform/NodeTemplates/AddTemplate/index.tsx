import React, { useState } from 'react';
import { FormProvider, useForm } from 'react-hook-form';

import { IWizardStep, TStepKey } from '@msa/kit/types';
import {
    Button,
    Drawer,
    IconPlus,
    Tabs,
    Tab,
    TabList,
    TabBody,
    useWizard,
    IconArrowRight, IconArrowLeft
} from '@msa/kit';

import { ProviderStep } from './ProviderStep';
// import { RancherTemplate } from './RancherTemplate';


interface IProps {
}


const wizardSteps: IWizardStep[] = [
    {
        index: 'provider',
        header: 'Select provider',
        component: ProviderStep
    },
    // {
    //     index: 'template',
    //     header: 'Set Rancher template',
    //     component: RancherTemplate
    // }
];


export const AddTemplate = (props: IProps) => {
    const {} = props;

    const [drawer, setDrawer] = useState<boolean>(false);
    const [submitted, setSubmitted] = useState<boolean>(false);

    const defaultValues = {
        name: '',
        provider: 'azure',
        // Amazon
        defaultRegion: '',
        cloudCredetials: '',

        rancher_name: '',
        description: ''
    };
    const formMethods = useForm({
        mode: 'onChange',
        reValidateMode: 'onChange',
        defaultValues
    });

    const {
        currentStepKey,
        setCurrentStepKey,
        isStepValid,
        isAllValid,
        setStepValid,
        prevStep,
        nextStep,
        resetStep,
        isLast,
        isFirst,
    } = useWizard(wizardSteps[0].index, wizardSteps);


    const onSubmit = (formData: any) => {
        console.log('formData', formData);
        setSubmitted(true);
    };


    const onReset = () => {
        formMethods.reset(defaultValues);
        resetStep();
    };


    const onClose = () => {
        setDrawer(false);
        onReset();
    };


    const handleNext = async (index?: TStepKey) => {
        const isStepValid = await formMethods.trigger();

        setStepValid(index ?? currentStepKey, isStepValid);

        return isStepValid;
    };


    const handlePrev = async (index?: TStepKey) => {
        const isStepValid = await formMethods.trigger();

        setStepValid(index ?? currentStepKey, isStepValid);
        index && setCurrentStepKey(index);

        return true;
    };


    return (
        <>
            <Button color="red" iconLeft={<IconPlus />} onClick={() => setDrawer(true)}>Add template</Button>

            <Drawer header="New template" open={drawer} onClose={onClose} backdrop>
                <FormProvider {...formMethods}>
                    <form onSubmit={formMethods.handleSubmit(onSubmit)} noValidate onReset={onReset}>
                        <Drawer.Body>
                            <Tabs>
                                <TabList>
                                    {
                                        wizardSteps.map((step: IWizardStep) => (
                                            <Tab
                                                key={step.index}
                                                value={currentStepKey}
                                                index={step.index}
                                                onClick={() => handlePrev(step.index)}>{step.header}</Tab>
                                        ))
                                    }
                                </TabList>
                                {
                                    wizardSteps.map((step: IWizardStep) => (
                                        <TabBody key={step.index} value={currentStepKey} index={step.index}>
                                            {
                                                step.component &&
                                                React.createElement(step.component, { setStepValid, isStepValid, index: step.index })
                                            }
                                        </TabBody>
                                    ))
                                }
                            </Tabs>
                        </Drawer.Body>
                    </form>
                </FormProvider>
                <Drawer.Footer>
                    <div className="inline-group">
                        {
                            !isFirst &&
                            <Button view="outlined" onClick={() => prevStep(handlePrev)} icon={<IconArrowLeft />} />
                        }
                        <Button onClick={onReset} view="outlined">Clear all</Button>
                    </div>
                    {
                        isLast ?
                            <Button disabled={!isAllValid()} loading={submitted} onClick={formMethods.handleSubmit(onSubmit)}>Done</Button> :
                            <Button loading={submitted} onClick={() => nextStep(handleNext)} icon={<IconArrowRight />} />
                    }
                </Drawer.Footer>
            </Drawer>
        </>
    );
};
