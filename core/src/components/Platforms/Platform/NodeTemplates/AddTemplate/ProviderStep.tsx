import React, { ChangeEvent, useCallback, useEffect } from 'react';
import { Controller, useFormContext, useWatch } from 'react-hook-form';

import { IStepComponentProps } from '@msa/kit/types';
import { Input } from '@msa/kit';

import settings from 'settings';
import { ECloudProvider } from 'types';
import { CloudProvider } from './CloudProvider';
import { AmazonFields } from './AmazonFields';

import css from './provider.module.scss';


interface IProps extends IStepComponentProps {}

export const ProviderStep = (props: IProps) => {
    const { setStepValid, index } = props;

    const {
        control,
        formState: { errors, isValid }
    } = useFormContext();

    const provider = useWatch({
        name: 'provider',
        control: control
    });


    useEffect(() => {
        setStepValid && setStepValid(index, isValid);
    }, [index, isValid]);


    const renderSection = useCallback(() => {
        switch(provider) {
            case 'amazon':
                return <AmazonFields />;
            // case 'azure':
            //     return <AzureFields />;
        }
    }, [provider]);


    return (
        <>
            <Controller control={control} name="name" rules={
                {
                    required: 'Name is required',
                    pattern: {
                        value: settings.NAME_REGEX,
                        message: 'Name must consist of lowercase alphanumeric characters'
                    }
                }
            } render={({ field: { name, value, onChange } }) => (
                <Input
                    name={name}
                    value={value}
                    label="Template name"
                    labelPosition="left"
                    required
                    error={errors.name && errors.name.message as string}
                    displayError
                    onChange={(e: ChangeEvent<HTMLInputElement>) => onChange(e.target.value)}
                    autoComplete="off" />
            )} />
            <h4 className="section__header">Cloud provider</h4>

            <div className={css.providers}>
                {
                    Object.keys(ECloudProvider).map(kind => {
                        return (
                            <Controller key={kind} control={control} name="provider" rules={
                                {
                                    required: 'Any service is required',
                                }
                            } render={({ field }) => (
                                <CloudProvider {...field} provider={kind as keyof typeof ECloudProvider} />
                            )} />
                        );
                    })
                }
            </div>

            {
                renderSection()
            }
        </>
    );
};
