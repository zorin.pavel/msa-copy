import React, { useState } from 'react';

import { Button, Dialog, IconDelete, IconEdit, StatusIcon, Table, Toast } from '@msa/kit';

import { INodeTemplate } from 'types';

import css from './row.module.scss';


interface IProps {
    template: INodeTemplate,
}

export const TemplateRow = React.memo((props: IProps) => {
    const { template } = props;

    const [edit, setEdit] = useState<boolean>(false);
    const [deleteDialog, setDeleteDialog] = useState<boolean>(false);


    const onDelete = () => {
        Toast.error({ message: 'Delete' });
    };


    return (
        <>
            <Table.Row key={template.name} className={css.row} valign="center">
                <Table.Cell colspan={2}>
                    <div className={css.rowHeader}>
                        <img src={`/icons/${template.provider}.svg`} alt={template.provider} className={css.rowLogo}/>
                        <span>{template.provider}</span>
                    </div>
                </Table.Cell>
                <Table.Cell colspan={2}>{template.name}</Table.Cell>
                <Table.Cell colspan={2}>{template.owner}</Table.Cell>
                <Table.Cell colspan={2}>{template.location}</Table.Cell>
                <Table.Cell colspan={1}>{template.size}</Table.Cell>
                <Table.Cell colspan={1}><StatusIcon status={template.status} /></Table.Cell>
                <Table.Cell actions={2}>
                    <IconEdit onClick={() => setEdit(true)} />
                    <IconDelete onClick={() => setDeleteDialog(true)} />
                </Table.Cell>
            </Table.Row>

            {
                edit &&
                <>edit</>
            }
            
            {
                <Dialog
                    open={deleteDialog}
                    header="Delete Node template"
                    buttons={[
                        <Button key="cancel" onClick={() => setDeleteDialog(false)} view="outlined">Cancel</Button>,
                        <Button key="delete" onClick={onDelete}>Delete</Button>
                    ]}
                    onClose={() => setDeleteDialog(false)}>
                    Do you really want delete template <b>{template.name}</b>?
                </Dialog>
            }
        </>
    );
});
