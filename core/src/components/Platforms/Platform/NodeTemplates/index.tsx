import React, { useCallback, useEffect, useState } from 'react';
import { useQuery } from '@tanstack/react-query';

import { IError, OrderDirection } from '@msa/kit/types';
import { Loader, Error, Table, useTable, sort } from '@msa/kit';

import { INodeTemplate } from 'types';
import { getNodeTemplates } from 'actions';
import { TemplateRow } from './TemplateRow';
import { AddTemplate } from './AddTemplate';


export const NodeTemplates = () => {
    const { orderedBy, setOrderedBy } = useTable();

    const { isLoading, data, error } = useQuery<INodeTemplate[], IError>(
        ['nodeTemplates'],
        getNodeTemplates
    );
    const [templates, setTemplates] = useState<INodeTemplate[]>(data ?? []);


    useEffect(() => {
        if(data)
            setTemplates(data);
    }, [data]);


    const orderBy = useCallback((fieldName: string, orderDirection: OrderDirection) => {
        if(setOrderedBy)
            setOrderedBy({ [fieldName]: orderDirection });

        setTemplates(sort(data ?? [], fieldName, orderDirection));
    }, [data, setOrderedBy]);

    if(isLoading)
        return <Loader />;

    if(error)
        return <Error {...error} />;


    return (
        <>
            <div className="layout__header">
                <h1 className="layout__title">Node templates</h1>
                <AddTemplate />
            </div>

            <Table cells={11}>
                <Table.Header>
                    <Table.Cell colspan={2}>Provider</Table.Cell>
                    <Table.Cell colspan={2}>Name <Table.ActionSort fieldName="name" orderedBy={orderedBy} orderBy={orderBy} /></Table.Cell>
                    <Table.Cell colspan={2}>Owner</Table.Cell>
                    <Table.Cell colspan={2}>Location</Table.Cell>
                    <Table.Cell colspan={1}>Size</Table.Cell>
                    <Table.Cell colspan={1}>Status</Table.Cell>
                    <Table.Cell actions={2} />
                </Table.Header>
                {
                    templates &&
                    templates.map((template) => <TemplateRow key={template.name} template={template} />)
                }
            </Table>
        </>
    );
};
