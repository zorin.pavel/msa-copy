import React, { useContext } from 'react';
import { useQuery } from '@tanstack/react-query';
import { useTranslation } from 'react-i18next';

import { Badge, Button, IconEdit, Table } from '@msa/kit';
import { Colors, IError, IRbacGroupObject } from '@msa/kit/types';

import { loadPlatformPermissions } from 'actions';
import { badgeColor } from 'components/Iam';
import { PlatformContext } from 'components/Platforms/Platform';
import { EditPermissions } from 'components/EditPermissions';


export const PlatformPermissions = () => {
    const { platform, can } = useContext(PlatformContext);
    const { t } = useTranslation()

    const { data: platformPermissions } = useQuery<IRbacGroupObject[], IError>(
        ['platform', platform?.name, 'permissions'],
        () => loadPlatformPermissions(platform?.name),
        {
            enabled: !!platform
        }
    );


    return (
        <>
            <div className="layout__header">
                <h1 className="layout__title">{platform?.name} <span>| {t('permissions.permissions')}</span></h1>
                <EditPermissions
                    button={<Button disabled={!can?.edit} iconLeft={<IconEdit />} color="dark">{t('permissions.edit')}</Button>}
                    platformName={platform?.name}
                    permissions={platformPermissions}/>
            </div>

            <Table cells={4}>
                <Table.Header>
                    <Table.Cell>{t('common.group')}</Table.Cell>
                    <Table.Cell>{t('common.objectName')}</Table.Cell>
                    <Table.Cell colspan={2}>{t('common.scope')}</Table.Cell>
                </Table.Header>

                {
                    platformPermissions &&
                    platformPermissions.map((object: IRbacGroupObject) => (
                        <Table.Row valign="center" key={`${object.platform}${object.cluster}${object.group}`} className="entity__row">
                            <Table.Cell>{object.group}</Table.Cell>
                            <Table.Cell>{object.platform}{object.cluster ? ` / ${object.cluster}` : ''}</Table.Cell>
                            <Table.Cell colspan={2}>
                                {
                                    object.scopes.map((scope: string) => <Badge key={scope} label={t(`scopes.${scope}`)} size="small" color={badgeColor[scope] as keyof typeof Colors}/>)
                                }
                            </Table.Cell>
                        </Table.Row>
                    ))
                }
            </Table>
        </>
    );
};
