import React, { createContext, useMemo } from 'react';
import { Outlet, useParams } from 'react-router-dom';
import { useQuery } from '@tanstack/react-query';
import { useKeycloak } from '@react-keycloak/web';

import { IError, IRbacGroupObject } from '@msa/kit/types';
import { Error, Loader } from '@msa/kit';

import type { IPlatform } from 'types';
import { loadPlatform, loadPlatformPermissions } from 'actions';


interface ContextProps {
    platform?: IPlatform,
    can?: Record<string, boolean | undefined>
}

export const PlatformContext = createContext<ContextProps>({});


export const Platform = () => {
    const { platformSlug } = useParams();
    const { keycloak } = useKeycloak();

    const { isLoading, data: platform, error } = useQuery<IPlatform, IError>(
        ['platform', platformSlug],
        () => loadPlatform(platformSlug as string)
    );
    const { data: platformPermissions } = useQuery<IRbacGroupObject[], IError>(
        ['platform', platform?.name, 'permissions'],
        () => loadPlatformPermissions(platform?.name),
        {
            enabled: !!platform
        }
    );

    const groupPermissions = useMemo(() => {
        if(!platformPermissions)
            return [];

        return platformPermissions
            .filter(permission => !!permission['platform'])
            .filter(permission => keycloak.tokenParsed?.groups.includes(permission.group))
            .map(gp => gp.scopes)
            .flat();
    }, [platformPermissions]);

    const can = useMemo(() => {
        if(!groupPermissions.length)
            return {};

        return {
            list: groupPermissions?.includes('list'),
            get: groupPermissions?.includes('get'),
            create: groupPermissions?.includes('create'),
            edit: groupPermissions?.includes('edit'),
            delete: groupPermissions?.includes('delete'),
        }
    }, [groupPermissions]);


    if(isLoading)
        return <Loader layout />;

    if(error)
        return <Error {...error} layout />;


    return (
        <PlatformContext.Provider value={{ platform, can }}>
            <Outlet />
        </PlatformContext.Provider>
    );
};
