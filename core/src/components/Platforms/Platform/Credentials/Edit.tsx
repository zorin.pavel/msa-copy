import React, { useContext, useState, ChangeEvent } from 'react';
import { Controller, useForm, useWatch } from 'react-hook-form';
import { useQueryClient } from '@tanstack/react-query';
import { useTranslation } from 'react-i18next';

import type { IError, IOption } from '@msa/kit/types';
import {
    Button,
    Drawer,
    IconEdit,
    Input,
    Select,
    Textarea,
    Toast,
    Uploader
} from '@msa/kit';

import settings from 'settings';
import { ICredential, ICredentialPost } from 'types';
import { postCredential } from 'actions';
import { PlatformContext } from 'components/Platforms/Platform';

// import css from './credentials.module.scss';

interface IProps {
    button?: React.ReactElement,
}

export const Edit = (props: IProps) => {
    const { button } = props;
    const queryClient = useQueryClient();
    const { platform } = useContext(PlatformContext);
    const { t } = useTranslation()

    const defaultValues: ICredentialPost =  {
        name: '',
        description: '',
        type: '',
        data: {
            username: '',
            password: '',
            // private_key: '',
            private_key_passphrase: '',
            cloud_data: {}
        }
    };


    const {
        handleSubmit,
        reset,
        setValue,
        control,
        formState: { errors, isValid },
    } = useForm<Partial<ICredentialPost>>({
        mode: 'onChange',
        reValidateMode: 'onChange',
        defaultValues
    });

    const [drawer, setDrawer] = useState<boolean>(false);
    const [submitted, setSubmitted] = useState<boolean>(false);
    // const [error, setError] = useState<boolean>(false);
    const [uploadType, setUploadType] = useState<string>('');
    const [uploadedData, setUploadedData] = useState(null);


    const typeOptions:IOption[] = [
        { label: t('credentials.passLog' ), value: 'login_pass' },
        { label: t('credentials.keyPair' ), value: 'ssh_key' },
        // { label: 'Cloud credential', value: 'cloud_credential' },
    ];
    const uploadOptions:IOption[] = [
        { label: t('credentials.genPair' ), value: 'generate' },
        { label: t('credentials.uploadKeyFile' ), value: 'key_file' },
        { label: t('credentials.uploadKeyText' ), value: 'key_text' },
    ];


    const type = useWatch({
        name: 'type',
        control
    });
    const data = useWatch({
        name: 'data',
        control
    });


    const onSubmit = (formData: Partial<ICredentialPost>) => {
        setSubmitted(true);
        switch(type) {
            case 'login_pass': 
                delete formData.data.private_key;
                delete formData.data.private_key_passphrase;
                delete formData.data.cloud_data;
                break;
            case 'ssh_key':
                delete formData.data.cloud_data;
                delete formData.data.password;
                if(uploadedData) {
                    formData.data.private_key = uploadedData;
                    delete data.private_key_passphrase;
                } 

                break;
            case 'cloud_credential':
                delete formData.data.username;
                delete formData.data.password;
                delete formData.data.private_key;
                delete formData.data.private_key_passphrase;
                break;
        }

        postCredential(platform?.name, formData)
            .then((response: ICredential) => {
                setDrawer(false);
                reset(defaultValues);
                Toast.success({ message: t('toasts.add', { entity: t('credentials.credentials', { context: 'edit' }), entityName: response.name }) });
                queryClient.invalidateQueries(['platform', platform?.name, 'creds']);
            })
            .catch((error: IError) => {
                Toast.error({ message: error.message });
            })
            .finally(() => setSubmitted(false));
    };

    // const renderSection = useCallback(() => {
    //     switch(provider) {
    //         case 'amazon':
    //             return <AmazonFields />;
    //         case 'azure':
    //             return <AzureFields />;
    //     }
    // }, [provider]);

    // const onChange = (values: Record<string, any>) => {
    //     setValue('data.cloud_data', values, { shouldValidate: true, shouldTouch: true });
    // };

    const handleFileUpload = (data: any) => {        
        if(data) {
            setUploadedData(data);
        } else {
            setValue('data', defaultValues.data);
            setUploadType('');
            setUploadedData(null);
        }
    };

    const onReset = () => {
        reset(defaultValues);
    };


    return (
        <>
            {
                button ?
                    React.cloneElement(button, { onClick: () => setDrawer(true) }) :
                    <IconEdit onClick={() => setDrawer(true)} />
            }

            <Drawer header={t('credentials.add' )} open={drawer} onClose={() => setDrawer(false)} backdrop>
                <form onSubmit={handleSubmit(onSubmit)} noValidate onReset={onReset}>
                    <Drawer.Body>
                        <Controller
                            control={control}
                            name="name"
                            rules={
                                {
                                    required: t('validation.isRequired', { entityName: t('common.name' )}),
                                    pattern: {
                                        value: settings.NAME_REGEX,
                                        message: t('validation.alphanumericCharacters', { entityName: t('common.name' )})
                                    }
                                }
                            }
                            render={({ field }) => (
                                <Input
                                    {...field}
                                    required
                                    label={t('common.name' )}
                                    placeholder={t('common.name' )}
                                    labelPosition="left"
                                    error={errors.name && errors.name.message}
                                    displayError
                                    autoComplete="off" />
                            )} />
                        <Controller
                            control={control}
                            name="description"
                            render={({ field }) => (
                                <Textarea
                                    {...field}
                                    label={t('common.description' )}
                                    labelPosition="left"
                                    rows={5} />
                            )} />
                        <Controller
                            control={control}
                            name="type"
                            rules={
                                {
                                    required: t('validation.isRequired', { entityName: t('common.type' )}),
                                }
                            }
                            render={({ field: { name, value, onChange } }) => (
                                <Select
                                    name={name}
                                    value={value}
                                    label={t('common.type' )}
                                    labelPosition="left"
                                    required
                                    error={errors.type && errors.type.message as string}
                                    onChange={(e: ChangeEvent<HTMLInputElement>) => onChange(e.target.value)}
                                    autoComplete="off"
                                    options={typeOptions} />
                            )} />
                        {
                            type === 'login_pass' && 
                            <>
                                <Controller
                                    control={control}
                                    name="data.username"
                                    rules={
                                        {
                                            required: t('validation.isRequired', { entityName: t('common.username' )}),
                                        }
                                    }
                                    render={({ field }) => (
                                        <Input
                                            {...field}
                                            required
                                            label={t('common.username' )}
                                            placeholder={t('common.username' )}
                                            labelPosition="left"
                                            displayError
                                            autoComplete="off" />
                                    )} />
                                <Controller
                                    control={control}
                                    name="data.password"
                                    rules={
                                        {
                                            required: t('validation.isRequired', { entityName: t('common.password' )}),
                                        }
                                    }
                                    render={({ field }) => (
                                        <Input
                                            {...field}
                                            required
                                            type="password"
                                            label={t('common.password' )}
                                            placeholder={t('common.password' )}
                                            labelPosition="left"
                                            displayError
                                            autoComplete="off" />
                                    )} />
                            </>
                        }
                        {
                            type === 'ssh_key' && 
                            <>
                                <Controller
                                    control={control}
                                    name="data.username"
                                    rules={
                                        {
                                            required: t('validation.isRequired', { entityName: t('common.username' )}),
                                        }
                                    }
                                    render={({ field }) => (
                                        <Input
                                            {...field}
                                            required
                                            label={t('common.username' )}
                                            placeholder={t('common.username' )}
                                            labelPosition="left"
                                            displayError
                                            autoComplete="off" />
                                    )} />
                                <Select
                                    value={uploadType}
                                    label={t('common.type' )}
                                    labelPosition="left"
                                    required
                                    error={errors.type && errors.type.message as string}
                                    onChange={(e: ChangeEvent<HTMLInputElement>) => setUploadType(e.target.value)}
                                    autoComplete="off"
                                    options={uploadOptions} 
                                />
                                {uploadType === 'generate' && 
                                <>
                                    <Controller
                                        control={control}
                                        name="data.private_key_passphrase"
                                        render={({ field }) => (
                                            <Input
                                                {...field}
                                                label={t('credentials.passphrase' )}
                                                placeholder={t('credentials.passphrase' )}
                                                labelPosition="left"
                                                autoComplete="off" />
                                        )} />
                                </>
                                }
                                {
                                    uploadType === 'key_file' && 
                                        <Uploader demandedType='text/plain' accept=".txt" onFileUpload={handleFileUpload} />
                                }
                                {
                                    uploadType === 'key_text' && 
                                    <Controller
                                        control={control}
                                        name="data.private_key"
                                        render={({ field }) => (
                                            <Textarea
                                                {...field}
                                                label="Private Key"
                                                placeholder="Enter private key"
                                                labelPosition="left"
                                                rows={5}
                                                autoComplete="off" />
                                        )} />
                                }
                            </>
                        }
                        {/* {
                            type === 'cloud_credential' && 
                            <>
                                {
                                    error ?
                                        <IconError className={css.iconError} /> :
                                        <IconSuccess className={css.iconSuccess} />
                                }
                                <Controller
                                    control={control}
                                    name="data.cloud_data"
                                    render={() => (
                                        <YamlEditor
                                            value={data.cloud_data}
                                            height="100"
                                            onChange={onChange}
                                            onError={setError} />
                                    )} />
                            </>
                        } */}

                    </Drawer.Body>
                    <Drawer.Footer>
                        <Button type="reset" view="outlined">{t('buttons.clear')}</Button>
                        <Button disabled={!isValid} loading={submitted} type="submit">{t('buttons.done')}</Button>
                    </Drawer.Footer>
                </form>
            </Drawer>
        </>
    );
};
