import React, { useContext, useState } from 'react';
import { useQueryClient } from '@tanstack/react-query';
import { useTranslation } from 'react-i18next';

import { Button, Dialog, IconDelete, IconDownload, Table, Textarea, Toast } from '@msa/kit';

import { ICredential } from 'types';
import { deleteCredKey, downloadCredKey } from 'actions';
import { PlatformContext } from 'components/Platforms/Platform';



interface IProps {
    cred: ICredential
}

export const Row = (props: IProps) => {
    const { cred } = props;
    const queryClient = useQueryClient();
    const { platform, can } = useContext(PlatformContext);
    const { t } = useTranslation()

    const [deleteDialog, setDeleteDialog] = useState<boolean>(false);
    // const [regenerateDialog, setRegenerateDialog] = useState<boolean>(false);
    const [keyDialog, setKeyDialog] = useState<boolean>(false);
    const [credBody, setSshKeyBody] = useState<string>('');


    const onDelete = () => {
        return deleteCredKey(platform?.name, cred.name)
            .then(() => {
                Toast.success({ message: t('toasts.delete', { entity: t('credentials.credentials', {context: 'zero'}), entityName: cred.name }) });
                queryClient.invalidateQueries(['platform', platform?.name, 'creds']);
            })
            .catch((error) => {
                Toast.error({ message: error.message });

                return false;
            });
    };


    const onDownload = () => {
        return downloadCredKey(platform?.name, cred.name)
            .then((response) => {
                setSshKeyBody(response);
                setKeyDialog(true);
            })
            .catch((error) => {
                Toast.error({ message: error.message });

                return false;
            });
    };


    return (
        <>
            <Table.Row  className="entity__row" valign="center">
                <Table.Cell colspan={2}>{cred.name}</Table.Cell>
                {/* <Table.Cell colspan={2}>{cred.tags}</Table.Cell> */}
                <Table.Cell colspan={2}>{cred.description}</Table.Cell>
                <Table.Cell colspan={2}>{cred.type}</Table.Cell>
                <Table.Cell actions={2}>
                    {cred.type === 'key' && <IconDownload onClick={onDownload} />}
                    <IconDelete onClick={() => setDeleteDialog(true)} disabled={!can?.delete} />
                </Table.Cell>
            </Table.Row>

            {
                deleteDialog &&
                <Dialog
                    open={deleteDialog}
                    header={t('credentials.delete')}
                    buttons={[
                        <Button key="cancel" onClick={() => setDeleteDialog(false)} view="outlined">{t('buttons.cancel')}</Button>,
                        <Button key="delete" onClick={onDelete}>{t('buttons.delete')}</Button>
                    ]}
                    onClose={() => setDeleteDialog(false)}>
                    {t('dialogs.delete', {entityName: t('logs.credential')})} <b>{cred?.name}</b>?
                </Dialog>
            }

            {
                keyDialog &&
                <Dialog
                    style={{ width: '40%', left: '30%' }}
                    open={keyDialog}
                    header={t('common.downloadSSH')}
                    buttons={[
                        <Button key="cancel" onClick={() => setKeyDialog(false)} view="outlined">{t('buttons.cancel')}</Button>,
                    ]}
                    onClose={() => setKeyDialog(false)}>
                    <Textarea value={credBody} rows={8} label={cred?.name} style={{ width: '100%' }}/>
                </Dialog>
            }
        </>
    );
};
