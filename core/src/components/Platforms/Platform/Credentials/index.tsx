import React, { useContext } from 'react';
import { useQuery } from '@tanstack/react-query';
import { useTranslation } from 'react-i18next';
import i18next from 'i18next';

import { IError } from '@msa/kit/types';
import { Button,  IconPlus, Loader, Table, Error } from '@msa/kit';

import { PlatformContext } from 'components/Platforms/Platform';
import { ICredential } from 'types';
import { getCredentials } from 'actions';
import { Edit } from './Edit';
import { Row } from './Row';

import css from './credentials.module.scss';


export const Credentials = () => {
    const { platform, can } = useContext(PlatformContext);
    const { t } = useTranslation()

    const { isLoading, data: creds, error } = useQuery<ICredential[], IError>(
        ['platform', platform?.name, 'creds'],
        () => getCredentials(platform?.name)
    );


    if(isLoading)
        return <Loader />;

    if(error)
        return <Error {...error} />;


    return (
        <>
            <div className="layout__header">
                <h1 className="layout__title">{platform?.name} <span>| {i18next.format(t('credentials.credentials'), 'capitalize')}</span></h1>
                <Edit button={<Button iconLeft={<IconPlus />} disabled={!can?.create}>{t('credentials.add')}</Button>}/>
            </div>

            <Table className={css.keys} cells={8}>
                <Table.Header className={css.keyHeader}>
                    <Table.Cell colspan={2}>{t('common.name')}</Table.Cell>
                    {/* <Table.Cell colspan={2}>Tags</Table.Cell> */}
                    <Table.Cell colspan={2}>{t('common.description')}</Table.Cell>
                    <Table.Cell colspan={2}>{t('common.type')}</Table.Cell>
                    <Table.Cell actions={2} />
                </Table.Header>
                {
                    creds &&
                    creds.map((cred: ICredential) => <Row cred={cred} key={cred.UUID} />)
                }
            </Table>
        </>
    );
};
