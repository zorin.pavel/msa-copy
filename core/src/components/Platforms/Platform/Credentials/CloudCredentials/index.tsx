import React, { useCallback, useEffect, useState } from 'react';
import { useQuery } from '@tanstack/react-query';

import { Loader, Error, Table, useTable, sort } from '@msa/kit';
import { IError, OrderDirection } from '@msa/kit/types';

import { ICloudCredential } from 'types';
import { getCloudCredentials } from 'actions';
// import { AddCredentials } from './AddCredentials';
import { Row } from './Row';


interface IProps {
}

export const CloudCredentials = (props: IProps) => {
    const {} = props;
    const { orderedBy, setOrderedBy } = useTable();

    const { isLoading, data, error } = useQuery<ICloudCredential[], IError>(['cloudCredentials'], getCloudCredentials);
    const [credentials, setCredentials] = useState<ICloudCredential[]>(data ?? []);


    useEffect(() => {
        if(data)
            setCredentials(sort(data, 'name', OrderDirection.asc));
    }, [data]);


    const orderBy = useCallback((fieldName: string, orderDirection: OrderDirection) => {
        if(setOrderedBy)
            setOrderedBy({ [fieldName]: orderDirection });

        setCredentials(sort(data ?? [], fieldName, orderDirection));
    }, [data, setOrderedBy]);

    if(isLoading)
        return <Loader layout />;

    if(error)
        return <Error {...error} layout />;


    return (
        <>
            <div className="layout__header">
                <h1 className="layout__title">Cloud credentials</h1>
                {/*<AddCredentials />*/}
            </div>

            <Table cells={15}>
                <Table.Header>
                    <Table.Cell colspan={2}>Cloud <Table.ActionSort fieldName="provider" orderedBy={orderedBy} orderBy={orderBy} /></Table.Cell>
                    <Table.Cell colspan={2}>ID</Table.Cell>
                    <Table.Cell colspan={2}>Name</Table.Cell>
                    <Table.Cell colspan={4}>Description</Table.Cell>
                    <Table.Cell colspan={3}>API Key</Table.Cell>
                    <Table.Cell colspan={1}>Age</Table.Cell>
                    <Table.Cell actions={2} />
                </Table.Header>
                {
                    credentials &&
                    credentials.map((credential) => <Row key={credential.name} credential={credential} />)
                }
            </Table>
        </>
    );
};
