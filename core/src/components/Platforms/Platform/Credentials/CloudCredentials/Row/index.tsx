import React, { useState } from 'react';

import { getEnumValueByKey } from '@msa/kit/types';
import { Button, Dialog, Drawer, IconDelete, IconEdit, Table, Toast } from '@msa/kit';

import { ECloudProvider, ICloudCredential } from 'types';

import css from './row.module.scss';


interface IProps {
    credential: ICloudCredential,
}

export const Row = React.memo((props: IProps) => {
    const { credential } = props;
    const kindValue = getEnumValueByKey(ECloudProvider, credential.provider);


    const [drawer, setDrawer] = useState<boolean>(false);
    const [deleteDialog, setDeleteDialog] = useState<boolean>(false);
    const [submitted, setSubmitted] = useState<boolean>(false);


    const onDelete = () => {
        Toast.error({ message: 'Delete' });
    };


    const onCloseDrawer = () => {
        setDrawer(false);
    };


    const onSubmit = (formData: any) => {
        console.log('formData', formData);
        setSubmitted(true);
    };


    return (
        <>
            <Table.Row key={credential.id} className={css.row} valign="center">
                <Table.Cell colspan={2}>
                    <div className={css.rowHeader}>
                        <img src={`/icons/${credential.provider}.svg`} alt={kindValue} className={css.rowLogo}/>
                        <span>{kindValue}</span>
                    </div>
                </Table.Cell>
                <Table.Cell colspan={2}>{credential.id}</Table.Cell>
                <Table.Cell colspan={2}>{credential.name}</Table.Cell>
                <Table.Cell colspan={4}>{credential.description}</Table.Cell>
                <Table.Cell colspan={3} nowrap>{credential.apiKey}</Table.Cell>
                <Table.Cell colspan={1}>{credential.age}</Table.Cell>
                <Table.Cell actions={2}>
                    <IconEdit onClick={() => setDrawer(true)} />
                    <IconDelete onClick={() => setDeleteDialog(true)} />
                </Table.Cell>
            </Table.Row>

            <Drawer header={`Edit ${kindValue} ${credential.id}`} open={drawer} onClose={onCloseDrawer} backdrop>
                <Drawer.Body>
                    Edit form
                </Drawer.Body>
                <Drawer.Footer>
                    <Button type="reset" view="outlined" color="red" onClick={onCloseDrawer}>Cancel</Button>
                    <Button disabled={false} loading={submitted} type="submit" color="red" onClick={onSubmit}>Done</Button>
                </Drawer.Footer>
            </Drawer>
            
            {
                deleteDialog &&
                <Dialog
                    open={deleteDialog}
                    header="Delete Cloud credentials"
                    buttons={[
                        <Button key="cancel" onClick={() => setDeleteDialog(false)} view="outlined">Cancel</Button>,
                        <Button key="delete" onClick={onDelete}>Delete</Button>
                    ]}
                    onClose={() => setDeleteDialog(false)}>
                    Do you really want delete credential <b>{credential.name}</b>?
                </Dialog>
            }
        </>
    );
});
