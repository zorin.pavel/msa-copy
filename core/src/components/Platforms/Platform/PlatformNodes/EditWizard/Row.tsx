import React, { useContext, useEffect, useState } from 'react';
import { useQuery } from '@tanstack/react-query';
import { Controller, useFormContext, useWatch } from 'react-hook-form';

import type { IError, IOption } from '@msa/kit/types';
import { Button, Checkbox, IconDelete, IconPlus, Input, Select, Table, Textarea } from '@msa/kit';

import settings from 'settings';
import { ICredential } from 'types';
import { getCredentials } from 'actions';
import { PlatformContext } from 'components/Platforms/Platform';
import { Edit } from 'components/Platforms/Platform/Credentials/Edit';

import css from './nodes.module.scss';


interface IProps {
    index: number,
    edit?: boolean,
    remove: (index?: number | number[]) => void,
}

export const Row = (props: IProps) => {
    const { index, remove, edit } = props;
    const { platform } = useContext(PlatformContext);

    const { isLoading: dataKeysLoading, data: dataKeys } = useQuery<ICredential[], IError>(
        ['platform', platform?.name, 'creds'],
        () => getCredentials(platform?.name)
    );

    const [credsOptions, setCredsOptions] = useState<IOption[]>([]);

    useEffect(() => {
        if(dataKeys) {
            setCredsOptions(dataKeys.map((key: ICredential) => ({ label: key.name, value: key.name })));
        }
    }, [dataKeys]);


    const {
        control,
        setValue,
        formState: { errors },
    } = useFormContext();


    const nodes = useWatch({
        name: 'nodes',
        control
    });

    const uploadKey = useWatch({
        name: `nodes.${index}.upload_key`,
        control
    });


    useEffect(() => {
        setValue(`nodes.${index}.password`, '', { shouldValidate: true, shouldTouch: true });
    }, [uploadKey, setValue, index]);


    if(!platform)
        return null;


    return (
        <Table.Row valign="center">
            <Table.Cell colspan={1}>{index + 1}</Table.Cell>
            <Table.Cell colspan={3}>
                <Controller
                    control={control}
                    name={`nodes.${index}.ip`}
                    rules={
                        {
                            required: 'Node IP is required',
                            pattern: {
                                value: settings.IP_REGEX,
                                message: 'IP is not valid'
                            },
                        }
                    }
                    render={({ field }) => (
                        <Input
                            {...field}
                            required
                            placeholder="0.0.0.0"
                            readonly={edit}
                            // @ts-ignore
                            error={errors.nodes && errors.nodes[index]?.ip}
                            autoComplete="off" />
                    )} />
            </Table.Cell>
            <Table.Cell colspan={3}>
                <Controller
                    control={control}
                    name={`nodes.${index}.name`}
                    rules={
                        {
                            required: 'Node name is required',
                            pattern: {
                                value: settings.NAME_REGEX,
                                message: 'Node name must consist of lowercase alphanumeric characters'
                            }
                        }
                    }
                    render={({ field }) => (
                        <Input
                            {...field}
                            required
                            // @ts-ignore
                            error={errors.nodes && errors.nodes[index]?.name}
                            autoComplete="off" />
                    )} />
            </Table.Cell>
            <Table.Cell colspan={4}>
                <Controller
                    control={control}
                    name={`nodes.${index}.credential_name`}
                    rules={
                        {
                            required: 'Credential name is required',
                        }
                    }
                    render={({ field }) => (
                        <Select
                            {...field}
                            required
                            // @ts-ignore
                            error={errors.nodes && errors.nodes[index]?.credential_name}
                            options={credsOptions}
                            loading={dataKeysLoading}
                            addonRight={<Edit button={<Button icon={<IconPlus/>}/>} />} />
                    )} />
            </Table.Cell>
            <Table.Cell colspan={3}>
                <Controller
                    control={control}
                    defaultValue=""
                    name={`nodes.${index}.description`}
                    render={({ field }) => (
                        <Textarea
                            {...field}
                            className={css.textarea}
                            rows={1}
                        />
                    )} />
            </Table.Cell>
            {/* <Table.Cell colspan={3}>
                <Controller
                    control={control}
                    name={`nodes.${index}.tags`}
                    render={({ field }) => (
                        <Textarea
                            {...field}
                            className={css.textarea}
                            rows={1}
                        />
                    )} />
            </Table.Cell> */}
            <Table.Cell colspan={2}>
                <Controller
                    control={control}
                    name={`nodes.${index}.upload_key`}
                    render={({ field }) => (
                        <Checkbox
                            {...field}
                            checked={!!field.value}
                        />
                    )} />
            </Table.Cell>
            <Table.Cell colspan={3}>
                <Controller
                    control={control}
                    defaultValue=""
                    name={`nodes.${index}.password`}
                    rules={{
                        required: nodes[index]?.upload_key ? 'Password is required' : false,
                    }}
                    render={({ field }) => (
                        <Input
                            {...field}
                            required={!!nodes[index]?.upload_key}
                            type="password"
                            disabled={!nodes[index]?.upload_key}
                            // @ts-ignore
                            error={errors.nodes && errors.nodes[index]?.password}
                            autoComplete="off" />
                    )} />
            </Table.Cell>
            <Table.Cell actions={1}>
                {
                    !edit &&
                    <IconDelete disabled={!index} onClick={() => remove(index)} />
                }
            </Table.Cell>
        </Table.Row>
    );
};
