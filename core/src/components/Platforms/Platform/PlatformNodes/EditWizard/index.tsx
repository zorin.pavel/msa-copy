import React, { ReactElement, useContext,  useState } from 'react';
import { FormProvider, useFieldArray, useForm } from 'react-hook-form';
import {  useQueryClient } from '@tanstack/react-query';
import { useTranslation } from 'react-i18next';

import { Button, Drawer, IconEdit, IconPlus, Table, Toast } from '@msa/kit';
import { useSocket } from '@msa/host';

import type { IPlatformNode } from 'types';
import {  putPlatformNode, postPlatformNode } from 'actions';
import { PlatformContext } from 'components/Platforms/Platform';
import { Row } from './Row';

import { env } from 'env';

import css from './nodes.module.scss';


interface IProps {
    node?: IPlatformNode
    button?: ReactElement,
    disabled?: boolean,
}

export const Edit = (props: IProps) => {
    const { node, button, disabled } = props;
    const { platform } = useContext(PlatformContext);
    const queryClient = useQueryClient();
    const { t } = useTranslation()

    const [submitted, setSubmitted] = useState<boolean>(false);
    const [drawer, setDrawer] = useState<boolean>(false);

    if(node && !node?.credential) node.credential = null;

    useSocket(
        { tenant: env.MSA_DEFAULT_TENANT, platform: platform?.name, object_type: 'node', type: 'info', state: 'Failed' },
        {
            toast: true
        }
    );

    const defaultValues = node ?
        {
            nodes: [{
                ip: node.ip,
                name: node.name,
                description: node.description ?? '',
                credential_name: node.credential?.name,
                password: node.password ?? '',
                upload_key: node.upload_key ?? false,
            }]
        } :
        {
            nodes: [{
                ip: '',
                name: '',
                description: '',
                credential_name: '',
                password: '',
                upload_key: false,
            }]
        };


    const formMethods = useForm({
        mode: 'onChange',
        reValidateMode: 'onChange',
        defaultValues,
    });

    const { fields, append, remove } = useFieldArray({
        control: formMethods.control,
        name: 'nodes'
    });    

    const addNode = () => {
        append({
            ip: '',
            name: '',
            description: '',
            credential_name: '',
            password: '',
            upload_key: false
        });
    };

    const onReset = () => {
        formMethods.reset(defaultValues);
    };    

    const onSubmit = (formData: any) => {
        setSubmitted(true);

        if(node) {
            return putPlatformNode(platform?.name, node.name, formData.nodes[0])
                .then((response: Record<string, any> & IPlatformNode) => {
                    setDrawer(false);
                    onReset();

                    if(response.ip)
                        Toast.success({ message: t('toasts.edit', { entity: t('nodes.count', { context: 'few' }), entityName: response.ip }) });
                    else // response.status 206
                        Toast.warning({ message: t('toasts.editProgress', { entity: t('nodes.count', { context: 'few' })}) });

                    queryClient.invalidateQueries(['platform', platform?.name, 'nodes']);
                })
                .catch(() => {

                    return false;
                })
                .finally(() => setSubmitted(false));
        } else {
            return postPlatformNode(platform?.name, formData.nodes)
                .then((response: Record<string, any> & IPlatformNode) => {
                    setDrawer(false);
                    onReset();
                    if(response.ip)
                        Toast.success({ message: t('toasts.add', { entity: t('nodes.count', { context: 'few' }), entityName: response.ip }) });
                    else // response.status 206
                    Toast.warning({ message: t('toasts.addProgress', { entity: t('nodes.count', { context: 'few' })}) });

                    queryClient.invalidateQueries(['platform', platform?.name, 'nodes']);
                })
                .catch(() => { 
                    return false;
                })
                .finally(() => setSubmitted(false));
        }
    };

    let drawerHeader = t('nodes.add', {context: "few"});
    if(node)
        drawerHeader = t('nodes.edit', { context: 'few' }) + ' ' + node.name;


    return (
        <>
            {
                button ?
                    React.cloneElement(button, { onClick: () => setDrawer(true), disabled }) :
                    <IconEdit onClick={() => setDrawer(true)} disabled={disabled} />
            }

            {
                <Drawer header={drawerHeader} open={drawer} onClose={() => setDrawer(false)} wide backdrop>
                    <form onSubmit={formMethods.handleSubmit(onSubmit)} noValidate onReset={onReset}>
                        <FormProvider {...formMethods}>
                            <Drawer.Body>
                                <>
                                    <Table cells={20} className={css.nodes}>
                                        <Table.Header >
                                            <Table.Cell colspan={1}>#</Table.Cell>
                                            <Table.Cell colspan={3}>IP</Table.Cell>
                                            <Table.Cell colspan={3}>{t('common.name')}</Table.Cell>
                                            <Table.Cell colspan={4}>{t('credentials.name')}</Table.Cell>
                                            <Table.Cell colspan={3}>{t('common.description')}</Table.Cell>
                                            <Table.Cell colspan={2}>{t('common.uploadSSH')}</Table.Cell>
                                            <Table.Cell colspan={3}>{t('common.password')}</Table.Cell>
                                            <Table.Cell actions={1}></Table.Cell>
                                        </Table.Header>
                                        {
                                            fields.map((field, index) => <Row key={index} index={index} remove={remove} edit={!!node} />)
                                        }
                                    </Table>
                                    <div className={css.nodesButtons}>
                                        {
                                            !node &&
                                            <Button view="outlined" icon={<IconPlus />} size="small" className={css.buttonLong} onClick={addNode} />
                                        }
                                    </div>
                                </>
                            </Drawer.Body>
                        </FormProvider>
                        <Drawer.Footer>
                            <Button type="reset" view="outlined" color="red" onClick={onReset}>{t('buttons.clear')}</Button>
                            <Button disabled={!formMethods.formState.isValid}  loading={submitted} type="submit" color="red" onClick={formMethods.handleSubmit(onSubmit)}>{t('buttons.done')}</Button>
                        </Drawer.Footer>
                    </form>
                </Drawer>
            }
        </>
    );
};
