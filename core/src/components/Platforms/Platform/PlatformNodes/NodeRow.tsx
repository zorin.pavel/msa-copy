import React, { useContext, useState } from 'react';
import classNames from 'classnames';
import { useQueryClient } from '@tanstack/react-query';
import { Link } from 'react-router-dom';
import { useTranslation } from 'react-i18next';

import { IError } from '@msa/kit/types';
import {
    Button, Dialog, IconCluster,
    IconDelete, IconInfrastructure, IconNode,
    Input, Image, Table, Toast, StatusIcon
} from '@msa/kit';

import type { IPlatformNode } from 'types';
import { deletePlatformNode, putPlatformNode } from 'actions';
import { PlatformContext } from 'components/Platforms/Platform';
import { Edit } from './EditWizard';

import css from './nodes.module.scss';


interface IProps {
    node: IPlatformNode,
}

export const NodeRow = (props: IProps) => {
    const { node } = props;
    const { platform, can } = useContext(PlatformContext);
    const queryClient = useQueryClient();
    const { t } = useTranslation()

    const [deleteDialog, setDeleteDialog] = useState<boolean>(false);
    const [passwordDialog, setPasswordDialog] = useState<boolean>(false);
    const [currentNode, setCurrentNode] = useState<IPlatformNode>({ ...node, credential: node.credential, upload_key: true });
    const [submitted, setSubmitted] = useState<boolean>(false);


    const onDelete = () => {
        setSubmitted(true);

        return deletePlatformNode(platform?.name, node.name)
            .then(() => {
                setDeleteDialog(false);
                Toast.success({ message: t('toasts.delete', { entity: t('nodes.node', { context: 'single' }), entityName: node.ip }) });
                Toast.success({ message: `<b>${node.ip}</b> delete in progress` });
                queryClient.invalidateQueries(['platform', platform?.name, 'nodes']);
            })
            .catch(error => {
                Toast.error({ message: error.message });

                return false;
            })
            .finally(() => setSubmitted(false));
    };


    const putNode = () => {
        setSubmitted(true);

        return putPlatformNode(platform?.name, currentNode.name, currentNode)
            .then((response: IPlatformNode) => {
                setPasswordDialog(false);
                Toast.success({ message: `<b>${response.ip}</b> was successfully updated` });
                queryClient.invalidateQueries(['platform', platform?.name, 'nodes']);
            })
            .catch((error: IError) => {
                Toast.error({ message: error.message });

                return false;
            })
            .finally(() => setSubmitted(false));
    };


    if(!platform)
        return null;


    return (
        <Table.Row valign="center" className={classNames('entity__row', css.nodeRow, !node.is_online && css.fail)}>
            <Table.Cell actions={1} className={css.os}>
                <Image src={`/icons/${node.os_family?.toLowerCase()}.svg`} fallBack={<IconNode />} alt="os" className={css.image} />
            </Table.Cell>
            <Table.Cell colspan={2}>{node.os_version ? `${node.os_version}` : ''}</Table.Cell>
            <Table.Cell colspan={3}>{node.kernel ? `${node.kernel}` : ''}</Table.Cell>
            <Table.Cell colspan={2}>{node.ip}</Table.Cell>
            <Table.Cell colspan={2}>{node.name}</Table.Cell>
            <Table.Cell colspan={3}>{node.description}</Table.Cell>
            <Table.Cell colspan={2} style={{ flexDirection: 'column' }}>
                {node.cpu && <div>{`${node.cpu} CPU `}</div>}
                {node.mem && <div>{`${node.mem} GB RAM `}</div>}
                {node.disk && <div>{`${node.disk} GB DISK `}</div>}
            </Table.Cell>
            <Table.Cell colspan={3} className={css.belongs}>
                {
                    node.cluster &&
                    <>
                        <IconCluster />
                        <Link to={`/platforms/${platform.name}/clusters/${node.cluster.name}/nodes`}>{node.cluster.name}</Link>
                    </>
                }
                {
                    node.infrastructure &&
                    <>
                        <IconInfrastructure />
                        <Link to={`/platforms/${platform.name}/infrastructures/${node.infrastructure.name}/nodes`}>{node.infrastructure.name}</Link>
                    </>
                }
            </Table.Cell>
            <Table.Cell colspan={2}>
                {
                    node.credential ?
                        node.credential.name :
                        ''
                }
            </Table.Cell>
            <Table.Cell align="center">
                <StatusIcon status={node.is_online ? 'Success' : 'Fail'} caption={false} />
            </Table.Cell>
            <Table.Cell actions={3}>
                <Edit node={node} disabled={!can?.edit} />
                {/*<IconShield onClick={() => setPasswordDialog(true)} disabled={!node.credential} />*/}
                <IconDelete onClick={() => setDeleteDialog(true)} disabled={!can?.delete} />
            </Table.Cell>

            {
                deleteDialog &&
                <Dialog open={deleteDialog} header={t('nodes.delete', { context: 'single' })} buttons={[
                    <Button key="cancel" onClick={() => setDeleteDialog(false)} view="outlined">{t('buttons.cancel')}</Button>,
                    <Button key="delete" onClick={onDelete} loading={submitted}>{t('buttons.delete')}</Button>
                ]} onClose={() => setDeleteDialog(false)}>
                    {t('dialogs.delete', {entityName: t('nodes.count', { context: 'single' })})} <b>{node.ip}</b>?
                </Dialog>
            }

            {
                passwordDialog &&
                <Dialog open={passwordDialog} header={t('common.uploadKey')} buttons={[
                    <Button key="cencel" onClick={() => setPasswordDialog(false)} view="outlined">{t('buttons.cancel')}</Button>,
                    <Button key="push" onClick={putNode} loading={submitted}>{t('buttons.uploadKey')}</Button>
                ]} onClose={() => setPasswordDialog(false)}>
                    <div className="section__header">{node.credential ? node.credential.name : 'credential'}</div>

                    <Input
                        value={node.credential ? node.credential.name : 'credential'}
                        name="username"
                        type="text"
                        // readOnly={!!node.credential.name}
                        label={t('common.username')}
                        labelPosition="left"
                        autoComplete="off" />
                    <Input
                        value={node?.password}
                        name="password"
                        type="password"
                        label={t('common.password')}
                        labelPosition="left"
                        required
                        autoComplete="off"
                        onChange={(e) => setCurrentNode(node => {
                            if(node)
                                return { ...node, password: e.target.value };

                            return node;
                        })}
                    />
                </Dialog>
            }
        </Table.Row>
    );
};
