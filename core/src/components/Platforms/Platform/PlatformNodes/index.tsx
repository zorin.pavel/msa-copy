import React, { useCallback, useContext, useEffect, useState } from 'react';
import { useQuery, useQueryClient } from '@tanstack/react-query';
import { useTranslation } from 'react-i18next';
import i18next from 'i18next';

import { IError, OrderDirection } from '@msa/kit/types';
import { Button, Error, IconPlus, Loader, sort, Table, useTable } from '@msa/kit';

import { useSocket } from '@msa/host';

import { env } from 'env';
import type { IPlatformNode } from 'types';
import { getPlatformNodes } from 'actions';
import { PlatformContext } from 'components/Platforms/Platform';
import { NodeRow } from './NodeRow';
import { Edit } from './EditWizard';

import css from './nodes.module.scss';


export const PlatformNodes = () => {
    const queryClient = useQueryClient();
    const { platform, can } = useContext(PlatformContext);
    const { t } = useTranslation()

    useSocket(
        { tenant: env.MSA_DEFAULT_TENANT, platform: platform?.name, object_type: 'node', type: 'status' },
        {
            callback: () => queryClient.invalidateQueries(['platform', platform?.name, 'nodes']),
            toast: true
        }
    );

    const { isLoading, data, error } = useQuery<IPlatformNode[], IError>(
        ['platform', platform?.name, 'nodes'],
        () => getPlatformNodes(platform?.name)
    );

    const { orderedBy, setOrderedBy } = useTable();
    const [nodes, setNodes] = useState<IPlatformNode[]>(data ?? []);


    useEffect(() => {
        if(data)
            setNodes(sort<IPlatformNode[]>(data, 'name', OrderDirection.asc));
    }, [data, setNodes]);


    const orderBy = useCallback((fieldName: string, orderDirection: OrderDirection) => {
        if(setOrderedBy)
            setOrderedBy({ [fieldName]: orderDirection });

        setNodes(sort<IPlatformNode[]>(data ?? [], fieldName, orderDirection));
    }, [data, setNodes, setOrderedBy]);


    // const orderByUsed = useCallback((fieldName: string, orderDirection: OrderDirection) => {
    //     if(setOrderedBy)
    //         setOrderedBy({ [fieldName]: orderDirection });

    //     setNodes(sort<IPlatformNode[]>(data ?? [], ['infrastructure', 'cluster'], orderDirection));
    // }, [data]);


    if(isLoading)
        return <Loader layout />;

    if(error)
        return <Error {...error} layout />;


    return (
        <>
            <div className="layout__header">
                <h1 className="layout__title">{platform?.name} <span>| {i18next.format(t('nodes.nodes'), 'capitalize')}</span></h1>
                <Edit button={<Button iconLeft={<IconPlus />}>{t('nodes.add', {context: "few"})}</Button>} disabled={!can?.create} />
            </div>

            <Table cells={22} className={css.nodes}>
                <Table.Header>
                    <Table.Cell actions={1} />
                    <Table.Cell colspan={2}>{t('common.version')}<Table.ActionSort fieldName="os_version" orderedBy={orderedBy} orderBy={orderBy} /></Table.Cell>
                    <Table.Cell colspan={3}>{t('common.kernel')} <Table.ActionSort fieldName="kernel" orderedBy={orderedBy} orderBy={orderBy} /></Table.Cell>
                    <Table.Cell colspan={2}>IP <Table.ActionSort fieldName="ip" orderedBy={orderedBy} orderBy={orderBy} /></Table.Cell>
                    <Table.Cell colspan={2}>{t('common.name')}<Table.ActionSort fieldName="name" orderedBy={orderedBy} orderBy={orderBy} /></Table.Cell>
                    <Table.Cell colspan={3}>{t('common.description')}</Table.Cell>
                    <Table.Cell colspan={2}>{t('common.params')}</Table.Cell>
                    <Table.Cell colspan={3}>{t('common.usedBy')}</Table.Cell>
                    <Table.Cell colspan={2}>{t('credentials.credentials')}</Table.Cell>
                    <Table.Cell colspan={1}>{t('common.online')}</Table.Cell>
                    <Table.Cell actions={3} />
                </Table.Header>
                {
                    nodes &&
                    nodes.map((node: IPlatformNode) => <NodeRow key={node.name} node={node} />)
                }
            </Table>
        </>
    );
};
