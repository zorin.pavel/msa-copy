import React from 'react';
import { Outlet, useMatch } from 'react-router-dom';
import { useQuery } from '@tanstack/react-query';
import { useTranslation } from 'react-i18next';

import type { IError } from '@msa/kit/types';
import { Authorized, Button, Error, IconPlus, Loader } from '@msa/kit';

import type { IPlatform } from 'types';
import { getPlatforms } from 'actions';
import { PlatformRow } from './PlatformRow';
import { Edit } from './Edit';
import { Placeholder } from './Placeholder';

import css from './platforms.module.scss';

export const Platforms = () => {
    const match = useMatch('/platforms/:children/*');
    const { t } = useTranslation()

    const { isLoading, data, error } = useQuery<IPlatform[], IError>(['platform'], getPlatforms);

    if(isLoading)
        return <Loader layout caption="Fetching platforms..."/>;

    if(error)
        return <Error {...error} />;


    if(!data || !data.length)
        return <Placeholder />;

    if(match)
        return <Outlet />;

    return (
        <>
            <div className="layout__header">
                <h1 className="layout__title">{t('platforms.platforms')}</h1>
                <Authorized permissions={{ platforms: ['create'] }}>
                    <Edit button={<Button iconLeft={<IconPlus />} label={t('platforms.add', { context: 'single' })} />} />
                </Authorized>
            </div>

            <div className={css.platforms}>
                {
                    data &&
                    data.map((platform) => <PlatformRow key={platform.name} platform={platform} />)
                }
            </div>
        </>
    );
};
