import React, { useState } from 'react';
import { useNavigate } from 'react-router-dom';
import { useQuery, useQueryClient } from '@tanstack/react-query';
import { useTranslation } from 'react-i18next';
import classNames from 'classnames';

import { IError, IRbacGroupObject } from '@msa/kit/types';
import {
    IconDelete,
    IconInfrastructure,
    IconCluster,
    IconNode,
    Button,
    Dialog,
    Toast,
    Avatar, useObjectPermissions
} from '@msa/kit';

import { IPlatform } from 'types';
import { deletePlatform, loadPlatformPermissions } from 'actions';
import { EditPermissions } from 'components/EditPermissions';
import { Edit } from '../Edit';

import css from './platform.module.scss';


interface IProps {
    platform: IPlatform,
}

export const PlatformRow = React.memo((props: IProps) => {
    const { platform } = props;
    const navigate = useNavigate();
    const queryClient = useQueryClient();
    const { t } = useTranslation()

    const navigationPath = `/platforms/${platform.name}`;

    const { data: platformPermissions } = useQuery<IRbacGroupObject[], IError>(
        ['platform', platform.name, 'permissions'],
        () => loadPlatformPermissions(platform.name),
        {
            enabled: !!platform
        }
    );

    const [deleteDialog, setDeleteDialog] = useState<boolean>(false);

    const canGetPlatform = useObjectPermissions(platformPermissions, 'platform', ['get']);
    const canDeletePlatform = useObjectPermissions(platformPermissions, 'platform', ['delete']);
    const canEditPlatform = useObjectPermissions(platformPermissions, 'platform', ['edit']);
    const clustersCount = platform.clusters?.length ?? 0;
    const infraCount = platform.infrastructures?.length ?? 0;
    const nodesCount = platform.nodes?.length ?? 0

    const onDelete = () => {
        return deletePlatform(platform.name)
            .then(() => {
                Toast.success({ message: t('toasts.delete', { entity: t('platforms.platform', { context: 'edit' }), entityName: platform.name }) });
                queryClient.invalidateQueries(['platform']);
            })
            .catch(error => {
                let message = error.message;

                if(error.detail) {
                    message = t('toasts.deleteErrorResources', { entity: t('platforms.platform', { context: 'one' }), entityName: platform.name });

                    if(error.detail.clusters.length)
                        message += `<br />kubernetes_clusters: <b>${error.detail.clusters.join(', ')}</b>`;
                    if(error.detail.infrastructures.length)
                        message += `<br />infra_services: <b>${error.detail.infrastructures.join(', ')}</b>`;
                }

                Toast.error({ message });

                return false;
            });
    };


    return (
        <>
            <div className={css.platform}>
                <div className={css.platformHeader}>
                    <h3 className={classNames(css.platformName, canGetPlatform && css.active)} onClick={() => canGetPlatform && navigate(navigationPath)}>{platform.name}</h3>
                    <div className={css.platformActions}>
                        <EditPermissions permissions={platformPermissions} platformName={platform.name} disabled={!canEditPlatform} />
                        <Edit platform={platform} disabled={!canEditPlatform} />
                        <IconDelete onClick={() => setDeleteDialog(true)} disabled={!canDeletePlatform} />
                    </div>
                </div>
                <div className={classNames(css.platformBody, canGetPlatform && css.active)} onClick={() => canGetPlatform && navigate(navigationPath)}>
                    <div className={css.platformDescription}>
                        <Avatar name={platform.name} />
                        <span>{platform.description}</span>
                    </div>
                    <div className={css.platformOptions}>
                        <div className={css.option}>
                            <div className={css.optionCount}>{infraCount}</div>
                            <div className={css.optionIcon}><IconInfrastructure /> {t('infrastructures.count', {count: infraCount})}</div>
                        </div>
                        <div className={css.option}>
                            <div className={css.optionCount}>{clustersCount}</div>
                            <div className={css.optionIcon}><IconCluster /> {t('clusters.count', {count: clustersCount})}</div>
                        </div>
                        <div className={css.option}>
                            <div className={css.optionCount}>{nodesCount}</div>
                            <div className={css.optionIcon}><IconNode /> {t('nodes.count', {count: nodesCount})}</div>
                        </div>
                    </div>
                </div>
            </div>

            {
                deleteDialog &&
                <Dialog
                    open={deleteDialog}
                    header={t('platforms.delete', { context: 'single' })}
                    buttons={[
                        <Button key="cancel" onClick={() => setDeleteDialog(false)} view="outlined">{t('buttons.cancel')}</Button>,
                        <Button key="delete" onClick={onDelete}>{t('buttons.delete')}</Button>
                    ]}
                    onClose={() => setDeleteDialog(false)}>
                    {t('dialogs.delete', {entityName: t('platforms.platform', { context: 'single' })})} <b>{platform.name}</b>?
                </Dialog>
            }
        </>
    );
});
