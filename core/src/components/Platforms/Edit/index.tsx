import React, { ReactElement, useCallback, useEffect, useState } from 'react';
import { Controller, useForm } from 'react-hook-form';
import { useQuery, useQueryClient } from '@tanstack/react-query';
import { useKeycloak } from '@react-keycloak/web';
import { useTranslation } from 'react-i18next';
import i18next from 'i18next';

import { getEnumKeyByValue, IError, IOption } from '@msa/kit/types';
import { Button, Drawer, IconEdit, Input, MultiSelect, Textarea, Toast } from '@msa/kit';

import settings from 'settings';
import { EPlatformKind, IPlatform } from 'types';
import { getGroupsList, postPlatform, putPlatform } from 'actions';


interface IProps {
    disabled?: boolean,
    platform?: IPlatform,
    button?: ReactElement
}

export const Edit = (props: IProps) => {
    const { platform, button, disabled } = props;
    const queryClient = useQueryClient();
    const { keycloak } = useKeycloak();
    const { t } = useTranslation();

    const { data: groups } = useQuery<string[], IError>(
        ['group', 'list'],
        getGroupsList
    );

    const isAdmin = keycloak.tokenParsed?.groups.some((el: string) => el === 'admin');
    let groupsOptions: IOption[] = []

    if(isAdmin && groups) {
        groupsOptions = groups.map((group: string) => ({ label: group, value: group }));
    } else {
        groupsOptions = keycloak.tokenParsed?.groups.map((group: string) => ({ label: group, value: group }));
    }


    const defaultValues: IPlatform = platform ?? {
        name: '',
        description: '',
        groups: '',
        kind: getEnumKeyByValue(EPlatformKind, EPlatformKind.baremetal),
    };

    const {
        control,
        setValue,
        handleSubmit,
        reset,
        formState: { errors, isValid },
    } = useForm({
        mode: 'onChange',
        reValidateMode: 'onChange',
        defaultValues
    });

    const [submitted, setSubmitted] = useState<boolean>(false);
    const [drawer, setDrawer] = useState<boolean>(false);


    useEffect(() => {
        if(groups)
            setValue('groups', keycloak.tokenParsed?.groups, { shouldValidate: true, shouldTouch: true });
    }, [groups]);


    const onSubmit = useCallback((formData: IPlatform) => {
        setSubmitted(true);

        if(typeof formData.groups === 'string') {
            formData.groups = formData.groups.split(',').filter(a => a !== '');
        }

        if(platform) {
            return putPlatform(platform.name, formData)
                .then((response: IPlatform) => {
                    setDrawer(false);
                    Toast.success({ message: t('toasts.edit', { entity: t('platforms.platform', { context: 'edit' }), entityName: response.name }) });
                    queryClient.invalidateQueries(['platform']);
                })
                .catch((error: IError) => {
                    Toast.error({ message: error.message });

                    return false;
                })
                .finally(() => {
                    setSubmitted(false);
                });
        } else {
            return postPlatform(formData)
                .then((response: IPlatform) => {
                    setDrawer(false);
                    Toast.success({ message: t('toasts.add', { entity: t('platforms.platform', { context: 'edit' }), entityName: response.name }) });
                    queryClient.invalidateQueries(['platform']);
                })
                .catch((error: IError) => {
                    Toast.error({ message: error.message });

                    return false;
                })
                .finally(() => {
                    setSubmitted(false);
                });
        }
    }, [platform, queryClient]);


    const onReset = () => {
        reset(defaultValues);
    };


    let drawerHeader = t('platforms.new', {context: 'one'});
    if(platform)
        drawerHeader = t('platforms.edit', { context: 'single' }) + ' ' + platform.name;

    return (
        <>
            {
                button ?
                    React.cloneElement(button, { onClick: () => setDrawer(true), disabled }) :
                    <IconEdit onClick={() => setDrawer(true)} disabled={disabled} />
            }

            {
                drawer &&
                <Drawer header={drawerHeader} open={drawer} onClose={() => setDrawer(false)} backdrop>
                    <form onSubmit={handleSubmit(onSubmit)} noValidate onReset={onReset}>
                        <Drawer.Body>
                            <Controller
                                control={control}
                                name="name"
                                rules={
                                    {
                                        required: t('validation.isRequired', { entityName: t('platforms.name' )}),
                                        pattern: {
                                            value: settings.NAME_REGEX,
                                            message: t('validation.alphanumericCharacters', { entityName: i18next.format(t('platforms.name'), 'capitalize')})
                                        }
                                    }
                                }
                                render={({ field }) => (
                                    <Input
                                        {...field}
                                        label={i18next.format(t('platforms.name'), 'capitalize')}
                                        placeholder={i18next.format(t('platforms.name'), 'capitalize')}
                                        labelPosition="left"
                                        required
                                        error={errors.name && errors.name.message as string}
                                        displayError
                                        autoComplete="off" />
                                )} />
                            <Controller
                                control={control}
                                name="description"
                                render={({ field }) => (
                                    <Textarea
                                        {...field}
                                        label={t('common.description')}
                                        placeholder={t('common.description')}
                                        labelPosition="left"
                                        rows={5}
                                        error={!!errors.description} />
                                )} />
                            {
                                !platform &&
                                <Controller
                                    control={control}
                                    name="groups"
                                    rules={
                                        {
                                            required: t('validation.isRequired', { entityName: t('common.group' )}),
                                        }
                                    }
                                    render={({ field }) => (
                                        <MultiSelect
                                            {...field}
                                            required
                                            label={t('common.groups')}
                                            labelPosition="left"
                                            error={!!errors.groups}
                                            options={groupsOptions}
                                            maxBadges={10} />
                                    )} />
                            }
                        </Drawer.Body>
                        <Drawer.Footer>
                            <Button type="reset" view="outlined" color="red" onClick={onReset}>{t('buttons.clear')}</Button>
                            <Button disabled={!isValid} loading={submitted} type="submit" onClick={handleSubmit(onSubmit)}>{t('buttons.done')}</Button>
                        </Drawer.Footer>
                    </form>
                </Drawer>
            }
        </>
    );
};
