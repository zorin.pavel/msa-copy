import React from 'react';
import css from './placeholder.module.scss';

import { Authorized, Button, IconPlus, isAuthorized } from '@msa/kit';

import { Edit } from '../Edit';


export const Placeholder = () => {
    return (
        <>
            <div className="layout__header">
                <h1 className="layout__title">Platforms</h1>
            </div>
            <div className={css.platformPlaceholder}>
                <div className={css.text}>
                    Create your first platform<br/>to start exploring the service and benefit from all its capabilities
                </div>
                <Edit button={<Button iconLeft={<IconPlus />} disabled={!isAuthorized({ platforms: ['create'] })} label="Create Platform" />} />
                <Authorized not permissions={{ platforms: ['create'] }}>
                    <p>
                        <i>You don't have permissions to create platforms</i>
                    </p>
                </Authorized>
            </div>
        </>
    );
};
