import React from 'react';
import { Repos } from '../Repos';


export const Docker = () => {
    const repoType = 'docker';

    return (
        <Repos repoType={repoType} />
    );
};
