import React, { useContext } from 'react';
import { useQuery } from '@tanstack/react-query';
import { useTranslation } from 'react-i18next';

import { IError } from '@msa/kit/types';
import { Error, Loader, Table, ucFirst } from '@msa/kit';

import type { IRepository } from 'types';
import { getVendorRepos, getPlatformRepos } from 'actions';
import { PlatformContext } from 'components/Platforms/Platform';
import { Edit } from './Edit';
import { RepoRow } from './RepoRow';

import css from './repos.module.scss';


interface IProps {
    repoType: IRepository['type'],
}

export const Repos = (props: IProps) => {
    const { repoType } = props;
    const { platform, can } = useContext(PlatformContext);
    const { t } = useTranslation()

    let keys = ['repos', repoType];
    let queryFunction = () => getVendorRepos(repoType);

    if(platform) {
        keys = ['platform', platform?.name, 'repos', repoType];
        queryFunction = () => getPlatformRepos(platform.name, repoType);
    }

    const { isLoading, data: repos, error } = useQuery<IRepository[], IError>(keys, queryFunction);

    if(isLoading)
        return <Loader />;

    if(error)
        return <Error {...error} />;


    return (
        <>
            <div className="layout__header">
                <h1 className="layout__title">{ucFirst(repoType)} {t('common.repositories')}</h1>
                <Edit repoType={repoType} platform={platform} disabled={platform && !can?.create} />
            </div>

            <Table className={css.repos} cells={6}>
                <Table.Header className={css.repoHeader}>
                    <Table.Cell colspan={2}>{t('common.name')}</Table.Cell>
                    <Table.Cell colspan={3}>Url</Table.Cell>
                    <Table.Cell actions={3} className={css.actions} />
                </Table.Header>
                {
                    repos &&
                    repos.map((repo: IRepository) => <RepoRow repo={repo} key={repo.UUID} repoType={repoType} />)
                }
            </Table>
        </>
    );
};
