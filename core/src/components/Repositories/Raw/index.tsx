import React from 'react';
import { Repos } from '../Repos';


export const Raw = () => {
    const repoType = 'raw';

    return (
        <Repos repoType={repoType} />
    );
};
