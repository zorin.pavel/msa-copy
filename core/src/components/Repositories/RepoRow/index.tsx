import React, { useCallback, useContext, useEffect, useState } from 'react';
import { useQueryClient } from '@tanstack/react-query';
import { useTranslation } from 'react-i18next';


import type { IError } from '@msa/kit/types';
import { Button, Dialog, IconDelete, IconVendor, StatusIcon, Table, Toast } from '@msa/kit';

import type { IRepository } from 'types';
import { checkPlatformRepo, checkVendorRepo, deletePlatformRepo, deleteVendorRepo } from 'actions';
import { PlatformContext } from 'components/Platforms/Platform';

import css from './repo.module.scss';


interface IProps {
    repo: IRepository,
    repoType: IRepository['type'],
}

export const RepoRow = (props: IProps) => {
    const { repo, repoType } = props;
    const queryClient = useQueryClient();
    const { platform, can } = useContext(PlatformContext);
    const { t } = useTranslation()

    let keys = ['repos', repoType];
    let deleteFunction = () => deleteVendorRepo(repoType, repo.name);
    let checkFunction = () => checkVendorRepo(repoType, repo.name);


    if(platform) {
        keys = ['platform', platform?.name, 'repos', repoType];
        deleteFunction = () => deletePlatformRepo(platform.name, repoType, repo.name);
        checkFunction = () => checkPlatformRepo(platform.name, repoType, repo.name);
    }

    const [deleteDialog, setDeleteDialog] = useState<boolean>(false);
    const [submitted, setSubmitted] = useState<boolean>(false);
    const [accessStatus, setAccessStatus] = useState<string>(repo.is_online ? 'Success' : 'Loading');


    const onDelete = useCallback(() => {
        setSubmitted(true);

        return deleteFunction()
            .then(() => {
                Toast.success({ message: t('toasts.delete', { entity: t('common.repository'), entityName: repo.name}) });
                queryClient.invalidateQueries(keys);
            })
            .catch((error: IError) => {
                Toast.error({ message: error.message });

                return false;
            })
            .finally(() => setSubmitted(false));
    }, [repo, queryClient]);


    const checkAccess = useCallback(() => {
        checkFunction()
            .then(() => setAccessStatus('Success'))
            .catch(() => setAccessStatus('Fail'));
    }, [setAccessStatus, checkFunction]);


    useEffect(() => {
        if(accessStatus !== 'Success')
            checkAccess();
    }, [repo, accessStatus, checkAccess]);


    return (
        <>
            <Table.Row valign="center" className="entity__row">
                <Table.Cell colspan={2}>{repo.name}</Table.Cell>
                <Table.Cell colspan={3}>{repo.URL}</Table.Cell>
                <Table.Cell actions={3}>
                    {
                        !platform &&
                        <IconVendor className={css.icon} />
                    }
                    <StatusIcon status={accessStatus} caption={false} className={css.icon} />
                    <IconDelete onClick={() => setDeleteDialog(true)} disabled={platform && !can?.delete} />
                </Table.Cell>
            </Table.Row>

            {
                deleteDialog &&
                <Dialog
                    open={deleteDialog}
                    header={t('helmRepos.delete', { context: 'one' })}
                    buttons={[
                        <Button key="cancel" onClick={() => setDeleteDialog(false)} view="outlined">{t('buttons.cancel')}</Button>,
                        <Button key="delete" onClick={onDelete} loading={submitted}>{t('buttons.delete')}</Button>
                    ]}
                    onClose={() => setDeleteDialog(false)}>
                    {t('dialogs.delete', {entityName: t('helmRepos.count', { context: 'one' })})} <b>{repo?.name}</b>?
                </Dialog>
            }
        </>
    );
};
