import React from 'react';
import { Repos } from '../Repos';


export const Helm = () => {
    const repoType = 'helm';

    return (
        <Repos repoType={repoType} />
    );
};
