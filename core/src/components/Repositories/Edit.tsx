import React, { ChangeEvent, useEffect, useState } from 'react';
import { Controller, useForm, useWatch } from 'react-hook-form';
import { useQueryClient } from '@tanstack/react-query';
import { useTranslation } from 'react-i18next';
import i18next from 'i18next';

import { IError } from '@msa/kit/types';
import { Button, Checkbox, Drawer, IconEdit, IconPlus, Input, Toast } from '@msa/kit';

import settings from 'settings';
import { IPlatform, IRepository } from 'types';
import { postPlatformRepo, postVendorRepo } from 'actions';


interface IProps {
    platform?: IPlatform,
    repoType: IRepository['type'],
    repo?: IRepository,
    disabled?: boolean,
}

export const Edit = (props: IProps) => {
    const { repo, repoType, platform, disabled } = props;
    const queryClient = useQueryClient();
    const { t } = useTranslation()
    // const { platform } = useContext(PlatformContext);

    let keys = ['repos', repoType];
    let postFunction = (formData: IRepository) => postVendorRepo(repoType, formData);

    if(platform) {
        keys = ['platform', platform?.name, 'repos', repoType];
        postFunction = (formData: IRepository) => postPlatformRepo(platform.name, repoType, formData);
    }

    const defaultValues = repo ?? {
        name: '',
        URL: '',
        credentials: false,
        username: '',
        password: '',
        proxy: false,
        // proxy_server: ''
    };

    const {
        handleSubmit,
        reset,
        control,
        register,
        unregister,
        setValue,
        formState: { errors, isValid },
    } = useForm({
        mode: 'onChange',
        reValidateMode: 'onChange',
        defaultValues
    });

    const [drawer, setDrawer] = useState<boolean>(false);
    const [submitted, setSubmitted] = useState<boolean>(false);

    const credentials = useWatch({
        name: 'credentials',
        control
    });

    const proxy = useWatch({
        name: 'proxy',
        control
    });


    useEffect(() => {
        if(!credentials)
            unregister(['username', 'password']);
        else {
            register('username', { required: true });
            register('password', { required: true });
        }
    }, [credentials, register, unregister]);


    useEffect(() => {
        if(!proxy) {
            unregister(['proxy_server']);
        } else {
            register('proxy_server', { required: true });

            if(repo)
                setValue('proxy_server', repo.https_proxy ?? repo.http_proxy ?? '');
        }
    }, [proxy, repo, register, unregister, setValue]);


    const onSubmit = (formData: IRepository) => {
        setSubmitted(true);

        delete formData.credentials;
        delete formData.proxy;

        formData.http_proxy = formData.proxy_server;
        formData.https_proxy = formData.proxy_server;

        delete formData.proxy_server;

        postFunction(formData)
            .then((response: IRepository) => {
                setDrawer(false);
                onReset();
                Toast.success({ message: t('toasts.add', { entity: t('helmRepos.helmRepos', { context: 'add' }), entityName: response.name }) });
                Toast.success({ message: `<b>${response.name}</b> repository was successfully added` });
                queryClient.invalidateQueries(keys);
            })
            .catch((error: IError) => {
                Toast.error({ message: error.message });
            })
            .finally(() => {
                setSubmitted(false);
            });
    };


    const onReset = () => {
        reset(defaultValues);
    };


    let drawerHeader = t('helmRepos.add', {context: "one"});

    if(repo)
        drawerHeader = t('helmRepos.edit', {context: "one"}) + ' ' + repo.name;

    return (
        <>
            {
                repo ?
                    <IconEdit onClick={() => setDrawer(true)} disabled={disabled} /> :
                    <Button iconLeft={<IconPlus />} onClick={() => setDrawer(true)} disabled={disabled}>{t('helmRepos.add', {context: "one"} )}</Button>
            }
            <Drawer header={drawerHeader} open={drawer} onClose={() => setDrawer(false)} backdrop>
                <form onSubmit={handleSubmit(onSubmit)} noValidate onReset={onReset}>
                    <Drawer.Body>
                        <Controller
                            control={control}
                            name="name"
                            rules={
                                {
                                    required: t('validation.isRequired', { entityName: t('common.name' )}),
                                    pattern: {
                                        value: settings.NAME_REGEX,
                                        message: t('validation.alphanumericCharacters', { entityName: t('common.name' )})
                                    }
                                }
                            }
                            render={({ field }) => (
                                <Input
                                    {...field}
                                    required
                                    label={t('common.name' )}
                                    placeholder={t('common.name' )}
                                    labelPosition="left"
                                    error={errors.name && errors.name.message as string}
                                    displayError
                                    autoComplete="off" />
                            )} />
                        <Controller
                            control={control}
                            name="URL"
                            rules={
                                {
                                    required: t('validation.isRequired', { entityName: "URL"}),
                                    pattern: {
                                        value: settings.DOMAIN_REGEX,
                                        message: t('validation.patternValid', { entityName: "URL"}),
                                    }
                                }
                            }
                            render={({ field }) => (
                                <Input
                                    {...field}
                                    required
                                    label="URL"
                                    placeholder="https://"
                                    labelPosition="left"
                                    error={!!errors.URL}
                                    autoComplete="off" />
                            )} />
                        <Controller
                            control={control}
                            name="credentials"
                            render={({ field }) => (
                                <Checkbox
                                    {...field}
                                    value="1"
                                    checked={field.value}
                                    label={i18next.format(t('credentials.credentials'), 'capitalize')}
                                    labelPosition="right"
                                    error={!!errors.credentials} />
                            )} />
                        {
                            credentials &&
                            <>
                                <Controller
                                    control={control}
                                    name="username"
                                    rules={
                                        {
                                            required: t('validation.isRequired', { entityName: "SSH" + t('common.username' )}),
                                            pattern: {
                                                value: /^[A-Za-z-_\d]+$/,
                                                message: t('validation.symbols', { entityName: t('common.username' )})
                                            },
                                        }
                                    }
                                    render={({ field: { name, value, onChange } }) =>
                                        <Input
                                            name={name}
                                            label={t('common.username' )}
                                            value={value}
                                            required
                                            labelPosition="left"
                                            error={errors.username && errors.username.message as string}
                                            onChange={(e: ChangeEvent<HTMLInputElement>) => onChange(e.target.value)} />
                                    }
                                />
                                <Controller
                                    control={control}
                                    name="password"
                                    rules={
                                        {
                                            required: t('validation.isRequired', { entityName: t('common.password' )}),
                                        }
                                    }
                                    render={({ field: { name, value, onChange } }) =>
                                        <Input
                                            type="password"
                                            name={name}
                                            value={value}
                                            label={t('common.password' )}
                                            required
                                            labelPosition="left"
                                            error={errors.password && errors.password.message as string}
                                            onChange={(e: ChangeEvent<HTMLInputElement>) => onChange(e.target.value)} />
                                    }
                                />
                            </>
                        }
                        <Controller
                            control={control}
                            name="proxy"
                            render={({ field }) => (
                                <Checkbox
                                    {...field}
                                    value="1"
                                    checked={field.value}
                                    label={t('common.proxyInstall' )}
                                    labelPosition="right"
                                    error={!!errors.proxy} />
                            )} />
                        {
                            proxy &&
                            <>
                                <Controller
                                    control={control}
                                    name={'proxy_server'}
                                    rules={
                                        {
                                            required: 'Proxy IP is required',
                                            pattern: {
                                                value: /^https?:\/\/([\d\w_-]+)(\.\w{2,3})([/\d\w._-]+)?(:\d{1,4})?$/,
                                                message: 'Proxy server is not valid'
                                            },
                                        }
                                    }
                                    render={({ field }) =>
                                        <Input
                                            {...field}
                                            required
                                            placeholder="http://0.0.0.0:3000"
                                            error={!!errors.proxy_server}
                                            label={t('common.proxyServer' )}
                                            labelPosition="left"
                                            addonLeft={{ label: 'https://' }} />
                                    }
                                />
                            </>
                        }

                    </Drawer.Body>
                    <Drawer.Footer>
                        <Button type="reset" view="outlined">{t('buttons.clear')}</Button>
                        <Button disabled={!isValid} loading={submitted} type="submit">{t('buttons.done')}</Button>
                    </Drawer.Footer>
                </form>
            </Drawer>
        </>
    );
};
