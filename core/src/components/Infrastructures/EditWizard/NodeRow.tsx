import React, { useContext, useEffect, useMemo } from 'react';
import { useQuery } from '@tanstack/react-query';
import { Controller, useFormContext, useWatch } from 'react-hook-form';

import { IError, OrderDirection } from '@msa/kit/types';
import { IconDelete, Select, sort, Table } from '@msa/kit';

import { IPlatformNode } from 'types';
import { getPlatformNodes } from 'actions';
import { PlatformContext } from 'components/Platforms/Platform';


interface IProps {
    index: number,
    remove: (index?: number | number[]) => void,
}

export const NodeRow = (props: IProps) => {
    const { index, remove } = props;
    const { platform } = useContext(PlatformContext);

    const {
        control,
        setValue,
        formState: { errors }
    } = useFormContext();


    const { isLoading: platformNodesLoading, data: platformNodes } = useQuery<IPlatformNode[], IError>(
        ['platform', platform?.name, 'nodes'],
        () => getPlatformNodes(platform?.name)
    );

    const platformNodesOptions = useMemo(() => {
        return sort<IPlatformNode[]>(platformNodes ?? [], 'name', OrderDirection.asc)
            .filter((platformNode: IPlatformNode) => !platformNode.cluster && !platformNode.infrastructure)
            .map((platformNode: IPlatformNode) => ({
                label: `${platformNode.name} (${platformNode.ip})`,
                value: platformNode.name
            }));
    }, [platformNodes]);


    const nodeName = useWatch({
        name: `nodes.${index}.name`,
        control
    });


    useEffect(() => {
        const { ip } = platformNodes?.filter(node => node.name === nodeName)[0] ?? {};

        setValue(`nodes.${index}.ip`, ip, { shouldValidate: true, shouldTouch: true });
    }, [index, nodeName, platformNodes, setValue]);

    return (
        <Table.Row valign="center">
            <Table.Cell actions={1}>{index + 1}</Table.Cell>
            <Table.Cell colspan={6}>
                <Controller control={control} name={`nodes.${index}.name`} rules={
                    {
                        required: 'Node is required',
                    }
                } render={({ field }) => (
                    <Select
                        {...field}
                        required
                        error={!!errors.nodes}
                        options={platformNodesOptions}
                        loading={platformNodesLoading} />
                )} />
            </Table.Cell>
            <Table.Cell actions={1}>
                <IconDelete disabled={!index} onClick={() => remove(index)} />
            </Table.Cell>
        </Table.Row>
    );
};
