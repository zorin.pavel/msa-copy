import React, { useEffect, useMemo } from 'react';
import { Controller, useFormContext } from 'react-hook-form';
import { useQuery } from '@tanstack/react-query';
import i18next from 'i18next';

import { IError, IStepComponentProps } from '@msa/kit/types';
import { Input, Select, Textarea } from '@msa/kit';

import settings from 'settings';
import type { IPlatform } from 'types';
import { getPlatforms } from 'actions';
import { useTranslation } from 'react-i18next';



interface IProps extends IStepComponentProps {}

export const Service = (props: IProps) => {
    const { setStepValid } = props;
    const { t } = useTranslation()

    const { data: platforms } = useQuery<IPlatform[], IError>(['platform'], getPlatforms);
    const platformsOptions = useMemo(
        () => platforms ? platforms.map((platform: IPlatform) => ({ label: platform.name, value: platform.name })) : [],
        [platforms]
    );


    const {
        control,
        formState: { errors, isValid }
    } = useFormContext();


    useEffect(() => {
        setStepValid && setStepValid('service', isValid);
    }, [isValid]);


    return (
        <>
            <Controller
                control={control}
                name="platform.name"
                rules={
                    {
                        required: t('validation.isRequired', { entityName: t('platforms.name' )}),
                    }
                }
                render={({ field }) => (
                    <Select
                        {...field}
                        label={i18next.format(t('platforms.name'), 'capitalize')}
                        required
                        labelPosition="left"
                        error={!!errors.platform}
                        autoComplete="off"
                        options={platformsOptions}
                    />
                )} />
            <Controller
                control={control}
                name="name"
                rules={
                    {
                        required: t('validation.isRequired', { entityName: t('infrastructures.name' )}),
                        pattern: {
                            value: settings.NAME_REGEX,
                            message: t('validation.alphanumericCharacters', { entityName: t('infrastructures.name' )})
                        }
                    }
                }
                render={({ field }) => (
                    <Input
                        {...field}
                        label={t('infrastructures.name' )}
                        placeholder={t('infrastructures.name' )}
                        labelPosition="left"
                        required
                        error={errors.name && errors.name.message as string}
                        displayError
                        autoComplete="off" />
                )} />
            <Controller
                control={control}
                name="description"
                render={({ field }) => (
                    <Textarea
                        {...field}
                        label={t('common.description')}
                        placeholder={t('common.description')}
                        labelPosition="left"
                        rows={5}
                        error={!!errors.description} />
                )} />
        </>
    );
};
