import React, { useEffect, useState } from 'react';
import { useFormContext, useWatch } from 'react-hook-form';

import { IStepComponentProps } from '@msa/kit/types';
import { IconFullScreen, Loader, YamlEditor, IconError, IconSuccess } from '@msa/kit';

import css from './values.module.scss';


interface IProps extends IStepComponentProps {
    wideDrawer?: boolean,
    setWideDrawer?: (wide: boolean) => void
}

export const Values = (props: IProps) => {
    const { setStepValid, wideDrawer, setWideDrawer } = props;
    const {
        control,
        setValue,
        formState: { isValid }
    } = useFormContext();

    const values = useWatch({
        name: 'values',
        control
    });

    const [error, setError] = useState<boolean>(false);


    useEffect(() => {
        setStepValid && setStepValid('values', isValid && !error);
    }, [isValid, error]);


    const onChange = (values: Record<string, any>) => {
        setValue('values', values, { shouldValidate: true, shouldTouch: true });
    };


    return (
        <>
            <div className={css.buttons}>
                {
                    error ?
                        <IconError className={css.iconError} /> :
                        <IconSuccess className={css.iconSuccess} />
                }
                <IconFullScreen className={css.buttonIcon} onClick={() => setWideDrawer && setWideDrawer(!wideDrawer)} />
            </div>

            {
                false &&
                <Loader size="small" />
            }

            {
                values &&
                <YamlEditor
                    value={values}
                    onChange={onChange}
                    onError={setError} />
            }
        </>
    );
};
