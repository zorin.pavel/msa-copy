import React, { ReactElement, useContext, useState } from 'react';
import { FormProvider, useForm } from 'react-hook-form';
import { useQueryClient } from '@tanstack/react-query';
import { useTranslation } from 'react-i18next';

import { IError, IWizardStep } from '@msa/kit/types';
import { Button, Drawer, IconArrowLeft, IconArrowRight, Tab, TabBody, TabList, Tabs, Toast, useWizard, IconEdit } from '@msa/kit';

import type { IInfrastructure } from 'types';
import { postInfrastructure } from 'actions';
import { PlatformContext } from 'components/Platforms/Platform';
import { Service } from './Service';
import { Nodes } from './Nodes';
import { Values } from './Values';

import css from './wizard.module.scss';


interface IProps {
    type: string,
    values: Record<string, any>,
    infrastructure?: IInfrastructure,
    button?: ReactElement,
}

export const EditWizard = (props: IProps) => {
    const { type, values, infrastructure, button } = props;
    const queryClient = useQueryClient();
    const { platform } = useContext(PlatformContext);
    const { t } = useTranslation()

    const wizardSteps: IWizardStep[] = [
        {
            index: 'service',
            header: t('wizards.chooseServise'),
            component: Service
        },
        {
            index: 'nodes',
            header: t('wizards.setNodes'),
            component: Nodes
        },
        {
            index: 'values',
            header: t('wizards.setValues'),
            component: Values
        }
    ];

    const {
        currentStepKey,
        setCurrentStepKey,
        isStepValid,
        setStepValid,
        nextStep,
        prevStep,
        resetStep,
        isLast,
        isFirst,
        isDisabled
    } = useWizard('service', wizardSteps);


    const defaultValues = infrastructure ??  {
        platform: {
            name: platform?.name
        },
        name: '',
        description: '',
        nodes: [
            {
                name: '',
                ip: '',
                abels: [],
                taints: [],
            }
        ],
        type,
        values
    };

    const formMethods = useForm({
        mode: 'onChange',
        reValidateMode: 'onChange',
        defaultValues,
    });

    const [submitted, setSubmitted] = useState<boolean>(false);
    const [drawer, setDrawer] = useState<boolean>(false);
    const [wideDrawer, setWideDrawer] = useState<boolean>(false);


    const onSubmit = (formData: Partial<IInfrastructure>) => {
        setSubmitted(true);
        
        return postInfrastructure(platform?.name, formData)
            .then((response: IInfrastructure) => {
                setDrawer(false);
                onReset();
                Toast.success({ message: t('toasts.add', { entity: t('infrastructures.infrastructures'), entityName: response.name }) });
                queryClient.invalidateQueries(['platform', platform?.name, 'infrastructures']);
            })
            .catch((error: IError) => {
                Toast.error({ message: error.message });

                return false;
            })
            .finally(() => setSubmitted(false));
    };


    const onReset = () => {
        formMethods.reset();
        resetStep();
    };


    return (
        <>
            {
                button ?
                    React.cloneElement(button, { onClick: () => setDrawer(true) }) :
                    <IconEdit onClick={() => setDrawer(true)} />
            }

            <Drawer header={infrastructure ? infrastructure.name : t('infrastructures.create')} open={drawer} onClose={() => {
                setDrawer(false);
                onReset();
            }} wide={wideDrawer} backdrop>
                <FormProvider {...formMethods}>
                    <form onSubmit={formMethods.handleSubmit(onSubmit)} noValidate onReset={onReset}>
                        <Drawer.Body>
                            <Tabs>
                                <TabList>
                                    {
                                        wizardSteps.map((step: IWizardStep) => {
                                            return (
                                                <Tab
                                                    key={step.index}
                                                    value={currentStepKey}
                                                    index={step.index}
                                                    onClick={() => setCurrentStepKey(step.index)}
                                                    disabled={isDisabled(step.index)}>{step.header}</Tab>
                                            );
                                        })
                                    }
                                </TabList>
                                {
                                    wizardSteps.map((step: IWizardStep) => (
                                        <TabBody key={step.index} value={currentStepKey} index={step.index}>
                                            {
                                                step.component &&
                                                React.createElement(step.component, { setStepValid, isStepValid, wideDrawer, setWideDrawer, index: step.index })
                                            }
                                        </TabBody>
                                    ))
                                }
                            </Tabs>

                        </Drawer.Body>
                        <Drawer.Footer>
                            <div className={css.buttonsGroup}>
                                {
                                    !isFirst &&
                                <Button view="outlined" onClick={() => prevStep()} icon={<IconArrowLeft />} />
                                }
                            <Button type="reset" view="outlined" color="red" onClick={onReset}>{t('buttons.clear')}</Button>
                            </div>
                            {
                                isLast ?
                                    <Button disabled={!isStepValid(currentStepKey)} loading={submitted} onClick={formMethods.handleSubmit(onSubmit)}>{t('buttons.done')}</Button> :
                                    <Button disabled={!isStepValid(currentStepKey)} loading={submitted} onClick={() => nextStep()} icon={<IconArrowRight />} />
                            }
                        </Drawer.Footer>
                    </form>
                </FormProvider>
            </Drawer>
        </>
    );
};
