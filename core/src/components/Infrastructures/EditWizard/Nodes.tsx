import React, { useEffect } from 'react';
import { useFieldArray, useFormContext } from 'react-hook-form';
import { useTranslation } from 'react-i18next';

import { IStepComponentProps } from '@msa/kit/types';
import { Button, IconPlus, Table } from '@msa/kit';

import { NodeRow } from './NodeRow';

import css from './nodes.module.scss';


interface IProps extends IStepComponentProps {}

export const Nodes = (props: IProps) => {
    const { setStepValid } = props;
    const { t } = useTranslation()

    const {
        control,
        formState: { isValid }
    } = useFormContext();


    useEffect(() => {
        setStepValid && setStepValid('nodes', isValid);
    }, [isValid]);


    const { fields, append, remove } = useFieldArray({
        control,
        name: 'nodes'
    });


    const addNode = () => {
        append({
            name: '',
            ip: '',
        });
    };


    return (
        <>
            <Table cells={7} className={css.nodes}>
                <Table.Header>
                    <Table.Cell actions={1} />
                    <Table.Cell colspan={6}>{t('nodes.count', {context: 'one'})}</Table.Cell>
                </Table.Header>
                {
                    fields.map((field, index) => <NodeRow key={index} index={index} remove={remove} />)
                }
            </Table>
            <div className={css.nodesButtons}>
                <Button view="outlined" icon={<IconPlus />} size="small" className={css.buttonLong} onClick={addNode} />
            </div>
        </>
    );
};
