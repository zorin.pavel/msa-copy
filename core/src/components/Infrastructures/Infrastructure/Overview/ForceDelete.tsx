import React, { useCallback, useContext, useState } from 'react';
import { useNavigate } from 'react-router-dom';
import { useQueryClient } from '@tanstack/react-query';

import { Button, Dialog, IconDelete, Toast } from '@msa/kit';

import settings from 'settings';
import { deleteInfrastructureForce } from 'actions';
import { PlatformContext } from 'components/Platforms/Platform';
import { InfrastructureContext } from 'components/Infrastructures/Infrastructure';


interface IProps {
    disabled?: boolean
}

export const ForceDelete = (props: IProps) => {
    const { disabled } = props;
    const navigate = useNavigate();
    const { platform } = useContext(PlatformContext);
    const { infrastructure } = useContext(InfrastructureContext);
    const queryClient = useQueryClient();

    const [deleteDialog, setDeleteDialog] = useState<boolean>(false);


    const onDelete = useCallback(() => {
        return deleteInfrastructureForce(platform?.name, infrastructure?.name)
            .then(() => {
                Toast.success({ message: `Infrastructure <b>${infrastructure?.name}</b> force deleted` });
                queryClient.invalidateQueries(['platform', platform?.name, 'infrastructures']);
                navigate(`/platforms/${platform?.name}/infrastructures`);
            })
            .catch((error) => {
                Toast.error({ message: error.message });

                return false;
            });
    }, [navigate, queryClient, platform, infrastructure]);


    return (
        <>
            {
                settings.STATE.FAIL.includes(infrastructure?.state as string) &&
                !settings.STATE.PROGRESS.includes(infrastructure?.state as string) &&
                !disabled &&
                <Button onClick={() => setDeleteDialog(true)} iconRight={<IconDelete />}>Force delete</Button>
            }

            {
                deleteDialog &&
                <Dialog open={deleteDialog} header="Delete Cluster" buttons={[
                    <Button key="cancel" onClick={() => setDeleteDialog(false)} view="outlined">Cancel</Button>,
                    <Button key="delete" onClick={onDelete}>Delete</Button>
                ]} onClose={() => setDeleteDialog(false)}>
                    Do you really want delete infrastructure <b>{infrastructure?.name}</b>?
                </Dialog>
            }
        </>
    );
};
