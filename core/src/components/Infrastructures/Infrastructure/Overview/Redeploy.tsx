import React, { useContext, useState } from 'react';
import { useQueryClient } from '@tanstack/react-query';

import { Button, Dialog, Toast } from '@msa/kit';

import settings from 'settings';
import { IInfrastructure } from 'types';
import { putInfrastructure } from 'actions';
import { PlatformContext } from 'components/Platforms/Platform';
import { InfrastructureContext } from 'components/Infrastructures/Infrastructure';
import { useTranslation } from 'react-i18next';


interface IProps {
    disabled?: boolean
}

export const Redeploy = (props: IProps) => {
    const { disabled } = props;
    const queryClient = useQueryClient();
    const { platform } = useContext(PlatformContext);
    const { infrastructure } = useContext(InfrastructureContext);
    const { t } = useTranslation();

    const [redeployLoading, setRedeployLoading] = useState<boolean>(false);
    const [redeployDialog, setRedeployDialog] = useState<boolean>(false);


    const onDeploy = () => {
        setRedeployLoading(true);

        return putInfrastructure(platform?.name, infrastructure?.name, infrastructure)
            .then((response: IInfrastructure) => {
                Toast.success({ message: t('toasts.redeploy', { entity: t('infrastructures.count', { context: 'few' }), entityName: infrastructure?.name }) });
                queryClient.setQueryData(['infrastructure', infrastructure?.name], response);
                queryClient.invalidateQueries(['platform', platform?.name, 'infrastructures']);
            })
            .catch(error => {
                Toast.error({ message: error.message });

                return false;
            })
            .finally(() => setRedeployLoading(false));
    };

    return (
        <>
            <Button
                view="outlined"
                disabled={settings.STATE.PROGRESS.includes(infrastructure?.state as string) || disabled}
                loading={redeployLoading}
                onClick={() => setRedeployDialog(true)}>{t('common.redeploy')}</Button>

            <Dialog
                open={redeployDialog}
                header={t('common.redeploy')}
                buttons={[
                    <Button key="cancel" onClick={() => setRedeployDialog(false)} view="outlined">{t('buttons.cancel')}</Button>,
                    <Button key="deploy" onClick={onDeploy} loading={redeployLoading}>{t('common.redeploy')}</Button>
                ]}
                onClose={() => setRedeployDialog(false)}>
                {t('dialogs.redeploy', {entityName: t('infrastructures.infrastructure', { context: 'single' })})} <b>{infrastructure?.name}</b>?
            </Dialog>
        </>
    );
};
