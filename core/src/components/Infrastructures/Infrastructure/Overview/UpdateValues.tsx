import React, { useContext, useState } from 'react';
import { FormProvider, useForm } from 'react-hook-form';
import { useQueryClient } from '@tanstack/react-query';
import { useTranslation } from 'react-i18next';

import { DrawerFooterJustify, IError } from '@msa/kit/types';
import { Button, Drawer, Toast } from '@msa/kit';

import settings from 'settings';
import type { IInfrastructure } from 'types';
import { putInfrastructure } from 'actions';
import { Values } from 'components/Infrastructures/EditWizard/Values';
import { PlatformContext } from 'components/Platforms/Platform';
import { InfrastructureContext } from 'components/Infrastructures/Infrastructure';


interface IProps {
    disabled?: boolean
}

export const UpdateValues = (props: IProps) => {
    const { disabled } = props;
    const queryClient = useQueryClient();
    const { platform } = useContext(PlatformContext);
    const { infrastructure } = useContext(InfrastructureContext);
    const { t } = useTranslation();

    const formMethods = useForm({
        mode: 'onChange',
        reValidateMode: 'onChange',
        defaultValues: infrastructure
    });

    const [drawer, setDrawer] = useState<boolean>(false);
    const [wideDrawer, setWideDrawer] = useState<boolean>(false);
    const [submitted, setSubmitted] = useState<boolean>(false);


    const onSubmit = () => {
        const formData = formMethods.getValues();

        setSubmitted(true);

        return putInfrastructure(platform?.name, infrastructure?.name, {
            ...infrastructure,
            ...formData
        })
            .then((response: IInfrastructure) => {
                setDrawer(false);
                onReset();
                Toast.success({ message: t('toasts.update', { entity: t('infrastructures.count', { context: 'few' }), entityName: infrastructure?.name }) });
                queryClient.setQueryData(['infrastructure', infrastructure?.name], response);
                queryClient.invalidateQueries(['platform', platform?.name, 'infrastructures']);
            })
            .catch((error: IError) => {
                Toast.error({ message: error.message });

                return false;
            })
            .finally(() => setSubmitted(false));
    };


    const onReset = () => {
        formMethods.reset(infrastructure);
    };


    return (
        <>
            <Button
                view="outlined"
                disabled={!settings.STATE.SUCCESS.includes(infrastructure?.state as string) || disabled}
                onClick={() => setDrawer(true)}>{t('common.upgrade')}</Button>

            <Drawer header={t('common.upgrade') + " " + infrastructure?.name} open={drawer} onClose={() => {
                setDrawer(false);
                onReset();
            }} wide={wideDrawer} backdrop>
                <FormProvider {...formMethods}>
                    <form noValidate onReset={onReset} onSubmit={formMethods.handleSubmit(onSubmit)}>
                        <Drawer.Body>
                            <Values index="values" setWideDrawer={setWideDrawer} wideDrawer={wideDrawer} />
                        </Drawer.Body>
                        <Drawer.Footer justify={DrawerFooterJustify.end}>
                            <Button disabled={!formMethods.formState.isValid} loading={submitted} onClick={() => onSubmit()}>{t('buttons.done')}</Button>
                        </Drawer.Footer>
                    </form>
                </FormProvider>
            </Drawer>
        </>
    );
};
