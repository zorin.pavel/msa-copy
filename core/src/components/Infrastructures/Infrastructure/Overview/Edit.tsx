import React, { useContext, useMemo, useState } from 'react';
import { Controller, useForm } from 'react-hook-form';
import { useQuery, useQueryClient } from '@tanstack/react-query';
import { useNavigate } from 'react-router-dom';
import { useTranslation } from 'react-i18next';
import i18next from 'i18next';

import { IError } from '@msa/kit/types';
import { Button, Drawer, IconEdit, Input, Select, Textarea, Toast } from '@msa/kit';

import settings from 'settings';
import type { IInfrastructure, IPlatform } from 'types';
import { getPlatforms, putInfrastructure } from 'actions';
import { PlatformContext } from 'components/Platforms/Platform';


interface IProps {
    infrastructure: IInfrastructure,
    disabled?: boolean,
}

export const Edit = (props: IProps) => {
    const { infrastructure, disabled } = props;
    const navigate = useNavigate();
    const queryClient = useQueryClient();
    const { platform } = useContext(PlatformContext);
    const { t } = useTranslation()

    const { data: platforms } = useQuery<IPlatform[], IError>(['platform'], getPlatforms);
    const platformsOptions = useMemo(
        () => platforms ? platforms.map((platform: IPlatform) => ({ label: platform.name, value: platform.name })) : [],
        [platforms]
    );

    const {
        handleSubmit,
        reset,
        control,
        formState: { errors, isValid },
    } = useForm({
        mode: 'onChange',
        reValidateMode: 'onChange',
        defaultValues: infrastructure
    });

    const [submitted, setSubmitted] = useState<boolean>(false);
    const [drawer, setDrawer] = useState<boolean>(false);


    const onSubmit = (formData: IInfrastructure) => {
        setSubmitted(true);

        putInfrastructure(platform?.name, infrastructure.name, {
            ...infrastructure,
            ...formData
        })
            .then((response: IInfrastructure) => {
                setDrawer(false);
                onReset();
                Toast.success({ message: t('toasts.edit', { entity: t('infrastructures.infrastructure', { context: 'edit' }), entityName: response.name }) });
                queryClient.invalidateQueries(['platform', platform?.name, 'infrastructures']);
                navigate(`/platforms/${platform?.name}/infrastructures/${response.name}`);
            })
            .catch(error => {
                Toast.error({ message: error.message });
            })
            .finally(() => {
                setDrawer(false);
                setSubmitted(false);
            });
    };


    const onReset = () => {
        reset();
    };


    return (
        <>
            <Button
                view="outlined"
                icon={<IconEdit />}
                disabled={!settings.STATE.SUCCESS.includes(infrastructure.state) || disabled}
                onClick={() => setDrawer(true)} />

            <Drawer header={t('infrastructures.edit', { context: 'single' }) + ' ' + infrastructure.name} open={drawer} onClose={() => {
                setDrawer(false);
                onReset();
            }} backdrop>
                <form onSubmit={handleSubmit(onSubmit)} noValidate onReset={onReset}>
                    <Drawer.Body>
                        <Controller
                            control={control}
                            name="platform.name"
                            rules={
                                {
                                    required: t('validation.isRequired', { entityName: t('platforms.name' )}),
                                }
                            }
                            render={({ field }) => (
                                <Select
                                    {...field}
                                    label={i18next.format(t('platforms.name'), 'capitalize')}
                                    required
                                    readonly
                                    labelPosition="left"
                                    error={!!errors.platform}
                                    autoComplete="off"
                                    options={platformsOptions}
                                />
                            )} />
                        <Controller
                            control={control}
                            name="name"
                            rules={
                                {
                                    required: t('validation.isRequired', { entityName: t('infrastructures.name' )}),
                                    pattern: {
                                        value: settings.NAME_REGEX,
                                        message: t('validation.alphanumericCharacters', { entityName: t('infrastructures.name' )})
                                    }
                                }
                            }
                            render={({ field }) => (
                                <Input
                                    {...field}
                                    label={t('infrastructures.name' )}
                                    placeholder={t('infrastructures.name' )}
                                    labelPosition="left"
                                    required
                                    error={errors.name && errors.name.message}
                                    displayError
                                    autoComplete="off" />
                            )} />
                        <Controller
                            control={control}
                            name="description"
                            render={({ field }) => (
                                <Textarea
                                    {...field}
                                    label={t('common.description')}
                                    placeholder={t('common.description')}
                                    labelPosition="left"
                                    rows={5}
                                    error={!!errors.description} />
                            )} />
                    </Drawer.Body>
                    <Drawer.Footer>
                        <Button type="reset" view="outlined" color="red" onClick={onReset}>{t('buttons.clear')}</Button>
                        <Button disabled={!isValid} loading={submitted} type="submit" onClick={handleSubmit(onSubmit)}>{t('buttons.done')}</Button>
                    </Drawer.Footer>
                </form>
            </Drawer>
        </>
    );
};
