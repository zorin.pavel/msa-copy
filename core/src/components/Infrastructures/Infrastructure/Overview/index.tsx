import React, { useContext } from 'react';
import { useTranslation } from 'react-i18next';
import i18next from 'i18next';

import classNames from 'classnames';

import { brakeWord, StatusIcon } from '@msa/kit';

import { InfrastructureContext } from 'components/Infrastructures/Infrastructure';
import { Edit } from './Edit';
import { UpdateValues } from './UpdateValues';
import { Redeploy } from './Redeploy';
import { ForceDelete } from './ForceDelete';

import css from './infrastructure.module.scss';


export const InfrastructureOverview = () => {
    const { infrastructure, can } = useContext(InfrastructureContext);
    const { t } = useTranslation();


    if(!infrastructure)
        return null;


    return (
        <div className="row">
            <div className={classNames('c1of2', 'paper')} style={{ flexGrow: 0 }}>
                <div className="row align-center justify-between">
                    <h4 className="paper__header">{t('infrastructures.info')}</h4>
                    <Edit infrastructure={infrastructure} disabled={!can?.edit} />
                </div>
                <div className={classNames('row', 'gap1', css.info)}>
                    <div className={classNames('c2of5', css.infoLabel)}>{t('common.deployStatus')}</div>
                    <div className="c3of5">
                        <StatusIcon status={infrastructure.state} />
                    </div>
                </div>
                <div className={classNames('row', 'gap1', css.info)}>
                    <div className={classNames('c2of5', css.infoLabel)}>{i18next.format(t('platforms.name'), 'capitalize')}</div>
                    <div className="c3of5"><b>{infrastructure.platform.name}</b></div>
                </div>
                <div className={classNames('row', 'gap1', css.info)}>
                    <div className={classNames('c2of5', css.infoLabel)}>{t('common.description')}</div>
                    <div className="c3of5"><p>{ infrastructure.description && brakeWord(infrastructure.description, 135) }</p></div>
                </div>

                <div className="section__divider" />

                <h4 className="paper__header">{t('common.actions')}</h4>
                <UpdateValues disabled={!can?.edit} />
                <Redeploy disabled={!can?.edit} />
                <ForceDelete disabled={!can?.delete} />
            </div>
        </div>
    );
};
