import React, { useContext } from 'react';

import { Circle, Table } from '@msa/kit';

import type { IInfrastructureNode } from 'types';
import { InfrastructureContext } from 'components/Infrastructures/Infrastructure';

import css from './nodes.module.scss';


export const InfrastructureNodes = () => {
    const { infrastructure } = useContext(InfrastructureContext);

    return (
        <>
            <Table cells={12} className={css.nodes}>
                <Table.Header className={css.nodesHeader}>
                    <Table.Cell colspan={3}>IP</Table.Cell>
                    <Table.Cell colspan={2}>SWAP</Table.Cell>
                    <Table.Cell colspan={2}>HDD</Table.Cell>
                    <Table.Cell colspan={2}>CPU</Table.Cell>
                    <Table.Cell colspan={2}>RAM</Table.Cell>
                    <Table.Cell actions={1} />
                </Table.Header>
                {
                    infrastructure &&
                    infrastructure.nodes &&
                    infrastructure.nodes.map((node: IInfrastructureNode) => {
                        return (
                            <Table.Row valign="center" key={node.name} className={css.nodeRow}>
                                <Table.Cell colspan={3} className={css.nodeName}>
                                    <span>{node.ip}</span>
                                </Table.Cell>
                                <Table.Cell colspan={2}>
                                    <Circle progress={0} capacity={0} size="xsmall" />
                                </Table.Cell>
                                <Table.Cell colspan={2}>
                                    <Circle progress={0} capacity={0} size="xsmall" />
                                </Table.Cell>
                                <Table.Cell colspan={2}>
                                    <Circle progress={0} capacity={0} size="xsmall" />
                                </Table.Cell>
                                <Table.Cell colspan={2}>
                                    <Circle progress={0} capacity={0} size="xsmall" />
                                </Table.Cell>
                                <Table.Cell actions={1} />
                            </Table.Row>
                        );
                    })
                }
            </Table>
        </>
    );
};
