import React, { createContext, useContext } from 'react';
import { Outlet, useParams } from 'react-router-dom';
import { useQuery, useQueryClient } from '@tanstack/react-query';

import { IError } from '@msa/kit/types';
import { Error, Loader } from '@msa/kit';

import { useSocket } from '@msa/host';

import { env } from 'env';
import type { IInfrastructure } from 'types';
import { loadInfrastructure } from 'actions';
import { PlatformContext } from 'components/Platforms/Platform';


interface ContextProps {
    infrastructure?: IInfrastructure,
    disabled?: boolean,
    can?: Record<string, boolean | undefined>
}

export const InfrastructureContext = createContext<ContextProps>({});

export const Infrastructure = () => {
    const queryClient = useQueryClient();
    const { infrastructureSlug } = useParams();
    const { platform, can } = useContext(PlatformContext);

    const { isLoading, data: infrastructure, error } = useQuery<IInfrastructure, IError>(
        ['platform', platform?.name, 'infrastructures', infrastructureSlug],
        () => loadInfrastructure(platform?.name, infrastructureSlug),
    );

    useSocket(
        { tenant: env.MSA_DEFAULT_TENANT, platform: platform?.name, object_type: 'infrastructure', type: 'status', name: infrastructureSlug },
        {
            callback: () => queryClient.invalidateQueries(['platform', platform?.name, 'infrastructures', infrastructureSlug]),
            toast: true
        }
    );


    if(isLoading)
        return <Loader />;

    if(error)
        return <Error {...error} />;

    if(!infrastructure)
        return null;


    return (
        <InfrastructureContext.Provider value={{ infrastructure, can }}>
            <Outlet />
        </InfrastructureContext.Provider>
    );
};
