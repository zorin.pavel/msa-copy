import React from 'react';
import { useTranslation } from 'react-i18next';

import { Button, IconPlus, Image } from '@msa/kit';

import type { IInfraService } from 'types';
import { EditWizard } from 'components/Infrastructures/EditWizard';

import css from './infrastructure.module.scss';


interface IProps {
    service: IInfraService,
    canCreate?: boolean,
}

export const Service = (props: IProps) => {
    const { service, canCreate } = props;
    const { t } = useTranslation()

    return (
        <div className={css.plate}>
            <h4 className={css.title}>
                {service.title}
            </h4>
            <div className={css.section}>
                <Image src={`/icons/${service.type}.svg`} alt={service.title} className={css.logo} />
            </div>
            <p className={css.description}>{service.description}</p>
            <div className={css.actions}>
                <EditWizard
                    type={service.type}
                    values={service.values}
                    button={<Button iconLeft={<IconPlus />} size="small" disabled={!canCreate}>{t('infrastructures.install')}</Button>} />
            </div>
        </div>
    );
};
