import React, { useContext, useEffect, useState } from 'react';
import { Outlet, useMatch } from 'react-router-dom';
import { useQuery, useQueryClient } from '@tanstack/react-query';
import { useTranslation } from 'react-i18next';
import i18next from 'i18next';

import { IError, OrderDirection } from '@msa/kit/types';
import { Loader, Error, sort } from '@msa/kit';

import { useSocket } from '@msa/host';

import { env } from 'env';
import { IInfraService, IInfrastructure } from 'types';
import { getInfrastructures, getInfrastructureServices } from 'actions';
import { InfrastructureRow } from './InfrastructureRow';
import { PlatformContext } from 'components/Platforms/Platform';
import { Service } from 'components/Infrastructures/Service';

import css from './infrastructure.module.scss';


export const Infrastructures = () => {
    const match = useMatch('/platforms/:platformSlug/infrastructures/:children/*');
    const { platform, can } = useContext(PlatformContext);
    const queryClient = useQueryClient();
    const { t } = useTranslation()

    useSocket(
        { tenant: env.MSA_DEFAULT_TENANT, platform: platform?.name, object_type: 'infrastructure', type: 'status' },
        {
            callback: () => queryClient.invalidateQueries(['platform', platform?.name, 'infrastructures']),
            toast: !match
        }
    );

    const { isLoading, data, error } = useQuery<IInfrastructure[], IError>(
        ['platform', platform?.name, 'infrastructures'],
        () => getInfrastructures(platform?.name)
    );
    const { isLoading: isLoadingServices, data: services } = useQuery<IInfraService[], IError>(
        ['platform', platform?.name, 'services'],
        () => getInfrastructureServices(platform?.name)
    );

    const [infrastructures, setInfrastructures] = useState<IInfrastructure[]>(data ?? []);


    useEffect(() => {
        if(data) {          
            setInfrastructures(sort(data, 'name', OrderDirection.asc));
        }
    }, [data]);


    if(match)
        return <Outlet />;

    if(isLoading)
        return <Loader layout />;

    if(error)
        return <Error {...error} layout />;


    return (
        <>
            <div className="layout__header">
                <h1 className="layout__title">{platform?.name} <span>| {i18next.format(t('infrastructures.infrastructures'), 'capitalize')}</span></h1>
            </div>

            {
                (infrastructures && !!infrastructures.length) &&
                    <div className={css.list}>
                        {
                            infrastructures.map((infrastructure: IInfrastructure) => (
                                <InfrastructureRow infrastructure={infrastructure} key={infrastructure.UUID} canDelete={can?.delete} />)
                            )
                        }
                    </div>
            }

            <h4 className="section__header">{t('infrastructures.available')}</h4>
            {
                isLoadingServices ?
                    <Loader caption="Loading services" /> :
                    <div className={css.list}>
                        {
                            services &&
                            services.map(service => <Service key={service.type} service={service} canCreate={can?.create} />)
                        }
                    </div>
            }
        </>
    );
};
