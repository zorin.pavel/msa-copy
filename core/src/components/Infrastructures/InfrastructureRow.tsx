import React, { useCallback, useContext, useState } from 'react';
import { useNavigate } from 'react-router-dom';
import { useQueryClient } from '@tanstack/react-query';
import { useTranslation } from 'react-i18next';

import classNames from 'classnames';

import { Button, Dialog, IconDelete, Toast, brakeWord, StatusIcon, Image, Badge } from '@msa/kit';

import settings from 'settings';
import type { IInfrastructure, IInfrastructureNode } from 'types';
import { deleteInfrastructure } from 'actions';
import { PlatformContext } from 'components/Platforms/Platform';

import css from './infrastructure.module.scss';


interface IProps {
    infrastructure: IInfrastructure;
    canDelete?: boolean,
}

export const InfrastructureRow = (props: IProps) => {
    const { infrastructure, canDelete } = props;
    const navigate = useNavigate();
    const queryClient = useQueryClient();
    const { platform } = useContext(PlatformContext);
    const { t } = useTranslation()

    const disabled = settings.STATE.PROGRESS.includes(infrastructure.state) || !canDelete;
    const [deleteDialog, setDeleteDialog] = useState<boolean>(false);


    const onDelete = useCallback(() => {
        return deleteInfrastructure(platform?.name, infrastructure.name)
            .then(() => {
                Toast.success({ message: t('toasts.delete', { entity: t('infrastructures.infrastructures'), entityName: infrastructure.name }) });
                queryClient.invalidateQueries(['platform', platform?.name, 'infrastructures']);
            })
            .catch((error) => {
                Toast.error({ message: error.message });

                return false;
            });
    }, [queryClient, platform, infrastructure]);


    return (
        <>
            <div className={classNames(css.plate, css.installed)} onClick={() => navigate(`${infrastructure.name}`)}>
                <div className={css.title}>
                    <h4 onClick={() => navigate(`${infrastructure.name}`)}>{infrastructure.name}</h4>
                    <StatusIcon status={infrastructure.state} />
                </div>
                <div className={css.section}>
                    <Image src={`/icons/${infrastructure.type}.svg`} alt={infrastructure.type} className={css.logo} />
                    {
                        infrastructure.nodes.map((node: IInfrastructureNode) => (
                            <div key={node.UUID} className={css.nodeName}>
                                <span>{node.name}</span>
                                <Badge>{node.ip}</Badge>
                            </div>
                        ))
                    }
                </div>
                <p className={classNames(css.description)}>{brakeWord(infrastructure.description, 180)}</p>
                <div className={css.actions}>
                    <Button
                        icon={<IconDelete />}
                        view="outlined"
                        onClick={(e) => {
                            e.stopPropagation();
                            setDeleteDialog(true)
                        }}
                        disabled={disabled} />
                </div>
            </div>

            {
                deleteDialog &&
                <Dialog
                    open={deleteDialog}
                    header={t('infrastructures.delete', { context: 'single' })}
                    buttons={[
                        <Button key="cancel" onClick={() => setDeleteDialog(false)} view="outlined">{t('buttons.cancel')}</Button>,
                        <Button key="delete" onClick={onDelete}>{t('buttons.delete')}</Button>
                    ]}
                    onClose={() => setDeleteDialog(false)}>
                    {t('dialogs.delete', {entityName: t('infrastructures.infrastructure', { context: 'single' })})} <b>{infrastructure.name}</b>?
                </Dialog>
            }
        </>
    );
};
