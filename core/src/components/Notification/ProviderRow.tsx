import React, { useCallback, useState } from 'react';
import { useQueryClient } from '@tanstack/react-query';
import { useTranslation } from 'react-i18next';

import type { IError } from '@msa/kit/types';
import { Button, Dialog, IconDelete, isAuthorized, Table, Toast } from '@msa/kit';

import type { IProvider } from 'types';
import { deleteProvider } from 'actions';
import { Edit } from 'components/Notification/Edit';


interface IProps {
    provider: IProvider,
}

export const ProviderRow = (props: IProps) => {
    const { provider } = props;
    const queryClient = useQueryClient();
    const { t } = useTranslation();

    const [submitted, setSubmitted] = useState<boolean>(false);
    const [deleteDialog, setDeleteDialog] = useState<boolean>(false);


    const onDelete = useCallback(() => {
        setSubmitted(true);

        return deleteProvider(provider.name)
            .then(() => {
                Toast.success({ message: t('toasts.delete', { entity: t('common.provider'), entityName: provider.name }) });
                queryClient.invalidateQueries(['notification']);
            })
            .catch((error: IError) => {
                Toast.error({ message: error.message });

                return false;
            })
            .finally(() => setSubmitted(false));
    }, [provider, queryClient]);


    return (
        <Table.Row valign="center" className="entity__row">
            <Table.Cell>{provider.name}</Table.Cell>
            <Table.Cell>{provider.type}</Table.Cell>
            <Table.Cell actions={2}>
                <Edit disabled={!isAuthorized({ settings: ['delete'] })} provider={provider} />
                <IconDelete onClick={() => setDeleteDialog(true)} disabled={!isAuthorized({ settings: ['delete'] })} />
            </Table.Cell>

            {
                deleteDialog &&
                <Dialog
                    open={deleteDialog}
                    header={t('common.delete', { entity: t('common.provider', {context: 'single'}) })}
                    buttons={[
                        <Button key="cancel" onClick={() => setDeleteDialog(false)} view='outlined'>{t('buttons.cancel')}</Button>,
                        <Button key="delete" loading={submitted} onClick={onDelete}>{t('buttons.delete')}</Button>
                    ]}
                    onClose={() => setDeleteDialog(false)}>
                    {t('dialogs.delete', {entityName: t('common.provider', {context: 'single'})})} <b>{provider.name}</b>?

                </Dialog>
            }
        </Table.Row>
    );
};
