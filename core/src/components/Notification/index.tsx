import React from 'react';
import { Outlet, useMatch } from 'react-router-dom';
import { useQuery } from '@tanstack/react-query';
import { useTranslation } from 'react-i18next';
import i18next from 'i18next';

import type { IError } from '@msa/kit/types';
import { Button, Error, IconPlus, Loader, Placeholder, Table } from '@msa/kit';

import type { IProvider } from 'types';
import { getProviders } from 'actions';
import { Edit } from 'components/Notification/Edit';
import { ProviderRow } from 'components/Notification/ProviderRow';


export const Providers = () => {
    const match = useMatch('/notification/providers/:children/*');
    const { t } = useTranslation();

    const { isLoading, data, error } = useQuery<IProvider[], IError>(
        ['notification', 'providers'],
        getProviders
    );

    if(isLoading)
        return <Loader layout />;

    if(error)
        return <Error {...error} />;


    if(!data || !data.length)
        return <Placeholder
            message={t('dialogs.createProviders')}
            button={<Edit button={<Button iconLeft={<IconPlus />}>{t('common.create', { entity: t('common.provider', {context: "single"}) })}</Button>} />} />


    if(match)
        return <Outlet />;


    return (
        <>
            <div className="layout__header">
            <h1 className="layout__title">{i18next.format(t('navigation.providers'), 'capitalize')} <span>| {t('common.notifications')}</span></h1>
                {
                    <Edit button={<Button iconLeft={<IconPlus />} label={t('common.create', { entity: t('common.provider', {context: "single"}) })} />} />
                }
            </div>

            <Table cells={3}>
                <Table.Header>
                    <Table.Cell>{t('common.name')}</Table.Cell>
                    <Table.Cell>{t('common.type' )}</Table.Cell>
                    <Table.Cell actions={2} />
                </Table.Header>
                {
                    data.map(provider => <ProviderRow key={provider.UUID} provider={provider} />)
                }
            </Table>
        </>
    );
};
