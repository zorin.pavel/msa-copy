import React from 'react'
import { useFormContext } from 'react-hook-form';
import classNames from 'classnames';

import { IOption } from '@msa/kit/types'
import { Badge } from '@msa/kit'

import { EAlertLevel } from 'types';
import { badgeColors } from 'components/Notification/Alerts';

import css from './edit.module.scss';


interface IProps {
    options: IOption<EAlertLevel, EAlertLevel>[],
    name: string,
    disabled?: boolean
}

export const BadgeButtons = (props: IProps) => {
    const { options, name, disabled } = props;

    const {
        setValue,
        getValues
    } = useFormContext();

    const values = getValues(name);


    const selectScope = (scope: string) => {
        if(disabled)
            return;

        setValue(name,
            values.includes(scope) ? values.filter((el: string) => el !== scope) : [...values, scope],
            { shouldValidate: true, shouldTouch: true }
        );
    };

    return (
        options.map((level) => (
            <Badge
                className={classNames(css.level, disabled && css.disabled)}
                onClick={() => selectScope(level.value)}
                key={level.value}
                label={level.label}
                size="small"
                color={values.includes(level.value) ? badgeColors[level.value] : 'default'}
            />)
        )
    )
}
