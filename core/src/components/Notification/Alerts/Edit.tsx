import React, { ReactElement, useCallback, useState } from 'react';
import { useQuery, useQueryClient } from '@tanstack/react-query';
import { Controller, FormProvider, useForm } from 'react-hook-form';
import { useTranslation } from 'react-i18next';

import {  IError } from '@msa/kit/types';
import { Button, Drawer, IconEdit, Input, Select, Textarea, Toast, useOptions } from '@msa/kit';

import {
    EAlertLevel,
    EProviderType,
    IAlert,
    type IProvider,
    TNotificationForm, TNotificationPostData
} from 'types';
import { getProviders, postChannel, putChannel } from 'actions';
import { BadgeButtons } from 'components/Notification/Alerts/BadgeButtons';


interface IProps {
    disabled?: boolean,
    button?: ReactElement,
    alert?: IAlert,
}

export const Edit = (props: IProps) => {
    const { button, disabled, alert } = props;
    const queryClient = useQueryClient();
    const { t } = useTranslation();

    const [submitted, setSubmitted] = useState<boolean>(false);
    const [drawer, setDrawer] = useState<boolean>(false);

    const defaultValues: TNotificationForm = alert ?
        {
            ...alert,
            provider_name: alert.notification_provider.name,
            destination: alert.destination.mail_addresses?.join('; ') ?? alert.destination.telegram_chat_ids?.join('; ')
        } :
        {
            name: '',
            provider_name: '',
            destination: '',
            enabled: true,
            level: [EAlertLevel.ERROR]
        };

    const formMethods = useForm({
        mode: 'onChange',
        reValidateMode: 'onChange',
        defaultValues
    });
    const {
        control,
        reset,
        handleSubmit,
        formState: { errors, isValid }
    } = formMethods;


    const { data: providers } = useQuery<IProvider[], IError>(
        ['notification', 'providers'],
        getProviders
    );
    const providerOptions = useOptions(providers, 'name', 'name');
    const alertOptions = useOptions<any, EAlertLevel, EAlertLevel>(Object.keys(EAlertLevel).map(level => ({ label: t(`status.${level.toLowerCase()}`), value: level })), 'value', 'label');

    const onSubmit = useCallback((formData: TNotificationForm) => {
        if(!providers) {
            Toast.error({ message: t('status.failed' ) });

            return false;
        }

        setSubmitted(true);
        const postData: TNotificationPostData = { ...formData, destination: {} };

        const provider = providers.find(provider => provider.name === formData.provider_name);

        switch(provider?.type) {
            case EProviderType.EMAIL:
                postData.destination.mail_addresses = formData.destination?.split(/[;,\s]+/).filter(email => email !== '');
                break;
            case EProviderType.TELEGRAM:
                postData.destination.telegram_chat_ids = formData.destination?.split(/[;,\s]+/).filter(email => email !== '');
                break;
            default:
                Toast.error({ message: 'Unexpected error' });
                setSubmitted(false);

                return false;
        }

        return (alert ? putChannel(alert.name, postData) : postChannel(postData))
            .then((response: IAlert) => {
                setDrawer(false);
                Toast.success({ message: t('toasts.edit', { entity: t('common.channel'), entityName: response.name }) });
                queryClient.invalidateQueries(['notification', 'channels']);
            })
            .catch((error: IError) => {
                Toast.error({ message: error.message });

                return false;
            })
            .finally(() => setSubmitted(false));
    }, [providers, alert]);


    const onReset = () => {
        reset(defaultValues);
    };


    let drawerHeader = t('common.create', { entity: t('common.channel') });
    if(alert)
        drawerHeader = t('common.edit', { entity: t('common.channel') }) + alert.name;

    return (
        <>
            {
                button ?
                    React.cloneElement(button, { onClick: () => setDrawer(true), disabled }) :
                    <IconEdit onClick={() => setDrawer(true)} disabled={disabled} />
            }

            {
                drawer &&
                <Drawer header={drawerHeader} open={drawer} onClose={() => setDrawer(false)} backdrop>
                    <FormProvider {...formMethods}>
                        <form onSubmit={handleSubmit(onSubmit)} noValidate onReset={onReset}>
                            <Drawer.Body>
                                <Controller
                                    control={control}
                                    name="name"
                                    rules={
                                        {
                                            required: t('validation.isRequired', { entityName: t('common.channelName' )}),
                                        }
                                    }
                                    render={({ field }) => (
                                        <Input
                                            {...field}
                                            label={t('common.channelName' )}
                                            placeholder={t('common.channelName' )}
                                            labelPosition="left"
                                            required
                                            error={!!errors.name}
                                            autoComplete="off" />
                                    )} />
                                <Controller
                                    control={control}
                                    name="provider_name"
                                    rules={
                                        {
                                            required: t('validation.isRequired', { entityName: t('common.provider' )}),
                                        }
                                    }
                                    render={({ field }) => (
                                        <Select
                                            {...field}
                                            label={t('common.provider' )}
                                            labelPosition="left"
                                            required
                                            error={!!errors.provider_name}
                                            disabled={!!alert}
                                            options={providerOptions} />
                                    )} />
                                <div className="row align-center" style={{ marginBottom: '1.5rem' }}>
                                    <label className="c1of3">{t('common.messageLevel' )}</label>
                                    <div className="c2of3"><BadgeButtons options={alertOptions} name="level" /></div>
                                </div>
                                <Controller
                                    control={control}
                                    name="destination"
                                    rules={
                                        {
                                            required: t('validation.isRequired', { entityName: t('common.destination' )}),
                                        }
                                    }
                                    render={({ field }) => (
                                        <Textarea
                                            {...field}
                                            label={t('common.destination' )}
                                            placeholder="username@email.com; ..."
                                            labelPosition="left"
                                            required
                                            error={!!errors.destination}
                                            autoComplete="off" />
                                    )} />
                            </Drawer.Body>
                            <Drawer.Footer>
                                <Button type="reset" view="outlined">{t('buttons.clear')}</Button>
                                <Button disabled={!isValid} loading={submitted} type="submit">{t('buttons.done')}</Button>
                            </Drawer.Footer>
                        </form>
                    </FormProvider>
                </Drawer>
            }
        </>
    );
};
