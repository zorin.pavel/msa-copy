import React from 'react';
import { Outlet, useMatch } from 'react-router-dom';
import { useQuery } from '@tanstack/react-query';
import { useTranslation } from 'react-i18next';
import i18next from 'i18next';

import { Colors, IError } from '@msa/kit/types';
import { Button, Error, IconPlus, Loader, Placeholder, Table } from '@msa/kit';

import { EAlertLevel, IAlert } from 'types';
import { getChannels } from 'actions';
import { Edit } from 'components/Notification/Alerts/Edit';
import { AlertRow } from 'components/Notification/Alerts/AlertRow';


export const badgeColors: Record<EAlertLevel, keyof typeof Colors> = {
    ERROR: 'red',
    WARNING: 'yellow',
    INFO: 'blue',
    // ONLINE: 'green',
};



export const Alerts = () => {
    const match = useMatch('/notification/alerts/:children/*');
    const { t } = useTranslation();

    const { isLoading, data, error } = useQuery<IAlert[], IError>(
        ['notification', 'channels'],
        getChannels
    );

    if(isLoading)
        return <Loader layout />;

    if(error)
        return <Error {...error} />;


    if(!data || !data.length)
        return <Placeholder
            message={t('dialogs.createChannel')}
            button={<Edit button={<Button iconLeft={<IconPlus />}>{t('common.create', { entity: t('common.channel') })}</Button>} />} />


    if(match)
        return <Outlet />;


    return (
        <>
            <div className="layout__header">
                <h1 className="layout__title">{i18next.format(t('nav.alerts'), 'capitalize')} <span>| {t('common.notifications')}</span></h1>
                {
                    <Edit button={<Button color="red" label={t('common.create', { entity: t('nav.alert') })} iconLeft={<IconPlus />}></Button>} />
                }
            </div>

            <Table cells={7}>
                <Table.Header>
                    <Table.Cell>{t('common.name')}</Table.Cell>
                    <Table.Cell>{t('common.type')}</Table.Cell>
                    <Table.Cell>{t('common.provider')}</Table.Cell>
                    <Table.Cell>{t('common.level')}</Table.Cell>
                    <Table.Cell />
                    <Table.Cell>{t('common.enable')}</Table.Cell>
                    <Table.Cell actions={2} />
                </Table.Header>
                {
                    data.map(notification => <AlertRow key={notification.UUID} alert={notification} />)
                }
            </Table>
        </>
    );
};
