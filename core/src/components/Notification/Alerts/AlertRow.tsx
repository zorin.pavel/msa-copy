import React, { useCallback, useState } from 'react';
import { useQueryClient } from '@tanstack/react-query';
import { useTranslation } from 'react-i18next';
import classNames from 'classnames';

import type { IError } from '@msa/kit/types';
import { Badge, Button, Dialog, IconDelete, isAuthorized, Switcher, Table, Toast } from '@msa/kit';

import type { IAlert } from 'types';
import { deleteChannel, enableChannel, testChannel } from 'actions';
import { Edit } from 'components/Notification/Alerts/Edit';
import { badgeColors } from 'components/Notification/Alerts/index';


interface IProps {
    alert: IAlert,
}

export const AlertRow = (props: IProps) => {
    const { alert } = props;
    const queryClient = useQueryClient();
    const { t } = useTranslation();

    const [submitted, setSubmitted] = useState<boolean>(false);
    const [deleteDialog, setDeleteDialog] = useState<boolean>(false);


    const onDelete = useCallback(() => {
        setSubmitted(true);

        return deleteChannel(alert.name)
            .then(() => {
                Toast.success({ message: t('toasts.delete', { entity: t('common.channel'), entityName: alert.name }) });
                queryClient.invalidateQueries(['notification', 'channels']);
            })
            .catch((error: IError) => {
                Toast.error({ message: error.message });

                return false;
            })
            .finally(() => setSubmitted(false));
    }, [alert, queryClient]);


    const handleEnable = useCallback(() => {
        return enableChannel(alert.name, { ...alert, enabled: !alert.enabled })
            .then(() => {
                Toast.success({ message: t('toasts.edit', { entity: t('common.channel'), entityName: alert.name }) });
            })
            .catch((error: IError) => {
                Toast.error({ message: error.message });

                return false;
            })
            .finally(() => queryClient.invalidateQueries(['notification', 'channels']));
    }, [alert, enableChannel]);




    const handleSendTest = useCallback(() => {
        setSubmitted(true);

        return testChannel(alert.name)
            .then((response) => {
                Toast.success({ message: response.message });
            })
            .catch((error: IError) => {
                Toast.error({ message: error.message });

                return false;
            })
            .finally(() => setSubmitted(false))
    }, [alert, testChannel]);


    return (
        <Table.Row valign="center" className={classNames('entity__row', !alert.enabled && 'disabled')}>
            <Table.Cell>{alert.name}</Table.Cell>
            <Table.Cell>{alert.notification_provider.type}</Table.Cell>
            <Table.Cell>{alert.notification_provider.name}</Table.Cell>
            <Table.Cell>
                {
                    alert.level.map(level => <Badge key={level} label={t(`status.${level.toLowerCase()}`)} size="xsmall" color={badgeColors[level]} />)
                }
            </Table.Cell>
            <Table.Cell>
                <Button
                    size="small"
                    color="dark"
                    onClick={handleSendTest}
                    loading={submitted}>{t('buttons.sendMessage')}</Button>
            </Table.Cell>
            <Table.Cell>
                <Switcher
                    name={`2fa-${alert.name}`}
                    value="enable"
                    checked={alert.enabled}
                    onChange={handleEnable}
                    disabled={!isAuthorized({ settings: ['edit'] })} />
            </Table.Cell>
            <Table.Cell actions={2}>
                <Edit disabled={!isAuthorized({ settings: ['delete'] })} alert={alert} />
                <IconDelete onClick={() => setDeleteDialog(true)} disabled={!isAuthorized({ settings: ['delete'] })} />
            </Table.Cell>

            {
                deleteDialog &&
                <Dialog
                    open={deleteDialog}
                    header={t('common.delete', { entity: t('common.channel') })}
                    buttons={[
                        <Button key="cancel" onClick={() => setDeleteDialog(false)} view='outlined'>{t('buttons.cancel')}</Button>,
                        <Button key="delete" loading={submitted} onClick={onDelete}>{t('buttons.delete')}</Button>
                    ]}
                    onClose={() => setDeleteDialog(false)}>
                    {t('dialogs.delete', {entityName: t('common.channel')})} <b>{alert.name}</b>?
                </Dialog>
            }
        </Table.Row>
    );
};
