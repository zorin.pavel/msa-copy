import React, { ReactElement, useCallback, useState } from 'react';
import { useQueryClient } from '@tanstack/react-query';
import { Controller, useForm, useWatch } from 'react-hook-form';
import { useTranslation } from 'react-i18next';

import { IError } from '@msa/kit/types';
import { Button, Checkbox, Drawer, IconEdit, Input, Select, Toast } from '@msa/kit';

import { EProviderType, IProvider } from 'types';
import { postProvider, putProvider } from 'actions';


interface IProps {
    disabled?: boolean,
    button?: ReactElement,
    provider?: IProvider,
}


export const Edit = (props: IProps) => {
    const { button, disabled, provider } = props;
    const queryClient = useQueryClient();
    const { t } = useTranslation();

    const [submitted, setSubmitted] = useState<boolean>(false);
    const [drawer, setDrawer] = useState<boolean>(false);

    const typesOptions = [
        { label: t('notifications.email'), value: EProviderType.EMAIL },
        { label: t('notifications.telegram'), value: EProviderType.TELEGRAM },
        { label: t('notifications.msTeams'), value: EProviderType.TEAMS, disabled: true },
        { label: t('notifications.trueConf'), value: EProviderType.TRUCONF, disabled: true },
    ];

    const defaultValues: IProvider = provider ??
        {
            name: '',
            type: EProviderType.EMAIL,
            settings: {
                smtp_server: '',
                smtp_port: 587,
                smtp_tls: true,
            }
        };

    const {
        control,
        handleSubmit,
        reset,
        formState: { errors, isValid },
    } = useForm({
        mode: 'onChange',
        reValidateMode: 'onChange',
        defaultValues
    });


    const target = useWatch({
        control,
        name: 'type'
    });


    const onSubmit = useCallback((formData: IProvider) => {
        setSubmitted(true);

        // can come with provider
        delete formData.notifications;

        switch(formData.type) {
            case EProviderType.EMAIL:
            default:
                delete formData.settings.telegram_bot_token;
                break;
            case EProviderType.TELEGRAM:
                delete formData.settings.smtp_server;
                delete formData.settings.smtp_port;
                delete formData.settings.smtp_tls;
                delete formData.settings.smtp_username;
                delete formData.settings.smtp_password;
                break;
        }

        return (provider ? putProvider(provider.name, formData) : postProvider(formData))
            .then((response: IProvider) => {
                setDrawer(false);
                Toast.success({ message: t('toasts.edit', { entity: t('common.provider'), entityName: response.name }) });
                queryClient.invalidateQueries(['notification']);
            })
            .catch((error: IError) => {
                Toast.error({ message: error.message });

                return false;
            })
            .finally(() => setSubmitted(false));
    }, [provider]);


    const onReset = () => {
        reset(defaultValues);
    };


    const renderSettings = useCallback(() => {
        switch(target) {
            case EProviderType.TELEGRAM:
                return (
                    <Controller
                        control={control}
                        name="settings.telegram_bot_token"
                        rules={
                            {
                                required: 'Token is required',
                            }
                        }
                        render={({ field }) => (
                            <Input
                                {...field}
                                label="Bot token"
                                labelPosition="left"
                                required
                                // @ts-ignore
                                error={!!(errors.settings && errors.settings.telegram_bot_token)}
                                autoComplete="off" />
                        )} />
                );
            case EProviderType.EMAIL:
            default:
                return (
                    <>
                        <Controller
                            control={control}
                            name="settings.smtp_server"
                            rules={
                                {
                                    required: t('validation.isRequired', { entityName: t('common.smtpServer' )}),
                                }
                            }
                            render={({ field }) => (
                                <Input
                                    {...field}
                                    label={t('common.smtpServer' )}
                                    labelPosition="left"
                                    required
                                    error={!!(errors.settings && errors.settings.smtp_server)}
                                    autoComplete="off" />
                            )} />
                        <Controller
                            control={control}
                            name="settings.smtp_port"
                            rules={
                                {
                                    required: t('validation.isRequired', { entityName: t('common.smtpPort' )}),
                                }
                            }
                            render={({ field }) => (
                                <Input
                                    {...field}
                                    label={t('common.smtpPort' )}
                                    labelPosition="left"
                                    required
                                    error={!!(errors.settings && errors.settings.smtp_port)}
                                    autoComplete="off" />
                            )} />
                        <Controller
                            control={control}
                            name="settings.smtp_tls"
                            rules={
                                {
                                    required: t('validation.isRequired', { entityName: t('common.smtpTls' )}),
                                }
                            }
                            render={({ field }) => (
                                <Checkbox
                                    {...field}
                                    checked={!!field.value}
                                    label={t('common.smtpTls' )}
                                    labelPosition="formLeft"
                                    required
                                    error={!!(errors.settings && errors.settings.smtp_tls)}
                                    autoComplete="off" />
                            )} />
                        <Controller
                            control={control}
                            name="settings.smtp_username"
                            render={({ field }) => (
                                <Input
                                    {...field}
                                    label={t('common.smtpUser' )}
                                    labelPosition="left"
                                    error={!!(errors.settings && errors.settings.smtp_username)}
                                    autoComplete="off" />
                            )} />
                        <Controller
                            control={control}
                            name="settings.smtp_password"
                            render={({ field }) => (
                                <Input
                                    {...field}
                                    type="password"
                                    label={t('common.smtpPass' )}
                                    labelPosition="left"
                                    error={!!(errors.settings && errors.settings.smtp_password)}
                                    autoComplete="off" />
                            )} />
                    </>
                );
        }
    }, [target]);


    let drawerHeader = t('common.create', { entity: t('common.provider', {context: "single"}) });
    if(provider)
        drawerHeader = t('common.edit', { entity: t('common.provider', {context: "single"}) }) + provider.name;

    return (
        <>
            {
                button ?
                    React.cloneElement(button, { onClick: () => setDrawer(true), disabled }) :
                    <IconEdit onClick={() => setDrawer(true)} disabled={disabled} />
            }

            {
                drawer &&
                <Drawer header={drawerHeader} open={drawer} onClose={() => setDrawer(false)} backdrop>
                    <form onSubmit={handleSubmit(onSubmit)} noValidate onReset={onReset}>
                        <Drawer.Body>
                            <Controller
                                control={control}
                                name="name"
                                rules={
                                    {
                                        required: t('validation.isRequired', { entityName: t('common.notificationProvider' )}),
                                    }
                                }
                                render={({ field }) => (
                                    <Input
                                        {...field}
                                        label={t('common.notificationProvider' )}
                                        placeholder={t('common.notificationProvider' )}
                                        labelPosition="left"
                                        required
                                        error={errors.name && errors.name.message as string}
                                        autoComplete="off" />
                                )} />
                            <Controller
                                control={control}
                                name="type"
                                rules={
                                    {
                                        required: t('validation.isRequired', { entityName: t('common.providerTarget' )}),
                                    }
                                }
                                render={({ field }) => (
                                    <Select
                                        {...field}
                                        disabled={!!provider}
                                        label={t('common.providerTarget' )}
                                        labelPosition="left"
                                        required
                                        error={!!errors.type}
                                        options={typesOptions} />
                                )} />
                            {
                                renderSettings()
                            }
                        </Drawer.Body>
                        <Drawer.Footer>
                            <Button type="reset" view="outlined" color="red" onClick={onReset}>{t('buttons.clear')}</Button>
                            <Button disabled={!isValid} loading={submitted} type="submit" onClick={handleSubmit(onSubmit)}>{t('buttons.done')}</Button>
                        </Drawer.Footer>
                    </form>
                </Drawer>
            }
        </>
    );
};
