import { env } from 'env';
import { IRepository } from 'types';
import { API } from 'helpers/apiInstance';


export const getVendorRepos = (repoType: string) => {
    return API.get<any, IRepository[]>(`/tenants/${env.MSA_DEFAULT_TENANT}/vendor_repos/${repoType}/`);
};


export const postVendorRepo = (repoType: IRepository['type'], params: IRepository) => {
    return API.post<IRepository, IRepository, IRepository>(`/tenants/${env.MSA_DEFAULT_TENANT}/vendor_repos/${repoType}/`, params);
};


export const deleteVendorRepo = (repoType: IRepository['type'], repoName: string) => {
    return API.delete(`/tenants/${env.MSA_DEFAULT_TENANT}/vendor_repos/${repoType}/${repoName}`);
};


export const checkVendorRepo = (repoType: IRepository['type'], repoName: string) => {
    return API.get(`/tenants/${env.MSA_DEFAULT_TENANT}/vendor_repos/${repoType}/${repoName}/check`);
};


export const getPlatformRepos = (platformName?: string, repoType?: string) => {
    return API.get<any, IRepository[]>(`/tenants/${env.MSA_DEFAULT_TENANT}/platforms/${platformName}/user_repos/${repoType}/`);
};


export const postPlatformRepo = (platformName: string, repoType: IRepository['type'], params: IRepository) => {
    return API.post<IRepository, IRepository, IRepository>(`/tenants/${env.MSA_DEFAULT_TENANT}/platforms/${platformName}/user_repos/${repoType}/`, params);
};


export const deletePlatformRepo = (platformName: string, repoType: IRepository['type'], repoName: string) => {
    return API.delete(`/tenants/${env.MSA_DEFAULT_TENANT}/platforms/${platformName}/user_repos/${repoType}/${repoName}`);
};


export const checkPlatformRepo = (platformName: string, repoType: IRepository['type'], repoName: string) => {
    return API.get(`/tenants/${env.MSA_DEFAULT_TENANT}/platforms/${platformName}/user_repos/${repoType}/${repoName}/check`);
};


