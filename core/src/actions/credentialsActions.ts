import { env } from 'env';
import { ICredential, ICredentialPost } from 'types';
import { API } from 'helpers/apiInstance';


export const getCredentials = (platformSlug?: string) => {
    return API.get<any, ICredential[]>(`/tenants/${env.MSA_DEFAULT_TENANT}/platforms/${platformSlug}/credentials/`);
};

export const postCredential = (platformSlug?: string, params?: Partial<ICredentialPost>) => {
    return API.post<Partial<ICredentialPost>, ICredential>(`/tenants/${env.MSA_DEFAULT_TENANT}/platforms/${platformSlug}/credentials/`, params);
};

export const downloadCredKey = (platformSlug?: string, credName?: string) => {
    return API.get<any, string>(`/tenants/${env.MSA_DEFAULT_TENANT}/platforms/${platformSlug}/credentials/${credName}/download`);
};

export const deleteCredKey = (platformSlug?: string, credName?: string) => {
    return API.delete(`/tenants/${env.MSA_DEFAULT_TENANT}/platforms/${platformSlug}/credentials/${credName}`);
};

export const getCloudCredentials = () => {
    return [];
}
