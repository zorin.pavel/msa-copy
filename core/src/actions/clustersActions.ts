import { IRbacGroupObject } from '@msa/kit/types';

import { env } from 'env';
import type {
    ICluster,
    IClusterInfo,
    IClusterValues,
    IClusterNode,
    IClusterNodeMetrics, INodeLabel, INodeTaint, IClusterNodeLog,
} from 'types';
import { API } from 'helpers/apiInstance';


export const getClusters = (platformSlug?: string) => {
    return API.get<any, ICluster[]>(`/tenants/${env.MSA_DEFAULT_TENANT}/platforms/${platformSlug}/clusters/`);
};


export const loadCluster = (platformSlug?: string, clusterSlug?: string) => {
    return API.get<any, ICluster>(`/tenants/${env.MSA_DEFAULT_TENANT}/platforms/${platformSlug}/clusters/${clusterSlug}`);
};


export const loadClusterPermissions = (platformSlug?: string, clusterSlug?: string) => {
    return API.get<any, IRbacGroupObject[]>(`/tenants/${env.MSA_DEFAULT_TENANT}/platforms/${platformSlug}/clusters/${clusterSlug}/permissions`);
};


export const getClusterValues = (platformSlug?: string) => {
    return API.get<any, IClusterValues>(`/tenants/${env.MSA_DEFAULT_TENANT}/platforms/${platformSlug}/clusters/choices`);
};


export const postCluster = (platformSlug?: string, params?: Partial<ICluster>) => {
    return API.post<ICluster, ICluster>(`/tenants/${env.MSA_DEFAULT_TENANT}/platforms/${platformSlug}/clusters/`, params)
        .then((response: ICluster) => response);
};


export const putCluster = (platformSlug?: string, clusterSlug?: string, params?: Partial<ICluster>) => {
    return API.put<ICluster, ICluster>(`/tenants/${env.MSA_DEFAULT_TENANT}/platforms/${platformSlug}/clusters/${clusterSlug}`, params)
        .then((response: ICluster) => response);
};


export const redeployCluster = (platformSlug?: string, params?: Partial<ICluster>) => {
    return API.put<ICluster, ICluster>(
        `/tenants/${env.MSA_DEFAULT_TENANT}/platforms/${platformSlug}/clusters/${params?.name}/recreate`,
        params
    )
        .then((response: ICluster) => response);
};


export const getClusterConfig = (platformSlug?: string, clusterSlug?: string) => {
    return API.get<any, Blob | MediaSource>(
        `/tenants/${env.MSA_DEFAULT_TENANT}/platforms/${platformSlug}/clusters/${clusterSlug}/kubeconfig_file`,
        { responseType: 'blob' }
    )
        .then((response: Blob | MediaSource) => {
            const href = URL.createObjectURL(response);

            const link = document.createElement('a');

            link.href = href;
            link.setAttribute('download', `${platformSlug}-${clusterSlug}-kubeconfig.yml`);
            document.body.appendChild(link);
            link.click();

            document.body.removeChild(link);
            URL.revokeObjectURL(href);
        });
};


export const getClusterInfo = (platformSlug?: string, clusterSlug?: string) => {
    return API.get<any, IClusterInfo>(`/tenants/${env.MSA_DEFAULT_TENANT}/platforms/${platformSlug}/clusters/${clusterSlug}/info/cluster`);
};


export const getClusterInfoNodes = (platformSlug: string, clusterSlug: string) => {
    return API.get<any, any>(`/tenants/${env.MSA_DEFAULT_TENANT}/platforms/${platformSlug}/clusters/${clusterSlug}/info/nodes`);
};


export const getClusterPods = (platformSlug?: string, clusterSlug?: string) => {
    return API.get<any, any>(`/tenants/${env.MSA_DEFAULT_TENANT}/platforms/${platformSlug}/clusters/${clusterSlug}/info/master_pods`);
};


export const getClusterMetrics = (platformSlug?: string, clusterSlug?: string) => {
    return API.get<any, IClusterNodeMetrics[]>(`/tenants/${env.MSA_DEFAULT_TENANT}/platforms/${platformSlug}/clusters/${clusterSlug}/metrics/nodes`);
};


export const getUpgradeVersions = (platformSlug?: string, clusterSlug?: string) => {
    return API.get<any, Record<string, boolean>>(`/tenants/${env.MSA_DEFAULT_TENANT}/platforms/${platformSlug}/clusters/${clusterSlug}/upgrade_versions`);
};


export const upgradeCluster = (platformSlug?: string, clusterSlug?: string, params?: Partial<ICluster>) => {
    return API.put<ICluster, ICluster>(`/tenants/${env.MSA_DEFAULT_TENANT}/platforms/${platformSlug}/clusters/${clusterSlug}/upgrade?kube_version=${params?.kube_version}`, params);
};


export const addClusterWorker = (platformSlug?: string, clusterSlug?: string, params?: Partial<IClusterNode>[]) => {
    return API.put<ICluster, ICluster>(`/tenants/${env.MSA_DEFAULT_TENANT}/platforms/${platformSlug}/clusters/${clusterSlug}/add_worker`, params);
};


export const removeClusterWorker = (platformSlug?: string, clusterSlug?: string, nodeName?: string, force = false) => {
    return API.put<ICluster, ICluster>(`/tenants/${env.MSA_DEFAULT_TENANT}/platforms/${platformSlug}/clusters/${clusterSlug}/remove_worker`,
        [nodeName],
        {
            params: { force },
        });
};


export const installTools = (platformSlug?: string, clusterSlug?: string, params?: any) => {
    return API.put<any, ICluster>(`/tenants/${env.MSA_DEFAULT_TENANT}/platforms/${platformSlug}/clusters/${clusterSlug}/install_cluster_tools?vendor_repo_name=${params.vendor_repo_name}`, params);
};


export const deleteCluster = (platformSlug?: string, clusterSlug?: string) => {
    return API.delete(`/tenants/${env.MSA_DEFAULT_TENANT}/platforms/${platformSlug}/clusters/${clusterSlug}`);
};


export const deleteClusterForce = (platformSlug?: string, clusterSlug?: string) => {
    return API.delete(`/tenants/${env.MSA_DEFAULT_TENANT}/platforms/${platformSlug}/clusters/${clusterSlug}/force`);
};


export const installClusterDashboard = (platformSlug?: string, clusterSlug?: string, params?: Record<string, string>) => {
    return API.put(`/tenants/${env.MSA_DEFAULT_TENANT}/platforms/${platformSlug}/clusters/${clusterSlug}/add_dashboard?url=${params?.url}&oauth2domain=${params?.oauth2domain}`);
};


export const putClusterGroups = (platformSlug?: string, clusterSlug?: string, data?: IRbacGroupObject[]) => {
    return API.put(`/tenants/${env.MSA_DEFAULT_TENANT}/platforms/${platformSlug}/clusters/${clusterSlug}/edit_permissions`, data);
}


export const putClusterNodeLabels = (platformSlug?: string, clusterSlug?: string, nodeName?: string, labels?: INodeLabel[]) => {
    return API.put<INodeLabel[], IClusterNode>(`/tenants/${env.MSA_DEFAULT_TENANT}/platforms/${platformSlug}/clusters/${clusterSlug}/node/${nodeName}/labels`, labels);
}


export const deleteClusterNodeLabel = (platformSlug?: string, clusterSlug?: string, nodeName?: string, labels?: INodeLabel[]) => {
    return API.delete<INodeLabel[], IClusterNode>(`/tenants/${env.MSA_DEFAULT_TENANT}/platforms/${platformSlug}/clusters/${clusterSlug}/node/${nodeName}/labels`, { data: labels });
}


export const putClusterNodeTaints = (platformSlug?: string, clusterSlug?: string, nodeName?: string, taints?: INodeTaint[]) => {
    return API.put<INodeTaint[], IClusterNode>(`/tenants/${env.MSA_DEFAULT_TENANT}/platforms/${platformSlug}/clusters/${clusterSlug}/node/${nodeName}/taints`, taints);
}


export const deleteClusterNodeTaint = (platformSlug?: string, clusterSlug?: string, nodeName?: string, taints?: INodeTaint[]) => {
    return API.delete<INodeTaint[], IClusterNode>(`/tenants/${env.MSA_DEFAULT_TENANT}/platforms/${platformSlug}/clusters/${clusterSlug}/node/${nodeName}/taints`, { data: taints });
}


export const loadClusterNodeLogs = (platformSlug?: string, clusterSlug?: string, nodeName?: string, params?: any) => {
    return API.get<any, IClusterNodeLog[]>(`/tenants/${env.MSA_DEFAULT_TENANT}/platforms/${platformSlug}/clusters/${clusterSlug}/node/${nodeName}/logs`, { params });
}
