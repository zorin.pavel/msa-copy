import { IRbacGroupObject } from '@msa/kit/types';

import { env } from 'env';
import { IPlatform, IPlatformNode } from 'types';
import { API } from 'helpers/apiInstance';


export const getPlatforms = () => {
    return API.get<any, IPlatform[]>(`/tenants/${env.MSA_DEFAULT_TENANT}/platforms/`);
};


export const loadPlatform = (platformSlug: string) => {
    return API.get<any, IPlatform>(`/tenants/${env.MSA_DEFAULT_TENANT}/platforms/${platformSlug}`);
};


export const loadPlatformPermissions = (platformSlug?: string) => {
    return API.get<any, IRbacGroupObject[]>(`/tenants/${env.MSA_DEFAULT_TENANT}/platforms/${platformSlug}/permissions`);
};


export const postPlatform = (params: IPlatform) => {
    return API.post<IPlatform, IPlatform>(`/tenants/${env.MSA_DEFAULT_TENANT}/platforms/`, params);
};


export const putPlatform = (platformSlug: string, params: IPlatform) => {
    return API.put<IPlatform, IPlatform>(`/tenants/${env.MSA_DEFAULT_TENANT}/platforms/${platformSlug}`, params);
};


export const deletePlatform = (platformSlug: string) => {
    return API.delete(`/tenants/${env.MSA_DEFAULT_TENANT}/platforms/${platformSlug}`);
};


export const getPlatformNodes = (platformSlug?: string) => {
    return API.get<any, IPlatformNode[]>(`/tenants/${env.MSA_DEFAULT_TENANT}/platforms/${platformSlug}/nodes/`);
};


export const postPlatformNode = (platformSlug?: string, node?: Partial<IPlatformNode>) => {
    return API.post<Partial<IPlatformNode>, IPlatformNode>(`/tenants/${env.MSA_DEFAULT_TENANT}/platforms/${platformSlug}/nodes/`, node);
};


export const putPlatformNode = (platformSlug?: string, nodeName?: string, node?: Partial<IPlatformNode>) => {
    return API.put<Partial<IPlatformNode>, IPlatformNode>(`/tenants/${env.MSA_DEFAULT_TENANT}/platforms/${platformSlug}/nodes/${nodeName}`, node);
};


export const deletePlatformNode = (platformSlug?: string, nodeName?: string) => {
    return API.delete(`/tenants/${env.MSA_DEFAULT_TENANT}/platforms/${platformSlug}/nodes/${nodeName}`);
};


export const getNodeTemplates = () => {
    return [];
}

export const putPlatformGroups = (platformSlug?: string, data?: IRbacGroupObject[]) => {
    return API.put(`/tenants/${env.MSA_DEFAULT_TENANT}/platforms/${platformSlug}/edit_permissions`, data);
}
