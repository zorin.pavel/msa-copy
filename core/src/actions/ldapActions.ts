import { env } from 'env';
import type { ILdap, ILdapPost } from 'types';
import { API } from 'helpers/apiInstance';


export const getLdap = () => {
    return API.get<any, ILdap[]>(`/tenants/${env.MSA_DEFAULT_TENANT}/iam/ldap/` );
};


export const postLdap = (params: ILdapPost) => {
    return API.post<ILdapPost, ILdap>(`/tenants/${env.MSA_DEFAULT_TENANT}/iam/ldap/`, {
        ...params,
        ldap_name: params.ldapName,
        group_dn: params.groupDn,
        first_name_ldap_attribute: params.firstNameLdapAttribute
    });
};


export const putLdap = (params: Partial<ILdap>) => {
    return API.put<Partial<ILdap>, ILdap>(`/tenants/${env.MSA_DEFAULT_TENANT}/iam/ldap/${params.name}?enabled=${params.config?.enabled}`);
};


export const deleteLdap = (name: string) => {
    return API.delete(`/tenants/${env.MSA_DEFAULT_TENANT}/iam/ldap/${name}`);
};


export const syncLdap = (name: string, sync_type: string) => {
    return API.get(`/tenants/${env.MSA_DEFAULT_TENANT}/iam/ldap/${name}/sync`, {
        params: {
            sync_type
        }
    });
};
