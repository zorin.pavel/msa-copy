import { env } from 'env';
import { API } from 'helpers/apiInstance';
import { ILogMessage } from 'types';


export const getLogs = (params: Record<string, string | number | undefined | boolean>) => {
    const currentDate = new Date();
    const stop: number = Math.floor(currentDate.getTime() / 1000);
    const period = params.selectedPeriod as number;
    const start: number = stop - period;
    delete params.selectedPeriod;

    const { action, object_type, username, object_name, advanced } = params;

    Object.keys(params).map(key => {
        if(!params[key]) {
            return delete params[key];
        }
    });

    return API.get<any, ILogMessage[]>(`/tenants/${env.MSA_DEFAULT_TENANT}/user_log/`, {
        params: {
            start,
            stop,
            action,
            object_type,
            object_name,
            username,
            advanced
        }
    })
        .then((response) => response)
        .catch(() => [])
};
