import { env } from 'env';
import { IAlert, IProvider, TNotificationPostData } from 'types';
import { API } from 'helpers/apiInstance';


export const getProviders = () => {
    return API.get<any, IProvider[]>(`/tenants/${env.MSA_DEFAULT_TENANT}/notification_providers/`);
};


export const postProvider = (params: IProvider) => {
    return API.post<IProvider, IProvider>(`/tenants/${env.MSA_DEFAULT_TENANT}/notification_providers/`, params);
};


export const putProvider = (providerName: string, params: IProvider) => {
    return API.put<IProvider, IProvider>(`/tenants/${env.MSA_DEFAULT_TENANT}/notification_providers/${providerName}`, params);
};


export const deleteProvider = (providerName: string) => {
    return API.delete(`/tenants/${env.MSA_DEFAULT_TENANT}/notification_providers/${providerName}`);
};


export const getChannels = () => {
    return API.get<any, IAlert[]>(`/tenants/${env.MSA_DEFAULT_TENANT}/notifications/`);
};


export const postChannel = (params: TNotificationPostData) => {
    return API.post<TNotificationPostData, IAlert>(`/tenants/${env.MSA_DEFAULT_TENANT}/notifications/`, params);
};


export const putChannel = (channelName: string, params: TNotificationPostData) => {
    return API.put<TNotificationPostData, IAlert>(`/tenants/${env.MSA_DEFAULT_TENANT}/notifications/${channelName}`, params);
};


export const deleteChannel = (channelName: string) => {
    return API.delete(`/tenants/${env.MSA_DEFAULT_TENANT}/notifications/${channelName}`);
};


export const enableChannel = (channelName: string, params: IAlert) => {
    return API.put(`/tenants/${env.MSA_DEFAULT_TENANT}/notifications/${channelName}`, params);
};


export const testChannel = (channelName: string) => {
    return API.get<any, { message: string }>(`/tenants/${env.MSA_DEFAULT_TENANT}/notifications/${channelName}/send_test_message`);
};


