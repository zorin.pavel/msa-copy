import { env } from 'env';
import { ICluster, INamespace, IProject } from 'types';
import { API } from 'helpers/apiInstance';


export const getClusterProjects = (platformName?: string, clusterName?: string) => {
    return API.get<any, IProject[]>(`/tenants/${env.MSA_DEFAULT_TENANT}/platforms/${platformName}/clusters/${clusterName}/projects/`);
};


export const loadClusterProject = (platformName?: string, clusterName?: string, projectName?: string) => {
    return API.get<any, IProject>(`/tenants/${env.MSA_DEFAULT_TENANT}/platforms/${platformName}/clusters/${clusterName}/projects/${projectName}`);
};


export const postClusterProject = (platformName?: string, clusterName?: string, projectName?: string, params?: Partial<ICluster>) => {
    return API.post<IProject, IProject>(`/tenants/${env.MSA_DEFAULT_TENANT}/platforms/${platformName}/clusters/${clusterName}/projects/${projectName}`, params);
};


export const putClusterProject = (platformName?: string, clusterName?: string, projectName?: string, params?: Partial<ICluster>) => {
    return API.put<IProject, IProject>(`/tenants/${env.MSA_DEFAULT_TENANT}/platforms/${platformName}/clusters/${clusterName}/projects/${projectName}`, params);
};


export const deleteClusterProject = (platformName?: string, clusterName?: string, projectName?: string) => {
    return API.delete(`/tenants/${env.MSA_DEFAULT_TENANT}/platforms/${platformName}/clusters/${clusterName}/projects/${projectName}`);
};


export const getClusterNamespaces = (platformName?: string, clusterName?: string) => {
    return API.get<any, INamespace[]>(`/tenants/${env.MSA_DEFAULT_TENANT}/platforms/${platformName}/clusters/${clusterName}/namespaces/`);
};


export const deleteClusterNamespace = (platformName?: string, clusterName?: string, namespaceName?: string) => {
    return API.delete(`/tenants/${env.MSA_DEFAULT_TENANT}/platforms/${platformName}/clusters/${clusterName}/namespaces/${namespaceName}`);
};


export const postClusterNamespace = (platformName?: string, clusterName?: string, params?: Partial<INamespace>) => {
    return API.post<INamespace, INamespace>(`/tenants/${env.MSA_DEFAULT_TENANT}/platforms/${platformName}/clusters/${clusterName}/namespaces/`, params);
};


export const putClusterNamespace = (platformName?: string, clusterName?: string, namespaceName?: string, params?: Partial<INamespace>) => {
    return API.put<INamespace, INamespace>(`/tenants/${env.MSA_DEFAULT_TENANT}/platforms/${platformName}/clusters/${clusterName}/namespaces/${namespaceName}`, params);
};


