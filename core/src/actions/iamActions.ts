import { IPagesResponse, IRbacObjects, IRbacResource } from '@msa/kit/types';

import { env } from 'env';
import type { IRealmRole, ISessionInfo, IUserInfo, IRbacGroup  } from 'types';
import { API } from 'helpers/apiInstance';


export const getUsers = (offset?: number, limit?: number, searchUser?: string) => {
    return API.get<IUserInfo[], IPagesResponse<IUserInfo[]>>(
        `/tenants/${env.MSA_DEFAULT_TENANT}/iam/users/`,
        {
            params: {
                offset,
                limit,
                search_value: searchUser?.length ? searchUser : undefined
            }
        }
    );
};


export const postUser = (params: Partial<IUserInfo>) => {
    const group = params.group;
    params.enabled = true;
    params.credentials = [
        {
            value: params.password as string,
            type: 'password'
        }
    ];
    delete params.password;

    // temporary before multiselect component is done
    params.group = group;

    return API.post<Partial<IUserInfo>, IUserInfo>(`/tenants/${env.MSA_DEFAULT_TENANT}/iam/users/`, params);
};


export const putUser = (params: Partial<IUserInfo>, rowEditor?: boolean) => {
    if (!rowEditor) {
        if (params.password)
            params.credentials = [
                {
                    value: params.password as string,
                    type: 'password'
                }
            ];
        delete params.password;

        if (!params.role) delete params.role;
    }

    return API.put<Partial<IUserInfo>, IUserInfo>(
        `/tenants/${env.MSA_DEFAULT_TENANT}/iam/users/${params.username}`,
        params
    );
};


export const enableUser2fa = (username: string, action: 'enable' | 'disable') => {
    return API.put<Partial<IUserInfo>, IUserInfo>(`/tenants/${env.MSA_DEFAULT_TENANT}/iam/users/${username}/2fa/${action}`);
};


export const getStatus2fa = () => {
    return API.get<any, { status: boolean }>(`/tenants/${env.MSA_DEFAULT_TENANT}/iam/users/2fa/status`);
};


export const enableAll2fa = (action: 'enable' | 'disable') => {
    return API.put(`/tenants/${env.MSA_DEFAULT_TENANT}/iam/users/2fa/${action}`);
};


export const deleteUser = (username: string) => {
    return API.delete(`/tenants/${env.MSA_DEFAULT_TENANT}/iam/users/${username}`);
};


interface IParams {
    offset?: number,
    limit?: number,
    search_value?: string,
    local?: boolean,
}

export const getGroups = (params?: IParams) => {
    return API.get<any, IPagesResponse<IRbacGroup[]>>(`/tenants/${env.MSA_DEFAULT_TENANT}/iam/groups/`,
        {
            params: {
                ...params,
                search_value: params?.search_value?.length ? params.search_value : undefined,
            }
        }
    );
};

export const getGroupsList = () => {
    return API.get<string, string[]>(`/tenants/${env.MSA_DEFAULT_TENANT}/iam/users/groups`);
};

export const postGroup = (data: any) => {
    return API.post<any, any>(`/tenants/${env.MSA_DEFAULT_TENANT}/iam/groups/`, data);
};


export const putGroup = (groupName: string, data: any) => {
    return API.put<any, any>(`/tenants/${env.MSA_DEFAULT_TENANT}/iam/groups/${groupName}`, data);
};


export const loadGroup = (groupName: string) => {
    return API.get<any, IRbacGroup>(`/tenants/${env.MSA_DEFAULT_TENANT}/iam/groups/${groupName}`);
};


export const deleteGroup = (groupName: string) => {
    return API.delete(`/tenants/${env.MSA_DEFAULT_TENANT}/iam/groups/${groupName}`);
};


export const getResources = () => {
    return API.get<any, IRbacResource>(`/tenants/${env.MSA_DEFAULT_TENANT}/globals`);
};


export const getObjects = () => {
    return API.get<any, IRbacObjects>(`/tenants/${env.MSA_DEFAULT_TENANT}/objects`);
};


export const denyGroupAccess = (groupId: string, role: IRealmRole[]) => {
    return API.put(`/tenants/${env.MSA_DEFAULT_TENANT}/iam/rbac/deny_access_to_group?group_id=${groupId}`, role);
};


export const getSessions = () => {
    return API.get<any, ISessionInfo[]>(`/tenants/${env.MSA_DEFAULT_TENANT}/iam/users/sessions`);
};


export const killSession = (sessionId: string) => {
    return API.delete<string, any>(`/tenants/${env.MSA_DEFAULT_TENANT}/iam/users/sessions/${sessionId}`);
};


export const getGroupUsers = (groupName?: string, offset?: number, limit?: number) => {
    return API.get<IUserInfo[], IPagesResponse<IUserInfo[]>>(`/tenants/${env.MSA_DEFAULT_TENANT}/iam/groups/${groupName}/members`,
        {
            params: {
                offset,
                limit
            }
        }
    );
};

export const deleteGroupUser = (groupName?: string, users?: IUserInfo[]) => {
    return API.delete(`/tenants/${env.MSA_DEFAULT_TENANT}/iam/groups/${groupName}/members`, {
        data: users
    } );
};

export const addGroupUsers = (groupName: string, users: IUserInfo[]) => {
    return API.put(`/tenants/${env.MSA_DEFAULT_TENANT}/iam/groups/${groupName}/members`, users);
}
