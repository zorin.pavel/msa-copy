import { env } from 'env';

import type { IClusterTool, IDevTool } from 'types';
import { API } from 'helpers/apiInstance';


export const getClusterTools = (platformName?: string, clusterName?: string) => {
    return API.get<any, IClusterTool[]>(`/tenants/${env.MSA_DEFAULT_TENANT}/platforms/${platformName}/clusters/${clusterName}/vendor_helm_charts/`);
};


export const installClusterTool = (platformName?: string, clusterName?: string, params?: any) => {
    return API.post<any, IClusterTool>(`/tenants/${env.MSA_DEFAULT_TENANT}/platforms/${platformName}/clusters/${clusterName}/vendor_helm_charts/?repo_name=${params.repo_name}`, params);
};


export const upgradeClusterTool = (platformName?: string, clusterName?: string, params?: IClusterTool) => {
    return API.put<any, IClusterTool>(`/tenants/${env.MSA_DEFAULT_TENANT}/platforms/${platformName}/clusters/${clusterName}/vendor_helm_charts/${params?.release_name}?repo_name=${params?.vendor_helm_repo.name}`, params);
};


export const deleteClusterTool = (platformName?: string, clusterName?: string, releaseName?: string, repoName?: string) => {
    return API.delete(`/tenants/${env.MSA_DEFAULT_TENANT}/platforms/${platformName}/clusters/${clusterName}/vendor_helm_charts/${releaseName}?repo_name=${repoName}`);
};


export const getToolVersions = (platformName?: string, clusterName?: string, repoName?: string, chartName?: string) => {
    return API.get<any, string[]>(`/tenants/${env.MSA_DEFAULT_TENANT}/platforms/${platformName}/clusters/${clusterName}/vendor_helm_charts/${repoName}/${chartName}/versions`);
};


export const getToolValues = (platformName?: string, clusterName?: string, repoName?: string, chartName?: string, version?: string) => {
    return API.get<any, string[]>(`/tenants/${env.MSA_DEFAULT_TENANT}/platforms/${platformName}/clusters/${clusterName}/vendor_helm_charts/${repoName}/${chartName}/${version}/values`);
};


export const getClusterToolValues = (platformName: string, clusterName: string, chartName: string) => {
    return API.get<any, string[]>(`/tenants/${env.MSA_DEFAULT_TENANT}/platforms/${platformName}/clusters/${clusterName}/vendor_helm_charts/${chartName}`);
};


export const getDevToolsChartsOptions = (platformName: string, clusterName: string, repoName: string) => {
    return API.get<string[], string[]>(`/tenants/${env.MSA_DEFAULT_TENANT}/platforms/${platformName}/clusters/${clusterName}/user_helm_charts/${repoName}/charts`);
};


export const getDevToolsNamespaceOptions = (platformName?: string, clusterName?: string) => {
    return API.get<string[], string[]>(`/tenants/${env.MSA_DEFAULT_TENANT}/platforms/${platformName}/clusters/${clusterName}/namespaces/`);
};


export const getDevToolsVersionOptions = (platformName?: string, clusterName?: string, repoName?: string, chartName?: string) => {
    return API.get<string[], string[]>(`/tenants/${env.MSA_DEFAULT_TENANT}/platforms/${platformName}/clusters/${clusterName}/user_helm_charts/${repoName}/${chartName}/versions`);
};


export const getDevToolsValues = (platformName?: string, clusterName?: string, repoName?: string, chartName?: string, version?: string) => {
    return API.get<string[], string[]>(`/tenants/${env.MSA_DEFAULT_TENANT}/platforms/${platformName}/clusters/${clusterName}/user_helm_charts/${repoName}/${chartName}/${version}/values`);
};


export const installDevToolsChart = (platformName?: string, clusterName?: string, repoName?: string, formData?: any) => {
    return API.post<string[], IDevTool>(`/tenants/${env.MSA_DEFAULT_TENANT}/platforms/${platformName}/clusters/${clusterName}/user_helm_charts/?repo_name=${repoName}`, formData);
};


export const updateDevToolsChart = (platformName?: string, clusterName?: string, repoName?: string, releaseName?: string, formData?: any) => {
    return API.put<string[], any>(`/tenants/${env.MSA_DEFAULT_TENANT}/platforms/${platformName}/clusters/${clusterName}/user_helm_charts/${releaseName}?repo_name=${repoName}`, formData);
};


export const deleteDevToolsChart = (platformName: string, clusterName: string, repoName: string, releaseName: string) => {
    return API.delete<string[], string[]>(`/tenants/${env.MSA_DEFAULT_TENANT}/platforms/${platformName}/clusters/${clusterName}/user_helm_charts/${releaseName}?repo_name=${repoName}`);
};

