import { env } from 'env';
import type { IInfraService, IInfrastructure } from 'types';
import { API } from 'helpers/apiInstance';


export const getInfrastructures = (platformName?: string) => {
    return API.get<any, IInfrastructure[]>(`/tenants/${env.MSA_DEFAULT_TENANT}/platforms/${platformName}/infrastructures/`);
};


export const loadInfrastructure = (platformName?: string, infraName?: string) => {
    return API.get<any, IInfrastructure>(`/tenants/${env.MSA_DEFAULT_TENANT}/platforms/${platformName}/infrastructures/${infraName}`);
};


export const postInfrastructure = (platformName?: string, params?: Partial<IInfrastructure>) => {
    return API.post<IInfrastructure, IInfrastructure, Partial<IInfrastructure>>(`/tenants/${env.MSA_DEFAULT_TENANT}/platforms/${platformName}/infrastructures/`, params);
};


export const putInfrastructure = (platformName?: string, infraName?: string, params?: Partial<IInfrastructure>) => {
    return API.put<IInfrastructure, IInfrastructure, Partial<IInfrastructure>>(`/tenants/${env.MSA_DEFAULT_TENANT}/platforms/${platformName}/infrastructures/${infraName}`, params);
};


export const deleteInfrastructure = (platformName?: string, infraName?: string) => {
    return API.delete<any, IInfrastructure>(`/tenants/${env.MSA_DEFAULT_TENANT}/platforms/${platformName}/infrastructures/${infraName}`);
};


export const deleteInfrastructureForce = (platformName?: string, infraName?: string) => {
    return API.delete<any, IInfrastructure>(`/tenants/${env.MSA_DEFAULT_TENANT}/platforms/${platformName}/infrastructures/${infraName}/force`);
};


export const getInfrastructureServices = (platformName?: string) => {
    return API.get<any, IInfraService[]>(`/tenants/${env.MSA_DEFAULT_TENANT}/platforms/${platformName}/infrastructures/marketplace`);
};
