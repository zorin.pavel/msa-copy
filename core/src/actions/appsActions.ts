import { env } from 'env';

import type { IDevTool } from 'types';
import { API } from 'helpers/apiInstance';


export const getClusterApps = (platformName: string, clusterName: string) => {
    return API.get<any, IDevTool[]>(`/tenants/${env.MSA_DEFAULT_TENANT}/platforms/${platformName}/clusters/${clusterName}/user_helm_charts/`);
};


export const forceSyncTools = (repo_name:string) => { 
    return API.get<any, any[]>(`/tenants/${env.MSA_DEFAULT_TENANT}/vendor_repos/helm/${repo_name}/sync_force`);
};


export const getDevToolByRepo = (platformName?: string, clusterName?: string, repo_name?:string) => {
    return API.get<any, IDevTool[]>(`/tenants/${env.MSA_DEFAULT_TENANT}/platforms/${platformName}/clusters/${clusterName}/user_helm_charts/${repo_name}/repo`);
};


export const forceSyncDevTools = (platformName?: string, repo_name?:string) => {
    return API.get<any, any[]>(`/tenants/${env.MSA_DEFAULT_TENANT}/platforms/${platformName}/user_repos/helm/${repo_name}/sync_force`);
};


export const installClusterApp = (platformName: string, clusterName: string, params: any) => {
    return API.post<any, IDevTool>(`/tenants/${env.MSA_DEFAULT_TENANT}/platforms/${platformName}/clusters/${clusterName}/user_helm_charts/?repo_name=${params.repo_name}`, params);
};


export const upgradeClusterApp = (platformName: string, clusterName: string, params: any) => {
    return API.put<any, IDevTool>(`/tenants/${env.MSA_DEFAULT_TENANT}/platforms/${platformName}/clusters/${clusterName}/user_helm_charts/${params.release_name}?repo_name=${params.repo_name}`, params);
};


export const deleteClusterApp = (platformName: string, clusterName: string, releaseName: string, repoName: string) => {
    return API.delete(`/tenants/${env.MSA_DEFAULT_TENANT}/platforms/${platformName}/clusters/${clusterName}/user_helm_charts/${releaseName}?repo_name=${repoName}`);
};


export const getAppVersions = (platformName: string, clusterName: string, repoName: string, chartName: string) => {
    return API.get<any, string[]>(`/tenants/${env.MSA_DEFAULT_TENANT}/platforms/${platformName}/clusters/${clusterName}/user_helm_charts/${repoName}/${chartName}/versions`);
};


export const getAppValues = (platformName: string, clusterName: string, repoName: string, chartName: string, version: string) => {
    return API.get<any, string[]>(`/tenants/${env.MSA_DEFAULT_TENANT}/platforms/${platformName}/clusters/${clusterName}/user_helm_charts/${repoName}/${chartName}/${version}/values`);
};
