import { useQuery } from '@tanstack/react-query';
import { createBrowserRouter, Navigate, Outlet, RouterProvider, useParams } from 'react-router-dom';
import { useTranslation } from 'react-i18next';
import i18next from 'i18next';

import { IError } from '@msa/kit/types';
import {
    Error,
    IconCluster,
    IconDev,
    IconInfrastructure, IconNamespaces,
    IconNode, IconNotification,
    IconOverview,
    IconPlatform,
    IconRepos,
    IconShield,
    IconSummary,
    IconTools,
    IconUsers,
    isAuthorized,
    Loader,
    preparePath,
    useQuerySelector
} from '@msa/kit';

import { IRoute, ITenant } from '@msa/host/types';
import { ErrorLayout, Layout, loadTenant, NavigationLayout, WideLayout } from '@msa/host';

import { env } from 'env';
import { ICluster, IInfrastructure, IPlatform, IRbacGroup } from 'types';
import { loadCluster, loadGroup, loadInfrastructure, loadPlatform } from 'actions';

import { Logs } from 'components/Logs';
import { Iam } from 'components/Iam';
import { Users } from 'components/Iam/Users';
import { Group } from 'components/Iam/Groups/Group';
import { Groups } from 'components/Iam/Groups';
import { Ldap } from 'components/Iam/Ldap';
import { Sessions } from 'components/Iam/Sessions';

import { Repositories } from 'components/Repositories';
import { Helm } from 'components/Repositories/Helm';
import { Docker } from 'components/Repositories/Docker';
import { Raw } from 'components/Repositories/Raw';

import { Platforms } from 'components/Platforms';
import { Platform } from 'components/Platforms/Platform';
import { Credentials } from 'components/Platforms/Platform/Credentials';
import { PlatformNodes } from 'components/Platforms/Platform/PlatformNodes';
// import { NodeTemplates } from 'components/Platforms/NodeTemplates';
import { PlatformPermissions } from 'components/Platforms/Platform/PlatformPermissions';

import { Clusters } from 'components/Clusters';
import { Cluster } from 'components/Clusters/Cluster';
import { ClusterOverview } from 'components/Clusters/Cluster/Overview';
// import { Components } from 'components/Clusters/Cluster/Components';
import { ClusterNodes } from 'components/Clusters/Cluster/Nodes';
import { ClusterNode } from 'components/Clusters/Cluster/Nodes/Node';
// import { Security } from 'components/Clusters/Cluster/Security';
import { Tools } from 'components/Clusters/Cluster/Tools';
import { DevTools } from 'components/Clusters/Cluster/DevTools';
import { Apps } from 'components/Clusters/Cluster/DevTools/Apps';
// import { Projects } from 'components/Clusters/Cluster/Projects';
// import { Project } from 'components/Clusters/Cluster/Projects/Project';
import { Namespaces } from 'components/Clusters/Cluster/Namespaces';
import { ClusterPermissions } from 'components/Clusters/Cluster/ClusterPermissions';

import { Infrastructures } from 'components/Infrastructures';
import { Infrastructure } from 'components/Infrastructures/Infrastructure';
import { InfrastructureOverview } from 'components/Infrastructures/Infrastructure/Overview';
import { InfrastructureNodes } from 'components/Infrastructures/Infrastructure/Nodes';
import { History } from 'components/History';
import { Providers } from 'components/Notification';
import { Alerts } from 'components/Notification/Alerts';


export const useGetRoutes = () => {
    const params = useParams();
    const { t } = useTranslation()

    const routes = [
        {
            path: env.MSA_BASE_PATH,
            Component: Layout,
            ErrorBoundary: ErrorLayout,
            handle: {
                name: t('common.home'),
            },
            children: [
                {
                    index: true,
                    element: <Navigate to="platforms" replace />,
                    handle: {
                        visible: false
                    }
                },
                {
                    path: 'platforms',
                    Component: Platforms,
                    handle: {
                        name: 'Platforms',
                        icon: <IconPlatform />,
                        layout: <WideLayout />,
                        crumb: false
                    },
                    children: [
                        {
                            path: ':platformSlug',
                            Component: Platform,
                            handle: {
                                selector: useQuerySelector<IPlatform>(
                                    () => loadPlatform(params.platformSlug as string),
                                    ['platform'],
                                    params.platformSlug
                                ),
                                layout: <NavigationLayout />
                            },
                            children: [
                                {
                                    index: true,
                                    element: <Navigate to="clusters" replace />,
                                    handle: {
                                        visible: false
                                    }
                                },
                                {
                                    path: 'clusters',
                                    Component: Clusters,
                                    handle: {
                                        name: i18next.format(t('clusters.clusters'), 'capitalize'),
                                        icon: <IconCluster />,
                                        crumb: false,
                                    },
                                    children: [
                                        {
                                            path: ':clusterSlug',
                                            Component: Cluster,
                                            handle: {
                                                selector: useQuerySelector<ICluster>(
                                                    () => loadCluster(params.platformSlug as string, params.clusterSlug as string),
                                                    ['platform', params.platformSlug as string, 'cluster'],
                                                    params.clusterSlug
                                                ),
                                                visible: false,
                                            },
                                            children: [
                                                {
                                                    index: true,
                                                    Component: ClusterOverview,
                                                    handle: {
                                                        name: t('navigation.summary'),
                                                        icon: <IconSummary />,
                                                        crumb: false
                                                    }
                                                },
                                                // {
                                                //     path: 'components',
                                                //     Component: Components,
                                                //     handle: {
                                                //         name: 'Components',
                                                //         icon: <IconComponent />,
                                                //     }
                                                // },
                                                {
                                                    path: 'nodes',
                                                    Component: ClusterNodes,
                                                    handle: {
                                                        name: i18next.format(t('nodes.nodes'), 'capitalize'),
                                                        icon: <IconNode />,
                                                    },
                                                    children: [
                                                        {
                                                            path: ':nodeSlug',
                                                            Component: ClusterNode,
                                                            handle: {
                                                                visible: false,
                                                                crumb: false,
                                                            }
                                                        }
                                                    ]
                                                },
                                                // {
                                                //     path: 'security',
                                                //     Component: Security,
                                                //     handle: {
                                                //         name: 'Security',
                                                //         icon: <IconSecurity />,
                                                //     },
                                                // },
                                                {
                                                    path: 'namespaces',
                                                    Component: Namespaces,
                                                    handle: {
                                                        name: t('navigation.namespaces'),
                                                        icon: <IconNamespaces />,
                                                    },
                                                },
                                                // {
                                                //     path: 'projects',
                                                //     Component: Projects,
                                                //     handle: {
                                                //         name: 'Projects',
                                                //         icon: <IconProjects />,
                                                //     },
                                                //     children: [
                                                //         {
                                                //             path: ':projectSlug',
                                                //             Component: Project,
                                                //             handle: {
                                                //                 selector: useQuerySelector<IProject>(
                                                //                     () => loadClusterProject(params.platformSlug as string, params.clusterSlug as string, params.projectSlug as string),
                                                //                     ['platform', params.platformSlug as string, 'cluster', params.clusterSlug!, 'project'],
                                                //                     params.projectSlug
                                                //                 ),
                                                //                 visible: false,
                                                //             },
                                                //             children: [
                                                //                 {
                                                //                     index: true,
                                                //                     element: <>Summary</>,
                                                //                     handle: {
                                                //                         visible: false,
                                                //                         crumb: false,
                                                //                     }
                                                //                 },
                                                //                 {
                                                //                     path: 'cicd',
                                                //                     element: <>CI/CD</>,
                                                //                     handle: {
                                                //                         visible: false,
                                                //                     }
                                                //                 }
                                                //             ]
                                                //         },
                                                //     ]
                                                // },
                                                {
                                                    path: 'permissions',
                                                    Component: ClusterPermissions,
                                                    handle: {
                                                        name: t('navigation.permissions'),
                                                        icon: <IconUsers />,
                                                    }
                                                },
                                                {
                                                    path: 'tools',
                                                    Component: Tools,
                                                    handle: {
                                                        name: t('common.clusterTools'),
                                                        icon: <IconTools />,
                                                    },
                                                },
                                                {
                                                    path: 'devtools',
                                                    Component: DevTools,
                                                    handle: {
                                                        name: t('common.devTools'),
                                                        icon: <IconDev />
                                                    },
                                                    children: [
                                                        {
                                                            path: 'apps',
                                                            Component: Apps,
                                                            handle: {
                                                                name: t('common.helmApp'),
                                                                visible: false
                                                            }
                                                        }
                                                    ]
                                                }
                                            ]
                                        }
                                    ]
                                },
                                {
                                    path: 'infrastructures',
                                    Component: Infrastructures,
                                    handle: {
                                        name: i18next.format(t('infrastructures.infrastructures'), 'capitalize'),
                                        icon: <IconInfrastructure />,
                                    },
                                    children: [
                                        {
                                            path: ':infrastructureSlug',
                                            Component: Infrastructure,
                                            handle: {
                                                selector: useQuerySelector<IInfrastructure>(
                                                    () => loadInfrastructure(params.platformSlug as string, params.infrastructureSlug as string),
                                                    ['infrastructure'],
                                                    params.infrastructureSlug
                                                ),
                                                visible: false
                                            },
                                            children: [
                                                {
                                                    index: true,
                                                    Component: InfrastructureOverview,
                                                    handle: {
                                                        name: t('navigation.summary'),
                                                        icon: <IconOverview />,
                                                        crumb: false
                                                    }
                                                },
                                                {
                                                    path: 'nodes',
                                                    Component: InfrastructureNodes,
                                                    handle: {
                                                        name: i18next.format(t('nodes.nodes'), 'capitalize'),
                                                        icon: <IconNode />,
                                                    },
                                                },
                                            ]
                                        }
                                    ]
                                },
                                {
                                    path: 'permissions',
                                    Component: PlatformPermissions,
                                    handle: {
                                        name: i18next.format(t('permissions.permissions'), 'capitalize'),
                                        icon: <IconUsers />,
                                    }
                                },
                                {
                                    path: 'credentials',
                                    Component: Credentials,
                                    handle: {
                                        name: i18next.format(t('credentials.credentials'), 'capitalize'),
                                        icon: <IconShield />,
                                    }
                                },
                                {
                                    path: 'nodes',
                                    Component: PlatformNodes,
                                    handle: {
                                        name: i18next.format(t('nodes.nodes'), 'capitalize'),
                                        icon: <IconNode />,
                                    }
                                },
                                // {
                                //     path: 'templates',
                                //     Component: NodeTemplates,
                                //     handle: {
                                //         name: 'Node templates',
                                //         icon: <IconPlatform />,
                                //     }
                                // },
                                {
                                    path: 'repositories',
                                    Component: Helm,
                                    handle: {
                                        name: i18next.format(t('helmRepos.helmRepos'), 'capitalize'),
                                        icon: <IconRepos />,
                                    }
                                }
                            ]
                        }
                    ]
                },
                {
                    path: 'logs',
                    Component: Logs,
                    handle: {
                        name: 'User Logs',
                        layout: <WideLayout />,
                        permission: isAuthorized({ user_logs: ['list'] })
                    }
                },
                {
                    path: 'history',
                    Component: History,
                    handle: {
                        name: 'History Logs',
                        layout: <WideLayout />
                    }
                },
                {
                    path: 'settings',
                    handle: {
                        name: 'Platform settings',
                        layout: <NavigationLayout />,
                        crumb: false,
                    },
                    children: [
                        {
                            index: true,
                            element: <Navigate to="management" replace />,
                            handle: {
                                visible: false
                            }
                        },
                        {
                            path: 'management',
                            Component: Iam,
                            handle: {
                                name:  t('navigation.usermanagement'),
                                icon: <IconUsers />,
                                permission: isAuthorized({ settings: ['list'] })
                            },
                            children: [
                                {
                                    index: true,
                                    Component: Users,
                                    handle: {
                                        name: t('navigation.users'),
                                    }
                                },
                                {
                                    path: 'groups',
                                    Component: Groups,
                                    handle: {
                                        name: t('navigation.groups'),
                                    },
                                    children: [
                                        {
                                            path: ':groupSlug',
                                            Component: Group,
                                            handle: {
                                                selector: useQuerySelector<IRbacGroup>(
                                                    () => loadGroup(params.groupSlug as string),
                                                    ['group'],
                                                    params.groupSlug
                                                ),
                                                visible: false
                                            }
                                        },
                                    ]
                                },
                                {
                                    path: 'providers',
                                    Component: Ldap,
                                    handle: {
                                        name: t('common.provider', {context: "few"}) ,
                                    },
                                },
                                {
                                    path: 'sessions',
                                    Component: Sessions,
                                    handle: {
                                        name: t('navigation.sessions'),
                                    },
                                },
                            ]
                        },
                        {
                            path: 'notification',
                            Component: Outlet,
                            handle: {
                                name: i18next.format(t('nav.notifications'), 'capitalize'),
                                icon: <IconNotification />,
                                permission: isAuthorized({ settings: ['list'] })
                            },
                            children: [
                                {
                                    index: true,
                                    Component: Providers,
                                    handle: {
                                        name: i18next.format(t('nav.providers'), 'capitalize'),
                                    },
                                },
                                {
                                    path: 'alerts',
                                    Component: Alerts,
                                    handle: {
                                        name: i18next.format(t('nav.alerts'), 'capitalize'),
                                    },
                                },
                            ]
                        },
                        {
                            path: 'repositories',
                            Component: Repositories,
                            handle: {
                                name: t('navigation.repositories'),
                                icon: <IconRepos />,
                            },
                            children: [
                                {
                                    index: true,
                                    Component: Helm,
                                    handle: {
                                        name: t('navigation.helm'),
                                        icon: <IconRepos />,
                                    }
                                },
                                {
                                    path: 'docker',
                                    Component: Docker,
                                    handle: {
                                        name: t('navigation.docker'),
                                        icon: <IconRepos />,
                                    }
                                },
                                {
                                    path: 'raw',
                                    Component: Raw,
                                    handle: {
                                        name: t('navigation.raw'),
                                        icon: <IconRepos />,
                                    }
                                }
                            ]
                        }
                    ]
                }
            ]
        }
    ];

    return preparePath(routes, params);
};


export const Routes = () => {
    const routes: IRoute[] = useGetRoutes();

    const { isError, isLoading, error } = useQuery<ITenant, IError>(
        ['tenant', env.MSA_DEFAULT_TENANT],
        () => loadTenant(env.MSA_DEFAULT_TENANT),
        {
            // retry: 1
            // refetchOnWindowFocus: true
        }
    );

    if (isError)
        return <Error layout {...error} message={`Can'\t access tenant "${env.MSA_DEFAULT_TENANT}"`} />;

    if(isLoading)
        return <Loader layout caption={`Fetching '${env.MSA_DEFAULT_TENANT}' environment...`} />;

    return <RouterProvider router={createBrowserRouter(routes)} />;
};
