import { IHeaderNavigation } from '@msa/host/types';
import { headerNavigation as hostNavigation } from '@msa/host';

import { env } from 'env';


export const headerNavigation: IHeaderNavigation[] = [
    ...hostNavigation,
    {
        name: 'Platforms',
        path: '/platforms',
        permission: { platforms: ['list'] }
    },
    {
        name: 'Monitoring',
        path: env.MSA_MONITOR_URL,
        target: '_blank',
        permission: { monitoring: ['list'] }
    },
    {
        name: 'Logging',
        path: env.MSA_LOG_URL,
        target: '_blank',
        permission: { logging: ['list'] }
    },
    {
        name: 'Settings',
        path: '/settings',
        permission: { settings: ['list'] }
    },
    {
        name: 'User log',
        path: '/logs',
        permission: { user_log: ['list'] }
    },
];


