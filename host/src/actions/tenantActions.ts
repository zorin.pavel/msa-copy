import { ITenant } from '@msa/host/types';
import { API } from '@msa/host';


export const loadTenant = (tenantSlag?: string) => {
    return API.get<any, ITenant>(`/tenants/${tenantSlag}`)
        .then((response: ITenant) => response);
};
