import { env } from 'env';
import { API } from '@msa/host';

export * from './tenantActions';
export * from './loggerActions';


export const getPermissions = () => {
    return API.get<any, any>(`/tenants/${env.MSA_DEFAULT_TENANT}/permissions`);
};
