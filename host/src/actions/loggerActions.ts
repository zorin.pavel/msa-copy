import { env } from 'env';
import { API } from 'helpers/apiInstance';

import { IMessageData } from '@msa/host/types';


export const getLoggerMessages = (params: Record<string, string | number | undefined>) => {
    const currentDate = new Date();
    const stop: number = Math.floor(currentDate.getTime() / 1000);
    const period = Number(params.selectedPeriod);
    const start: number = stop - period;
    delete params.selectedPeriod;

    Object.keys(params).map((key) => {
        if(!params[key]) {
            delete params[key];
        }
    });

    return API.get<any, IMessageData[]>(`/tenants/${env.MSA_DEFAULT_TENANT}/messages/`, {
        params: {
            ...params,
            start,
            stop
        }
    });
};
