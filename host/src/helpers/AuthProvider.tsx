import { ReactNode, useEffect } from 'react';
import { useKeycloak } from '@react-keycloak/web';

import { useGlobalState, Loader, permissionStorage } from '@msa/kit';
import type { IKeycloakUser } from '@msa/host/types';
import { getPermissions } from '@msa/host';


export const AuthProvider = ({ children }: { children: ReactNode }) => {
    const { keycloak, initialized } = useKeycloak();

    const [user, setUser] = useGlobalState<IKeycloakUser>('user');


    useEffect(() => {
        if(user)
            return;

        if(!initialized)
            return;

        if(!keycloak.authenticated) {
            keycloak.login();

            return;
        }

        keycloak.onTokenExpired = () => {
            keycloak.updateToken(50);
        };

        Promise.all([keycloak.loadUserInfo(), getPermissions()])
            .then(([userInfo, resources]) => {
                setUser(userInfo as IKeycloakUser);

                permissionStorage.setItem(JSON.stringify(resources));
            })
            .catch((error) => {
                console.error('Permissions error:', error);
            });
    }, [keycloak, initialized, user, setUser]);


    return (
        <>
            {
                user ?
                    children :
                    !initialized ?
                        <Loader layout caption="Keycloak initialization..." /> :
                        // <Error status={401} message="Keycloak connection error" layout /> :
                        null
            }
        </>
    );
};

