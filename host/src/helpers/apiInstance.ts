import axios, { AxiosError, AxiosInstance } from 'axios';
import { v4 as uuidV4 } from 'uuid';

import { env } from 'env';
import type { IError } from '@msa/kit/types';
import { authStorage } from '@msa/kit';
import i18n from '@msa/host/src/i18n';


const ApiInstance = (baseURL?: string): AxiosInstance => {
    const _API = axios.create({
        baseURL: baseURL ?? env.MSA_API_URL,
        responseType: 'json',
        withCredentials: false,
        timeout: 1000 * 15,
        headers: {
            'Content-Type': 'application/json',
            'X-Request-ID': uuidV4(),
            'Cache-Control': 'no-cache, no-store, must-revalidate, max-age=40000',
        },

    });

    _API.interceptors.request.use(request => {
        request.headers.Authorization = `Bearer ${authStorage.getItem()}`;

        if(env.MSA_SUPPORTED_LANGUAGES) {
            let q = 1;
            const [current, ...languages] = i18n.languages;

            request.headers['Accept-Language'] = `${current},${languages.map(lng => {
                q = q - 0.1;
                return `${lng};q=${q}`;
            }).join(',')}`;
        }

        return request;
    });

    _API.interceptors.response.use(
        async (response) => {
            // if([201, 202, 204].includes(response.status))
            //     await keycloakInstance.updateToken(300);

            if(response.headers['x-count'])
                return { items: response.data, count: response.headers['x-count']};
            if(response.headers['content-disposition']) {
                return { items: response.data, filename: response.headers['content-disposition'].match(/filename="([^"]+)"/)[1]};
            }

            return response.data;
        },
        (error: AxiosError<IError>) => {
            let errorData: IError = {
                code: error.code,
                message: error.message,
            };

            if(error.response) {
                if(error.response.status === 401)
                    window.location.reload();

                errorData.status = error.response.status;
                errorData.statusText = error.message;

                if(error.response.data) {
                    if(Array.isArray(error.response.data.detail)) {
                        if(error.response?.data.detail[0].loc)
                            errorData.message = '<b>' + (error.response.data.detail[0].loc[2] ?? error.response.data.detail[0].loc[1]) + '</b> ';

                        errorData.message += error.response.data.detail[0].msg;
                    } else if(typeof error.response.data.detail === 'string')
                        errorData.message = error.response.data.detail;
                    else
                        errorData = { ...errorData, ...error.response.data };
                }
            }

            // console.error('errorData', errorData);

            return Promise.reject<IError>(errorData);
        }
    );

    return _API;
};

const API = ApiInstance();

export { ApiInstance, API };
