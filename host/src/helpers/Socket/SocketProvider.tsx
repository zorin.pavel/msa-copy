import React, { createContext, ReactNode, useEffect, useState } from 'react';

import { IMessageData } from '@msa/host/types';

import { env } from 'env';


interface ContextProps {
    socket: WebSocket | null,
    socketMessage: IMessageData | null,
    setSocketMessage?: React.Dispatch<React.SetStateAction<IMessageData | null>>
}

const readyState = ['CONNECTING', 'OPEN', 'CLOSING', 'CLOSED', 'UNDEFINED']

const SocketConnect = () => new WebSocket(`${env.MSA_NOTIFIER_URL}tenants/${env.MSA_DEFAULT_TENANT}/ws/messages`);
let socket = env.MSA_LOGGER ? SocketConnect() : null;


export const SocketContext = createContext<ContextProps>({ socket, socketMessage: null });


export const SocketProvider = ({ children }: { children: ReactNode }) => {
    const [message, setMessage] = useState<ContextProps['socketMessage']>(null);

    useEffect(() => {
        if(socket) {
            socket.onmessage = (event) => {
                env.MSA_DEVELOP &&
                console.log('%cSocket onMessage', 'color: #3C6AA6', JSON.parse(event.data));

                setMessage(JSON.parse(event.data));
            };

            socket.onopen = () => {
                env.MSA_DEVELOP &&
                console.log('%cSocket connection open', 'color: #5BA01A', readyState[socket?.readyState ?? 4]);
            };

            socket.onclose = () => {
                env.MSA_DEVELOP &&
                console.log('%cSocket connection close', 'color: #C4A000', readyState[socket?.readyState ?? 4]);
            };

            socket.onerror = () => {
                env.MSA_DEVELOP &&
                console.log('%cSocket connection error', 'color: #CC0000', readyState[socket?.readyState ?? 4]);
            };
        }

        return () => {
            socket && socket.close();
        };
    }, []);


    const onWindowFocus = () => {
        env.MSA_DEVELOP &&
        console.log('%cSocket status', 'color: #127DA4', readyState[socket?.readyState ?? 4]);

        if(!socket || socket.readyState !== 1) {
            socket = SocketConnect();
            env.MSA_DEVELOP &&
            console.log('%cSocket new instance', 'color: #5BA01A', readyState[socket?.readyState ?? 4]);
        }
    };


    useEffect(() => {
        window.addEventListener('focus', onWindowFocus);

        onWindowFocus();

        return () => {
            window.removeEventListener('focus', onWindowFocus);
        };
    }, []);


    return (
        <SocketContext.Provider value={{
            socket,
            socketMessage: message,
            setSocketMessage: setMessage
        }}>
            {children}
        </SocketContext.Provider>
    );
};
