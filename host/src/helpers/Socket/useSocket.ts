import { useContext, useEffect } from 'react';

import { TFilters, TOptions } from '@msa/host/types';
import { Toast } from '@msa/kit';

import settings from 'settings';
import { SocketContext } from './SocketProvider';



export const useSocket = (filters: TFilters, options?: TOptions) => {
    const { callback, toast } = options ?? {};
    const { socket, socketMessage, setSocketMessage } = useContext(SocketContext);

    // const [message, setMessage] = useState<IMessageData | null>(null);


    useEffect(() => {
        if(!socketMessage)
            return;

        for(const key in filters) {
            if(!socketMessage[key] || socketMessage[key] !== filters[key])
                return;
        }

        // setMessage(socketMessage);

        if(callback)
            callback();

        if(toast) {
            if(socketMessage.detail) {
                if(settings.STATE.FAIL.includes(socketMessage.state))
                    Toast.error({ message: socketMessage.detail, toastId: socketMessage.detail });

                if(settings.STATE.SUCCESS.includes(socketMessage.state))
                    Toast.success({ message: socketMessage.detail, toastId: socketMessage.detail });
            }

            setSocketMessage && setSocketMessage(null);
            // setMessage(null);
        }

        // return () => {
        //     setMessage(null);
        // };
    }, [socketMessage, callback, filters, setSocketMessage]);


    return { socket, socketMessage };
};
