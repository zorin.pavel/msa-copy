import Keycloak from 'keycloak-js';

import { env } from 'env';


export const keycloakInstance = new Keycloak({
    url: env.MSA_KEYCLOAK_URL as string,
    realm: env.MSA_KEYCLOAK_REALM as string,
    clientId: env.MSA_KEYCLOAK_CLIENT_ID as string,
});
