import React, { useMemo } from 'react';
import { matchRoutes, NavLink, useLocation } from 'react-router-dom';
import classNames from 'classnames';

import { IconChevronRight } from '@msa/kit';
import type { IRoute } from '@msa/host/types';

import { useGetRoutes } from 'Routes';

import css from './breadcrumbs.module.scss';


export const Breadcrumbs = React.memo(() => {
    const routes: IRoute[] = useGetRoutes();
    const location = useLocation();
    const routesStack = matchRoutes(routes, location);


    const crumbs = useMemo(() => {
        if(routesStack) {
            return routesStack
                // .filter(({ route }) => !route.index)
                .filter(({ route }) => !(route.handle && route.handle.crumb === false))
                .filter(({ route }) => !route.handle || route.handle && (!!route.handle.permission || route.handle.permission === undefined));
        }

        return [];
    }, [routesStack]);


    return (
        <div className={css.container}>
            {/*<span className={css.element}><NavLink to={'/'}>Home</NavLink></span>*/}
            {
                crumbs.map(({ route }, i) => {
                    return (
                        <span key={i} className={classNames(css.element, (i === crumbs.length - 1) && css.current)}>
                            {
                                <>
                                    {
                                        i > 0 &&
                                        <IconChevronRight size="small" />
                                    }
                                    {
                                        route.handle?.selector ?
                                            <NavLink to={route.handle.path}>{route.handle.selector.data?.name}</NavLink> :
                                            route.handle?.name ?
                                                <NavLink to={route.handle.path}>{route.handle.name}</NavLink> :
                                                null
                                    }
                                </>
                            }
                        </span>
                    );
                })
            }
        </div>
    );
});
