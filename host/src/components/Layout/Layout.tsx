import React, { ReactNode } from 'react';
import { matchRoutes, Outlet, useLocation } from 'react-router-dom';

import type { IRoute } from '@msa/host/types';
import { Header } from '@msa/host';

import { useGetRoutes } from 'Routes';

import css from './layout.module.scss';


interface IProps {
    children?: ReactNode,
}

export const Layout = React.memo((props: IProps) => {
    const routes: IRoute[] = useGetRoutes();
    const location = useLocation();
    const routesStack = matchRoutes(routes, location)?.reverse();

    // let permission = true;
    let layout = undefined;

    if(routesStack) {
        // permission = !routesStack[0].route.handle ||
        //     routesStack[0].route.handle && (routesStack[0].route.handle.permission || routesStack[0].route.handle.permission === undefined);

        layout = routesStack[0].route.handle?.layout;
    }


    // show Navigation and then next Layout
    return (
        <div className={css.wrapper}>
            <Header />
            <div className={css.container}>
                {
                    // show Header and then next Layout
                    props.children ??
                    <>
                        {
                            layout ?? <Outlet />
                        }
                    </>
                }
            </div>
        </div>
    );
});
