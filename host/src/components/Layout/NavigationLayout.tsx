import React, { ReactNode, useCallback, useState } from 'react';
import { Outlet } from 'react-router-dom';
import classNames from 'classnames';

import { LocalStorage, IconDoubleLeft, IconDoubleRight } from '@msa/kit';
import { Navigation, Breadcrumbs, Footer, Logger } from '@msa/host';

import { env } from 'env';

import css from './navigation.module.scss';


export const navigationStorage = new LocalStorage({ storageKey: 'navigationState' });

interface IProps {
    children?: ReactNode,
}

export const NavigationLayout = React.memo((props: IProps) => {
    // inverse expanded
    const [expanded, setExpanded] = useState<boolean>(!navigationStorage.getItem());

    const handleExpand = useCallback(() => {
        setExpanded(prev => {
            if(!prev)
                navigationStorage.removeItem();
            else
                navigationStorage.setItem('collapsed');

            return !prev;
        });
    }, []);


    // show Navigation and then next Layout
    return (
        <>
            <div className={classNames(css.navigation, expanded && css.expanded)}>
                <Navigation expanded={expanded} setExpanded={setExpanded} />
                <div className={css.navigationState} onClick={handleExpand}>
                    {
                        expanded ?
                            <IconDoubleLeft size="large" /> :
                            <IconDoubleRight size="large" />
                    }
                </div>
            </div>
            <div className={classNames(css.layout, expanded && css.expanded)}>
                <Breadcrumbs />

                <div className={css.outlet}>
                    {
                        props.children ?? <Outlet />
                    }
                </div>

                <Footer />
                {
                    env.MSA_LOGGER &&
                    <Logger />
                }
            </div>
        </>
    );
});
