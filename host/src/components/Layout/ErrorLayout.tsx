import React from 'react';
import classNames from 'classnames';
import { useNavigate, useRouteError } from 'react-router-dom';

import { IError } from '@msa/kit/types';
import { Button, IconArrowLeft, IconHome } from '@msa/kit';

import { Layout, WideLayout } from './';

import frame404 from './frame404.svg';
import frame500 from './frame500.svg';

import css from './error.module.scss';


export const ErrorLayout = () => {
    const routerError = useRouteError() as Response;
    const navigate = useNavigate();
    const { status, statusText, message } = routerError as IError;

    const frames = [frame404, frame500];


    const goBack = () => {
        navigate(-1);
    };


    const goHome = () => {
        navigate('/', { replace: true });
    };


    return (
        <Layout>
            <WideLayout>
                 <div className={css.container}>
                     <div className={css.body}>
                         <div className={css.label}>
                             <img className={classNames(css.frame, status === 401 && css.grayscale)} src={frames[(Math.floor(Math.random() * frames.length))]} alt="Error" />
                             <h2 className={css.wrong}>Something went wrong...</h2>
                         </div>
                         <div className={css.desc}>
                             <h4 className={css.error}>Error {status} {statusText}</h4>
                             {
                                 // то что приходит с сервера
                                 message &&
                                 <p className={css.message}>{message}</p>
                             }
                         </div>
                         <div className={css.tip}>
                             Please go back or refresh the page
                         </div>
                         <div className={css.buttons}>
                             <Button size="small" color="dark" view="outlined" iconLeft={
                                 <IconArrowLeft />} onClick={goBack}>Go back</Button>
                             <Button size="small" color="dark" view="outlined" iconLeft={
                                 <IconHome />} onClick={goHome}>Go Home</Button>
                         </div>
                     </div>
                 </div>
            </WideLayout>
        </Layout>
    );
};
