import React, { Dispatch, Fragment, SetStateAction, useCallback, useMemo } from 'react';
import { matchRoutes, NavLink, useLocation, useNavigate } from 'react-router-dom';
import classNames from 'classnames';

import { IconArrowLeft, IconChevronDown } from '@msa/kit';
import type { IRoute } from '@msa/host/types';

import { useGetRoutes } from 'Routes';

import css from './navigation.module.scss';


interface IProps {
    expanded?: boolean,
    setExpanded?: Dispatch<SetStateAction<boolean>>
}

export const Navigation = React.memo((props: IProps) => {
    const { expanded, setExpanded } = props;

    const routes: IRoute[] = useGetRoutes();
    const navigate = useNavigate();
    const location = useLocation();
    const routesStack = matchRoutes(routes, location)?.reverse();

    let currentRoute = routes[0];

    // составляем массив активных ссылок ['platform', ':slug']
    const activeRoutes = useMemo(() => {
        if(routesStack)
            return routesStack
                .filter(({ route }) => !!route.path)
                .map(({ route }) => route.path);

        return [];
    }, [routesStack]);


    // проверяем доступность children
    const getChildren = useCallback((children: IRoute[]) => {
        return children
            // .filter((route: IRoute) => !route.index)
            .filter((route: IRoute) => !(route.handle && route.handle.visible === false))
            .filter((route: IRoute) => !route.handle || route.handle && (route.handle.permission || route.handle.permission === undefined));
    }, []);


    if(routesStack) {
        currentRoute = routesStack[0].route;

        // проверяем уровень вложенности
        // если в route.path есть (:slug) останавливаемся на этом уровне
        for(const { route } of routesStack) {
            currentRoute = route;

            if((route.path && route.path.match(/^:/) && (route.children && !!getChildren(route.children).length)) || (!route.element && !route.Component)) {
                break;
            }
        }
    }

    // собственно текущий уровень навигации
    const navigation = useMemo(() => {
        // если не нашли entity не показываем меню
        // if(currentRoute.handle && !!currentRoute.handle.selector && currentRoute.handle.selector.error)
        //     return [];

        if(currentRoute.children)
            return getChildren(currentRoute.children);

        return [];
    }, [currentRoute, getChildren]);


    const renderLevel = useCallback((navigation: IRoute[]) => {
        return navigation.map((route: IRoute, key: number) => {
            // смотрим наличие children у элемента
            const children = route.children && getChildren(route.children);

            return (
                <Fragment key={route.path ?? key}>
                    {
                        children &&
                        children.length ?
                            <div className={classNames(css.section, activeRoutes.includes(route.path) && css.active)}>
                                <NavLink end className={({ isActive }) => classNames(css.parent, isActive && css.active)} to={route.handle.path} onClick={() => setExpanded && setExpanded(true)}>
                                    {
                                        route.handle.icon &&
                                        React.cloneElement(route.handle.icon, { size: expanded ? 'medium' : 'large' })
                                    }
                                    {
                                        expanded &&
                                        <span className={css.name}>{route.handle.name}</span>
                                    }
                                    {
                                        expanded &&
                                        (
                                            // active.includes(route.path) ?
                                            //     <IconChevronUp className={css.toggle} /> :
                                            <IconChevronDown className={css.toggle} />
                                        )
                                    }
                                </NavLink>
                                {
                                    expanded &&
                                    activeRoutes.includes(route.path) &&
                                    children.map((route: IRoute) => {
                                        // проверка наличия вложенных children
                                        const children = route.children && getChildren(route.children);

                                        return (
                                            children &&
                                            children.length ?
                                                renderLevel([route]) :
                                                <NavLink key={route.handle.path} end className={({ isActive }) => classNames(css.child, isActive && css.active)} to={route.handle.path}>
                                                    {route.handle.icon}
                                                    <span className={css.name}>{route.handle.name}</span>
                                                </NavLink>
                                        );
                                    })
                                }
                            </div> :
                            <NavLink end className={({ isActive }) => classNames(css.parent, (isActive || activeRoutes.includes(route.path)) && css.active)} to={route.handle.path}>
                                {
                                    route.handle.icon &&
                                    React.cloneElement(route.handle.icon, { size: expanded ? 'medium' : 'large' })
                                }
                                {
                                    expanded &&
                                    <span className={css.name}>{route.handle.name}</span>
                                }
                            </NavLink>
                    }
                </Fragment>
            );
        });
    }, [currentRoute, getChildren, setExpanded]);


    const getUpPath = useMemo(() => {
        let endIndex = 0;

        if(currentRoute.handle?.selector) {
            endIndex = currentRoute.handle?.selector.data?.name ?
                location.pathname.lastIndexOf(`/${currentRoute.handle?.selector.data?.name}`) : 0;

            if(endIndex <= 0) {
                endIndex = currentRoute.handle?.selector.data?.id ?
                    location.pathname.lastIndexOf(`/${currentRoute.handle?.selector.data?.id}`) : 0;
            }
        }

        return location.pathname.substring(0, endIndex);
    }, [currentRoute, location]);


    return (
        <nav className={css.nav}>
            {
                currentRoute.handle &&
                !!currentRoute.handle.selector &&
                <div className={css.current}>
                    <IconArrowLeft className={css.navBack} onClick={() => navigate(getUpPath)} size={expanded ? 'medium' : 'large'}/>
                    {
                        expanded &&
                        <h1 className={css.title}>{currentRoute.handle.selector.data?.name}</h1>
                    }
                </div>
            }

            {
                navigation &&
                navigation.length ?
                    renderLevel(navigation) :
                    currentRoute.handle?.fallBack
            }
        </nav>
    );
});
