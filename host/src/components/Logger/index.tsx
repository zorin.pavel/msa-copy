import React, { useCallback, useEffect, useRef, useState } from 'react';
import classNames from 'classnames';
import { useNavigate } from 'react-router-dom';
import { format } from 'date-fns';
import { useTranslation } from 'react-i18next';

import { OrderDirection } from '@msa/kit/types';
import {
    Button,
    Divider,
    IconDoubleLines,
    IconLogs,
    isAuthorized,
    Loader,
    sort,
    StatusIcon,
    Tab,
    Table,
    TabList,
    Tabs
} from '@msa/kit';
import { IMessageData } from '@msa/host/types';
import { getLoggerMessages, useSocket } from '@msa/host';

import { env } from 'env';

import css from './logger.module.scss';


const maxHeight = 100;

export const Logger = () => {
    const { socket, socketMessage } = useSocket({ tenant: env.MSA_DEFAULT_TENANT });
    const navigate = useNavigate();
    const { t } = useTranslation();

    const containerRef = useRef<HTMLDivElement>(null);
    const bodyRef = useRef<HTMLDivElement>(null);

    const [isConnected, setConnected] = useState(!!socket?.readyState);
    const [type, setType] = useState<string>(t('common.actual'));
    const [actual, setActual] = useState<IMessageData[]>([]);
    const [history, setHistory] = useState<IMessageData[]>([]);
    const [loading, setLoading] = useState<boolean>(false);
    const [loggerDialog, setLoggerDialog] = useState<boolean>(false);
    const [isResizing, setResizing] = useState(false);
    const [startY, setStartY] = useState<number>(0);
    const [startHeight, setStartHeight] = useState<number>(0);

    const tabs: string[] = [
        t('common.actual'),
        t('common.history')
    ];


    useEffect(() => {
        if(socketMessage)
            setActual((prevEvents: IMessageData[]) => ([...prevEvents, socketMessage]));
    }, [socketMessage]);

    useEffect(() => {
        setType(t('common.actual'));
    }, [t]);


    useEffect(() => {
        setConnected(!!socket?.readyState);
    }, [socket?.readyState]);


    useEffect(() => {
        if(bodyRef.current) {
            bodyRef.current.scrollTop = bodyRef.current.scrollHeight;
        }
    }, [actual, history, bodyRef]);


    const handleClick = () => {
        setLoggerDialog(hidden => !hidden);
    };


    const setMessagesType = (type: string) => {
        setType(type);

        switch(type) {
            case t('common.history'):
                setLoading(true);
                //logs per day
                getLoggerMessages({ selectedPeriod: 86400, name: '' })
                    .then((response: IMessageData[]) => {
                        setHistory(sort(response, 'timestamp', OrderDirection.asc));
                        setLoading(false);
                    });
                break;
            case t('common.actual'):
                setHistory([]);
                break;
        }
    };


    const handleMouseDown = useCallback((event: React.MouseEvent<HTMLDivElement>) => {
        if(containerRef.current && event.button === 0) { // Проверяем, что нажата левая кнопка мыши
            setResizing(true);
            setStartY(event.clientY);
            setStartHeight(containerRef.current?.offsetHeight);
        }
    }, [containerRef, setResizing, setStartY, setStartHeight]);


    const handleMouseMove = useCallback((event: MouseEvent) => {
        if(isResizing && containerRef.current) {
            const deltaY = startY - event.clientY;
            const height = Math.max(maxHeight, startHeight + deltaY);

            if(height > window.document.body.clientHeight - 50)
                return;

            containerRef.current.style.height = `${height}px`;

            if(height <= maxHeight + 20)
                setLoggerDialog(false);
        }
    }, [containerRef, isResizing]);


    const handleMouseUp = useCallback(() => {
        setResizing(false);
    }, [setResizing]);


    const getHistoryPage = () => {
        setLoggerDialog(false);
        navigate('./history');
    }


    useEffect(() => {
        document.addEventListener('mousemove', handleMouseMove);
        document.addEventListener('mouseup', handleMouseUp);

        return () => {
            document.removeEventListener('mousemove', handleMouseMove);
            document.removeEventListener('mouseup', handleMouseUp);
        };
    }, [isResizing]);

console.log(type)
    return (
        <>
            <div className={css.wrapper}>
                <Button icon={<IconLogs />} onClick={handleClick} className={css.btn} />
            </div>
            {
                loggerDialog &&
                <div ref={containerRef} className={classNames(css.container, isResizing && css.resizing)}>
                    <div className={css.borderTop} onMouseDown={handleMouseDown}>
                        <IconDoubleLines className={css.icon} />
                    </div>
                    {
                        loading ?
                            <Loader size="small" className={css.loader} /> :
                            <>
                                <Tabs>
                                    <TabList className={css.tabs}>
                                        {
                                            isAuthorized({ messages: ['list'] }) &&
                                            tabs.map((tab: string) => {
                                                return (
                                                    <Tab key={tab} value={type} index={tab} onClick={() => setMessagesType(tab)}>
                                                        {tab[0].toUpperCase() + tab.slice(1)}
                                                    </Tab>
                                                );
                                            })
                                        }
                                    </TabList>
                                    {
                                        type === t('common.history') &&
                                        <Button size="xsmall" color="dark" label={t('common.messageLog')} className={css.fullscreen} onClick={() => getHistoryPage()} />
                                    }
                                </Tabs>

                                <div ref={bodyRef} className={classNames(css.scroll, (type === t('common.actual') ? actual : history).length === 0 && css.noMessages)}>
                                    {
                                        (type === t('common.actual') ? actual : history).length === 0 ?
                                            <h4>{t('common.noMessages')}</h4> :
                                            <Table cells={10} fontSize="small">
                                                {
                                                    (type === t('common.actual') ? actual : history).map((event: any, index: number) =>
                                                        <Table.Row key={index} valign="center">
                                                            <Table.Cell className={css.time} colspan="auto">{format(new Date(event.timestamp), 'yyyy-MM-dd HH:mm:ss')}</Table.Cell>
                                                            <Divider className={css.divider} />
                                                            <Table.Cell colspan={1}>{event.platform}</Table.Cell>
                                                            <Divider className={css.divider} />
                                                            <Table.Cell colspan={1}>{event.object_type}</Table.Cell>
                                                            <Divider className={css.divider} />
                                                            <Table.Cell colspan={1}>{event.name}</Table.Cell>
                                                            <Divider className={css.divider} />
                                                            <Table.Cell>{event.action}</Table.Cell>
                                                            <Divider className={css.divider} />
                                                            <Table.Cell className={classNames(css.state, event.severity && css[event.severity.toLowerCase()])}>{event.severity}</Table.Cell>
                                                            <Divider className={css.divider} />
                                                            <Table.Cell colspan={6}>{event.detail}</Table.Cell>
                                                        </Table.Row>
                                                    )
                                                }
                                            </Table>
                                    }
                                    {
                                        !isConnected &&
                                        <StatusIcon status={isConnected ? 'Success' : 'Fail'} action="Connection" />
                                    }
                                </div>
                            </>
                    }
                </div>
            }
        </>
    );
};
