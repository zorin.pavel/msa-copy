import React, { Fragment } from 'react';
import classNames from 'classnames';
import { useTranslation } from 'react-i18next';

import css from './languages.module.scss';
import { env } from 'env';


export const Languages = () => {
    const { i18n } = useTranslation();
    const currentLang = i18n.language;

    const changeLanguage = (language: string) => {
        i18n.changeLanguage(language);
    };


    if(env.MSA_SUPPORTED_LANGUAGES.split(/[,;|\s]+/).length <= 1)
        return;


    return (
        <div className={css.container}>
            {
                env.MSA_SUPPORTED_LANGUAGES.split(/[,;|\s]+/).map((language, index, array) => {
                    const charLang = language.split('-')[0];

                    return (
                        <Fragment key={language}>
                            <span
                                onClick={() => changeLanguage(language)}
                                className={classNames(css.locale, currentLang === language && css.active)}>
                                {charLang.charAt(0).toUpperCase()}{charLang.slice(1)}
                            </span>
                            {
                                index !== array.length - 1 && '|'
                            }
                        </Fragment>
                    );
                })
            }
        </div>
    );
};
