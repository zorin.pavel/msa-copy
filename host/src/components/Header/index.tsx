import { useCallback } from 'react';
import { NavLink, useNavigate } from 'react-router-dom';
import classNames from 'classnames';
import { useKeycloak } from '@react-keycloak/web';
import { useTranslation } from 'react-i18next';

import { env } from 'env';
import type { IKeycloakUser } from '@msa/host/types';
import { authStorage, IconSignOut, isAuthorized, useGlobalState } from '@msa/kit';

import { headerNavigation } from 'navigation';
import { Languages } from './Languages';

import logo from './logo.svg';
import css from './header.module.scss';


export const Header = () => {
    const navigate = useNavigate();
    const { keycloak } = useKeycloak();
    const { t } = useTranslation();

    const [user] = useGlobalState<IKeycloakUser>('user');


    const logOut = useCallback(() => {
        authStorage.removeItem();
        keycloak.logout();
    }, [keycloak]);


    return (
        <header className={css.container}>
            <img src={logo} className={css.logo} alt="MSA" onClick={() => navigate(env.MSA_BASE_PATH ?? '/')} />
            <div className={css.navBlock}>
                {
                    headerNavigation.map((navigation) => {
                            if((navigation.permission && !isAuthorized(navigation.permission)) || !navigation.path)
                                return null;

                            return (
                                <NavLink key={navigation.path} className={({ isActive }) => classNames(css.navLink, isActive && css.active)} to={navigation.path} target={navigation?.target} rel="noopener noreferrer">
                                    {t(`navigation.${navigation.name.replaceAll(' ', '').toLowerCase()}`)}
                                </NavLink>
                            );
                        }
                    )
                }
            </div>

            {
                env.MSA_SUPPORTED_LANGUAGES &&
                <Languages />
            }

            <div className={css.userBlock}>
                <div className={css.userNavItem}>{user?.name ?? user?.preferred_username}</div>
                <IconSignOut onClick={logOut} className={css.logout} />
            </div>

        </header>
    );
};
