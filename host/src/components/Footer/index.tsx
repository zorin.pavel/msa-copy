import { env } from 'env';

import css from './footer.module.scss';


export const Footer = () => {
    return (
        <footer className={css.container}>
            &copy; 2023 Neomsa
            <span className={css.version}>{env.MSA_VERSION}</span>
        </footer>
    );
};
