import i18n from 'i18next'
import LanguageDetector from 'i18next-browser-languagedetector'
import { initReactI18next } from 'react-i18next'

import { env } from 'env';


i18n
    .use(LanguageDetector)
    .use(initReactI18next)
    .init({
        debug: !import.meta.env.PROD,
        lng: env.MSA_LANGUAGE,
        fallbackLng: env.MSA_LANGUAGE ?? 'en-US',
        supportedLngs: env.MSA_SUPPORTED_LANGUAGES ? env.MSA_SUPPORTED_LANGUAGES.split(/[,;|\s]+/).map(language => language.split('-')[0]) : ['en'],
        resources: {},
        ns: [],
        defaultNS: false,
        nonExplicitSupportedLngs: true,
        react: { useSuspense: true },
        initImmediate: false,
        interpolation: {
            escapeValue: false,
            format: function (value, format, lng) {
                if (format === 'uppercase') return value.toUpperCase();
                if (format === 'lowercase') return value.toLowerCase();
                if (format === 'capitalize') return `${value.substr(0, 1).toUpperCase()}${value.substr(1)}`;
                return value;
            },
        }
    });

env.MSA_LANGUAGE ?
    i18n.changeLanguage(env.MSA_LANGUAGE) :
    i18n.changeLanguage(i18n.language);



export default i18n;
