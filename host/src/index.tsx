import React from 'react';
import ReactDOM from 'react-dom/client';
import { toast, ToastContainer } from 'react-toastify';
import { QueryClient, QueryClientProvider } from '@tanstack/react-query';
import { ReactQueryDevtools } from '@tanstack/react-query-devtools';
import { ReactKeycloakProvider } from '@react-keycloak/web';

import { authStorage } from '@msa/kit';
import { keycloakInstance, AuthProvider, SocketProvider } from '@msa/host';
import { Routes } from 'Routes';

import 'react-toastify/dist/ReactToastify.css';
import '@msa/kit/src/index.scss';
import './index.scss';

import translations from './locale';
import i18n from './i18n';

i18n.addResourceBundle('en', import.meta.env.MSA_PACKAGE_NAME, translations.en, true);
i18n.addResourceBundle('ru', import.meta.env.MSA_PACKAGE_NAME, translations.ru, true);
i18n.setDefaultNamespace(import.meta.env.MSA_PACKAGE_NAME);


const root = ReactDOM.createRoot(
    document.getElementById('root') as HTMLElement
);

export const queryClient = new QueryClient({
    defaultOptions: {
        queries: {
            staleTime: 1000 * 30, // 30 sec
            retry: false,
            refetchOnWindowFocus: false
        },
    },
});


export const App = () => {
    return (
        <>
            {/*<React.StrictMode>*/}
            <ReactKeycloakProvider
                authClient={keycloakInstance}
                onTokens={({ token }) => authStorage.setItem(token as string)}
                initOptions={{ checkLoginIframe: false }}>
                <AuthProvider>
                    <SocketProvider>
                        <QueryClientProvider client={queryClient}>
                            <Routes />
                            <ReactQueryDevtools initialIsOpen={false} toggleButtonProps={{ style: { bottom: '2.5rem' } }} />
                        </QueryClientProvider>
                    </SocketProvider>
                </AuthProvider>
            </ReactKeycloakProvider>
            <ToastContainer position={toast.POSITION.TOP_RIGHT} autoClose={3000} hideProgressBar closeOnClick rtl={false} draggable={false} icon={false} pauseOnHover />
            {/*</React.StrictMode>*/}
        </>
    );
};

root.render(App());
