import { useQuery } from '@tanstack/react-query';
import { createBrowserRouter, RouterProvider, useParams } from 'react-router-dom';

import { IError } from '@msa/kit/types';
import { Loader, Error, preparePath } from '@msa/kit';
import { IRoute, ITenant } from '@msa/host/types';

import { ErrorLayout, Layout } from 'components/Layout';
import { loadTenant } from 'actions';

import { env } from 'env';


export const useGetRoutes = () => {
    const params = useParams();

    const routes: IRoute[] = [
        {
            path: env.MSA_BASE_PATH,
            element: <Layout />,
            errorElement: <ErrorLayout />,
        }
    ];


    return preparePath(routes, params);
};


export const Routes = () => {
    const routes: IRoute[] = useGetRoutes();

    const { isError, isLoading, error } = useQuery<ITenant, IError>(
        ['tenant', env.MSA_DEFAULT_TENANT],
        () => loadTenant(env.MSA_DEFAULT_TENANT),
        {
            retry: 2,
            // refetchOnWindowFocus: true
        }
    );

    if(isError)
        return <Error layout {...error} message={`Can'\t find tenant "${env.MSA_DEFAULT_TENANT}"`} />;

    if(isLoading)
        return <Loader layout caption={`Fetching '${env.MSA_DEFAULT_TENANT}' environment...`} />;

    return <RouterProvider router={createBrowserRouter(routes)} />;
};
