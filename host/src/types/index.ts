import { ComponentType, ReactNode } from 'react';
import { LoaderFunction, RouteObject } from 'react-router-dom';

import { IPermissions } from '@msa/kit/types';


export interface IHeaderNavigation {
    name: string,
    path: string,
    target?: '_blank',
    permission?: IPermissions
}


export type IRoute = RouteObject & {
    index?: boolean,
    path?: string,
    element?: ReactNode,
    Component?: ComponentType,
    errorElement?: ReactNode,
    loader?: LoaderFunction,
    handle?: {
        name?: string,
        // prepared path
        path?: string,
        // menu icon
        icon?: ReactNode,
        // menu visible
        visible?: boolean, // default true
        // what to show in menu if we can't display children elements
        fallBack?: ReactNode,
        layout?: ReactNode,
        // breadcrumb visible
        crumb?: boolean, // default true
        // function to fetch entity name typeof useQuerySelector
        selector?: any,
        data?: Record<string, any>,
        tab?: boolean | string,
        permission?: (_roles: string[]) => boolean,
    },
    children?: IRoute[]
}


export interface IBaseORM {
    UUID?: string,
    name: string,
    tags?: string,
}


export interface ITenant {
    name: string,
    description?: string,
    platforms: IBaseORM[],
}


export interface IKeycloakUser {
    email: string,
    email_verified: boolean,
    family_name: string,
    given_name: string,
    name: string,
    preferred_username: string,
}


export interface IMessageData extends Partial<Record<string, string | boolean | null>> {
    action: string | null, //TAction,
    detail: string | null,
    name: string,
    object_type: string,
    platform: string,
    state: string, //TState,
    tenant: string,
    timestamp: string,
    type: 'event' | 'info' | 'error' | 'status',
    severity: EMessageSeverity,
}

export enum EMessageSeverity {
    ERROR = 'ERROR',
    WARNING = 'WARNING',
    INFO = 'INFO'
}


export type TFilters = Partial<Record<keyof IMessageData, string | boolean>>;

export type TOptions = {
    // callback function on message received
    callback?: () => void,
    // show toast
    toast?: boolean,
};

export type LocaleSwitcherValues = 'en' | 'ru';

export enum Locale {
    en = 'en',
    ru = 'ru'
}