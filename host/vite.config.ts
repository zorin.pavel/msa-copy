import { defineConfig } from 'vite'
import react from '@vitejs/plugin-react'
import basicSsl from '@vitejs/plugin-basic-ssl';
import * as path from 'path';
// import federation from "@originjs/vite-plugin-federation";

export default defineConfig({
    envPrefix: 'MSA',
    base: '',
    build: {
        target: 'esnext' //browsers can handle the latest ES features
    },
    server: {
        port: Number(process.env.PORT),
        https: true,
    },
    plugins: [
        react(),
        basicSsl(),
        // federation({
        //     name: 'host',
        //     remotes: {
        //         cicd: 'https://localhost:4001/assets/pipelines.js'
        //         // remoteApp: {
        //         //     external: `Promise.resolve(window.remoteURL)`,
        //         //     from: 'vite',
        //         //     externalType: 'promise',
        //         // },
        //     },
        //     shared: ['react', 'classnames']
        // })
    ],
    css: {
        modules: {
            localsConvention: 'camelCase'
        },
        preprocessorOptions: {
            scss: {
                additionalData: '@import "@msa/kit/scss/config.scss";'
            }
        }
    },
    resolve: {
        alias: {
            types: path.resolve('src/types'),
            actions: path.resolve('src/actions'),
            settings: path.resolve('src/settings'),
            helpers: path.resolve('src/helpers'),
            components: path.resolve('src/components'),
            locale: path.resolve('src/locale'),
        },
    }
})
