import { CSSProperties, ChangeEventHandler, FC, InputHTMLAttributes, ReactElement, ComponentProps } from 'react';


export const getEnumValueByKey = <T extends object>(en: T, key: keyof typeof en) => {
    const index = Object.keys(en).indexOf(key as string);

    return Object.values(en)[index];
};


export const getEnumKeyByValue = <T extends object, U extends string>(en: T, value: U) => {
    const index = Object.values(en).indexOf(value);

    return Object.keys(en)[index] as keyof T;
};


export interface IError {
    code?: string,
    status?: number | string,
    statusText?: string,
    message?: string,
    detail?: string | IErrorData[],
    error?: string,
}


export interface IErrorData {
    loc: Array<string>,
    msg: string,
    type: string
}


export interface IPagesResponse<T> {
    items: T,
    count: number
}


export enum Colors {
    default = 'default',
    dark = 'dark',
    white = 'white',
    blue = 'blue',
    red = 'red',
    green = 'green',
    yellow = 'yellow',
}


export enum Variants {
    text = 'text',
    outlined = 'outlined',
    contained = 'contained'
}


export enum Sizes {
    xsmall = 'xsmall',
    small = 'small',
    medium = 'medium',
    large = 'large',
}


export enum Types {
    info = 'info',
    error = 'error',
    warning = 'warning',
    success = 'success',
}


export enum Positions {
    top = 'top',
    topright = 'topright',
    topleft = 'topleft',
    bottom = 'bottom',
    bottomright = 'bottom-right',
    bottomleft = 'bottom-left',
}


export enum LabelPosition {
    top = 'top',
    left = 'left',
    right = 'right',
    bottom = 'bottom',
    formLeft = 'form-left', // Checkbox
    inside = 'inside', // Select
}


export enum OrderDirection {
    asc = 'asc',
    desc = 'desc',
}


export interface IOrderedBy {
    [key: string]: keyof typeof OrderDirection
}


export interface IFilteredBy {
    [key: string]: string | boolean | null | number
}


export enum DrawerFooterJustify {
    between = 'between',
    end = 'end'
}


export interface IOption<L = string, V = number | string> {
    label: L,
    value: V,
    disabled?: boolean,
    group?: boolean,
}


export type TStepKey = string | number


export interface IStepValid {
    [key: TStepKey]: boolean
}


export interface IStepComponentProps {
    setStepValid?: TSetStepValid,
    isStepValid?: (_key: TStepKey) => boolean,
    index: TStepKey
}


export interface IDrawerComponentProps {
    wideDrawer?: boolean,
    setWideDrawer?: (wide: boolean) => void
}


export interface IWizardStepComponentProps extends IStepComponentProps, IDrawerComponentProps {}


export interface IWizardStep {
    index: TStepKey,
    header?: string
    component?: FC<IWizardStepComponentProps | IStepComponentProps> | null
}


export type TSetStepValid = (stepKey: TStepKey, isValid: boolean) => void


export type TTabKey = string | number


export interface ITab<T = ComponentProps<any>> {
    index: TTabKey,
    header?: string
    component?: FC<T> | null,
    router?: string
}


export interface IEventTarget {
    target: {
        name?: string
        value?: string | number,
    };
}


export interface SelectProps extends InputHTMLAttributes<HTMLInputElement> {
    disabled?: boolean,
    required?: boolean,
    readonly?: boolean,
    empty?: boolean,
    value?: string | number,
    label?: string,
    defaultLabel?: string,
    options: IOption[],
    labelPosition?: keyof typeof LabelPosition,
    onChange?: ChangeEventHandler<HTMLInputElement>,
    onFocus?: ChangeEventHandler<HTMLInputElement>,
    onBlur?: ChangeEventHandler<HTMLInputElement>,
    onInput?: ChangeEventHandler<HTMLInputElement>,
    maxHeight?: number,
    className?: string,
    style?: CSSProperties
    error?: string | boolean,
    displayError?: boolean,
    loading?: boolean,
    dataTestId?: string,
    addonRight?: ReactElement,
    dropUp?: boolean;
}


export interface MultiSelectProps extends Omit<SelectProps, 'value' | 'empty'> {
    value?: (string | number)[] | string;
    maxBadges?: number;
}


export interface AutocompleteProps extends Omit<MultiSelectProps, 'defaultLabel' | 'maxHeight'> {
    searchOptions?: (searchValue: string) => void;
    filterOutSelectedOptions?: boolean;
}


export interface IPermissions {
    [key: string]: string[]
}

export interface IRbacResource {
    [key: string]: string[]
}


export interface IRbacObjects {
    platforms: {
        [key: string]: {
            clusters: string[]
        }
    },
    scopes: string[]
}


export interface IRbacGroupObject extends Record<string, string | string[]> {
    group: string,
    platform: string,
    cluster: string,
    scopes: string[]
}
