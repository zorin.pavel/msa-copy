import React from 'react';
import classNames from 'classnames';

import { LabelPosition, Sizes } from '@msa/kit/types';

import css from './circle.module.scss';


interface IProps {
    caption?: string,
    captionPosition?: keyof typeof LabelPosition,
    size?: keyof typeof Sizes,
    progress: number,
    capacity: number,
    capacityDesc?: string,
    inverse?: boolean,
    colors?: boolean,
    animation?: boolean,
    displayPercentage?: boolean,
}

export const Circle = (props: IProps) => {
    const {
        caption,
        size = Sizes.medium,
        progress = 0,
        capacity = 0,
        capacityDesc,
        inverse,
        colors = true, animation = true,
        displayPercentage = true,
    } = props;

    const strokeWidth = 8;
    const intSize = { xsmall: 50, small: 80, medium: 110, large: 140 }[size];
    const center = intSize / 2;
    const radius = center - strokeWidth;

    let percentage = capacity ?
        inverse ?
            100 - Math.ceil(progress / capacity * 100) :
            Math.ceil(progress / capacity * 100) :
        0;
    let color = 'default';

    percentage = (percentage > 100 || !displayPercentage) ? 100 : percentage;

    const dashArray = 2 * Math.PI * radius;
    const dashOffset = dashArray * ((100 - percentage) / 100);

    if(percentage > 75)
        color = 'warning';
    if(percentage > 95)
        color = 'error';

    if(!displayPercentage)
        color = 'default';


    return (
        <div className={classNames(css.wrapper, css[size])}>
            {
                caption && size === Sizes.xsmall &&
                <div className={css.caption}>{caption}</div>
            }
            <svg className={css.svgIndicator} style={{ width: intSize, height: intSize }}>
                <circle
                    className={css.svgIndicatorTrack}
                    cx={center}
                    cy={center}
                    fill="transparent"
                    r={radius}
                    // stroke={trackColor}
                    strokeWidth={strokeWidth} />
                <circle
                    className={classNames(css.svgIndicatorIndication, colors && css[color], animation && css.animated)}
                    cx={center}
                    cy={center}
                    fill="transparent"
                    r={radius}
                    // stroke={trackColor}
                    strokeDasharray={dashArray}
                    strokeDashoffset={dashOffset}
                    strokeWidth={strokeWidth}
                    strokeLinecap="round">
                    {
                        // animation &&
                        // <animate attributeName="stroke-dashoffset" values={`${dashArray};${dashOffset}`} dur="0.5s" />
                    }
                </circle>
            </svg>
            <div className={css.label} style={{ transform: 'translateY(calc(' + center + 'px - 50%))' }}>
                {
                    displayPercentage && !!capacity &&
                    <div className={css.percentage}>{percentage}%</div>
                }
                {
                    capacity ?
                        <div className={css.capacity}>{progress}/{capacity} {capacityDesc}</div> :
                        <div className={css.capacity}>N/A {capacityDesc}</div>
                }
            </div>
            {
                caption && size !== Sizes.xsmall &&
                <div className={css.caption}>{caption}</div>
            }
        </div>
    );
};
