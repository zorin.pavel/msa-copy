import { useKeycloak } from '@react-keycloak/web';

import { IRbacGroupObject } from '@msa/kit/types';


export const useObjectPermissions = (permissions?: IRbacGroupObject[], objectType?: string, scopes?: string[]) => {
    const { keycloak } = useKeycloak();

    const getAccess = () => {
        if(!permissions)
            return false;

        const groupPermissions = permissions
            .filter(permission => objectType && !!permission[objectType])
            .filter(permission => keycloak.tokenParsed?.groups.includes(permission.group))
            .map(gp => gp.scopes);

        return groupPermissions.flat().some(scope => {
            return scopes?.includes(scope);
        })
    };

    return getAccess();
};
