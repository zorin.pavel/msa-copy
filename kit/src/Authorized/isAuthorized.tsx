import { IPermissions } from '@msa/kit/types';
import { LocalStorage } from '@msa/kit';

export const permissionStorage = new LocalStorage({ storageKey: 'permissions' });

/* global permissions */
export const isAuthorized = (permissions: IPermissions) => {
    const resources: IPermissions = JSON.parse(permissionStorage.getItem() ?? '');

    const getAccess = () => {
        if(!resources)
            return false;

        return Object.keys(resources).some(resource => {
            // if(process.env.NODE_ENV === 'development' && permissions[resource])
            //     console.log(resource, resources[resource], permissions[resource]);

            return permissions[resource] && resources[resource].filter(scope => permissions[resource].includes(scope)).length;
        })
    };

    return getAccess();
};
