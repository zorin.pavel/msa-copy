import React, { ReactNode } from 'react';

import { IPermissions } from '@msa/kit/types';

import { isAuthorized } from './isAuthorized';


export interface IProp {
    children: ReactNode;
    permissions: IPermissions,
    not?: boolean
}


export const Authorized = (props: IProp) => {
    const { children, permissions, not } = props;

    return (
        <>
            {
                (!not && isAuthorized(permissions) || not && !isAuthorized(permissions)) ?
                    children :
                    null
            }
        </>
    );
};
