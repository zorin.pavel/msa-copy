import React, { ReactNode } from 'react';

import css from './placeholder.module.scss';


interface IProps {
    message: string,
    button?: ReactNode
}

export const Placeholder = (props: IProps) => {
    const { message, button } = props;

    return (
        <div className={css.placeholder}>
            <div className={css.message}>
                {message}
            </div>
            {
                button
            }
        </div>

    );
};
