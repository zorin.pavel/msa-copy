import React, { MouseEventHandler, ReactNode, MouseEvent } from 'react';
import classNames from 'classnames';

import css from './backdrop.module.scss';


interface Props {
    open?: boolean,
    onClick?: MouseEventHandler,
    children?: ReactNode,
}

export const Backdrop = (props: Props) => {
    const {
        open, onClick,
        ...rest
    } = props;


    const handleClick = (e: MouseEvent) => {
        e.stopPropagation();
        onClick && onClick(e)
    };


    return (
        <div className={classNames(css.backdrop, (open && css.open))} onClick={handleClick} {...rest}>
            {props.children}
        </div>
    );
};
