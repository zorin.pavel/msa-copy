import React, {
    ChangeEvent,
    ChangeEventHandler,
    ForwardedRef,
    forwardRef,
    InputHTMLAttributes,
    useEffect,
    useState
} from 'react';
import classNames from 'classnames';

import { LabelPosition } from '@msa/kit/types';
import { IconCheckbox, IconRadio } from '@msa/kit';

import css from './checkbox.module.scss';


interface IProps extends Omit<InputHTMLAttributes<HTMLInputElement>, 'value'> {
    disabled?: boolean,
    required?: boolean,
    readonly?: boolean,
    value?: number | string | boolean,
    label?: string,
    type?: 'checkbox' | 'radio',
    checked?: boolean,
    labelPosition?: keyof typeof LabelPosition,
    onChange?: ChangeEventHandler<HTMLInputElement>,
    onInput?: ChangeEventHandler<HTMLInputElement>,
    className?: string,
    error?: string | boolean,
    dataTestId?: string,
}


export const Checkbox = forwardRef((props: IProps, ref: ForwardedRef<HTMLInputElement>) => {
    const {
        name, label, checked = false, type = 'checkbox', labelPosition = LabelPosition.top, error,
        required, disabled, readonly,
        onChange,
        className, style,
        dataTestId,
        ...rest
    } = props;
    // value has to be a string
    const value = props.value ? props.value.toString() : '';

    const [selected, setSelected] = useState<boolean>(checked);

    useEffect(() => {
        setSelected(checked);
    }, [checked]);


    const handleChange = (event: ChangeEvent<HTMLInputElement>) => {
        if(readonly || disabled)
            return;

        setSelected(!selected);

        if(onChange)
            onChange(event);
    };


    return (
        <div className={classNames(
            css.wrapper,
            (error && css.error),
            (disabled && css.disabled),
            (readonly && css.readonly),
            css.labelPosition,
            css[labelPosition],
            className,
        )} style={style}
             {...rest}>
            {
                label && [LabelPosition.top, LabelPosition.left, LabelPosition.formLeft].includes(LabelPosition[labelPosition]) &&
                <label htmlFor={name} className={css.label}>
                    {label}
                    {
                        required &&
                        <span className={css.mark}>*</span>
                    }
                </label>
            }
            <div className={css.placeholder}>
                <input type={type} name={name} value={value} id={name} data-testid={dataTestId} checked={selected} disabled={disabled} readOnly={readonly} ref={ref} onChange={handleChange} />
                {
                    type === 'checkbox' ?
                        <IconCheckbox selected={selected} className={css.icon} /> :
                        <IconRadio selected={selected} className={css.icon} />
                }
            </div>
            {
                label && [LabelPosition.right].includes(LabelPosition[labelPosition]) &&
                <label htmlFor={name} className={css.label}>
                    {
                        required &&
                        <span className={css.mark}>*</span>
                    }
                    {label}
                </label>
            }
            {
                // error &&
                // <p className={css.error}>{error}</p>
            }
        </div>
    );
});

export default Checkbox;
