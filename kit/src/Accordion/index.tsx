import React, { useState, ReactElement } from 'react';

import { IconChevronUp, IconChevronDown } from '@msa/kit';

import css from './accordion.module.scss';


interface IAccordion {
    title: string;
    children: ReactElement<AccordionSectionProps>[];
    items?: AccordionSectionItems[]
}

export interface AccordionSectionProps {
    children: React.ReactNode;
}

export interface AccordionSectionItems {
    name: string;
    description: string;
    status: string;
}

export const Accordion: React.FC<IAccordion> = ({ title, children, items }: IAccordion) => {

    const [isActive, setIsActive] = useState(true);

    const handleClick = () => {
        setIsActive(!isActive);
    };

    let allCount = undefined;
    let activeCount = undefined;

    if(items) {
        allCount = items.length;
        activeCount = items.reduce((sum, current) => {
            if(current.status === 'installed') sum++;
            return sum;
        }, 0);
    }
    
    return (
        <div className={css.accordion}>
            <div className={css.title} onClick={() => handleClick()}>
                <div className={css.arrow}>
                    {isActive ? <IconChevronDown /> : <IconChevronUp />}
                </div>
                {
                    allCount ?
                        <div className={css.desc}>{title} {activeCount} / <span className={css.secondary}> {allCount}</span></div> :
                        <div className={css.desc}>{title}</div>
                }
            </div>
            {isActive && children}
        </div>
    );
};
