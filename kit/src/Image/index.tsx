import React, { HTMLAttributes, ReactElement, useState } from 'react';
import classNames from 'classnames';

import css from './logo.module.scss';
import gear from './gear.svg';


interface IProps extends HTMLAttributes<HTMLElement> {
    src: string,
    alt?: string,
    className?: string,
    fallBack?: string | ReactElement,
}

export const Image = (props: IProps) => {
    const { src, alt, className, fallBack, ...rest } = props;
    const [imageLoaded, setImageLoaded] = useState<boolean>(true);


    return (
        <>
            {
                imageLoaded ?
                    <img onError={() => setImageLoaded(false)} className={classNames(css.logo, className)} src={src} alt={alt} {...rest} /> :
                    (
                        fallBack ?
                            (
                                typeof fallBack === 'string' ?
                                    <img src={fallBack} alt={alt} className={classNames(css.logo, css.disabled, className)} {...rest} /> :
                                    React.cloneElement(fallBack, { className: classNames(css.logo, css.disabled, className), ...rest })
                            ) :
                            <img src={gear} alt={alt} className={classNames(css.logo, css.disabled, className)} {...rest} />
                    )
            }
        </>
    );
};
