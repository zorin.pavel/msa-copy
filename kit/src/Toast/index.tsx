import React, { useState, useEffect, ReactNode, MouseEventHandler, CSSProperties, ReactElement } from 'react';
import classNames from 'classnames';
import { Id, toast } from 'react-toastify';
import parse from 'html-react-parser';

import { Positions, Types } from '@msa/kit/types';
import { Portal, IconClose, IconError, IconInfo, IconSuccess, IconWarning } from '@msa/kit';

import css from './toast.module.scss';


interface Props {
    open?: boolean,
    type?: keyof typeof Types,
    icon?: ReactElement,
    title?: string,
    message?: string | ReactNode,
    position?: keyof typeof Positions,
    timeout?: number | boolean,
    onClose?: MouseEventHandler<HTMLElement>,
    style?: CSSProperties,
    className?: string,
    children?: ReactNode,
}

export const Toast = (props: Props) => {
    let { icon } = props;

    const {
        type = Types.info, title, message, position = Positions.topright, open,
        timeout = 3000,
        onClose,
        className, style,
        ...rest
    } = props;

    const [animationOpen, setAnimationOpen] = useState<boolean>(true);
    const [animationClose, setAnimationClose] = useState<boolean>(false);

    useEffect(() => {
        if(open) {
            setAnimationOpen(false);
            setAnimationClose(true);
        } else {
            setAnimationOpen(true);

            setTimeout(() => {
                setAnimationClose(false);
            }, 300);
        }
    }, [open]);


    useEffect(() => {
        if(timeout)
            setTimeout(() => handleClose(), timeout as number);
    }, []);


    const handleClose = (event?: any) => {
        setAnimationOpen(true);

        setTimeout(() => {
            setAnimationClose(false);

            if(onClose)
                onClose(event);
        }, 300);
    };


    if(!icon) {
        if(type === 'info')
            icon = <IconInfo />;

        if(type === 'error')
            icon = <IconError />;

        if(type === 'warning')
            icon = <IconWarning />;

        if(type === 'success')
            icon = <IconSuccess />;
    }


    return (
        <>
            {
                (open || animationClose) &&
                <Portal id="toast" className={classNames(css.wrapper, css[position])}>
                    <div className={classNames(
                        css.toast,
                        css[type],
                        className,
                        (!animationOpen && animationClose) && css.open,
                    )}
                    style={style}
                    {...rest}>
                        {icon && React.cloneElement(icon, { size: 'large', className: css.icon })}
                        <div className={css.message}>
                            {
                                title &&
                                <h3 className={css.title}>{title}</h3>
                            }
                            <div className={css.body}>{message || props.children}</div>
                        </div>
                        <IconClose onClick={handleClose} size="large" className={css.close} />
                    </div>
                </Portal>
            }
        </>
    );
};

// use with react-tostify library
interface ITostifyOptions {
    title?: string,
    message?: string | ReactNode,
    toastId?: Id
}

Toast.show = (type: keyof typeof Types = 'info', options: ITostifyOptions) => {
    const { title, message, toastId } = options;
    let icon;

    if(type === 'info')
        icon = <IconInfo />;

    if(type === 'error')
        icon = <IconError />;

    if(type === 'warning')
        icon = <IconWarning />;

    if(type === 'success')
        icon = <IconSuccess />;

    toast(
        <div className={classNames(
            css.toastify,
            css[type],
        )}>
            {icon && React.cloneElement(icon, { size: 'large', className: css.icon })}
            <div className={css.message}>
                {
                    title &&
                    <h3 className={css.title}>{title}</h3>
                }
                <div className={css.body}>{parse(message as string)}</div>
            </div>
        </div>,
        { type, toastId }
    );
};


Toast.error = (options: ITostifyOptions) => {
    const { title, message, toastId } = options;

    toast(
        <div className={classNames(
            css.toastify,
            css.error,
        )}>
            <IconError size="large" className={css.icon} />
            <div className={css.message}>
                {
                    title &&
                    <h3 className={css.title}>{title}</h3>
                }
                <div className={css.body}>{parse(message as string)}</div>
            </div>
        </div>,
        { type: 'error', toastId }
    );
};


Toast.success = (options: ITostifyOptions) => {
    const { title, message, toastId } = options;
    toast(
        <div className={classNames(
            css.toastify,
            css.success,
        )}>
            <IconSuccess size="large" className={css.icon} />
            <div className={css.message}>
                {
                    title &&
                    <h3 className={css.title}>{title}</h3>
                }
                <div className={css.body}>{parse(message as string)}</div>
            </div>
        </div>,
        { type: 'success', toastId }
    );
};


Toast.warning = (options: ITostifyOptions) => {
    const { title, message, toastId } = options;

    toast(
        <div className={classNames(
            css.toastify,
            css.warning,
        )}>
            <IconWarning size="large" className={css.icon} />
            <div className={css.message}>
                {
                    title &&
                    <h3 className={css.title}>{title}</h3>
                }
                <div className={css.body}>{parse(message as string)}</div>
            </div>
        </div>,
        { type: 'warning', toastId }
    );
};
