import React, {
    ChangeEvent,
    ChangeEventHandler,
    ForwardedRef,
    forwardRef,
    InputHTMLAttributes,
    useCallback, useEffect,
    useState
} from 'react';
import classNames from 'classnames';

import { LabelPosition } from '@msa/kit/types';
import { Input } from '@msa/kit';

import css from './range.module.scss';


interface IProps extends InputHTMLAttributes<HTMLInputElement> {
    disabled?: boolean,
    required?: boolean,
    readonly?: boolean,
    value?: string | number,
    label?: string,
    min?: number,
    labelMin?: string,
    max?: number,
    labelMax?: string,
    step?: number,
    labelPosition?: keyof typeof LabelPosition,
    onChange?: ChangeEventHandler<HTMLInputElement>,
    onFocus?: ChangeEventHandler<HTMLInputElement>,
    onBlur?: ChangeEventHandler<HTMLInputElement>,
    onInput?: ChangeEventHandler<HTMLInputElement>,
    className?: string,
    error?: string | boolean,
    displayError?: boolean,
    displayValue?: boolean,
    displayLabels?: boolean,
    dataTestId?: string,
}

export const Range = forwardRef((props: IProps, ref: ForwardedRef<HTMLInputElement>) => {
    const {
        name, value = 0, label, labelPosition = LabelPosition.top,
        min = 0, max = 100, step = 1,
        labelMin, labelMax,
        error, displayError, displayValue, displayLabels,
        required, disabled, readonly, placeholder,
        onChange, onFocus, onBlur, onInput,
        className, style,
        dataTestId,
        ...rest
    } = props;
    const [currentValue, setValue] = useState(value);


    useEffect(() => {
        setValue(value);
    }, [value]);


    const handleChange = useCallback((event: ChangeEvent<HTMLInputElement>) => {
        const targetValue = Number(event.target.value) > max ? max : Number(event.target.value);

        setValue(targetValue);

        if(onChange)
            onChange(event);
    }, [onChange]);


    const handleFocus = useCallback((event: ChangeEvent<HTMLInputElement>) => {
        if(onFocus)
            onFocus(event);
    }, [onFocus]);


    const handleBlur = useCallback((event: ChangeEvent<HTMLInputElement>) => {
        if(onBlur)
            onBlur(event);
    }, [onBlur]);


    const handleInput = useCallback((event: ChangeEvent<HTMLInputElement>) => {
        if(onInput)
            onInput(event);
    }, [onInput]);


    return (
        <div
            className={classNames(
                css.range,
                (error && css.error),
                (disabled && css.disabled),
                (readonly && css.readonly),
                css.labelPosition,
                css[labelPosition],
                className,
            )}
            style={style} >
            {
                label &&
                <label htmlFor={name} className={css.label}>
                    {label}
                    {
                        required &&
                        <span className={css.mark}>*</span>
                    }
                </label>
            }
            <div className={css['errorWrapper']}>
                <div className={css['valueWrapper']}>
                    {
                        displayValue &&
                        <Input
                           type="number"
                           disabled={disabled}
                           required={required}
                           readOnly={readonly}
                           placeholder={placeholder}
                           value={typeof currentValue === 'string' ? currentValue : String(currentValue)}
                           onChange={handleChange}
                           max={max}
                           min={min}
                           error={error}
                           className={css.value} />
                    }
                    <div className={css['labelsWrapper']}>
                        {
                            (displayLabels || labelMin) &&
                            <span>{labelMin ?? min}</span>
                        }
                        <input
                            type="range"
                            tabIndex={-1}
                            min={min}
                            max={max}
                            step={step}
                            id={name}
                            name={name}
                            disabled={disabled || readonly}
                            required={required}
                            placeholder={placeholder}
                            value={Number(currentValue)}
                            onChange={handleChange}
                            onFocus={handleFocus}
                            onBlur={handleBlur}
                            onInput={handleInput}
                            data-testid={dataTestId}
                            ref={ref}
                            {...rest} />
                        {
                            (displayLabels || labelMax) &&
                            <span>{labelMax ?? max}</span>
                        }
                    </div>
                </div>
                {
                    displayError && error &&
                    <p className={css.error}>{error}</p>
                }
            </div>
        </div>
    );
});
