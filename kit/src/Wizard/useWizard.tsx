import { useCallback, useEffect, useState } from 'react';

import type { IStepValid, IWizardStep } from '@msa/kit/types';


export const useWizard = (defaultActiveStep: string | number, steps: IWizardStep[], propValidSteps: IStepValid = {}) => {
    const stepKeys = steps.map(step => step.index);
    const initialValidSteps: IStepValid = {};

    stepKeys.map(step => initialValidSteps[step] = (propValidSteps[step] !== undefined ? propValidSteps[step] : false));

    const [currentStepKey, setCurrentStepKey] = useState<string | number>(defaultActiveStep);
    const [validSteps, setValidSteps] = useState<IStepValid>(initialValidSteps);
    const [isLast, setLast] = useState<boolean>(false);
    const [isFirst, setFirst] = useState<boolean>(true);


    useEffect(() => {
        const index = stepKeys.findIndex(step => (step === currentStepKey));

        if(index + 1 === stepKeys.length)
            setLast(true);
        else
            setLast(false);


        if(index === 0)
            setFirst(true);
        else
            setFirst(false);
    }, [currentStepKey]);


    const isStepValid = useCallback((stepKey: number | string): boolean => {
        return validSteps[stepKey];
    }, [validSteps]);


    const isAllValid = useCallback(() => {
        return (Object.values(validSteps).filter(valid => valid).length === stepKeys.length);
    }, [validSteps]);


    const setStepValid = (stepKey: number | string, isValid?: boolean) => {
        setValidSteps(validSteps => ({ ...validSteps, [stepKey]: isValid !== undefined ? isValid : true }));
    };


    const nextStep = async (beforeNext?: Function, ...params: any[]) => {
        // beforeNext should return true if success
        if(beforeNext) {
            const beforeResult = await beforeNext(...params);

            if(!beforeResult)
                return currentStepKey;
        }

        const nextIndex = stepKeys.findIndex(step => (step === currentStepKey)) + 1;
        const nextKey = stepKeys[nextIndex];

        nextKey && setCurrentStepKey(nextKey);

        return nextKey;
    };


    const prevStep = async (beforePrev?: Function, ...params: any[]) => {
        // beforePrev should return true if success
        if(beforePrev) {
            const beforeResult = await beforePrev(...params);

            if(!beforeResult)
                return currentStepKey;
        }

        const prevIndex = stepKeys.findIndex(step => (step === currentStepKey)) - 1;
        const prevKey = stepKeys[prevIndex];

        prevKey && setCurrentStepKey(prevKey);

        return prevKey;
    };


    const resetStep = useCallback((callBack?: Function, ...params: any[]) => {
        setCurrentStepKey(defaultActiveStep);
        setValidSteps(initialValidSteps);

        if(callBack)
            callBack(defaultActiveStep, ...params);

        return defaultActiveStep;
    }, [defaultActiveStep, initialValidSteps]);


    const isDisabled = useCallback((stepKey: number | string) => {
        const prevIndex = stepKeys.findIndex(step => (step === stepKey)) - 1;
        const prevKey = stepKeys[prevIndex];

        if(prevKey)
            return !isStepValid(prevKey);

        return false;
    }, [stepKeys]);


    return {
        isStepValid,
        setStepValid,
        isAllValid,
        isDisabled,
        nextStep,
        prevStep,
        resetStep,
        isLast,
        isFirst,
        currentStepKey,
        setCurrentStepKey,
        validSteps,
    };
};
