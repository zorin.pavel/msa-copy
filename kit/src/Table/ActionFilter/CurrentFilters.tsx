import React from 'react';

import { IOption } from '@msa/kit/types';
import { IconFilter, Badge, IFilterProps } from '@msa/kit';

import css from './filter.module.scss';


interface IProps extends Omit<IFilterProps, 'fieldName'> {
    values: {
        [key: string]: IOption[]
    }
}

export const CurrentFilters = (props: IProps) => {
    const { filteredBy, filterBy, values, empty } = props;

    const clearThis = (fieldName: string) => {
        filterBy && filterBy(fieldName, {});
    };


    if(!Object.keys(filteredBy).filter(filter => filteredBy[filter] !== undefined).length)
        return null;


    return (
        <div className={css.current}>
            <IconFilter size="large" checked />
            {
                Object.keys(filteredBy).map(fieldName => {
                    const badge = values[fieldName].find(option => option.value === filteredBy[fieldName]);

                    if(!badge && empty !== undefined)
                        return <Badge key={fieldName} label={`Without any ${fieldName}`} onDelete={() => clearThis(fieldName)} />

                    if(badge)
                        return <Badge key={badge.value} label={badge.label} onDelete={() => clearThis(fieldName)} />
                })
            }
        </div>
    );
};
