import React, { ReactNode, useEffect, useRef, useState } from 'react';
import classNames from 'classnames';

import { IFilteredBy } from '@msa/kit/types';
import { IconFilter, useOnClickOutside } from '@msa/kit';
import { ByValues } from './ByValues';

import css from './filter.module.scss';


export interface IFilterProps {
    fieldName: string,
    filteredBy: IFilteredBy,
    filterBy: (fieldName: string, filterValue: IFilteredBy) => void,
    children?: ReactNode,
    disabled?: boolean,
    empty?: string | boolean | null | number
}

const ActionFilter = (props: IFilterProps) => {
    const { fieldName, filteredBy, disabled } = props;
    const containerRef = useRef<HTMLDivElement>(null);

    const [open, setOpen] = useState<boolean>(false);

    useEffect(() => {
        setOpen(false);
    }, [filteredBy]);


    useOnClickOutside(containerRef, async () => setOpen(false));


    return (
        <div className={css.container} ref={containerRef}>
            <IconFilter checked={filteredBy && filteredBy[fieldName] !== undefined} className={classNames(css.filter, filteredBy && filteredBy[fieldName] !== undefined && css.active)} disabled={disabled} onClick={() => setOpen(open => !open)} />
            {
                open &&
                <div className={css.actionContainer}>
                    {props.children}
                </div>
            }
        </div>
    );
};


ActionFilter.ByValues = ByValues;

export { ActionFilter };
