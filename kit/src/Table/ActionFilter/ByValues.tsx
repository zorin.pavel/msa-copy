import React, { ChangeEvent } from 'react';
import classNames from 'classnames';

import { IOption } from '@msa/kit/types';
import { ActionFilter, Checkbox, IFilterProps } from '@msa/kit';

import css from './filter.module.scss';


interface IProps extends IFilterProps {
    options: IOption[]
}

export const ByValues = (props: IProps) => {
    const { fieldName, options, filteredBy, filterBy, empty } = props;


    const clearAll = () => {
        filterBy && filterBy(fieldName, {});
    };


    const setEmpty = () => {
        filterBy && empty !== undefined && filterBy(fieldName, { [fieldName]: empty });
    };


    const onChange = (event: ChangeEvent<HTMLInputElement>) => {
        filterBy && filterBy(fieldName, { [fieldName]: event.target.value });
    };


    return (
        <ActionFilter {...props}>
            <div className={css.option}>
                <Checkbox
                    type="radio"
                    value={''}
                    checked={!Object.keys(filteredBy).filter(filter => filteredBy[filter] !== undefined).length}
                    label="All"
                    onChange={clearAll}
                    labelPosition="right"
                    className={css.checkbox}
                />
            </div>
            {
                empty !== undefined &&
                <div className={css.option}>
                    <Checkbox
                        type="radio"
                        value={'empty'}
                        checked={filteredBy[fieldName] === empty}
                        label={`Without any ${fieldName}`}
                        onChange={setEmpty}
                        labelPosition="right"
                        className={css.checkbox}
                    />
                </div>
            }
            {
                options.map(option => {
                    return (
                        <Checkbox
                            type="radio"
                            key={option.value}
                            value={option.value}
                            checked={filteredBy[fieldName] === option.value}
                            label={option.label}
                            labelPosition="right"
                            className={classNames(css.checkbox, css.option)}
                            onChange={onChange}
                        />
                    );
                })
            }
        </ActionFilter>
    );
};
