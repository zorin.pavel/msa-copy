import React from 'react';


interface IProps {
    buttons?: number,
}

export const Space = (props: IProps) => {
    const { buttons = 1 } = props;
    // size of one Button Icon
    const iconSize = 2.25;

    return (
        <div style={{ minWidth: iconSize * buttons + 'rem' }} />
    );
};

export default Space;
