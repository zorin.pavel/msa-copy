import React from 'react';
import classNames from 'classnames';

import { IOrderedBy, OrderDirection } from '@msa/kit/types';
import { IconSortDown, IconSortUp } from '@msa/kit';

import css from './icon.module.scss';


interface Props {
    fieldName: string,
    orderedBy?: IOrderedBy,
    orderBy: (_fieldName: string, _orderDirection: OrderDirection) => void
}

export const ActionSort = (props: Props) => {
    const { fieldName, orderedBy, orderBy } = props;

    return (
        orderedBy && orderedBy[fieldName] === OrderDirection.asc ?
            <IconSortDown className={classNames(css.sort, css.active)} onClick={() => orderBy(fieldName, OrderDirection.desc)} /> :
            orderedBy && orderedBy[fieldName] === OrderDirection.desc ?
                <IconSortUp className={classNames(css.sort, css.active)} onClick={() => orderBy(fieldName, OrderDirection.asc)} /> :
                <IconSortDown className={css.sort} onClick={() => orderBy(fieldName, OrderDirection.asc)} />
    );
};

export default ActionSort;
