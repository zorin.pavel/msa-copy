import React, { createContext, Dispatch, ReactNode, RefObject, SetStateAction, useContext, useState } from 'react';

import { IFilteredBy, IOrderedBy, OrderDirection } from '@msa/kit/types';


interface IProps {
    tableRef: null | RefObject<HTMLDivElement>,
    children?: ReactNode,
    orderedBy?: IOrderedBy,
    setOrderedBy?: Dispatch<SetStateAction<IOrderedBy>>
}

const defaultContextValue = {
    tableRef: null,
};

const TableContext = createContext<IProps>(defaultContextValue);

const TableProvider = (props: IProps) => {
    const [orderedBy, setOrderedBy] = useState<IOrderedBy>({ 'name': OrderDirection.asc });

    return (
        <TableContext.Provider value={{
            ...defaultContextValue,
            orderedBy,
            setOrderedBy,
            ...props,
        }}>
            {props.children}
        </TableContext.Provider>
    );
};



interface useTableOptions {
    order?: IOrderedBy
    filter?: IFilteredBy
}

// can be used only in one component
const useTable = (options?: useTableOptions | IOrderedBy) => {
    const order = (options?.order ?? options) as IOrderedBy;
    const filter = options?.filter as IFilteredBy;

    const [orderedBy, setOrderedBy] = useState<IOrderedBy>(order ?? { 'name': OrderDirection.asc });
    const [filteredBy, setFilteredBy] = useState<IFilteredBy>(filter ?? {});

    return { orderedBy, setOrderedBy, filteredBy, setFilteredBy };
};

// if you need to update orderedBy from somewhere use context
const useTableContext = () => useContext(TableContext);

export { TableProvider, useTable, useTableContext, TableContext };
