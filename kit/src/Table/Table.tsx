import React, { HTMLAttributes, useRef } from 'react';
import classNames from 'classnames';

import { Sizes } from '@msa/kit/types';

import { TableProvider } from './TableProvider';
import { TableHeader } from './TableHeader';
import { TableRow } from './TableRow';
import { TableCell } from './TableCell';
import { ActionSort } from './ActionSort';
import { ActionFilter } from './ActionFilter';
import { CurrentFilters } from './ActionFilter/CurrentFilters';

import css from './table.module.scss';


interface Props extends HTMLAttributes<HTMLDivElement> {
    cells?: number,
    nowrap?: boolean,
    fontSize?: keyof typeof Sizes,
    align?: string,
    valign?: string,
}

const Table = (props: Props) => {
    const tableRef = useRef(null);

    const {
        cells, nowrap, align, valign, fontSize,
        className, style,
        ...rest
    } = props;


    return (
        <TableProvider
            tableRef={tableRef}>
            <div
                ref={tableRef}    
                className={classNames(
                    css['wrapper'],
                    className,
                )}>

                <div {...rest}
                    className={classNames(
                        css.table,
                        (cells && css[`cells${cells}`]),
                        (align && css[`align-${align}`]),
                        (valign && !scroll && css[`valign-${valign}`]),
                        (fontSize && css[`font-${fontSize}`]),
                        (nowrap && css.nowrap)
                    )}
                    style={style}
                >
                    {props.children}
                </div>
            </div>
        </TableProvider>
    );
};


Table.Row = TableRow;
Table.Cell = TableCell;
Table.Header = TableHeader;
Table.ActionSort = ActionSort;
Table.ActionFilter = ActionFilter;
Table.CurrentFilters = CurrentFilters;

export { Table };
