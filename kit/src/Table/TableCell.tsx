import React, { HTMLAttributes } from 'react';
import classNames from 'classnames';

import { ucFirst } from '@msa/kit/src/helpers/ucFirst';
// import { useTable } from './TableProvider';

import css from './table.module.scss';


interface Props extends HTMLAttributes<HTMLDivElement> {
    actions?: number,
    wrap?: boolean,
    nowrap?: boolean,
    align?: string,
    valign?: string,
    colspan?: number | 'auto' | 'grow'
    rowSize?: 'small'| 'medium'
}

export const TableCell = (props: Props) => {
    // const { tableRef } = useTable();
    const {
        actions, wrap, nowrap, valign, align, colspan,
        className, style, rowSize = 'medium',
        ...rest
    } = props;

    return (
        <div {...rest}
            className={classNames(
                css.cell,
                (colspan && css[`colspan${ucFirst(colspan)}`]),
                (valign && css[`valign${ucFirst(valign)}`]),
                (align && css[`align${ucFirst(align)}`]),
                (rowSize && css[`${rowSize}`]),
                (nowrap && css.nowrap),
                (wrap && css.wrap),
                (actions && [css.actions, css[`action${actions}`]]),
                className,
            )}
            style={style}
        >
            {props.children}
        </div>
    );
};
