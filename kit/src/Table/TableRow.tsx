import React, { HTMLAttributes } from 'react';
import classNames from 'classnames';

import { ucFirst } from '@msa/kit/src/helpers/ucFirst';
// import { useTable } from './TableProvider';

import css from './table.module.scss';


interface Props extends HTMLAttributes<HTMLDivElement> {
    nowrap?: boolean,
    align?: string,
    valign?: string,
}

export const TableRow = (props: Props) => {
    // const { tableRef } = useTable();
    const {
        nowrap, valign, align,
        className, style,
        ...rest
    } = props;

    return (
        <div {...rest}
            className={classNames(
                css.row,
                (valign && css['valign' + ucFirst(valign)]),
                (align && css['align' + ucFirst(align)]),
                (nowrap && css['nowrap']),
                className,
            )}
            style={style}
        >
            {props.children}
        </div>
    );
};

export default TableRow;
