import React, { useState } from 'react';
import CodeMirror from '@uiw/react-codemirror';
import { StreamLanguage } from '@codemirror/language';
import { yaml } from '@codemirror/legacy-modes/mode/yaml';
import { load, dump } from 'js-yaml';
import { default as Ajv } from 'ajv';


const yaml2json = (yaml: string) => {
    try {
        return load(yaml) as Record<string, any>;
    } catch(e) {
        return yaml;
    }
};


const json2yaml = (json: Record<string, any> | unknown) => {
    return dump(json, { skipInvalid: true });
};


interface IProps {
    value: Record<string, any>,
    schema?: Record<string, any>,
    onChange: (_value: Record<string, any>) => void
    onError?: (_error: boolean) => void
    maxWidth?: string,
    height?: string,
    readOnly?: boolean,
}

export const YamlEditor = (props: IProps) => {
    const { value, onChange, onError, maxWidth, height, readOnly } = props;

    const [localValue, setLocalValue] = useState<string>(json2yaml(value));

    const schema = props.schema ?? {
        type: 'object',
        properties: {
        },
        additionalProperties: true
    };

    const ajv = new Ajv({
        // strict: true,
        allErrors: true
    });


    const handleChange = (yaml: string) => {
        setLocalValue(yaml);

        const json: Record<string, any> | string = yaml2json(yaml);
        const validate = ajv.compile(schema);
        const valid = validate(json);

        onError && onError(!valid);
        valid && onChange(json as Record<string, any>);
    };


    return (
        <CodeMirror
            value={localValue}
            height={height ?? '100%'}
            maxWidth={maxWidth ?? ''}
            readOnly={readOnly}
            extensions={[StreamLanguage.define(yaml)]}
            onChange={handleChange}
        />
    );
};

export default YamlEditor;
