import React from 'react';
import classNames from 'classnames';

import { IError } from '@msa/kit/types';

import css from './error.module.scss';
import frame500 from './frame500.svg';


interface IProps extends IError {
}

export const ComingSoon = (props: IProps) => {
    const { status = 'Coming soon...', message = 'This module will be added shortly' } = props;


    return (
        <div className={css.container}>
            <div className={css.body}>
                <div className={css.label}>
                    <img className={classNames(css.frame, css.grayscale)} src={frame500} alt="Error" />
                    <h2 className={css.wrong}>{status}</h2>
                </div>
                <div className={css.desc}>
                    {
                        message &&
                        <p className={css.message}>{message}</p>
                    }
                </div>
            </div>
        </div>
    );
};
