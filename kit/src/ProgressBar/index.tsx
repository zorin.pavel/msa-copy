import React, { useEffect, useState } from 'react';
import classNames from 'classnames';

import { Button, IconError, IconRun, IconSuccess } from '@msa/kit';

import css from './analyze.module.scss';


interface IProps {
    onSuccess?: () => void
}

export const ProgressBar = (props: IProps) => {
    const { onSuccess } = props;
    let interval: ReturnType<typeof setTimeout>;

    const [progress, setProgress] = useState<number>(0);
    const [progressState, setProgressState] = useState<'run' | 'success' | 'fail' | undefined>();

    useEffect(() => {
        if(progress >= 100) {
            clearInterval(interval);
            setProgressState('success');
            onSuccess && onSuccess();
        }
    }, [progress]);


    useEffect(() => {
        if(progressState === 'run') {
            interval = setInterval(() => {
                setProgress(progress => {
                    const p = Math.floor(Math.random() * 10);

                    if((progress + p) > 30 && (progress + p) < 33) {
                        clearInterval(interval);
                        setProgressState('fail');
                    }

                    return progress + p < 100 ? progress + p : 100;
                });
            }, 500);
        }

        return () => clearInterval(interval);
    }, [progressState]);


    const runAnalyze = () => {
        setProgress(0);
        setProgressState('run');
    };


    const renderBar = () => {
        return (
            <div className={css.bar}>
                <div className={classNames(css.filler, css[progressState as string])} style={{ width: `${progress}%` }}/>
            </div>
        );
    };


    const renderState = () => {
        return (
            <div className={css.state} onClick={runAnalyze}>
                {
                    progressState === 'run' &&
                    <>
                        <span>Analyzing...</span>
                        <span>{progress}%</span>
                    </>
                }
                {
                    progressState === 'success' &&
                    <>
                        <span>Analysis is completed</span>
                        <IconSuccess className={css.success} />
                    </>
                }
                {
                    progressState === 'fail' &&
                    <>
                        <span>Error</span>
                        <IconError className={css.error} />
                    </>
                }
            </div>
        );
    };


    return (
        <>
            {
                progressState ?
                    <div className={css.progress}>
                        { renderState() }
                        { renderBar() }
                    </div> :
                    <Button color="dark" onClick={runAnalyze} iconLeft={<IconRun />} label="Run analyze"/>
            }
        </>
    );
};
