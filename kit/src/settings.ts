export const settings = {
    NAME_REGEX: /^[a-z](([-a-z0-9]*[a-z0-9])?)*$/,
    DOMAIN_REGEX: /^https?:\/\/([\w_-]+)(\.\w{2,3})([/\w._-]+)?(:\d{1,4})?$/,
    IP_REGEX: /^\d{1,3}\.\d{1,3}\.\d{1,3}\.\d{1,3}(\/\d{1,3})?$/,
    STATE: {
        PROGRESS: ['In Progress'],
        FAIL: ['Error', 'Failed'],
        SUCCESS: ['Success'],
    },
};
