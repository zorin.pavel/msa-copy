import { useCallback, useState } from 'react';


export const usePagination = <T>(limit: number) => {
    const [currentPage, setCurrentPage] = useState<number>(0);


    const getPaginatedItems = (items: T) => {
        if(items) {
            const startIndex = currentPage * limit;
            const endIndex = startIndex + limit;

            // some sheet with TypeScript
            // @ts-ignore
            return items.slice(startIndex, endIndex);
        } else {
            return [];
        }
    };


    const onPageChange = useCallback(({ selected }: { selected: number }) => {
        setCurrentPage(selected);
    }, [setCurrentPage]);


    return {
        limit,
        onPageChange,
        getPaginatedItems,
        currentPage,
        setCurrentPage,
    };
};
