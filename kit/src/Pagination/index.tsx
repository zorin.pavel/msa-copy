import React from 'react';
import ReactPaginate from 'react-paginate';

import { IconChevronLeft, IconChevronRight } from '@msa/kit';

import css from './pagination.module.scss';

export * from './usePagination';


interface IProps {
    onChangePage: (selectedPage: { selected: number }) => void;
    pageRange?: number,
    limit?: number,
    itemsCount: number,
}

export const Pagination = (props: IProps) => {
    const { onChangePage, pageRange, itemsCount, limit = itemsCount } = props;


    return (
        <ReactPaginate
            breakLabel="..."
            className={css.root}
            nextLabel={<IconChevronRight />}
            previousLabel={<IconChevronLeft />}
            onPageChange={onChangePage}
            pageRangeDisplayed={pageRange}
            pageCount={Math.ceil(itemsCount / limit)}
            renderOnZeroPageCount={null}
            forcePage={0} />
    );
};
