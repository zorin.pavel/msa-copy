import React, {
    useState,
    useEffect,
    InputHTMLAttributes, ChangeEvent, ChangeEventHandler, forwardRef, ForwardedRef
} from 'react';
import classNames from 'classnames';

import { LabelPosition } from '@msa/kit/types';

import css from './textarea.module.scss';


interface Props extends InputHTMLAttributes<HTMLTextAreaElement> {
    disabled?: boolean,
    required?: boolean,
    readonly?: boolean,
    value?: string,
    rows?: number,
    label?: string,
    labelPosition?: keyof typeof LabelPosition,
    onChange?: ChangeEventHandler<HTMLTextAreaElement>,
    onFocus?: ChangeEventHandler<HTMLTextAreaElement>,
    onBlur?: ChangeEventHandler<HTMLTextAreaElement>,
    onInput?: ChangeEventHandler<HTMLTextAreaElement>,
    className?: string,
    error?: string | boolean,
    displayError?: boolean,
    dataTestId?: string,
}


export const Textarea = forwardRef((props: Props, ref: ForwardedRef<HTMLTextAreaElement>) => {
    const {
        name, value, label, labelPosition = LabelPosition.top,
        error, displayError,
        required, disabled, readonly, placeholder, rows,
        onChange, onFocus, onBlur, onInput,
        className, style,
        dataTestId,
        ...rest
    } = props;
    const [currentValue, setValue] = useState(value);
    const [focus, setFocus] = useState(false);

    useEffect(() => {
        setValue(value);
    }, [value]);


    const handleChange = (event: ChangeEvent<HTMLTextAreaElement>) => {
        setValue(event.target.value);

        if(onChange)
            onChange(event);
    };

    const handleFocus = (event: ChangeEvent<HTMLTextAreaElement>) => {
        setFocus(true);

        if(onFocus)
            onFocus(event);
    };

    const handleBlur = (event: ChangeEvent<HTMLTextAreaElement>) => {
        setFocus(false);

        if(onBlur)
            onBlur(event);
    };

    const handleInput = (event: ChangeEvent<HTMLTextAreaElement>) => {
        if(onInput)
            onInput(event);
    };

    return (
        <div
            className={classNames(
                css.textarea,
                (error && css.error),
                (disabled && css.disabled),
                (readonly && css.readonly),
                css.labelPosition,
                css[labelPosition],
                className,
            )}
            style={style} >
            {
                label &&
                    <label htmlFor={name} className={css.label}>
                        {label}
                        {
                            required &&
                            <span className={css.mark}>*</span>
                        }
                    </label>
            }
            <div className={css['errorWrapper']}>
                <div
                    className={classNames(
                        css.wrapper,
                        disabled && css['wrapperDisabled'],
                        readonly && css['wrapperReadonly'],
                        error && css['wrapperError'],
                        focus && css['focus'],
                    )}
                >
                    <textarea
                        id={name}
                        name={name}
                        disabled={disabled}
                        required={required}
                        readOnly={readonly}
                        placeholder={placeholder}
                        value={currentValue}
                        onChange={handleChange}
                        onFocus={handleFocus}
                        onBlur={handleBlur}
                        onInput={handleInput}
                        rows={rows}
                        ref={ref}
                        data-testid={dataTestId}
                        {...rest}
                    />
                </div>

                {
                    displayError && error &&
                    <p className={css.error}>{error}</p>
                }
            </div>
        </div>
    );
});

