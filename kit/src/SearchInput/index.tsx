import React, { ChangeEvent, InputHTMLAttributes, useEffect } from 'react';
import { useDebounce } from 'use-debounce';
import { useTranslation } from 'react-i18next';

import { Input } from '@msa/kit';
interface IProps extends InputHTMLAttributes<HTMLInputElement> {
  onSearch?: any;
  className: string;
  searchTerm: any;
  setSearchTerm: any;
}

export const SearchInput = (props: IProps) => {
    const { onSearch, searchTerm, setSearchTerm, ...rest } = props;
    const { t } = useTranslation();

    const [debounceSearchTerm] = useDebounce(searchTerm, 1000);

    useEffect(() => {
        onSearch(debounceSearchTerm);
    }, [debounceSearchTerm])


    const handleSearch = (event: ChangeEvent<HTMLInputElement>) => {
        setSearchTerm(event.target.value);
    };

    return (
        <Input
            {...rest}
            type="search"
            value={searchTerm}
            onChange={handleSearch}
            placeholder={t('common.search')} />
    );
};
