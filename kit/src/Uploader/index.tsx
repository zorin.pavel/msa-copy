import React, { ChangeEvent, useState } from 'react';
import { useTranslation } from 'react-i18next';

import { Input, Toast } from '@msa/kit';

import css from './uploader.module.scss';


interface IProps {
    onFileUpload: (data: any) => void;
    accept?: string;
    title?: string;
    demandedType?: string;
    unparsed?: boolean;
    unlabeled?: boolean;
}

export const Uploader = (props: IProps) => {
    const { onFileUpload, accept, unparsed, unlabeled, title, demandedType } = props;
    const { t } = useTranslation()

    const [fileName, setFileName] = useState<string | null>(null);


    const handleFileUpload = (event: ChangeEvent<HTMLInputElement>) => {
        const file = event.target.files && event.target.files[0];

        if(file) {
            setFileName(file.name);
            const reader = new FileReader();

            reader.onload = (e: ProgressEvent<FileReader>) => {
                try {
                    const fileContent = e.target?.result as string;
                    let parsedData = null;

                    if(demandedType && demandedType !== file.type) {
                        onFileUpload(null);
                        Toast.error({ message: 'Error uploading file' });

                        throw new Error('Error parsing file');
                    }

                    if(unparsed) {
                        onFileUpload({ filename: file.name, content: file });
                    } else {
                        parsedData = parseFileContent(fileContent);
                        onFileUpload(parsedData);
                    }

                    // const parsedData = !unparsed ? parseFileContent(fileContent) : fileContent;

                    // !unparsed ? onFileUpload(parsedData) : onFileUpload({ filename: file.name, content: parsedData });
                } catch(error) {
                    console.error('Error parsing file:', error);
                    onFileUpload(null);
                    return null;
                }
            };

            reader.readAsText(file);
        }
    };


    const parseFileContent = (fileContent: string) => {
        try {
            if(accept === 'application/json') {
                return JSON.parse(fileContent);
            } else if(accept === 'application/xml') {
                // const result = xml2js(fileContent, options);

                return fileContent;
            } else {
                return fileContent;
            }
        } catch(error) {
            console.error('Error parsing file:', error);
            return null;
        }
    };


    return (
        <div className={css.uploaderContainer}>
            <label className={css.uploaderButton}>
                {title ?? t('buttons.chooseFile')}
                <Input className={css.uploaderInput} type="file" accept={accept} onChange={handleFileUpload} />
            </label>
            {
                fileName &&
                !unlabeled &&
                <span className={css.uploadedName}>{fileName}</span>
            }
        </div>
    );
};
