import React, { useCallback } from 'react';
import { useTranslation } from 'react-i18next';

import classNames from 'classnames';

import { Sizes } from '@msa/kit/types';
import { IconCloseCircle, IconError, IconPause, IconSpinner, IconSuccess, IconTime, IconWarning } from '@msa/kit';

import css from './status.module.scss';


interface IProps {
    status: string,
    action?: string,
    size?: keyof typeof Sizes,
    caption?: boolean,
    className?: string,
}

export const StatusIcon = (props: IProps) => {
    const { status, action, size, caption = true, className } = props;
    const { t } = useTranslation()


    const renderIcon = useCallback(() => {
        switch(status.replace(' ', '_').toUpperCase()) {
            case 'SUCCESS':
            case 'ACTIVE':
            case 'INSTALLED':
            case 'COMPLETED':
            case 'ONLINE':
            case 'SUCCEEDED':
                return <IconSuccess className={classNames(css.icon, className)} size={size} />;
            case 'FAIL':
            case 'FAILED':
            case 'ERROR':
                return <IconError className={classNames(css.icon, className)} size={size} />;
            case 'IN_PROGRESS':
            case 'PROGRESS':
            case 'PROCESS':
            case 'LOADING':
            case 'WAITING':
            case 'RUNNING':
                return <IconSpinner className={classNames(css.icon, 'spin', className)} size={size} />;
            case 'PENDING':
            case 'OMITTED':
                return <IconTime className={classNames(css.icon, className)} size={size} />;
            case 'PAUSED':
                return <IconPause className={classNames(css.icon, className)} size={size} />;
            case 'WARNING':
                return <IconWarning className={classNames(css.icon, className)} size={size} />;
            case 'OFFLINE':
                return <IconError className={classNames(css.icon, className)} size={size} />;
            default:
                return (
                    <>
                        <IconCloseCircle className={classNames(css.icon, className)} size={size} />
                        { caption && 'N/A' }
                    </>
                );
        }
    }, [status, action]);


    return (
        <div className={classNames(css.status, css[status.replace(' ', '').toLowerCase()])}>
            {
                renderIcon()
            }
            {
                caption &&
                <>
                {action ? t(`status.${action.replaceAll(' ', '').toLowerCase()}`) : null}
                {" "} 
                {t(`status.${status.replaceAll(' ', '').toLowerCase()}`)}
                </>
            }
        </div>
    );
};
