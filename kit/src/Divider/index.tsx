import React from 'react';
import classNames from 'classnames';

import css from './devider.module.scss';


interface IProps {
    className?: string
}

export const Divider = ({ className }: IProps) => {
    return <div className={classNames(css.divider, className)} />;
};
