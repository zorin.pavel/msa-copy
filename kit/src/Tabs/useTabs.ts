import { useEffect, useState } from 'react';
import { useLocation, useNavigate } from 'react-router-dom';

import { ITab, TTabKey } from '@msa/kit/types';


interface IOptions {
    useRouter?: boolean
}

export const useTabs = (tabs: ITab[], defaultTabIndex?: TTabKey, options?: IOptions) => {
    const navigate = useNavigate();
    const location = useLocation();

    const [selectedTab, setSelectedTab] = useState<TTabKey>(defaultTabIndex ?? '');

    useEffect(() => {
        const routes = location.pathname.split('/');
        const currentTab = tabs.filter(tab => routes.includes(tab.router as string))[0] ?? tabs[0];

        setSelectedTab(currentTab.index);
    }, [location.pathname]);


    const onTabChange = (tab: ITab) => {
        setSelectedTab(tab.index);

        if(options?.useRouter) {
            tab.router && navigate(tab.router);
        }
    };

    return { selectedTab, onTabChange };
};
