import React, { HTMLAttributes } from 'react';
import classNames from 'classnames';

import { TabList } from './TabList';
import { TabBody } from './TabBody';
import { Tab } from './Tab';

import css from './tabs.module.scss';


interface Props extends HTMLAttributes<HTMLDivElement> {
    value?: number | string,
}

const Tabs = (props: Props) => {
    const {
        className, style,
        ...rest
    } = props;


    return (
        <div {...rest}
            className={classNames(
                css.tabs,
                className,
            )}
            style={style}
        >
            {props.children}
        </div>
    );
};


Tabs.List = TabList;
Tabs.Header = Tab;
Tabs.Body = TabBody;

export { Tabs };
