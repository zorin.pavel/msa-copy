import React, { HTMLAttributes } from 'react';
import classNames from 'classnames';
import css from './tabs.module.scss';


interface Props extends HTMLAttributes<HTMLDivElement> {
    value?: number | string,
    index: number | string,
}

export const TabBody = (props: Props) => {
    const {
        value, index,
        className, style,
        ...rest
    } = props;

    if(index !== value)
        return null;

    return (
        <div {...rest}
            className={classNames(
                css.tabBody,
                (index === value) && css.active,
                className,
            )}
            style={style}
        >
            {props.children}
        </div>
    );
};

export default TabBody;
