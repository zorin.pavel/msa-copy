import React, { HTMLAttributes } from 'react';
import classNames from 'classnames';
import css from './tabs.module.scss';


interface IProps extends HTMLAttributes<HTMLDivElement> {
    index: number | string,
    value?: number | string,
    disabled?: boolean,
}

export const Tab = (props: IProps) => {
    const {
        value, index, disabled,
        onClick,
        className, style,
        ...rest
    } = props;

    return (
        <div {...rest}
            className={classNames(
                css.tab,
                (value === index && css.active),
                (disabled && css.disabled),
                className,
            )}
            style={style}
            onClick={disabled ? undefined : onClick}
        >
            {props.children}
        </div>
    );
};

export default Tab;
