import React, { CSSProperties, MouseEventHandler, ReactElement, ReactNode, useRef } from 'react';
import classNames from 'classnames';

import { useOnClickOutside } from '@msa/kit';
import { Backdrop, Button, IconClose, Portal } from '@msa/kit';

import css from './dialog.module.scss';


interface IProps {
    header?: string,
    backdrop?: boolean,
    open: boolean,
    className?: string,
    style?: CSSProperties,
    buttons?: ReactElement[],
    onClose?: () => void,
    children?: ReactNode,
    actions?: Record<string, Function | void | MouseEventHandler<HTMLButtonElement>>
}

export const Dialog = (props: IProps) => {
    const { header, backdrop = true, open, className, style, onClose, buttons, actions } = props;
    const wrapperRef = useRef<HTMLDivElement>(null);

    // if onButtonClick returns false Dialog wouldn't close
    const handleClose = async (onButtonClick?: () => Promise<boolean | undefined>) => {
        let closeAfter;

        if(typeof onButtonClick === 'function') {
            closeAfter = await onButtonClick();
        }

        if(onClose && closeAfter !== false)
            onClose();
    };

    useOnClickOutside(wrapperRef, handleClose);


    return (
        <>
            {
                open ?
                    <Portal id="dialog-portal">
                        {
                            backdrop && <Backdrop open={open} />
                        }
                        <div className={classNames(
                            css.dialog,
                            className,
                        )} style={style} ref={wrapperRef}>
                            {
                                header !== undefined ?
                                    <div className={css.header}>
                                        <h3 className={css.title}>{header}</h3>
                                        <IconClose onClick={onClose} size="large" className={css.icon} />
                                    </div> :
                                    null
                            }
                            {
                                props.children ?
                                    <div className={css.body}>
                                        {props.children}
                                    </div> :
                                    null
                            }
                            {
                                // мутная тема
                                actions &&
                                <div className={css.actions}>
                                    {
                                        Object.keys(actions).map((action) => {
                                            return (
                                                <>
                                                    <Button onClick={() => actions[action]} view="outlined">{action}</Button>,
                                                    <Button key="delete" onClick={() => actions[action]}>{action}</Button>
                                                </>
                                            );
                                            // return React.cloneElement(
                                            //     button,
                                            //     { key, onClick: () => handleClose(button.props.onClick) }
                                            // );
                                        })
                                    }
                                </div>
                            }
                            {
                                buttons && buttons.length &&
                                <div className={css.actions}>
                                    {
                                        buttons.map((button, key) => {
                                            return React.cloneElement(
                                                button,
                                                { key, onClick: () => handleClose(button.props.onClick) }
                                            );
                                        })
                                    }
                                </div>
                            }
                        </div>
                    </Portal> :
                    null
            }
        </>
    );
};
