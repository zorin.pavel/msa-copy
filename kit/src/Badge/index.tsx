import React, { HTMLAttributes, MouseEventHandler, ReactElement } from 'react';
import classNames from 'classnames';

import { Sizes, Colors } from '@msa/kit/types';
import { IconClose } from '@msa/kit';

import css from './badge.module.scss';


interface IProps extends HTMLAttributes<HTMLElement> {
    label?: string,
    color?: keyof typeof Colors,
    size?: keyof typeof Sizes,
    className?: string,
    onDelete?: MouseEventHandler<HTMLElement>,
    icon?: ReactElement,
    dataTestId?: string,
}

export const Badge = (props: IProps) => {
    const {
        color = Colors.default, size = Sizes.medium,
        label, icon,
        onDelete,
        className, style,
        ...rest
    } = props;


    return (
        <div
            className={classNames(
                css.badge,
                css[size],
                css[color],
                onDelete && css.withAction,
                className,
            )}
            style={style}
            {...rest}>
            {
                icon &&
                React.cloneElement(icon, { className: css.icon })
            }
            <div className={css.message}>{label || props.children}</div>
            {
                onDelete &&
                <IconClose size="xsmall" className={css.action} onClick={onDelete} />
            }
        </div>
    );
};

