import React, {
    useState,
    useEffect,
    ReactElement,
    InputHTMLAttributes, ChangeEvent, ChangeEventHandler, forwardRef, ForwardedRef, useCallback
} from 'react';
import classNames from 'classnames';

import { LabelPosition } from '@msa/kit/types';
import { Helper, IconPreviewClose, IconPreviewOpen } from '@msa/kit';

import css from './input.module.scss';


interface IProps extends Omit<InputHTMLAttributes<HTMLInputElement>, 'value'> {
    disabled?: boolean,
    required?: boolean,
    readonly?: boolean,
    value?: string | number,
    label?: string,
    labelPosition?: keyof typeof LabelPosition,
    onChange?: ChangeEventHandler<HTMLInputElement>,
    onFocus?: ChangeEventHandler<HTMLInputElement>,
    onBlur?: ChangeEventHandler<HTMLInputElement>,
    onInput?: ChangeEventHandler<HTMLInputElement>,
    className?: string,
    error?: string | boolean,
    displayError?: boolean,
    iconLeft?: ReactElement,
    iconRight?: ReactElement,
    addonLeft?: { [label: string]: string },
    addonRight?: { [label: string]: string },
    dataTestId?: string,
    helper?: ReactElement | string,
    // TODO think twice do we need to store value as a local state?
}


export const Input = forwardRef((props: IProps, ref: ForwardedRef<HTMLInputElement>) => {
    const {
        type = 'text', name, label, labelPosition = LabelPosition.top,
        error, displayError,
        required, disabled, readonly, placeholder,
        onChange, onFocus, onBlur, onInput,
        addonLeft, addonRight,
        className, style, helper,
        dataTestId,
        ...restIcons
    } = props;
    let { iconLeft, iconRight, ...rest } = restIcons;
    // value has to be a string
    const value = props.value ? props.value.toString() : '';

    const [currentValue, setValue] = useState<string>(value);
    const [focus, setFocus] = useState<boolean>(false);
    const [showPass, setShowPass] = useState<boolean>(false);

    if(type === 'password') {
        iconRight =
            showPass ?
                <IconPreviewOpen onClick={() => setShowPass(s => !s)} className={css.iconRight} /> :
                <IconPreviewClose onClick={() => setShowPass(s => !s)} className={css.iconRight} />;
    }


    useEffect(() => {
        if(type === 'date' && value !== '') {
            const date = new Date(value as string) ;

            setValue(date.getFullYear() + '-' + (date.getMonth() + 1).toString().padStart(2, '0') + '-' + date.getDate().toString().padStart(2, '0'));
        } else
            setValue(value as string);
    }, [value]);


    const handleChange = useCallback((event: ChangeEvent<HTMLInputElement>) => {
        if(type === 'date' && event.target.value !== '') {
            const date = new Date(event.target.value);

            setValue(date.getFullYear() + '-' + (date.getMonth() + 1).toString().padStart(2, '0') + '-' + date.getDate().toString().padStart(2, '0'));
        } else {
            setValue(event.target.value);
        }

        if(onChange)
            onChange(event);
    }, [onChange]);


    const handleFocus = useCallback((event: ChangeEvent<HTMLInputElement>) => {
        setFocus(true);

        if(onFocus)
            onFocus(event);
    }, [onFocus]);


    const handleBlur = useCallback((event: ChangeEvent<HTMLInputElement>) => {
        setFocus(false);

        if(onBlur)
            onBlur(event);
    }, [onBlur]);


    const handleInput = useCallback((event: ChangeEvent<HTMLInputElement>) => {
        if(type === 'number') {
            event.target.value = event.target.value.replace(/\D/g, '');
        }

        if(onInput) {
            onInput(event);
        }
    }, [onInput]);


    return (
        <div
            className={classNames(
                css[type],
                (error && css.error),
                (disabled && css.disabled),
                (readonly && css.readonly),
                css.labelPosition,
                css[labelPosition],
                className,
            )}
            style={style} >
            {
                label &&
                    <label htmlFor={name} className={css.label}>
                        {label}
                        {
                            required &&
                            <span className={css.mark}>*</span>
                        }
                        {
                            helper &&
                            (typeof helper === 'string' ?
                                <Helper description={helper} /> :
                                React.cloneElement(helper, { name: '', className: classNames(helper.props.className, css.helper) }))
                        }
                    </label>
            }
            <div className={css['errorWrapper']}>
                <div className={css['addonWrapper']}>
                    {
                        addonLeft &&
                        <div className={classNames(css.addon, css.left)}>{addonLeft.label}</div>
                    }
                    <div
                        className={classNames(
                            css.wrapper,
                            disabled && css['wrapperDisabled'],
                            readonly && css['wrapperReadonly'],
                            error && css['wrapperError'],
                            addonLeft && css['wrapperAddonLeft'],
                            addonRight && css['wrapperAddonRight'],
                            focus && css['focus'],
                        )}>
                        {
                            iconLeft &&
                            React.cloneElement(iconLeft, { className: classNames(iconLeft.props.className, css['iconLeft']) })
                        }
                        <input
                            type={showPass ? 'text' : type}
                            id={name}
                            name={name}
                            disabled={disabled}
                            required={required}
                            readOnly={readonly}
                            placeholder={placeholder}
                            value={currentValue}
                            onChange={handleChange}
                            onFocus={handleFocus}
                            onBlur={handleBlur}
                            onInput={handleInput}
                            data-testid={dataTestId}
                            ref={ref}
                            {...rest} />
                        {
                            iconRight &&
                            React.cloneElement(iconRight, { className: classNames(iconRight.props.className, css['iconRight']) })
                        }
                    </div>
                    {
                        addonRight &&
                        <div className={classNames(css.addon, css.right)}>{addonRight.label}</div>
                    }
                </div>
                {
                    displayError && error &&
                    <p className={css.error}>{error}</p>
                }
            </div>
        </div>
    );
});

