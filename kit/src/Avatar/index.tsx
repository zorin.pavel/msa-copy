import React, { HTMLAttributes, useCallback } from 'react';
import Avvvatars from 'avvvatars-react'

import css from './avatar.module.scss';
import iconsSvg from './icons/icons.svg';

const icons = ['', 'bear', 'elephant', 'gorilla', 'leopard', 'rhino', 'tiger', 'tuna', 'turtle', 'while'];


interface IProps extends HTMLAttributes<HTMLElement> {
    name: string,
    className?: string,
}

export const Avatar = (props: IProps) => {
    const { name } = props;


    const _findIcon = useCallback((name: string) => {
        let number = name.charCodeAt(0) + name.charCodeAt(name.length - 1) +  + name.charCodeAt(Math.round(name.length/2));

        while(number >= icons.length) {
            number = number % 10 + Math.round(number/10);
        }

        return icons[number]
    }, []);


    return (
        <Avvvatars value={name} style="shape" size={64} borderSize={2} border borderColor="#F2F0F7" />

        // <div className={css.placeholder}>
        //     <div className={classNames(css.circle, css[`color-${name.charAt(0).toUpperCase()}`])} {...rest}>
        //         <Icon name={findIcon(name)} />
        //     </div>
        // </div>
    );
};


const _Icon = ({ name }: { name: string }) => (
    <svg className={css.icon}>
        <use xlinkHref={`${iconsSvg}#icon-${name}`} />
    </svg>
);
