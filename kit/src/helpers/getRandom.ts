export const getRandom = (min: number, max: number, d?: number) => {
    if(d) {
        min = min * d * 10;
        max = max * d * 10;
    }

    let random = Math.floor(Math.random() * (max - min) + min);

    if(d)
        random = random / (10 * d);

    return random;
};
