import { PlaceholderDataFunction, useQuery } from '@tanstack/react-query';

import type { IError } from '@msa/kit/types';


type TCallBack<T, E> = (_entity?: E) => Promise<T>;
type TPlaceholderData<T, D extends keyof T> = PlaceholderDataFunction<Pick<T, D>>;
type TEntity<T> = Partial<T>;


export const useQuerySelector = <T, E extends TEntity<E> = any>(callBack: TCallBack<T, E>, namespace: string[], entity?: E) => {
    return useQuery<T, IError, T>(
        [...namespace, entity],
        () => callBack(entity),
        {
            enabled: !!entity,
            placeholderData: { name: '...' } as TPlaceholderData<T, any>
        });
};
