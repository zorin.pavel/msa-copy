import { generatePath, Params } from 'react-router-dom';

import { IRoute } from '@msa/host/types';


// генерим реальные пути вида /platforms/<name>/clusters/<name>/nodes
export const preparePath = (children: IRoute[], params: Readonly<Params<string>>,  parentPath?: string, parentRoute?: IRoute ) => {

    return children
        .map((route: IRoute) => {
            if(!route.handle)
                route.handle = {};

            let handlePath = route.path;

            if(route.path) {
                const slugs = [...route.path.matchAll(/:(\w+)/g)];

                if(slugs.some(slug => params[slug[1]])) {
                    handlePath = generatePath(route.path, params as { [x: string]: string | null; });
                }
            }

            route.handle.path =
                route.index ?
                    parentPath :
                    parentPath ?
                        `${parentPath}${(parentPath.endsWith('/') ? '' : '/')}${handlePath}` :
                        `${handlePath}`;

            // nested permissions
            if(route.handle.permission === undefined)
                route.handle.permission = parentRoute?.handle?.permission;

            // nested layout
            if(route.handle.layout === undefined)
                route.handle.layout = parentRoute?.handle?.layout;

            route.children = route.children && preparePath(route.children,params,  route.handle?.path, route);

            return route;
        });
};
