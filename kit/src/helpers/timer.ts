export const timer = <R>(data: R | null, idleTime: number, requestName?: string) => {
    console.log('Run request:', idleTime, requestName, data);

    return new Promise<R>((resolve, reject) => {
        setTimeout(() => {
            data ?
                resolve(data) :
                reject({ status: 404, message: 'GET Request error should be here' });
        }, idleTime);
    });
};
