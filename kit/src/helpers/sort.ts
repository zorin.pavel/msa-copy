import { OrderDirection } from '@msa/kit/types';


/*
    data = [
        {name, scope: { amount }}
    ]

    sort(data, 'name', false);
    sort(data, 'scope.amount', true);
*/


export const sort = <T extends any[]>(data: T, fieldName: string | string[], orderDirection: keyof typeof OrderDirection = OrderDirection.asc) => {
    if(Array.isArray(fieldName)) {
        return data.slice().sort((a: any, b: any) => {
            let stringA = '';
            let stringB = '';

            for(const f of fieldName) {
                stringA += ` ${a[f]}`;
                stringB += ` ${b[f]}`;
            }

            return orderDirection === OrderDirection.desc ? -stringA.localeCompare(stringB, ['en', 'ru']) : stringA.localeCompare(stringB, ['en', 'ru']);
        });
    }

    const [scope, value] = fieldName.split('.');

    return data.slice().sort((a: any, b: any) => {
        if (a[scope] === null || b[scope] === null)
            return a[scope] === b[scope] ? 0 : (a[scope] === null ? (orderDirection === OrderDirection.asc ? 1 : -1) : (orderDirection === OrderDirection.asc ? -1 : 1));

        if(typeof a[scope] === 'boolean' || !a[scope])
            return orderDirection === OrderDirection.asc ? (a[scope] ? 1 : -1) : (a[scope] ? -1 : 1);

        if(typeof a[scope] === 'object') {
            if(typeof a[scope][value] === 'number')
                return orderDirection === OrderDirection.asc ? a[scope][value] > b[scope][value] ? 1 : -1 : a[scope][value] < b[scope][value] ? 1 : -1;

            return orderDirection === OrderDirection.desc ? -a[scope][value].localeCompare(b[scope][value], ['en', 'ru']) : a[scope][value].localeCompare(b[scope][value], ['en', 'ru']);
        }

        if(typeof a[fieldName] === 'number')
            return orderDirection === OrderDirection.asc ? a[fieldName] > b[fieldName] ? 1 : -1 : a[fieldName] < b[fieldName] ? 1 : -1;

        return orderDirection === OrderDirection.desc ? -a[fieldName].localeCompare(b[fieldName], ['en', 'ru']) : a[fieldName].localeCompare(b[fieldName], ['en', 'ru']);
    });
};
