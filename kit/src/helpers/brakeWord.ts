export const brakeWord = (string: string | undefined, max: number, after = '...') => {
    if(!string)
        return '';

    const wordArr = string.split(' ');
    let currentStr = '';

    for(const word of wordArr) {
        if(currentStr.length + word.length > max) {
            return currentStr += after;
        }

        currentStr += word + ' ';
    }

    return currentStr;
};
