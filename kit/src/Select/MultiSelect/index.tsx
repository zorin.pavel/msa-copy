import React, { ChangeEvent, ForwardedRef, forwardRef, MouseEventHandler, useCallback, useEffect, useRef, useState } from 'react';
import classNames from 'classnames';

import { useOnClickOutside } from '@msa/kit';
import { IEventTarget, IOption, LabelPosition, MultiSelectProps } from '@msa/kit/types';
import { DropdownToggle } from '../DropdownToggle';
import { SelectedBadges } from '../SelectedBadges';
import { SelectorLabel } from '../SelectorLabel';
import { getSelectedValues } from '../helpers/getSelectedValues';
import { convertInputValue } from '../helpers/convertInputValue';

import css from './multi-select.module.scss';


export const MultiSelect: React.ForwardRefExoticComponent<MultiSelectProps & React.RefAttributes<HTMLInputElement>> = forwardRef(
    (
        {
            name,
            label,
            labelPosition = LabelPosition.top,
            error,
            displayError,
            required,
            disabled,
            readonly,
            value,
            placeholder,
            onFocus,
            onBlur,
            onChange,
            defaultLabel,
            maxHeight,
            className,
            style,
            options,
            loading,
            dataTestId,
            addonRight,
            dropUp,
            maxBadges
        }: MultiSelectProps,
        _ref: ForwardedRef<HTMLInputElement>
    ): JSX.Element => {
        const [internalValue, setInternalValue] = useState<(string | number)[]>([]);
        const [internalOptions, setInternalOptions] = useState<IOption[]>([]);
        const [open, setOpen] = useState(false);
        const [focus, setFocus] = useState<boolean>(false);
        const [listFocus, setListFocus] = useState(false);
        const valueRef: React.RefObject<HTMLInputElement> =
            useRef<HTMLInputElement>(null);
        const labelRef: React.RefObject<HTMLInputElement> =
            useRef<HTMLInputElement>(null);
        const listRef: React.RefObject<HTMLUListElement> =
            useRef<HTMLUListElement>(null);

        useEffect((): void => {
            setInternalValue(convertInputValue(value));
        }, [value]);

        useEffect((): void => {
            setInternalOptions(
            options
                    .slice()
                    .sort((a: IOption, b: IOption): 1 | -1 =>
                        a.label > b.label ? 1 : -1
                )
            );
        }, [options]);

        const handleFocus: (event: ChangeEvent<HTMLInputElement>) => void =
            useCallback(
                (event: ChangeEvent<HTMLInputElement>): void => {
                    if(readonly || disabled) {
                        return;
                    }
                    setFocus(true);
                    labelRef.current?.focus();
                    setOpen(true);
                    if(onFocus) {
                        onFocus(event);
                    }
                },
                [disabled, onFocus, readonly]
            );

        const handleBlur: (event: ChangeEvent<HTMLInputElement>) => void =
            useCallback(
                (event: ChangeEvent<HTMLInputElement>): void => {
                    setFocus(false);
                    if(onBlur) {
                        onBlur(event);
                    }
                },
                [onBlur]
            );

        const updateValue: (newValue: (string | number)[]) => void = (
            newValue: (string | number)[]
        ): void => {
            if(valueRef.current) {
                setInternalValue(newValue);
                valueRef.current.value = newValue.join();
                const event = new Event('input', { bubbles: true });
                valueRef.current.dispatchEvent(event);
            }
        };

        const handleSetValue: (option: IOption) => void = useCallback(
            (option: IOption): void => {
                setListFocus(true);
                if(option.disabled) {
                    return;
                }
                updateValue(getSelectedValues(option.value, internalValue));
            },
            [internalValue]
        );

        const handleDelete: (
            optionValue: string | number
        ) => MouseEventHandler<HTMLElement> = (
            optionValue: string | number
        ): MouseEventHandler<HTMLElement> => {
            return (): void => {
                const newValue: (string | number)[] = internalValue.filter(
                    (itemValue: string | number): boolean => String(itemValue) !== String(optionValue)
                );
                updateValue(newValue);
            };
        };

        const handleChange: (
            event: ChangeEvent<HTMLInputElement> | IEventTarget
        ) => void = (event: ChangeEvent<HTMLInputElement> | IEventTarget): void => {
            if(onChange) {
                onChange(event as ChangeEvent<HTMLInputElement>);
            }
        };

        useOnClickOutside(listRef, (): void => {
            setListFocus(false);
        });

        useEffect((): void => {
            if (!listFocus && !focus) {
                setOpen(false);
            }
        }, [focus, listFocus]);

        return (
            <div className={classNames(
                css.multiSelect,
                error && css['error'],
                disabled && css['disabled'],
                readonly && css['readonly'],
                css.labelPosition,
                css[labelPosition],
                className
            )} style={style}>
                <SelectorLabel label={label} name={name} required={required} />
                <div className={css.errorWrapper}>
                    <div className={css.addonWrapper}>
                        <div className={classNames(
                            css.wrapper,
                            disabled && css.wrapperDisabled,
                            readonly && css.wrapperReadonly,
                            error && css.wrapperError,
                            focus && css.focus
                        )}>
                            <input id={name} readOnly type="hidden" name={name} value={internalValue.join()} disabled={disabled} data-testid={dataTestId} ref={valueRef} onInput={(e: React.FormEvent<HTMLInputElement>): void =>
                                handleChange(e as ChangeEvent<HTMLInputElement>)
                            } />
                            <div className={css.subWrapper}>
                                <label htmlFor={name + '-label'}></label>
                                <SelectedBadges values={internalValue} maxBadges={maxBadges} disabled={disabled} handleDelete={handleDelete} readonly={readonly} />
                                <input id={name + '-label'} name={name + '-label'} disabled={disabled} required={required} readOnly placeholder={placeholder} onFocus={handleFocus} onBlur={handleBlur} value={defaultLabel || '...'} size={1} type="text" autoComplete="off" ref={labelRef} />
                            </div>
                            <DropdownToggle loading={loading} open={open} handleBlur={handleBlur} handleFocus={handleFocus} error={error} disabled={disabled} readonly={readonly} />
                            <ul className={classNames(
                                css.options,
                                open && css.open,
                                dropUp ? css.up : css.down,
                                maxHeight && css[`height${maxHeight}`]
                            )} ref={listRef}>
                                {internalOptions.map(
                                    (option: IOption): JSX.Element => (
                                        <li key={option.value} className={classNames(
                                            css.option,
                                            option.disabled && css.disabled,
                                            internalValue.some(
                                                (itemValue: string | number): boolean =>
                                                    String(itemValue) === String(option.value)
                                            ) && css.active
                                        )} onMouseDown={(): void => {
                                            handleSetValue(option);
                                        }}>
                                            {option.label}
                                        </li>
                                    )
                                )}
                            </ul>
                        </div>
                        {
                            addonRight &&
                            React.cloneElement(addonRight, {
                                className: classNames(
                                    addonRight.props.className,
                                    css['addonRight']
                                )
                            })
                        }
                    </div>
                    {displayError && error && <p className={css.error}>{error}</p>}
                </div>
            </div>
        );
    }
);
