import { SelectProps } from '@msa/kit/types';

import css from './selector-label.module.scss';

interface SelectorLabelProps extends Pick<SelectProps, 'label' | 'name' | 'required'> {
}

export const SelectorLabel = (props: SelectorLabelProps) => {
    const { label, name, required } = props;

    return (
        <>
            {
                label &&
                <label htmlFor={name} className={css.label}>
                    {label}
                    {required && <span className={css.mark}>*</span>}
                </label>
            }
        </>
    );
};
