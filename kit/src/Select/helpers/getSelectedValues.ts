import { IOption } from '@msa/kit/types';

type IOptionValue = IOption['value'];

export const getSelectedValues: (
  value: IOptionValue,
  values: IOptionValue[]
) => IOptionValue[] = (
  value: IOptionValue,
  values: IOptionValue[]
): IOptionValue[] =>
  values.some((item: IOptionValue): boolean => String(item) === String(value))
    ? values.filter(
        (item: IOptionValue): boolean => String(item) !== String(value)
      )
    : values.concat(value);
