import classNames from 'classnames';
import { ChangeEvent } from 'react';

import { IconChevronDown, IconChevronUp, IconSpinner } from '@msa/kit';
import { SelectProps } from '@msa/kit/types';

import css from './dropdown-toggle.module.scss';

interface DropdownToggleProps extends Pick<SelectProps, 'error' | 'disabled' | 'readonly' | 'loading'> {
    open: boolean;
    handleBlur: (event: ChangeEvent<HTMLInputElement>) => void;
    handleFocus: (event: ChangeEvent<HTMLInputElement>) => void;
}

export const DropdownToggle = (props: DropdownToggleProps) => {
    const { error, disabled, readonly, loading, open, handleBlur, handleFocus } = props;

    return (
        <div className={classNames(
            error && css['error'],
            disabled && css['disabled'],
            readonly && css['readonly']
        )}>
            {
                loading ?
                    <IconSpinner className={classNames('spin', css.chevron)} button /> :
                    open ?
                        <IconChevronUp className={css.chevron} onClick={(): void => handleBlur({} as ChangeEvent<HTMLInputElement>)} /> :
                        <IconChevronDown className={css.chevron} onClick={(): void => handleFocus({} as ChangeEvent<HTMLInputElement>)} />
            }
        </div>
    );
};
