import { useOnClickOutside } from '@msa/kit';
import { AutocompleteProps, IEventTarget, IOption, LabelPosition } from '@msa/kit/types';
import classNames from 'classnames';
import List from 'rc-virtual-list';
import React, {
    ChangeEvent,
    ForwardedRef,
    forwardRef,
    MouseEventHandler,
    useCallback,
    useEffect,
    useRef,
    useState
} from 'react';
import { useDebounce } from 'use-debounce';
import { DropdownToggle } from '../DropdownToggle';
import { SelectedBadges } from '../SelectedBadges';
import { SelectorLabel } from '../SelectorLabel';
import { convertInputValue } from '../helpers/convertInputValue';
import { getSelectedValues } from '../helpers/getSelectedValues';

import css from './autocomplete.module.scss';

export const Autocomplete: React.ForwardRefExoticComponent<AutocompleteProps & React.RefAttributes<HTMLInputElement>> = forwardRef(
    (
        {
            name,
            label,
            labelPosition = LabelPosition.top,
            error,
            displayError,
            required,
            disabled,
            readonly,
            value,
            placeholder,
            onFocus,
            onBlur,
            onChange,
            className,
            style,
            options,
            loading,
            dataTestId,
            addonRight,
            dropUp,
            maxBadges,
            searchOptions,
            filterOutSelectedOptions
        }: AutocompleteProps,
        _ref: ForwardedRef<HTMLInputElement>
    ) => {
        const [internalValue, setInternalValue] = useState<(string | number)[]>([]);
        const [searchValue, setSearchValue] = useState('');
        const [debouncedSearchValue] = useDebounce(searchValue, 500);
        const [sortedOptions, setSortedOptions] = useState<IOption[]>([]);
        const [internalOptions, setInternalOptions] = useState<IOption[]>([]);
        const [filteredOptions, setFilteredOptions] = useState<IOption[]>([]);
        const [open, setOpen] = useState(false);
        const [focus, setFocus] = useState(false);
        const [listFocus, setListFocus] = useState(false);
        const valueRef: React.RefObject<HTMLInputElement> = useRef<HTMLInputElement>(null);
        const labelRef: React.RefObject<HTMLInputElement> = useRef<HTMLInputElement>(null);
        const listRef: React.RefObject<HTMLDivElement> = useRef<HTMLDivElement>(null);
        const listItemHeight = 35.5;
        const noDataOption: IOption[] = [
            { label: 'No data', disabled: true, value: '' }
        ];

        useEffect((): void => {
            setInternalValue(convertInputValue(value));
        }, [value]);

        useEffect((): void => {
            setSortedOptions(
                options
                    .slice()
                    .sort((a: IOption, b: IOption): 1 | -1 =>
                        a.label > b.label ? 1 : -1
                    )
            );
        }, [options]);

        useEffect((): void => {
            if(filterOutSelectedOptions) {
                setInternalOptions(
                    sortedOptions.filter(
                        (sortedOption: IOption): boolean =>
                            !internalValue.some(
                                (item: IOption['value']): boolean =>
                                    String(item) === String(sortedOption.value)
                            )
                    )
                );
            } else {
                setInternalOptions(sortedOptions);
            }
        }, [filterOutSelectedOptions, internalValue, sortedOptions]);

        useEffect((): void => {
            if(!listFocus && !focus) {
                setOpen(false);
            }
        }, [focus, listFocus]);

        const handleFocus: (event: ChangeEvent<HTMLInputElement>) => void =
            useCallback(
                (event: ChangeEvent<HTMLInputElement>): void => {
                    if(readonly || disabled) {
                        return;
                    }
                    setFocus(true);
                    labelRef.current?.focus();
                    setOpen(true);
                    if(onFocus) {
                        onFocus(event);
                    }
                },
                [disabled, onFocus, readonly]
            );

        const handleBlur: (event: ChangeEvent<HTMLInputElement>) => void =
            useCallback(
                (event: ChangeEvent<HTMLInputElement>): void => {
                    setFocus(false);
                    if(onBlur) {
                        onBlur(event);
                    }
                },
                [onBlur]
            );

        const updateValue = (newValue: (string | number)[]) => {
            if(valueRef.current) {
                setInternalValue(newValue);
                valueRef.current.value = newValue.join();
                const event = new Event('input', { bubbles: true });
                valueRef.current.dispatchEvent(event);
            }
        };

        const handleSetValue: (option: IOption) => void = useCallback(
            (option: IOption): void => {
                setListFocus(true);
                if(option.disabled) {
                    return;
                }
                updateValue(getSelectedValues(option.value, internalValue));
            },
            [internalValue]
        );

        const handleDelete: (
            optionValue: string | number
        ) => MouseEventHandler<HTMLElement> = (
            optionValue: string | number
        ): MouseEventHandler<HTMLElement> => {
            return (): void => {
                const newValue: (string | number)[] = internalValue.filter(
                    (itemValue: string | number): boolean =>
                        String(itemValue) !== String(optionValue)
                );
                updateValue(newValue);
            };
        };

        const handleChange: (
            event: ChangeEvent<HTMLInputElement> | IEventTarget
        ) => void = (event: ChangeEvent<HTMLInputElement> | IEventTarget): void => {
            if(onChange) {
                onChange(event as ChangeEvent<HTMLInputElement>);
            }
        };

        const handleSearch: (event: ChangeEvent<HTMLInputElement>) => void = ({
                                                                                  target: { value }
                                                                              }: ChangeEvent<HTMLInputElement>): void => {
            if(searchValue !== value) {
                setSearchValue(value);
            }
        };

        useEffect((): void => {
            if(searchOptions) {
                searchOptions(debouncedSearchValue);
            }
        }, [debouncedSearchValue, searchOptions]);

        useEffect((): void => {
            setFilteredOptions(
                searchOptions
                    ? internalOptions
                    : internalOptions.filter((option: IOption): boolean =>
                        String(option.value)
                            .toLowerCase()
                            .includes(debouncedSearchValue.toLowerCase())
                    )
            );
        }, [internalOptions, searchOptions, debouncedSearchValue]);

        useOnClickOutside(listRef, ({ target }: Event): void => {
            setListFocus(false);
            const { current } = labelRef;
            if(
                current &&
                (target instanceof Node || target === null) &&
                !current.contains(target)
            ) {
                setSearchValue('');
            }
        });

        return (
            <div className={classNames(
                css.autocomplete,
                error && css['error'],
                disabled && css['disabled'],
                readonly && css['readonly'],
                css.labelPosition,
                css[labelPosition],
                className
            )} style={style}>
                <SelectorLabel label={label} name={name} required={required} />
                <div className={css.errorWrapper}>
                    {
                        internalValue.length > 0 &&
                        <div className={css.badges}>
                            <SelectedBadges values={internalValue} maxBadges={maxBadges} disabled={disabled} handleDelete={handleDelete} readonly={readonly} />
                        </div>
                    }
                    <div className={css.addonWrapper}>
                        <div className={classNames(
                            css.wrapper,
                            disabled && css.wrapperDisabled,
                            readonly && css.wrapperReadonly,
                            error && css.wrapperError,
                            focus && css.focus
                        )}>
                            <input id={name} readOnly type="hidden" name={name} value={internalValue.join()} disabled={disabled} data-testid={dataTestId} ref={valueRef} onInput={(e: React.FormEvent<HTMLInputElement>): void =>
                                handleChange(e as ChangeEvent<HTMLInputElement>)
                            } />
                            <input id={name + '-label'} name={name + '-label'} readOnly={readonly} disabled={disabled} required={required} placeholder={placeholder} onFocus={handleFocus} onBlur={handleBlur} onInput={handleSearch} value={searchValue} type="text" autoComplete="off" ref={labelRef} />
                            <DropdownToggle loading={loading} open={open} handleBlur={handleBlur} handleFocus={handleFocus} error={error} disabled={disabled} readonly={readonly} />
                            <div className={classNames(
                                css.options,
                                dropUp ? css.up : css.down,
                                open && css.open
                            )} ref={listRef}>
                                <List data={
                                    filteredOptions.length > 0 ? filteredOptions : noDataOption
                                } height={
                                    filteredOptions.length > 4
                                        ? listItemHeight * 4
                                        : filteredOptions.length > 1
                                            ? listItemHeight * filteredOptions.length
                                            : listItemHeight
                                } itemHeight={listItemHeight} itemKey="value">
                                    {
                                        (option: IOption) => (
                                            <div
                                                className={classNames(
                                                    css.option,
                                                    option.disabled && css.disabled,
                                                    !filterOutSelectedOptions && internalValue.some((itemValue: string | number) => String(itemValue) === String(option.value)) && css.active
                                                )}
                                                onMouseDown={() => {
                                                    handleSetValue(option);
                                                }}>
                                                {option.label}
                                            </div>
                                        )
                                    }
                                </List>
                            </div>
                        </div>
                        {addonRight &&
                            React.cloneElement(addonRight, {
                                className: classNames(
                                    addonRight.props.className,
                                    css['addonRight']
                                )
                            })}
                    </div>
                    {displayError && error && <p className={css.error}>{error}</p>}
                </div>
            </div>
        );
    }
);
