import React, {
    ChangeEvent,
    ForwardedRef,
    forwardRef,
    useCallback,
    useEffect,
    useRef,
    useState
} from 'react';
import classNames from 'classnames';

import { IEventTarget, IOption, LabelPosition, SelectProps } from '@msa/kit/types';
import { DropdownToggle } from './DropdownToggle';
import { SelectorLabel } from './SelectorLabel';

import css from './select.module.scss';


export const Select = forwardRef((props: SelectProps, _ref: ForwardedRef<HTMLInputElement>) => {
    const {
        name, label, labelPosition = LabelPosition.top,
        error, displayError,
        required, disabled, readonly, placeholder,
        onFocus, onBlur, onChange,
        defaultLabel, empty, maxHeight,
        className, style,
        options, loading,
        dataTestId,
        addonRight,
        dropUp
    } = props;
    // value has to be a string
    const value = props.value ? props.value.toString() : '';

    // const [currentValue, setValue] = useState<string>(value);
    const [currentLabel, setLabel] = useState<SelectProps['defaultLabel']>(defaultLabel ?? '');
    const [open, setOpen] = useState<boolean>(false);
    const [focus, setFocus] = useState<boolean>(false);
    const [currentOptions, setOptions] = useState<IOption[]>([]);

    const labelRef = useRef<HTMLInputElement>(null);
    const valueRef = useRef<HTMLInputElement>(null);


    useEffect(() => {
        // setValue(value);
        handleSetLabel(value);
    }, [value]);

    // useEffect(() => {
    //     handleSetLabel(currentValue);
    //
    //     // это нужно чтобы форма получила новое значение
    //     if(selectRef.current) {
    //         const event = new Event('input', { bubbles: true });
    //
    //         selectRef.current.dispatchEvent(event);
    //     }
    // }, [currentValue]);

    useEffect(() => {
        // doesn't work
        if(empty && !required) {
            options
                .filter(option => option.value === '')
                .unshift({ label: '...', value: '' });
        }

        setOptions(
            options
                // .slice()
                // .sort((a: IOption, b: IOption): 1 | -1 =>
                //     a.label > b.label ? 1 : -1
                // )
        );
    }, [options, empty, required]);


    useEffect(() => {
        handleSetLabel();
    }, [currentOptions]);


    const handleSetValue = useCallback((option: IOption) => {
        // это нужно чтобы форма получила новое значение
        if(option.disabled)
            return;

        if(valueRef.current) {
            valueRef.current.value = option.value.toString();

            const event = new Event('input', { bubbles: true });

            valueRef.current.dispatchEvent(event);
        }
        // setValue(option.value as string);
        // setLabel(option.label);
    }, []);


    const handleSetLabel = useCallback((optionValue: string = value) => {
        const option = currentOptions.filter(option => (option.value.toString() === optionValue));

        if(option[0])
            setLabel(option[0].label);
        else
            setLabel(defaultLabel ?? optionValue);
    }, [currentOptions, value]);


    const handleChange = (event: ChangeEvent<HTMLInputElement> | IEventTarget) => {
        // setValue(event.target.value as typeof value);

        if(onChange)
            onChange(event as ChangeEvent<HTMLInputElement>);
    };


    const handleClose = useCallback(() => {
        setOpen(false);
    }, []);


    const handleFocus = useCallback((event: ChangeEvent<HTMLInputElement>) => {
        if(readonly || disabled)
            return;

        setFocus(true);
        labelRef.current?.focus();

        setOpen(true);

        if(onFocus)
            onFocus(event);
    }, [disabled, readonly, onFocus]);


    const handleBlur = useCallback((event: ChangeEvent<HTMLInputElement>) => {
        setFocus(false);

        handleClose();

        if(onBlur)
            onBlur(event);
    }, [handleClose, onBlur]);


    return (
        <div className={classNames(
            css.select,
            (error && css['error']),
            (disabled && css['disabled']),
            (readonly && css['readonly']),
            css.labelPosition,
            css[labelPosition],
            className,
        )} style={style}>
            <SelectorLabel label={label} name={name} required={required} />
            <div className={css.errorWrapper}>
                <div className={css.addonWrapper}>
                    <div className={classNames(
                        css.wrapper,
                        (disabled && css.wrapperDisabled),
                        (readonly && css.wrapperReadonly),
                        (error && css.wrapperError),
                        (focus && css.focus)
                    )}>
                        <input id={name} readOnly type="hidden" name={name} value={value} disabled={disabled} onInput={(e) => handleChange(e as ChangeEvent<HTMLInputElement>)} data-testid={dataTestId} ref={valueRef} />
                        <input id={name + '-label'} name={name + '-label'} disabled={disabled} required={required} readOnly placeholder={placeholder} onFocus={handleFocus} onBlur={handleBlur} value={currentLabel} type="text" autoComplete="off" ref={labelRef} />
                        <DropdownToggle loading={loading} open={open} handleBlur={handleBlur} handleFocus={handleFocus} error={error} disabled={disabled} readonly={readonly} />
                        <ul className={classNames(
                            css.options,
                            (open && css.open),
                            (dropUp ? css.up : css.down),
                            (maxHeight && css[`height${maxHeight}`])
                        )}>
                            {
                                currentOptions.map((option) => {
                                    return <li
                                        key={option.value}
                                        className={classNames(css.option, option.disabled && css.disabled, String(option.value) === value && css.active, option.group && css.group, !option.group && css.inGroup)}
                                        onMouseDown={() => handleSetValue(option)}>{option.label}</li>
                                })
                            }
                        </ul>
                    </div>
                    {
                        addonRight &&
                        React.cloneElement(addonRight, { className: classNames(addonRight.props.className, css['addonRight']) })
                    }
                </div>
                {
                    displayError && error &&
                    <p className={css.error}>{error}</p>
                }
            </div>
        </div>
    );
});
