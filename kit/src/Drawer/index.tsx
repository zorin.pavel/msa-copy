import React, { useState, useEffect, ReactNode } from 'react';
import classNames from 'classnames';

import { Backdrop, Portal, IconClose } from '@msa/kit';

import { DrawerHeader } from './DrawerHeader';
import { DrawerBody } from './DrawerBody';
import { DrawerFooter } from './DrawerFooter';

import css from './drawer.module.scss';


interface Props {
    id?: string,
    header?: string | ReactNode,
    backdrop?: boolean,
    open?: boolean,
    wide?: boolean,
    narrow?: boolean,
    onClose?: () => void,
    className?: string,
    children?: ReactNode,
}

const Drawer = (props: Props) => {
    const {
        id = 'drawer-portal',
        header, backdrop, open, wide, narrow,
        onClose,
        className,
        ...rest
    } = props;

    const [animationOpen, setAnimationOpen] = useState<boolean>(true);
    const [animationClose, setAnimationClose] = useState<boolean>(false);

    useEffect(() => {
        if(open) {
            setAnimationOpen(false);
            setAnimationClose(true);
        } else {
            setAnimationOpen(true);

            setTimeout(() => {
                setAnimationClose(false);

                if(onClose)
                    onClose();
            }, 300);
        }
    }, [open]);


    const handleClose = () => {
        setAnimationOpen(true);

        setTimeout(() => {
            setAnimationClose(false);

            if(onClose)
                onClose();
        }, 300);
    };


    return (
        <>
            {
                (open || animationClose) &&
                <Portal id={id}>
                    {
                        backdrop && <Backdrop onClick={handleClose} open={(!animationOpen && animationClose)} />
                    }
                    <div className={classNames(
                        css.drawer,
                        (!animationOpen && animationClose) && css.open,
                        (wide && css.wide),
                        (narrow && css.narrow),
                        className,
                    )} {...rest}>
                        {
                            header &&
                            React.isValidElement(header) ?
                                header :
                                <div className={css.header}>
                                    <h3 className={css.title}>{header}</h3>
                                    <IconClose onClick={handleClose} size="large" className={css.icon} />
                                </div>
                        }
                        {props.children}
                    </div>
                </Portal>
            }
        </>
    );
};


Drawer.Header = DrawerHeader;
Drawer.Body = DrawerBody;
Drawer.Footer = DrawerFooter;


export { Drawer };
