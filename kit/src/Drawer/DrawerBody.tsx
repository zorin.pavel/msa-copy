import React, { ReactNode } from 'react';
import classNames from 'classnames';
import css from './drawer.module.scss';


interface Props {
    className?: string,
    children?: ReactNode,
}

export const DrawerBody = (props: Props) => {
    const {
        className,
        ...rest
    } = props;

    return (
        <div className={classNames(
            css.body,
            className,
        )} {...rest}>
            {props.children}
        </div>
    );
};
