import React, { ReactNode } from 'react';
import classNames from 'classnames';
import css from './drawer.module.scss';


interface Props {
    className?: string,
    children?: ReactNode,
}

export const DrawerHeader = (props: Props) => {
    const {
        className,
        ...rest
    } = props;

    return (
        <div className={classNames(
            css.header,
            className,
        )} {...rest}>
            {props.children}
        </div>
    );
};
