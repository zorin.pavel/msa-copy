import React, { ReactNode } from 'react';
import classNames from 'classnames';

import { DrawerFooterJustify } from '@msa/kit/types';

import css from './drawer.module.scss';


interface Props {
    className?: string,
    justify?: DrawerFooterJustify,
    children?: ReactNode,
}

export const DrawerFooter = (props: Props) => {
    const {
        className,
        justify = DrawerFooterJustify.between,
        ...rest
    } = props;

    return (
        <div className={classNames(
            css.footer,
            (justify && css[justify]),
            className,
        )} {...rest}>
            {props.children}
        </div>
    );
};
