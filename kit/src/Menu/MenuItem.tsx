import React, { CSSProperties, MouseEventHandler, ReactElement, ReactNode } from 'react';
import classNames from 'classnames';

import { Colors } from '@msa/kit/types';
// import { MenuContext } from './';

import css from './menu.module.scss';


interface IProps {
    children?: ReactNode,
    trigger?: ReactElement,
    className?: string,
    style?: CSSProperties,
    disabled?: boolean,
    visible?: boolean,
    color?: keyof typeof Colors,
    onClick?: MouseEventHandler<HTMLLIElement>,
}

export const MenuItem = (props: IProps) => {
    const { trigger, className, style, disabled, visible, color, onClick } = props;
    // const { handleClose } = useContext(MenuContext);

    if(visible === false)
        return null;

    return (
        <li className={classNames(
            css.listItem,
            className,
            color && css[color],
            disabled && css.disabled
        )}
        onClick={onClick}
        style={{ ...style }}>
            {
                trigger ?
                    React.cloneElement(trigger, {}) :
                    props.children
            }
        </li>
    );
};

export default MenuItem;
