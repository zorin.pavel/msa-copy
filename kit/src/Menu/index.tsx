import React, {
    createContext,
    CSSProperties,
    ReactElement, ReactNode, RefObject,
    useEffect,
    useRef,
    useState
} from 'react';
import classNames from 'classnames';

import { useOnClickOutside } from '@msa/kit';
import { MenuList } from './MenuList';
import { MenuItem } from './MenuItem';

import css from './menu.module.scss';


interface IContextProps {
    wrapperRef: null | RefObject<HTMLDivElement>,
    children?: ReactNode,
    handleClose?: () => void
}

const defaultContextValue = {
    wrapperRef: null,
};

export const MenuContext = createContext<IContextProps>(defaultContextValue);


interface IProps {
    open?: boolean,
    className?: string,
    style?: CSSProperties,
    onClose?: () => void,
    button: ReactElement,
    children: ReactElement,
}

const Menu = (props: IProps) => {
    const { button, open, className, style, onClose } = props;
    const wrapperRef = useRef<HTMLDivElement>(null);

    const [isOpen, setIsOpen] = useState<boolean>(open || false);
    const [positionTop, setPositionTop] = useState<number>(0);
    const [positionRight] = useState<number>(0);


    useEffect(() => {
        if(wrapperRef.current && isOpen) {
            setPositionTop(wrapperRef.current.clientHeight);
        }
    }, [isOpen]);


    // const getMaxHeight = () => {
    //     const body = document.body,
    //         html = document.documentElement;
    //
    //     return Math.max(body.scrollHeight, body.offsetHeight, html.clientHeight, html.scrollHeight, html.offsetHeight);
    // };


    const handleClose = () => {
        if(onClose)
            onClose();

        setIsOpen(false);
    };


    useOnClickOutside(wrapperRef, handleClose);


    return (
        <div className={classNames(css.wrapper, className)} ref={wrapperRef}>
            <MenuContext.Provider value={{ ...defaultContextValue, wrapperRef, handleClose }}>
                {
                    React.cloneElement(button, { onClick: () => setIsOpen(!isOpen) })
                }
                {
                    // isOpen &&
                    React.cloneElement(props.children, { open: isOpen, 'style': { top: positionTop, right: positionRight, ...style } })
                }
            </MenuContext.Provider>
        </div>
    );
};


Menu.List = MenuList;
Menu.Item = MenuItem;

export { Menu };
