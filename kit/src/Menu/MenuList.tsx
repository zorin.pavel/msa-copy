import React, { CSSProperties, ReactNode } from 'react';
import classNames from 'classnames';
import css from './menu.module.scss';


interface IProps {
    children: ReactNode,
    open?: boolean,
    className?: string,
    style?: CSSProperties,
}

export const MenuList = (props: IProps) => {
    const { open, className, style } = props;

    return (
        <ul className={classNames(
            css.list,
            (open && css.open),
            className
        )} style={{ ...style }}>
            {props.children}
        </ul>
    );
};

export default MenuList;
