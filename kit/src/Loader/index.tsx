import React from 'react';
import classNames from 'classnames';

import { Sizes } from '@msa/kit/types';
import { IconSpinner } from '@msa/kit';

import css from './loader.module.scss';


interface IProps {
    layout?: boolean,
    caption?: string,
    size?: keyof typeof Sizes,
    className?: string,
}

export const Loader = (props: IProps) => {
    const { layout, caption, size = Sizes.medium, className } = props;


    return (
        <div className={classNames(css.container, layout && css.layout, className)}>
            <IconSpinner className={classNames('spin', css[size])} />
            {
                caption &&
                <p className={css.caption}>{caption}</p>
            }
        </div>
    );
};
