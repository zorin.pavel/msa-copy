import React from 'react';
import classNames from 'classnames';

import type { IError } from '@msa/kit/types';

import css from './error.module.scss';


interface IProps extends IError {
    layout?: boolean,
}

export const Error = ({ code, status, statusText, message, layout }: IProps) => {
    return (
        <div className={classNames(css.container, layout && css.layout)}>
            <div className={css.error}>
                {
                    status ?
                        <h1>{status}</h1> :
                        code ?
                            <h1>{code}</h1> :
                            null
                }
                <p>
                    {
                        message ?? statusText ?? null
                    }
                </p>
            </div>
        </div>
    );
};
