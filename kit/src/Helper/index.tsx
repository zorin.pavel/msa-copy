import React, {
    Fragment,
    MouseEvent,
    MouseEventHandler,
    ReactElement,
    useCallback,
    useEffect,
    useRef,
    useState
} from 'react';
import classNames from 'classnames';
import parse from 'html-react-parser';

import { IconHelpCircle } from '@msa/kit';
import { Sizes } from '@msa/kit/types';

import css from './helper.module.scss';


interface IProps {
    title?: string,
    description: string,
    size?: Sizes,
    className?: string,
    render?: ({ onMouseOver }: { onMouseOver: MouseEventHandler<HTMLElement> }) => ReactElement,
    children?: ReactElement,
}

export const Helper = (props: IProps) => {
    const { size = Sizes.small, description, title, className } = props;

    const parentRef = useRef<HTMLElement>(null);
    const tooltipRef = useRef<HTMLDivElement>(null);
    const pointerRef = useRef<HTMLDivElement>(null);

    let hideTimeout: ReturnType<typeof setTimeout>;
    let showTimeout: ReturnType<typeof setTimeout>;

    const [tooltip, showTooltip] = useState<boolean>(false);

    const handleMouseOver = (_event: MouseEvent<HTMLElement>) => {
        showTimeout = setTimeout(() => showTooltip(true), 300);
        clearTimeout(hideTimeout);
    };


    const handleMouseLeave = (_event: MouseEvent<HTMLElement>) => {
        hideTimeout = setTimeout(() => showTooltip(false), 500);
        clearTimeout(showTimeout);
    };


    const getOffset = useCallback((element: HTMLElement | null) => {
        if(!element)
            return { left: 0, top: 0 };

        const rect = element.getBoundingClientRect();

        return {
            left: rect.left + window.scrollX,
            top: rect.top + window.scrollY
        };
    }, []);


    useEffect(() => {
        if(tooltipRef.current && tooltip) {
            const tooltipStyles = window.getComputedStyle(tooltipRef.current);
            const position = getOffset(tooltipRef.current);

            tooltipRef.current.style.minWidth = (description.length > 25 ? 20 :
                (title ? (title.length / 2) + 1 : 5)) + 'em';

            let { m41: translateX, m42: translateY } = new WebKitCSSMatrix(tooltipStyles.transform);

            if((position.left + tooltipRef.current.clientWidth + translateX) > document.body.clientWidth) {
                const diff = (position.left + tooltipRef.current.clientWidth) - document.body.clientWidth + 30;
                translateX -= diff;
                tooltipRef.current.style.transform = `translate(${translateX}px, ${translateY}px)`;

                if(pointerRef.current) {
                    const pointerStyles = window.getComputedStyle(pointerRef.current);

                    pointerRef.current.style.left = `${(Number(pointerStyles.left.replace('px', '')) + diff)}px`;
                }
            }
        }
    }, [tooltip]);



    const TooltipWindow = (): ReactElement => {
        return (
            <Fragment key={description}>
                {
                    tooltip &&
                    <div ref={tooltipRef} className={classNames(css.tooltip, tooltip && css.visible)} onMouseOver={handleMouseOver} onMouseLeave={handleMouseLeave}>
                        {
                            title &&
                            <h3 className={css.title}>{title}</h3>
                        }
                        {parse(description)}
                        <div ref={pointerRef} className={css.pointer} />
                    </div>
                }
            </Fragment>
        );
    };

    if(!description && !title)
        return null;
    
    return (
        <>
            {
                props.children ?
                    React.cloneElement(props.children,
                        {
                            onMouseOver: handleMouseOver,
                            onMouseLeave: handleMouseLeave,
                            className: classNames(props.children.props.className, css.parent, className),
                            ref: parentRef
                        },
                        [
                            props.children.props.children,
                            TooltipWindow()
                        ]
                    ) :
                    <span className={classNames(css.helper, css.parent, className)} onMouseOver={handleMouseOver} onMouseLeave={handleMouseLeave}>
                        <IconHelpCircle size={size} />
                        <TooltipWindow />
                    </span>
            }
        </>
    );
};
