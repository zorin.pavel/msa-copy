import React, { CSSProperties, MouseEvent, MouseEventHandler, ReactNode, useCallback } from 'react';
import classNames from 'classnames';

import { Sizes } from '@msa/kit/types';
import { IconSpinner } from './';

import css from './icon.module.scss';


export interface IconProps {
    id?: string,
    size?: keyof typeof Sizes,
    children?: ReactNode,
    disabled?: boolean,
    checked?: boolean,
    button?: boolean,
    loading?: boolean,
    viewBox?: string,
    onClick?: MouseEventHandler<HTMLElement | SVGElement>,
    // onMouseOver?: MouseEventHandler<HTMLElement | SVGElement>,
    // onMouseLeave?: MouseEventHandler<HTMLElement | SVGElement>,
    className?: string,
    style?: CSSProperties,
}


export const Icon = (props: IconProps) => {
    const {
        size = Sizes.medium, disabled, checked, onClick, button,
        // onMouseOver, onMouseLeave,
        loading, viewBox = '0 0 24 24',
        className, style, ...rest
    } = props;


    const onIconClick = useCallback((event: MouseEvent<HTMLElement | SVGElement>) => {
        // event.stopPropagation();

        if(disabled)
            return;

        onClick && onClick(event);
    }, [onClick, disabled]);


    // const onIconMouseOver = useCallback((event: MouseEvent<HTMLElement | SVGElement>) => {
    //     onMouseOver && onMouseOver(event);
    // }, [onMouseOver]);
    //
    //
    // const onIconMouseLeave = useCallback((event: MouseEvent<HTMLElement | SVGElement>) => {
    //     onMouseLeave && onMouseLeave(event);
    // }, [onMouseOver]);


    if(loading) {
        const { loading: _loading, ...loadingProps } = props;

        return <IconSpinner className={classNames('spin', css.loading)} {...loadingProps} />;
    }


    return (
        <svg
            viewBox={viewBox}
            className={classNames(
                css.icon,
                css[size],
                (disabled && css.disabled),
                ((onClick || button) && css.action),
                className,
            )}
            onClick={onIconClick}
            // onMouseOver={onIconMouseOver}
            // onMouseLeave={onIconMouseLeave}
            style={style}
            {...rest}>
            {props.children}
            {
                checked &&
                <circle cx="21" cy="3" r="3" className={css.checked} />
            }
        </svg>
    );
};

export default Icon;
