import React from 'react';
import { Icon, IconProps } from '../Icon';

interface IconCheckboxProps extends IconProps {
    selected?: boolean
}

export const IconCheckbox = (props: IconCheckboxProps) => {
    return <Icon id="IconCheckbox" {...props}>
        {
            props.selected ?
                <path fillRule="evenodd" d="M4 0C1.7909 0 0 1.7909 0 4V20C0 22.2092 1.7909 24 4 24H20C22.2092 24 24 22.2092 24 20V4C24 1.7909 22.2092 0 20 0H4ZM19.2071 8.2071C19.5976 7.8166 19.5976 7.1834 19.2071 6.7929C18.8166 6.4024 18.1834 6.4024 17.7929 6.7929L9.5 15.0858 6.2071 11.7929C5.8166 11.4024 5.1834 11.4024 4.7929 11.7929C4.4024 12.1834 4.4024 12.8166 4.7929 13.2071L9.5 17.9142 19.2071 8.2071Z" /> :
                <path fillRule="evenodd" d="M20 2H4C2.8954 2 2 2.8954 2 4V20C2 21.1046 2.8954 22 4 22H20C21.1046 22 22 21.1046 22 20V4C22 2.8954 21.1046 2 20 2ZM4 0C1.7909 0 0 1.7909 0 4V20C0 22.2092 1.7909 24 4 24H20C22.2092 24 24 22.2092 24 20V4C24 1.7909 22.2092 0 20 0H4Z" />
        }
    </Icon>;
};

export default IconCheckbox;
