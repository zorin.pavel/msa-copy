import React from 'react';
import { Icon, IconProps } from '../Icon';


export const IconRun = (props: IconProps) => {
    return <Icon  id="IconRun" {...props}>
        <path d="M7 5.57196C7.3094 5.39333 7.6906 5.39333 8 5.57196L18.5 11.6342C18.8094 11.8128 19 12.1429 19 12.5002C19 12.8575 18.8094 13.1876 18.5 13.3662L8 19.4284C7.6906 19.607 7.3094 19.607 7 19.4284C6.6906 19.2498 6.5 18.9197 6.5 18.5624V6.43799C6.5 6.08072 6.6906 5.7506 7 5.57196ZM8.5 8.17004V16.8303L16 12.5002L8.5 8.17004Z" />
    </Icon>;
};

export default IconRun;
