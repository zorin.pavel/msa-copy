import React from 'react';
import { Icon, IconProps } from '../Icon';


export const IconDoubleLines = (props: IconProps) => {
    return <Icon id="IconDoubleLines" {...props}>
        <path d="M5 9H13H19M5 15H19" strokeLinecap="round" strokeWidth="2" stroke="currentColor" />
    </Icon>;
};

export default IconDoubleLines;
