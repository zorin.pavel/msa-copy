import React from 'react';
import { Icon, IconProps } from '../Icon';


export const IconMenu = (props: IconProps) => {
    return <Icon  id="IconMenu" {...props}>
        <circle cx="12" cy="6" r="1.5" />
        <circle cx="12" cy="12" r="1.5" />
        <circle cx="12" cy="18" r="1.5" />
    </Icon>;
};

export default IconMenu;
