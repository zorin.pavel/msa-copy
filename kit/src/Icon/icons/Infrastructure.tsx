import React from 'react';
import { Icon, IconProps } from '../Icon';


export const IconInfrastructure = (props: IconProps) => {
    return <Icon id="IconInfrastructure" {...props}>
        <path fillRule="evenodd" clipRule="evenodd" d="M6 4C6 3.44772 6.44772 3 7 3H21C21.5523 3 22 3.44772 22 4V10C22 10.5523 21.5523 11 21 11H8V14H21C21.5523 14 22 14.4477 22 15V21C22 21.5523 21.5523 22 21 22H7C6.44772 22 6 21.5523 6 21V19H3C2.44772 19 2 18.5523 2 18V7C2 6.44772 2.44772 6 3 6H6V4ZM8 9H20V5H8V9ZM6 8H4V17H6V8ZM8 20H20V16H8V20Z" />
    </Icon>;
};

export default IconInfrastructure;
