import React from 'react';
import { Icon, IconProps } from '../Icon';


export const IconPlatform = (props: IconProps) => {
    return <Icon id="IconPlatform" {...props}>
        <path fillRule="evenodd" clipRule="evenodd" d="M2 3C2 2.44772 2.44772 2 3 2H21C21.5523 2 22 2.44772 22 3V7C22 7.55228 21.5523 8 21 8H3C2.44772 8 2 7.55228 2 7V3ZM4 4V6H20V4H4ZM2 10.5C2 9.94772 2.44772 9.5 3 9.5H21C21.5523 9.5 22 9.94772 22 10.5V14.5C22 15.0523 21.5523 15.5 21 15.5H3C2.44772 15.5 2 15.0523 2 14.5V10.5ZM4 11.5V13.5H20V11.5H4ZM2 18C2 17.4477 2.44772 17 3 17H21C21.5523 17 22 17.4477 22 18V22C22 22.5523 21.5523 23 21 23H3C2.44772 23 2 22.5523 2 22V18ZM4 19V21H20V19H4Z" />
    </Icon>;
};

export default IconPlatform;
