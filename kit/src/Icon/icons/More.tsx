import React from 'react';
import { Icon, IconProps } from '../Icon';


export const IconMore = (props: IconProps) => {
    return <Icon  id="IconMore" {...props}>
        <circle cx="6" cy="12" r="1.5" />
        <circle cx="12" cy="12" r="1.5" />
        <circle cx="18" cy="12" r="1.5" />
    </Icon>;
};

export default IconMore;
