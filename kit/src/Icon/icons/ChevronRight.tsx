import React from 'react';
import { Icon, IconProps } from '../Icon';


export const IconChevronRight = (props: IconProps) => {
    return <Icon  id="IconChevronRight" {...props}>
        <path d="M8.4384 20.4977C8.14713 20.2032 8.14974 19.7283 8.44424 19.4371L15.9097 12.0534L8.52606 4.58803C8.23479 4.29353 8.2374 3.81866 8.5319 3.52739C8.82641 3.23611 9.30127 3.23873 9.59255 3.53323L17.5035 11.5319C17.7948 11.8264 17.7922 12.3013 17.4977 12.5925L9.49905 20.5035C9.20454 20.7948 8.72968 20.7922 8.4384 20.4977Z" />
    </Icon>;
};

export default IconChevronRight;
